package com.inwhoop.ddcj.view;

import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.inwhoop.R;
import com.pocketdigi.utils.FLameUtils;

/**
 * 
 * @Project: MainActivity
 * @Title: TouchSayButton.java
 * @Package com.inwhoop.gameproduct.view
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-6-14 下午1:46:34
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class TouchSayButton extends Button {

	private PopupWindow popupWindow, cancelPopupWindow;

	private Context context;

	private ImageView showImageView = null;

	private boolean isTouch = false;

	private int count = 0;

	private int[] voices = { R.drawable.amp1, R.drawable.amp2, R.drawable.amp3,
			R.drawable.amp4, R.drawable.amp5, R.drawable.amp6 };

	private OnRecordListener onRecordListener = null;
	/** 用于完成录音 */
	private AudioRecord mRecorder = null;

	private long startRecordTime = 0; // 录音开始时间

	private String path = "";

	private float fy = 0;

	private short[] mBuffer;
	private boolean isRecording = false;
	private double volume;
	private int bufferSize;

	public TouchSayButton(Context context) {
		super(context);
		this.context = context;
	}

	public TouchSayButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
		initPopwindow();
		initCancelPopwindow();
	}

	public TouchSayButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
	}

	public OnRecordListener getOnRecordListener() {
		return onRecordListener;
	}

	public void setOnRecordListener(OnRecordListener onRecordListener) {
		this.onRecordListener = onRecordListener;
	}

	@SuppressLint("ShowToast")
	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		if (!Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			if (ev.getAction() == MotionEvent.ACTION_DOWN)
				Toast.makeText(context, "sd卡不存在，不能进行语音发送操作", 0).show();
		} else {
			switch (ev.getAction()) {
			case MotionEvent.ACTION_DOWN:
				fy = ev.getY();
				isTouch = true;
				showPopupWindow(true, getRootView());
				setBackgroundResource(R.drawable.btn_voice_pressed);
				setText("松开发送");
				setTextColor(getResources().getColor(R.color.white));
				break;

			case MotionEvent.ACTION_UP:
				if (null != popupWindow) {
					popupWindow.dismiss();
				}
				if (null != cancelPopupWindow) {
					cancelPopupWindow.dismiss();
				}
				setBackgroundResource(R.drawable.btn_voice);
				isTouch = false;
				setText("按住说话");
				setTextColor(getResources().getColor(R.color.green));
				if (Math.abs(ev.getY() - fy) > 300) {
					onRecord("", stopVoice(startRecordTime) / 1000);
				} else {
					onRecord(path, stopVoice(startRecordTime) / 1000);
				}
				break;

			case MotionEvent.ACTION_MOVE:
				if (Math.abs(ev.getY() - fy) > 300) {
					popupWindow.dismiss();
					showCancelPopupwindow(getRootView());
				} else {
					cancelPopupWindow.dismiss();
					showPopupWindow(false, getRootView());
				}
				break;

			default:
				break;
			}
		}
		return super.onTouchEvent(ev);
	}

	/**
	 * 初始化popupwindow
	 * 
	 * @Title: initPopwindow
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	@SuppressWarnings("deprecation")
	private void initPopwindow() {
		View view = LayoutInflater.from(context).inflate(
				R.layout.record_popupwindow_layout, null);
		popupWindow = new PopupWindow(view, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		showImageView = (ImageView) view.findViewById(R.id.voiceimg);
	}

	@SuppressWarnings("deprecation")
	private void initCancelPopwindow() {
		View view = LayoutInflater.from(context).inflate(
				R.layout.cancel_popupwindow_layout, null);
		cancelPopupWindow = new PopupWindow(view, LayoutParams.WRAP_CONTENT,
				LayoutParams.WRAP_CONTENT, true);
		cancelPopupWindow.setContentView(view);
		cancelPopupWindow.setFocusable(true);
		cancelPopupWindow.setOutsideTouchable(false);
		cancelPopupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
	}

	/**
	 * 
	 * 
	 * @Title: showPopupWindow
	 * @Description: TODO
	 * @param @param view
	 * @return void
	 */
	public void showPopupWindow(boolean isF, View view) {
		popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
		if (isF) {
			showImageView.setBackgroundResource(voices[0]);
			count = 0;
			showVoicimg();
			startVoice(context);
		}
	}

	public void showCancelPopupwindow(View view) {
		cancelPopupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
	}

	/**
	 * 显示动态的声音
	 * 
	 * @Title: showVoicimg
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void showVoicimg() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isTouch) {
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					handler.sendEmptyMessage(0);
				}
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			try {
				int db = (int) volume;// 分贝
				switch (db / 10) {
				case 0:
					showImageView.setBackgroundResource(voices[0]);
					break;
				case 1:
					showImageView.setBackgroundResource(voices[1]);
					break;
				case 2:
					showImageView.setBackgroundResource(voices[2]);
					break;
				case 3:
					showImageView.setBackgroundResource(voices[3]);
					break;
				case 4:
					showImageView.setBackgroundResource(voices[4]);
					break;
				case 5:
					showImageView.setBackgroundResource(voices[5]);
					break;
				default:
					showImageView.setBackgroundResource(voices[0]);
					break;
				}
			} catch (Exception e) {

			}
		};
	};

	public interface OnRecordListener {
		public void onRecord(String filepath, long time);
	}

	private void onRecord(String filepath, long time) {
		if (null != onRecordListener) {
			onRecordListener.onRecord(filepath, time);
		}
	}

	/**
	 * 开始录音
	 */
	public String startVoice(Context context) {
		startRecordTime = System.currentTimeMillis();
		path = "/ddcj/audio/" + System.currentTimeMillis() + ".raw";
		/*
		 * String mpath = "/xinban/audio/" + System.currentTimeMillis() +
		 * ".mp3";
		 */
		// 设置录音保存路径
		try {
			String mFileName = Environment.getExternalStorageDirectory() + path;
			String state = android.os.Environment.getExternalStorageState();
			if (!state.equals(android.os.Environment.MEDIA_MOUNTED)) {
				return "";
			}
			File directory = new File(mFileName).getParentFile();
			if (!directory.exists() && !directory.mkdirs()) {
				return "";
			}
			bufferSize = AudioRecord
					.getMinBufferSize(16000, AudioFormat.CHANNEL_IN_MONO,
							AudioFormat.ENCODING_PCM_16BIT);
			mBuffer = new short[bufferSize];

			mRecorder = new AudioRecord(MediaRecorder.AudioSource.MIC, 16000,
					AudioFormat.CHANNEL_IN_MONO,
					AudioFormat.ENCODING_PCM_16BIT, bufferSize);
			mRecorder.startRecording();
			isRecording = true;
			startBufferedWrite(new File(mFileName));
		} catch (Exception e) {
			return "";
		}
		return path;
	}

	/**
	 * 写入到文件
	 * 
	 * @param file
	 */
	public void startBufferedWrite(final File file) {
		new Thread(new Runnable() {
			public void run() {
				DataOutputStream output = null;
				try {
					output = new DataOutputStream(new BufferedOutputStream(
							new FileOutputStream(file)));
					while (isRecording) {
						if (null != mRecorder
								&& mRecorder.getRecordingState() == AudioRecord.RECORDSTATE_RECORDING) {
							int readSize = mRecorder.read(mBuffer, 0,
									mBuffer.length);
							// int r = mRecorder.read(mBuffer, 0, bufferSize);
							long v = 0;
							// 将 buffer 内容取出，进行平方和运算
							for (int i = 0; i < mBuffer.length; i++) {
								v += mBuffer[i] * mBuffer[i];
							}
							// 平方和除以数据总长度，得到音量大小。
							double mean = v / (double) readSize;
							volume = 10 * Math.log10(mean); // 分贝值
							System.out.println("分贝值为===" + volume);

							for (int i = 0; i < readSize; i++) {
								output.writeShort(mBuffer[i]);
							}
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				} finally {
					if (output != null) {
						try {
							output.flush();
						} catch (IOException e) {
							e.printStackTrace();
						} finally {
							try {
								output.close();
							} catch (IOException e) {
							}
						}
					}
				}
			}
		}).start();
	}

	/**
	 * 停止录音
	 */
	public long stopVoice(long time) {
		try {
			isRecording = false;
			mRecorder.stop();
			mRecorder.release();
			mRecorder = null;
		} catch (Exception e) {
			System.out.println("==========mRecorder=======" + e.toString());
		}
		return System.currentTimeMillis() - time;
	}

}
