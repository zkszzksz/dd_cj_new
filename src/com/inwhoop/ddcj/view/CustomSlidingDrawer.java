package com.inwhoop.ddcj.view;

import com.inwhoop.R;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * 自定义抽屉
 * 
 * @Project: DDCJ
 * @Title: CustomSlidingDrawer.java
 * @Package com.inwhoop.ddcj.view
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-12-3 上午10:33:07
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class CustomSlidingDrawer extends LinearLayout implements
		GestureDetector.OnGestureListener, View.OnTouchListener {
	private final static String TAG = "CustomSlidingDrawer";

	private Button btnHandler;
	private LinearLayout container;
	private int mBottonMargin = 0;
	private GestureDetector mGestureDetector;
	private boolean mIsScrolling = false;
	private float mScrollY;
	private int moveDistance;

	public CustomSlidingDrawer(Context context, View otherView, int width,
			int height, int hideDistance) {
		super(context);
		moveDistance = hideDistance;
		// 定义手势识别
		mGestureDetector = new GestureDetector(context, this);
		mGestureDetector.setIsLongpressEnabled(false);

		// 改变CustomSlidingDrawer附近组件的属性
		LayoutParams otherLP = (LayoutParams) otherView.getLayoutParams();
		// 这一步很重要，要不组建不会显示
		otherLP.weight = 1;
		otherView.setLayoutParams(otherLP);

		// 设置CustomSlidingDrawer本身的属性
		LayoutParams lp = new LayoutParams(width, height);
		lp.bottomMargin = -moveDistance;
		mBottonMargin = Math.abs(lp.bottomMargin);
		this.setLayoutParams(lp);
		this.setOrientation(LinearLayout.VERTICAL);
		// this.setBackgroundColor(Color.BLUE);

		// 设置Handler的属性
		LayoutInflater inflater = LayoutInflater.from(context);
		btnHandler = new Button(context);
		btnHandler.setBackgroundResource(R.drawable.btn_up);
		LinearLayout.LayoutParams params = new LayoutParams(90, 45);
		params.gravity = Gravity.CENTER_HORIZONTAL;
		btnHandler.setLayoutParams(params);
		btnHandler.setOnTouchListener(this);
		this.addView(btnHandler);

		// 设置Container的属性
		container = new LinearLayout(context);
		// container.setBackgroundColor(Color.GREEN);
		container.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT));
		this.addView(container);
	}

	/**
	 * 把View放在CustomSlidingDrawer的Container
	 * 
	 * @param v
	 */
	public void fillPanelContainer(View v) {
		container.addView(v);
	}

	/**
	 * 异步移动CustomSlidingDrawer
	 */
	class AsynMove extends AsyncTask<Integer, Integer, Void> {
		@Override
		protected Void doInBackground(Integer... params) {
			Log.e(TAG, "AsynMove doInBackground");
			int times;
			if (mBottonMargin % Math.abs(params[0]) == 0)// 整除
				times = mBottonMargin / Math.abs(params[0]);
			else
				// 有余数
				times = mBottonMargin / Math.abs(params[0]) + 1;
			for (int i = 0; i < times; i++) {
				publishProgress(params);
			}
			return null;
		}

		@Override
		protected void onProgressUpdate(Integer... params) {
			Log.e(TAG, "AsynMove onProgressUpdate");
			LayoutParams lp = (LayoutParams) CustomSlidingDrawer.this
					.getLayoutParams();
			if (params[0] < 0)
				lp.bottomMargin = Math.max(lp.bottomMargin + params[0],
						(-mBottonMargin));
			else
				lp.bottomMargin = Math.min(lp.bottomMargin + params[0], 0);
			CustomSlidingDrawer.this.setLayoutParams(lp);
		}
	}

	@Override
	public boolean onDown(MotionEvent e) {
		mScrollY = 0;
		mIsScrolling = false;
		return false;
	}

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		LayoutParams lp = (LayoutParams) CustomSlidingDrawer.this
				.getLayoutParams();
		if (lp.bottomMargin < 0)
			new AsynMove().execute(new Integer[] { moveDistance });// 正数展开
		else if (lp.bottomMargin >= 0)
			new AsynMove().execute(new Integer[] { -moveDistance });// 负数收缩
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX,
			float distanceY) {
		mIsScrolling = true;
		mScrollY += distanceY;
		LayoutParams lp = (LayoutParams) CustomSlidingDrawer.this
				.getLayoutParams();
		if (lp.bottomMargin < -1 && mScrollY > 0) {// 往上拖拉
			lp.bottomMargin = Math.min((lp.bottomMargin + (int) mScrollY), 0);
			CustomSlidingDrawer.this.setLayoutParams(lp);
		} else if (lp.bottomMargin > -(mBottonMargin) && mScrollY < 0) {// 往下拖拉
			lp.bottomMargin = Math.max((lp.bottomMargin + (int) mScrollY),
					-mBottonMargin);
			CustomSlidingDrawer.this.setLayoutParams(lp);
		}
		return false;
	}

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,
			float velocityY) {
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
	}

	@Override
	public void onShowPress(MotionEvent e) {
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		if (event.getAction() == MotionEvent.ACTION_UP && // onScroll时的ACTION_UP
				mIsScrolling == true) {
			LayoutParams lp = (LayoutParams) CustomSlidingDrawer.this
					.getLayoutParams();
			if (lp.bottomMargin >= (-mBottonMargin / 2)) {// 往上超过一半
				new AsynMove().execute(new Integer[] { moveDistance });// 正数展开
			} else if (lp.bottomMargin < (-mBottonMargin / 2)) {// 往下拖拉
				new AsynMove().execute(new Integer[] { -moveDistance });// 负数收缩
			}
		}
		return mGestureDetector.onTouchEvent(event);
	}
}
