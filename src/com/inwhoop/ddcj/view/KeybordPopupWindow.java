package com.inwhoop.ddcj.view;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.PopupWindow;

import com.inwhoop.R;

/**
 * @Project: DDCJ
 * @Title: KeybordPopupWindow.java
 * @Package com.inwhoop.ddcj.view
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-11-28 上午10:10:48
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class KeybordPopupWindow implements OnClickListener {

	public PopupWindow keybordPopupWindow = null;

	private EditText editText = null;

	private Context context = null;

	private Button btn_600, btn_601, btn_000, btn_002, btn_300, btn_1, btn_2,
			btn_3, btn_4, btn_5, btn_6, btn_7, btn_8, btn_9, btn_0, btn_abc,
			btn_yc, btn_qk, btn_sure;

	private LinearLayout deleteLayout;

	private int sw = 0;

	private int item_space = 0;

	private int item_space_top = 0;

	private int leftwidth = 0;

	private int leftheight = 0;

	private int numwidth, numheight;

	private int handlewidth = 0;

	private OnSureListener onSureListener;

	public KeybordPopupWindow(Context context, int sw) {
		this.context = context;
		this.sw = sw;
		sw = (int) (sw - context.getResources().getDimension(
				R.dimen.keybord_space));
		item_space = (int) context.getResources().getDimension(
				R.dimen.keybord_item);
		item_space_top = (int) context.getResources().getDimension(
				R.dimen.keybord_item_top);
		leftwidth = (int) (sw / (float) 572 * 120);
		leftheight = (int) (leftwidth / (float) 120 * 78);

		numwidth = (int) (sw / (float) 572 * 108);
		numheight = (int) (numwidth / (float) 108 * 98);

		handlewidth = (int) (sw / (float) 572 * 128);
	}

	public void setOnSureListener(OnSureListener onSureListener) {
		this.onSureListener = onSureListener;
	}

	public void setEdittext(EditText editText) {
		this.editText = editText;
	}

	@SuppressWarnings("deprecation")
	public void showkeybordPopupWindow() {
		if (null != keybordPopupWindow && keybordPopupWindow.isShowing()) {
			return;
		}
		View view = LayoutInflater.from(context).inflate(
				R.layout.keybord_popupwindow_layout, null);
		keybordPopupWindow = new PopupWindow(view, LayoutParams.MATCH_PARENT,
				LayoutParams.WRAP_CONTENT, true);
		keybordPopupWindow.setContentView(view);
		keybordPopupWindow.setFocusable(false);
		keybordPopupWindow.setOutsideTouchable(false);
		keybordPopupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		initView(view);
		keybordPopupWindow.setAnimationStyle(R.style.dialogstyle);
		keybordPopupWindow.showAtLocation(editText, Gravity.BOTTOM, 0, 0);
	}

	private void initView(View view) {
		LinearLayout.LayoutParams leftparm = new LinearLayout.LayoutParams(
				leftwidth, leftheight);
		leftparm.topMargin = item_space_top;
		LinearLayout.LayoutParams leftoneparm = new LinearLayout.LayoutParams(
				leftwidth, leftheight);
		btn_600 = (Button) view.findViewById(R.id.btn_600);
		btn_600.setLayoutParams(leftoneparm);
		btn_600.setOnClickListener(this);
		btn_601 = (Button) view.findViewById(R.id.btn_601);
		btn_601.setLayoutParams(leftparm);
		btn_601.setOnClickListener(this);
		btn_000 = (Button) view.findViewById(R.id.btn_000);
		btn_000.setLayoutParams(leftparm);
		btn_000.setOnClickListener(this);
		btn_002 = (Button) view.findViewById(R.id.btn_002);
		btn_002.setLayoutParams(leftparm);
		btn_002.setOnClickListener(this);
		btn_300 = (Button) view.findViewById(R.id.btn_300);
		btn_300.setLayoutParams(leftparm);
		btn_300.setOnClickListener(this);

		LinearLayout.LayoutParams numparm = new LinearLayout.LayoutParams(
				numwidth, numheight);
		numparm.leftMargin = item_space;
		LinearLayout.LayoutParams numoneparm = new LinearLayout.LayoutParams(
				numwidth, numheight);
		btn_1 = (Button) view.findViewById(R.id.btn_1);
		btn_1.setLayoutParams(numoneparm);
		btn_1.setOnClickListener(this);
		btn_2 = (Button) view.findViewById(R.id.btn_2);
		btn_2.setLayoutParams(numparm);
		btn_2.setOnClickListener(this);
		btn_3 = (Button) view.findViewById(R.id.btn_3);
		btn_3.setLayoutParams(numparm);
		btn_3.setOnClickListener(this);
		btn_4 = (Button) view.findViewById(R.id.btn_4);
		btn_4.setLayoutParams(numoneparm);
		btn_4.setOnClickListener(this);
		btn_5 = (Button) view.findViewById(R.id.btn_5);
		btn_5.setLayoutParams(numparm);
		btn_5.setOnClickListener(this);
		btn_6 = (Button) view.findViewById(R.id.btn_6);
		btn_6.setLayoutParams(numparm);
		btn_6.setOnClickListener(this);
		btn_7 = (Button) view.findViewById(R.id.btn_7);
		btn_7.setLayoutParams(numoneparm);
		btn_7.setOnClickListener(this);
		btn_8 = (Button) view.findViewById(R.id.btn_8);
		btn_8.setLayoutParams(numparm);
		btn_8.setOnClickListener(this);
		btn_9 = (Button) view.findViewById(R.id.btn_9);
		btn_9.setLayoutParams(numparm);
		btn_9.setOnClickListener(this);
		btn_0 = (Button) view.findViewById(R.id.btn_0);
		btn_0.setLayoutParams(numparm);
		btn_0.setOnClickListener(this);
		LinearLayout.LayoutParams abcparm = new LinearLayout.LayoutParams(2
				* numwidth + item_space, numheight);
		btn_abc = (Button) view.findViewById(R.id.btn_abc);
		btn_abc.setLayoutParams(abcparm);
		btn_abc.setOnClickListener(this);

		LinearLayout.LayoutParams handleparm = new LinearLayout.LayoutParams(
				handlewidth, numheight);
		handleparm.leftMargin = item_space;
		btn_yc = (Button) view.findViewById(R.id.btn_yc);
		btn_yc.setLayoutParams(handleparm);
		btn_yc.setOnClickListener(this);
		btn_qk = (Button) view.findViewById(R.id.btn_qk);
		btn_qk.setLayoutParams(handleparm);
		btn_qk.setOnClickListener(this);
		btn_sure = (Button) view.findViewById(R.id.btn_sure);
		btn_sure.setLayoutParams(handleparm);
		btn_sure.setOnClickListener(this);
		deleteLayout = (LinearLayout) view.findViewById(R.id.btn_delete);
		deleteLayout.setLayoutParams(handleparm);
		deleteLayout.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.btn_600:
			setEdittext("600", 0);
			break;
		case R.id.btn_601:
			setEdittext("601", 0);
			break;
		case R.id.btn_000:
			setEdittext("000", 0);
			break;
		case R.id.btn_002:
			setEdittext("002", 0);
			break;
		case R.id.btn_300:
			setEdittext("300", 0);
			break;
		case R.id.btn_1:
			setEdittext("1", 0);
			break;
		case R.id.btn_2:
			setEdittext("2", 0);
			break;
		case R.id.btn_3:
			setEdittext("3", 0);
			break;
		case R.id.btn_4:
			setEdittext("4", 0);
			break;
		case R.id.btn_5:
			setEdittext("5", 0);
			break;
		case R.id.btn_6:
			setEdittext("6", 0);
			break;
		case R.id.btn_7:
			setEdittext("7", 0);
			break;
		case R.id.btn_8:
			setEdittext("8", 0);
			break;
		case R.id.btn_9:
			setEdittext("9", 0);
			break;
		case R.id.btn_0:
			setEdittext("0", 0);
			break;
		case R.id.btn_abc:
			setEdittext("", 1);
			break;
		case R.id.btn_yc:
			setEdittext("", 3);
			break;
		case R.id.btn_qk:
			setEdittext("", 4);
			break;
		case R.id.btn_sure:
			setEdittext("", 5);
			break;
		case R.id.btn_delete:
			setEdittext("", 2);
			break;
		default:
			break;
		}
	}

	private void setEdittext(String str, int handle) {
		switch (handle) {
		case 0:// 输入内容
			editText.setText(editText.getText().toString() + str);
			break;
		case 1:// 跳到字母键盘
			keybordPopupWindow.dismiss();
			keybordPopupWindow = null;
			editText.setInputType(InputType.TYPE_CLASS_TEXT);
			InputMethodManager inputManager = (InputMethodManager) editText
					.getContext()
					.getSystemService(Context.INPUT_METHOD_SERVICE);
			inputManager.showSoftInput(editText, 0);
			break;
		case 2:// 退格
			String s = editText.getText().toString();
			if (s.length() > 1) {
				editText.setText(s.substring(0, s.length() - 1));
			} else {
				editText.setText("");
			}
			break;
		case 3:// 影藏
			keybordPopupWindow.dismiss();
			keybordPopupWindow = null;
			break;
		case 4:// 清空
			editText.setText("");
			break;
		case 5:// 确认
			keybordPopupWindow.dismiss();
			keybordPopupWindow = null;
			onSure();
			break;

		default:
			break;
		}
	}

	public interface OnSureListener {
		public void onSure();
	}

	public void onSure() {
		if (null != onSureListener) {
			onSureListener.onSure();
		}
	}

}
