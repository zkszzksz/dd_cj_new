package com.inwhoop.ddcj.impl;

/**
 * 定位接口
 * 
 * @Project: DDCJ
 * @Title: MLocationLinsener.java
 * @Package com.inwhoop.ddcj.impl
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-10-24 下午7:52:26
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public interface MLocationLinsener {
	void mLocationSucess(String address);

	void mLocationFaile(String errorInfo);
}
