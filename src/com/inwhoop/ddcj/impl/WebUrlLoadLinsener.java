package com.inwhoop.ddcj.impl;

/**
 * 拦截url处理接口
 * 
 * @Project: DDCJ
 * @Title: WebUrlLoadLinsener.java
 * @Package com.inwhoop.ddcj.impl
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-12-10 下午4:19:17
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public interface WebUrlLoadLinsener {
	void loadTheme(String theme);

	void loadStock(String stockCode, String stockName);

}
