package com.inwhoop.ddcj.impl;
/**
 * 提示需要登录的接口
 * @Project: DDCJ
 * @Title: AlertLoginLisenner.java
 * @Package com.inwhoop.ddcj.impl
 * @Description: TODO
 *
 * @author ligang@inwhoop.com
 * @date 2015-2-6 下午2:07:21
 * @Copyright: 2015 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public interface AlertLoginLisenner {
   public void loginComplete();
}
