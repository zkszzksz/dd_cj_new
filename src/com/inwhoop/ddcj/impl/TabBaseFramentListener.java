package com.inwhoop.ddcj.impl;

import android.widget.AbsListView;

/**
 * ListView头部停靠的监听器
 * 
 * @Project: DDCJ
 * @Title: TabBaseFramentListener.java
 * @Package com.inwhoop.ddcj.impl
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-12-2 上午11:35:45
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public interface TabBaseFramentListener {
	/**
	 * 重置滚动状态
	 * 
	 * @Title: restScrollState
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	public void restScrollState();

	/**
	 * 更新滚动状态
	 * 
	 * @Title: onScroll
	 * @Description: TODO
	 * @param @param view
	 * @param @param firstVisibleItem
	 * @param @param visibleItemCount
	 * @param @param totalItemCount
	 * @return void
	 */
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount);
}
