package com.inwhoop.ddcj.impl;

import java.util.List;

import com.inwhoop.ddcj.bean.StockInfoBean;

/**
 * 管理股票界面删除股票接口
 * 
 * @Project: DDCJ
 * @Title: DetelStockLinsener.java
 * @Package com.inwhoop.ddcj.impl
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-27 下午3:19:08
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public interface DetelStockLinsener {
	void detelStock(List<StockInfoBean> deleteList);
}
