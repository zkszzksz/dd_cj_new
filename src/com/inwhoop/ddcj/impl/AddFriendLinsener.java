package com.inwhoop.ddcj.impl;

import com.inwhoop.ddcj.bean.UserBean;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: 列表点击关注后接口
 * @Package com.inwhoop.ddcj.impl
 * @Date: 2014/12/18 17:39
 * @version: 1.0
 */
public interface AddFriendLinsener {
	/**
	 * @param bean
	 *            需要去关注的哪个用户
	 * @param position
	 *            列表中的位置
	 */
	void addFriend(UserBean bean, int position);
}
