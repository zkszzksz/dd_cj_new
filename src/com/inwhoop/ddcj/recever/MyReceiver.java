package com.inwhoop.ddcj.recever;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import cn.jpush.android.api.JPushInterface;
import com.inwhoop.ddcj.activity.MainActivity;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.JpushBean;
import com.inwhoop.ddcj.db.PushMsgDb;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.TimeUtils;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: GuoTeng
 * @Title: MyRecever.java
 * @Package com.inwhoop.guoteng.recever
 * @Description: TODO
 * @date 2014-3-25 上午11:10:23
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class MyReceiver extends BroadcastReceiver {

    private static final String TAG = "MyReceiver";
    private JpushBean bean;

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        Log.d(TAG, "onReceive - " + intent.getAction() + ", extras: "
                + printBundle(bundle));
        String msgContent = bundle.getString(JPushInterface.EXTRA_EXTRA);
        System.out.println("获取到的自定义消息为111===" + msgContent);
        if (null != msgContent && !msgContent.equals("{}")) {
            try {
                bean = JsonUtils.getPushData(msgContent);
                bean.isread = "0";
                int typeI = Integer.valueOf(bean.type);
                switch (typeI) {
                    case 1:  //情报
                        bean.isAgree = "0";
                        Bundle bundle1 = new Bundle();
                        bundle1.putSerializable("pushBean", bean);
                        Intent intent1 = new Intent();
                        intent1.putExtras(bundle);
                        intent1.setAction(Configs.PUSH_ACTION1);
                        context.sendBroadcast(intent1);
                        break;
                    case 2: //资讯
                        break;
                    case 3: //单聊
                        break;
                    case 4:  //群聊
                        break;
                    case 5:  //登出通知
                        break;
                    case 6: //群审核通过或被拒绝
                        bean.isAgree = "0";
                        Bundle bundle6 = new Bundle();
                        bundle6.putSerializable("pushBean", bean);
                        Intent intent6 = new Intent();
                        intent6.putExtras(bundle);
                        intent6.setAction(Configs.PUSH_ACTION6);
                        context.sendBroadcast(intent6);
                        break;
                    case 7: //群审核已经通过或已经被拒绝
                        bean.isAgree = "0";
                        Bundle bundle7 = new Bundle();
                        bundle7.putSerializable("pushBean", bean);
                        Intent intent7 = new Intent();
                        intent7.putExtras(bundle);
                        intent7.setAction(Configs.PUSH_ACTION6);
                        context.sendBroadcast(intent7);
                        break;
                }
                if (Integer.valueOf(bean.type) == 1 ||
                        Integer.valueOf(bean.type) == 6 ||
                        Integer.valueOf(bean.type) == 7) {
                    PushMsgDb db = new PushMsgDb(context);
                    db.saveMsg(bean);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
            String regId = bundle
                    .getString(JPushInterface.EXTRA_REGISTRATION_ID);
            Log.d(TAG, "接收Registration Id : " + regId);
            // send the Registration Id to your server...
        } else if (JPushInterface.ACTION_UNREGISTER.equals(intent.getAction())) {
            String regId = bundle
                    .getString(JPushInterface.EXTRA_REGISTRATION_ID);
            Log.d(TAG, "接收UnRegistration Id : " + regId);
            // send the UnRegistration Id to your server...
        } else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent
                .getAction())) {
            Log.d(TAG,
                    "接收到推送下来的自定义消息: "
                            + bundle.getString(JPushInterface.EXTRA_MESSAGE));
            processCustomMessage(context, bundle);

        } else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent
                .getAction())) {
            String content = bundle.getString(JPushInterface.EXTRA_ALERT);
            Log.d(TAG, "接收到推送下来的通知" + content);
            /*
             * type=1 情报推送***=2 资讯推送***=0 登出通知***=3 群消息通知***=4 群消息通知
			 */
            Intent intent1 = new Intent();

            int notifactionId = bundle
                    .getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
            Log.d(TAG, "接收到推送下来的通知的ID: " + notifactionId);

        } else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent
                .getAction())) {
            Log.d(TAG, "用户点击打开了通知");

            // 打开自定义的Activity

            Intent i = new Intent(context, MainActivity.class);
            i.putExtras(bundle);
            i.putExtra("tag","push");
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(i);

        } else if (JPushInterface.ACTION_RICHPUSH_CALLBACK.equals(intent
                .getAction())) {
            Log.d(TAG,
                    "用户收到到RICH PUSH CALLBACK: "
                            + bundle.getString(JPushInterface.EXTRA_EXTRA));
            // 在这里根据 JPushInterface.EXTRA_EXTRA 的内容处理代码，比如打开新的Activity，
            // 打开一个网页等..

        } else {
            Log.d(TAG, "Unhandled intent - " + intent.getAction());
        }
    }

    // 打印所有的 intent extra 数据
    private static String printBundle(Bundle bundle) {
        StringBuilder sb = new StringBuilder();
        for (String key : bundle.keySet()) {
            if (key.equals(JPushInterface.EXTRA_NOTIFICATION_ID)) {
                sb.append("\nkey:" + key + ", value:" + bundle.getInt(key));
            } else {
                sb.append("\nkey:" + key + ", value:" + bundle.getString(key));
            }
        }
        return sb.toString();
    }

    // send msg to MainActivity
    private void processCustomMessage(Context context, Bundle bundle) {
        String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
        String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
        Intent msgIntent = new Intent(Configs.MESSAGE_RECEIVED_ACTION);
        msgIntent.putExtra(Configs.KEY_MESSAGE, message);
        if (!"".equals(extras)) {
            try {
                JSONObject extraJson = new JSONObject(extras);
                if (null != extraJson && extraJson.length() > 0) {
                    msgIntent.putExtra(Configs.KEY_EXTRAS, extras);
                }
            } catch (JSONException e) {

            }

        }
        context.sendBroadcast(msgIntent);
    }
    //
}
