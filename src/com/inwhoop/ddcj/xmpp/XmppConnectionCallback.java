package com.inwhoop.ddcj.xmpp;

public interface XmppConnectionCallback {
	/**
	 * The connection will retry to reconnect in the specified number of
	 * seconds.
	 * 
	 * @param seconds
	 *            remaining seconds before attempting a reconnection.
	 */
	public void XmppReconnectingIn(int seconds);
}
