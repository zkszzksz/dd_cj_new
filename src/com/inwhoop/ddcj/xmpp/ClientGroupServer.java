package com.inwhoop.ddcj.xmpp;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.ddcj.adapter.GroupChatlistAdapter;
import com.inwhoop.ddcj.bean.Groups;
import com.inwhoop.ddcj.db.HandleFriendchatlistRecordDB;
import com.inwhoop.ddcj.db.HandleGroupchatNoReadRecordDB;
import com.inwhoop.ddcj.db.HandleGroupchatRecordDB;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import org.jivesoftware.smackx.muc.MultiUserChat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.widget.ListView;

/**
 * @Project: WZSchool
 * @Title: ClientGroupServer.java
 * @Package com.wz.util
 * @Description: TODO 群聊监听
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-3-3 下午1:42:27
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ClientGroupServer {

	public static ClientGroupServer client = null;

	public static Context contxet = null;

	private HandleGroupchatRecordDB db = null;
	
	private HandleGroupchatNoReadRecordDB nodb = null;
	
	private HandleFriendchatlistRecordDB fdb = null;

	private GroupChatlistAdapter adapter = null;

	public boolean isChat = false;

	private ListView listView;
	
	private List<MultiUserChat> mulist = new ArrayList<MultiUserChat>();
	

	public static ClientGroupServer getInstance(Context mContext) {
		contxet = mContext;
		if (null == client) {
			client = new ClientGroupServer();
		}
		return client;
	}

	
	public void initMultiUserChat(List<Groups> list) {
		for (int i = 0; i < list.size(); i++) {
			try{
				MultiUserChat mu = XmppConnectionUtil.getInstance()
						.joinMultiUserChat(UserInfoUtil.getUserInfo(contxet).tel,
								list.get(i).groupid, "");
				mulist.add(mu);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
	}
	
	/*public void addMultiuserchat(String groupid,String groupname,MultiUserChat mu){
		Groups groups = new Groups();
		groups.groupid = groupid;
		groups.groupname = groupname;
		list.add(groups);
		mulist.add(mu);
	}*/
	
	public void addMultiuserchat(String groupid,String groupname,List<Groups> list){
		Groups groups = new Groups();
		groups.groupid = groupid;
		groups.groupname = groupname;
		list.add(groups);
		MultiUserChat mu = XmppConnectionUtil.getInstance()
				.joinMultiUserChat(UserInfoUtil.getUserInfo(contxet).tel,
						groupid, "");
		mulist.add(mu);
	}


	public MultiUserChat getChatMu(List<Groups> list,String groupid) {
		if (null != list && list.size() > 0) {
			for (int i = 0; i < list.size(); i++) {
				if (groupid.equals(list.get(i).groupid)) {
					if(i<mulist.size()){
						return mulist.get(i);
					}
					return null;
				}
			}
		}
		return null;
	}

}
