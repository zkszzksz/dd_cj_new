package com.inwhoop.ddcj.xmpp;

import java.lang.ref.WeakReference;
import java.util.Set;

import org.jivesoftware.smack.Connection;
import org.jivesoftware.smackx.ping.PingFailedListener;

public class JLServerPingTask implements Runnable {

	// This has to be a weak reference because IIRC all threads are roots
	// for objects and we have a new thread here that should hold a strong
	// reference to connection so that it can be GCed.
	private WeakReference<Connection> weakConnection;

	private int delta = 1000; // 1 seconds
	private int tries = 3; // 3 tries

	protected JLServerPingTask(Connection connection) {
		this.weakConnection = new WeakReference<Connection>(connection);
	}

	public void run() {
		Connection connection = weakConnection.get();
		if (connection == null) {
			// connection has been collected by GC
			// which means we can stop the thread by breaking the loop
			return;
		}
		if (connection.isAuthenticated()) {
			JLPingManager pingManager = JLPingManager
					.getInstanceFor(connection);
			boolean res = false;

			for (int i = 0; i < tries; i++) {
				if (i != 0) {
					try {
						Thread.sleep(delta);
					} catch (InterruptedException e) {
						// We received an interrupt
						// This only happens if we should stop pinging
						return;
					}
				}
				res = pingManager.pingMyServer();
				// stop when we receive a pong back
				if (res) {
					pingManager.lastSuccessfulPingByTask = System
							.currentTimeMillis();
					break;
				}
			}
			if (!res) {
				System.out
						.println("The Ping command is unsuccessfully runned!");
				Set<PingFailedListener> pingFailedListeners = pingManager
						.getPingFailedListeners();
				for (PingFailedListener l : pingFailedListeners) {
					l.pingFailed();
				}
			} else {
				// Ping was successful, wind-up the periodic task again
				System.out.println("The Ping command is successfully runned!");
				pingManager.maybeSchedulePingServerTask();
			}
		}
	}

}
