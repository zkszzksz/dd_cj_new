//package com.inwhoop.ddcj.xmpp;
//
//import java.util.List;
//
//import org.jivesoftware.smack.Chat;
//import org.jivesoftware.smack.ChatManager;
//
//import android.annotation.SuppressLint;
//import android.content.Context;
//import android.content.Intent;
//import android.os.Environment;
//import android.os.Handler;
//import android.os.Message;
//
//import com.inwhoop.ddcj.adapter.ChatlistAdapter;
//import com.inwhoop.ddcj.app.Configs;
//import com.inwhoop.ddcj.bean.FriendChatinfo;
//import com.inwhoop.ddcj.db.HandleFriendchatNoReadRecordDB;
//import com.inwhoop.ddcj.db.HandleFriendchatRecordDB;
//import com.inwhoop.ddcj.db.HandleFriendchatlistRecordDB;
//import com.inwhoop.ddcj.util.UserInfoUtil;
//import com.inwhoop.ddcj.util.Utils;
//import com.inwhoop.ddcj.view.XListView;
//
///**
// * @Project: WZSchool
// * @Title: ClienConServer.java
// * @Package com.wz.util
// * @Description: TODO 监听接收消息的
// * 
// * @author dyong199046@163.com 代勇
// * @date 2014-2-26 下午6:03:35
// * @Copyright: 2014 呐喊信息技术 All rights reserved.
// * @version V1.0
// */
//public class ClienConServer {
//
//	public static Context context;
//
//	private ChatManager cm = null;
//
//	private FriendChatinfo info = null;
//
//	public boolean isChat = false;
//
//	private List<FriendChatinfo> list;
//
//	private ChatlistAdapter adapter;
//
//	private XListView listview = null;
//
//	private String chatname;
//
//	private HandleFriendchatlistRecordDB db = null;
//
//	private HandleFriendchatRecordDB fdb = null;
//
//	private HandleFriendchatNoReadRecordDB ndb = null;
//
//	public static ClienConServer client = null;
//
//	public static ClienConServer getInstance(Context mcontext) {
//		context = mcontext;
//		if (null == client) {
//			client = new ClienConServer();
//		}
//		return client;
//	}
//
//	private ClienConServer() {
//		db = new HandleFriendchatlistRecordDB(context);
//		fdb = new HandleFriendchatRecordDB(context);
//		ndb = new HandleFriendchatNoReadRecordDB(context);
//		new Thread(new Runnable() {
//
//			@Override
//			public void run() {
//				Message msg = new Message();
//				try {
//					// cm = XmppConnectionUtil.getInstance().getConnection()
//					// .getChatManager();
//					msg.what = Configs.READ_SUCCESS;
//				} catch (Exception e) {
//					msg.what = Configs.READ_FAIL;
//				}
//				handler.sendMessage(msg);
//			}
//		}).start();
//	}
//
//	public Chat getChatManager(String username) {
//		if (null == cm) {
//			cm = XmppConnectionUtil.getInstance().getConnection()
//					.getChatManager();
//		}
//		Chat newchat = cm.createChat(username
//				+ "@"
//				+ XmppConnectionUtil.getInstance().getConnection()
//						.getServiceName(), null);
//		return newchat;
//	}
//
//	public void set(List<FriendChatinfo> list, ChatlistAdapter adapter,
//			XListView listview, String chatname) {
//		this.list = list;
//		this.adapter = adapter;
//		this.listview = listview;
//		this.chatname = chatname;
//	}
//
//	@SuppressLint("HandlerLeak")
//	private Handler handler = new Handler() {
//		public void handleMessage(Message msg) {
//			switch (msg.what) {
//			case Configs.READ_FAIL:
//				client = null;
//				break;
//
//			case Configs.READ_SUCCESS:
//				MypacketListener.getInstance().setfHandler(getHandler);
//				break;
//
//			default:
//				break;
//			}
//		};
//	};
//
//	@SuppressLint("HandlerLeak")
//	private Handler getHandler = new Handler() {
//		@SuppressLint("DefaultLocale")
//		public void handleMessage(Message msg) {
//			switch (msg.what) {
//			case 1:
//				String[] args = (String[]) msg.obj;
//				String from = args[0];
//				from = from.substring(0, from.indexOf("@"));
//				try {
//					info = SingleChatPacketExtension.getBosy(args[1]);
//					info.userid = UserInfoUtil.getUserInfo(context).userid;
//					info.username = from;
//					info.isMymsg = false;
//					if (info.msgtype == 3) {
//						Utils.decoderBase64File(info.content,
//								"" + Environment.getExternalStorageDirectory()
//										+ info.audiopath);
//						info.content = "";
//					}
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				if (!db.isExist(from, 1, info.time,
//						"" + UserInfoUtil.getUserInfo(context).userid)) {
//					db.saveGmember(from, 1, info.time,
//							"" + UserInfoUtil.getUserInfo(context).userid,
//							info.userheadpath, info.usernick);
//				} else {
//					db.UpdateChatagetTime(from, 1,
//							"" + UserInfoUtil.getUserInfo(context).userid,
//							info.time, info.userheadpath, info.usernick);
//				}
//				if (isChat && (from.equals(chatname))) {
//					list.add(info);
//					adapter.notifyDataSetChanged();
//					listview.setSelection(listview.getCount() - 1);
//					fdb.saveGmember(info);
//				} else {
//					ndb.saveGmember(info);
//					if (Configs.messagelistishiden) {
//						Intent intent = new Intent();
//						intent.setAction(Configs.ACTION_MESSAGE);
//						context.sendBroadcast(intent);
//					}
//				}
//				break;
//
//			default:
//				break;
//			}
//
//		};
//	};
//
//}
