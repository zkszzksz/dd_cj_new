package com.inwhoop.ddcj.xmpp;

import java.util.Timer;
import java.util.TimerTask;

import org.jivesoftware.smack.ConnectionListener;

import android.util.Log;

/**
 * XMPP连接监听类
 * 
 * @Project: WZSchool
 * @Title: TaxiConnectionListener.java
 * @Package com.wz.util
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-2-19 下午1:43:47
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class TaxiConnectionListener implements ConnectionListener {
	private Timer tExit;
	private String username;
	private String password;
	private int logintime = 2000;

	@Override
	public void connectionClosed() {
		Log.i("TaxiConnectionListener", "连接关闭");
		// 关闭连接
		XmppConnectionUtil.getInstance().closeConnection();
		// 重连服务器
		tExit = new Timer();
		tExit.schedule(new timetask(), logintime);
	}

	@Override
	public void connectionClosedOnError(Exception e) {
		Log.i("TaxiConnectionListener", "连接关闭异常");
		// 判断为账号已被登录
		boolean error = e.getMessage().equals("stream:error (conflict)");
		if (!error) {
			// 关闭连接
			XmppConnectionUtil.getInstance().closeConnection();
			// 重连服务器
			tExit = new Timer();
			tExit.schedule(new timetask(), logintime);
		}
	}

	class timetask extends TimerTask {
		@Override
		public void run() {
			// username = Utils.getInstance().getSharedPreferences("taxicall",
			// "account", MainActivity.context);
			// password = Utils.getInstance().getSharedPreferences("taxicall",
			// "password", MainActivity.context);
			if (username != null && password != null) {
				Log.i("TaxiConnectionListener", "尝试登录");
				// 连接服务器
				if (XmppConnectionUtil.getInstance().login(username, password)) {
					Log.i("TaxiConnectionListener", "登录成功");
				} else {
					Log.i("TaxiConnectionListener", "重新登录");
					tExit.schedule(new timetask(), logintime);
				}
			}
		}
	}

	@Override
	public void reconnectingIn(int arg0) {
	}

	@Override
	public void reconnectionFailed(Exception arg0) {
	}

	@Override
	public void reconnectionSuccessful() {
	}
}
