package com.inwhoop.ddcj.search;

import java.util.List;

import android.os.Handler;
import android.os.Message;

import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.ChannelList;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.SearchUtil;

public class SearchTask implements Runnable {
	public String taskName; // 搜索任务
	private String keyword;
	private String uid;
	private Handler handler;

	public SearchTask(String taskName, String keyword, String uid,
			Handler handler) {
		this.taskName = taskName;
		this.keyword = keyword;
		this.handler = handler;
		this.uid = uid;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void run() {
		Message message = handler.obtainMessage();
		try {
			if (taskName.contains("用户")) {
				Object[] data = JsonUtils.searchAllUser(keyword, uid, Configs.COUNT, 0);
				if ((Boolean) data[0]) {
					SearchUtil.userList = (List<UserBean>) data[2];
					message.what = Configs.SEARCH_USER_SUCESS;
				} else {
					message.what = Configs.SEARCH_USER_FAIL;
				}
			} else if (taskName.contains("群组")) {
				Object[] data = JsonUtils.searchAllGroup(keyword, uid, Configs.COUNT, 0);
				if ((Boolean) data[0]) {
					SearchUtil.groupList = (List<CateChannel>) data[2];
					message.what = Configs.SEARCH_GROUP_SUCESS;
				} else {
					message.what = Configs.SEARCH_GROUP_FAIL;
				}
			} else {
				Object[] data = JsonUtils.searchAllNews(keyword, uid, Configs.COUNT, 0);
				if ((Boolean) data[0]) {
					SearchUtil.newsList = (List<ChannelList>) data[2];
					message.what = Configs.SEARCH_NEWS_SUCESS;
				} else {
					message.what = Configs.SEARCH_NEWS_FAIL;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		handler.sendMessage(message);
	}

	public String getTaskName() {
		return taskName;
	}

}
