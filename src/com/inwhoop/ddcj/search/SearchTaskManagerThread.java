package com.inwhoop.ddcj.search;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SearchTaskManagerThread implements Runnable {
	private SearchTaskMannger searchTaskMannger;

	// 创建一个可重用固定线程数的线程池
	private ExecutorService pool;
	// 线程池大小
	private final int POOL_SIZE = 3;
	// 轮询时间
	private final int SLEEP_TIME = 1000;
	// 是否停止
	private boolean isStop = false;

	public SearchTaskManagerThread() {
		searchTaskMannger = SearchTaskMannger.getInstance();
		pool = Executors.newFixedThreadPool(POOL_SIZE);

	}

	@SuppressWarnings("unused")
	@Override
	public void run() {
		while (!isStop) {
			SearchTask searchTask = searchTaskMannger.getSearchTask();
			if (null!=searchTask) {
				pool.execute(searchTask);
			} else { // 如果当前没有searchTask在任务队列中
				try {
					// 查询任务完成失败的,重新加载任务队列
					// 轮询
					Thread.sleep(SLEEP_TIME);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
		if (isStop) {
			pool.shutdown();
		}

	}

	public void setStop(boolean isStop) {
		this.isStop = isStop;
	}
}
