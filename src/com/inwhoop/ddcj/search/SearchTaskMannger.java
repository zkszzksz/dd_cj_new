package com.inwhoop.ddcj.search;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
/**
 * 线程管理类
 * @Project: DDCJ
 * @Title: SearchTaskMannger.java
 * @Package com.inwhoop.ddcj.search
 * @Description: TODO
 *
 * @author ligang@inwhoop.com
 * @date 2015-1-23 下午1:37:56
 * @Copyright: 2015 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class SearchTaskMannger {
	// UI请求队列
	private LinkedList<SearchTask> searchTasks;
	// 任务不能重复
	private Set<String> taskIdSet;

	private static SearchTaskMannger searchTaskMannger;

	private SearchTaskMannger() {
		searchTasks = new LinkedList<SearchTask>();
		taskIdSet = new HashSet<String>();
		
	}

	public static synchronized SearchTaskMannger getInstance() {
		if (searchTaskMannger == null) {
			searchTaskMannger = new SearchTaskMannger();
		}
		
		return searchTaskMannger;
	}

	// 先执行
	public void addSearchTask(SearchTask searchTask) {
		synchronized (searchTasks) {
			if (!isTaskRepeat(searchTask.getTaskName())) {
				// 增加下载任务
				searchTasks.addLast(searchTask);
			}
		}

	}

	public boolean isTaskRepeat(String taskName) {
		synchronized (taskIdSet) {
			if (taskIdSet.contains(taskName)) {
				return true;
			} else {
				taskIdSet.add(taskName);
				return false;
			}
		}
	}

	public SearchTask getSearchTask() {
		synchronized (searchTasks) {
			if (searchTasks.size() > 0) {
				SearchTask searchTask = searchTasks.removeFirst();
				return searchTask;
			}
		}
		return null;
	}
	
	public void clear(){
		searchTasks.clear();
		taskIdSet.clear();
		
	}
}
