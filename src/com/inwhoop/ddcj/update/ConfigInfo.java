package com.inwhoop.ddcj.update;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.util.Log;
import com.inwhoop.R;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ConfigInfo {
	private static final String TAG = "Config";

	/**
	 * 读取AndroidManifest.xml中的versionCode
	 * 
	 * @param context
	 * @return
	 */
	public static int getVerCode(Context context) {
		int verCode = -1;

		try {
			verCode = context.getPackageManager().getPackageInfo(
					"com.inwhoop.showroom", 0).versionCode;
		} catch (NameNotFoundException e) {
			Log.e(TAG, e.getMessage());
		}
		return verCode;
	}

	/**
	 * 读取AndroidManifest.xml中的versionName
	 * 
	 * @param context
	 * @return
	 */
	public static String getVerName(Context context) {
		String verName = "";
		try {
			verName = context.getPackageManager().getPackageInfo(
					"com.inwhoop.showroom", 0).versionName;
		} catch (NameNotFoundException e) {
			Log.e(TAG, e.getMessage());
		}

		return verName;
	}

	/**
	 * 获取应用程序名称
	 * 
	 * @param context
	 * @return
	 */
	public static String getAppName(Context context) {
		String appName = context.getResources().getText(R.string.app_name)
				.toString();
		return appName;
	}

	/**
	 * 获取网址内容
	 * 
	 * @param url
	 * @return
	 * @throws Exception
	 */
	public static String getContantToNetWork(String url) throws Exception {
		StringBuilder sBuilder = new StringBuilder();
		HttpClient httpClient = new DefaultHttpClient();
		HttpParams hParams = httpClient.getParams();
		HttpConnectionParams.setConnectionTimeout(hParams, 3000);
		HttpConnectionParams.setSoTimeout(hParams, 5000);
		HttpResponse httpResponse = httpClient.execute(new HttpGet(url));
		HttpEntity hEntity = httpResponse.getEntity();
		if (hEntity != null) {
			BufferedReader bReader = new BufferedReader(new InputStreamReader(
					hEntity.getContent(), "UTF-8"), 8192);
			String line = "";
			while ((line = bReader.readLine()) != null) {
				sBuilder.append(line + "\n");
			}
			bReader.close();

		}
		return sBuilder.toString();
	}
}
