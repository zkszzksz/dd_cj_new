package com.inwhoop.ddcj.update;

import android.app.*;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.RemoteViews;
import com.google.gson.stream.JsonReader;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.util.Utils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class Updata {
	private static final String TAG = "Updata";
	// public ProgressDialog pDialog;
	private Handler handler = new Handler();
	public int newVerCode = 0;
	public String newVerName = "";
	private NotificationManager notificationManager;
	private Notification notification;
	private static final int NOTIFY_ID = 0;
	long lenght;
	int count = 0;
	int temp = 5;

	public void readUpdata() throws Exception {
		String path = Configs.HTTP_UPDATA + "http.txt";
		List<String> list = new ArrayList<String>();
		list = Utils.readTextCotant(path);
		if (list.size() > 0) {
			Configs.UPDATA_SERVER = list.get(0);

			// System.out.println("Contant.UPDATA_SERVER=  "
			// + Contant.UPDATA_SERVER);
		}
	}

	/**
	 * 版本检测
	 * 
	 * @return
	 * @throws Exception
	 */
	public boolean getServerVerCode() throws Exception {
		readUpdata();
		try {
			String verjson = ConfigInfo.getContantToNetWork(Configs.HTTP_UPDATA
					+ Configs.UPDATA_VERJSON);
			JsonReader jreader = new JsonReader(new StringReader(verjson));
			jreader.beginArray();
			while (jreader.hasNext()) {
				jreader.beginObject();
				while (jreader.hasNext()) {
					String tagName = jreader.nextName();
					if (tagName.equals("verName")) {
						newVerName = jreader.nextString();
					} else if (tagName.equals("verCode")) {
						newVerCode = jreader.nextInt();
					}
				}
				jreader.endObject();

			}

			jreader.endArray();

		} catch (Exception e) {
			newVerCode = -1;
			newVerName = "";
			Log.e(TAG, e.getMessage());
			return false;

		}

		return true;
	}

	/**
	 * 没有可以更新的版本
	 * 
	 * @param context
	 */
	public void notFindNewVerCode(Context context) {
		// int verCode = ConfigInfo.getVerCode(context);
		// String verName = ConfigInfo.getVerName(context);
		// StringBuffer sBuffer = new StringBuffer();
		// sBuffer.append("当前版本：");
		// sBuffer.append(verName);
		// sBuffer.append(" Code:");
		// sBuffer.append(verCode);
		// sBuffer.append(",\n已经是最新版，无需更新！");
		Dialog dialog = new AlertDialog.Builder(context).setTitle("软件更新")
				.setMessage("已经是最新版，无需更新！")// 设置内容
				.setPositiveButton("确定",// 设置确定按钮
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
							}

						}).create();// 创建
		// 显示对话框
		dialog.show();

	}

	public void findNewVerCode(final Context context) {
		// int verCode = ConfigInfo.getVerCode(context);
		// String verName = ConfigInfo.getVerName(context);
		// String currect_ver = "当前版本：" + verName + "Code:" + verCode;
		String new_ver = "发现新版本：" + newVerName + "\n是否更新？";
		StringBuffer sBuffer = new StringBuffer();
		// sBuffer.append(currect_ver);
		sBuffer.append(new_ver);
		Dialog dialog = new AlertDialog.Builder(context)
				.setTitle("软件更新")
				.setMessage(sBuffer.toString())
				.setPositiveButton("更新", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {

						// pDialog = new ProgressDialog(context);
						// pDialog.setTitle("正在下载");
						// pDialog.setMessage("请稍候...");
						// pDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
						down(context, Configs.UPDATA_SERVER);
						dialog.dismiss();

					}

				})
				.setNeutralButton("暂不更新",
						new DialogInterface.OnClickListener() {

							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();

							}
						}).create();
		dialog.show();
	}

	public void down(Context context, String url) {
		notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);

		notification = new Notification(R.drawable.down, "开始下载",
				System.currentTimeMillis());
		notification.flags = Notification.FLAG_ONGOING_EVENT;

		RemoteViews contentView = new RemoteViews(context.getPackageName(),
				R.layout.download_notification_layout);

		contentView.setTextViewText(R.id.fileName, Configs.UPDATA_SAVEAPK);
		notification.contentView = contentView;

		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(new File(Environment
				.getExternalStorageDirectory(), Configs.UPDATA_SAVEAPK)),
				"application/vnd.android.package-archive");

		PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
				intent, 0);
		notification.contentIntent = contentIntent;

		notificationManager.notify(NOTIFY_ID, notification);
		downFile(url, context);

	}

	public void downFile(final String url, final Context context) {
		// pDialog.show();
		new Thread() {

			@Override
			public void run() {
				HttpClient httpClient = new DefaultHttpClient();
				HttpGet httpGet = new HttpGet(url);
				try {
					HttpResponse hResponse = httpClient.execute(httpGet);
					HttpEntity hEntity = hResponse.getEntity();
					lenght = hEntity.getContentLength();
					InputStream iStream = hEntity.getContent();
					FileOutputStream fileOutputStream = null;
					if (iStream != null) {
						File file = new File(
								Environment.getExternalStorageDirectory(),
								Configs.UPDATA_SAVEAPK);
						fileOutputStream = new FileOutputStream(file);
						byte[] buff = new byte[1024];
						int i = -1;

						while ((i = iStream.read(buff)) != -1) {
							fileOutputStream.write(buff, 0, i);
							count += i;
							int temp_2 = (int) ((count / (lenght / 100)));
							// System.out.println("开始==  "+temp_2);
							if (temp_2 == temp) {
								temp += 5;
								Message msg = Message.obtain();
								msg.obj = context;
								msg.arg1 = temp_2;
								down_handler.sendMessage(msg);
							}

						}

					}

					fileOutputStream.flush();
					if (fileOutputStream != null) {
						fileOutputStream.close();
					}

				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}

		}.start();

	}

	private Handler down_handler = new Handler() {

		@Override
		public void handleMessage(Message msg) {
			if (count < lenght) {
				// 更新进度
				RemoteViews contentView = notification.contentView;
				contentView.setTextViewText(R.id.rate, msg.arg1 + "%");
				contentView.setProgressBar(R.id.progress, 100, msg.arg1, false);
			} else {
				// // 下载完毕后变换通知形式
				notification.flags = Notification.FLAG_AUTO_CANCEL;
				notification.contentView = null;
				// Intent intent = new Intent((Context)msg.obj,
				// FileMgrActivity.class);
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setDataAndType(
						Uri.fromFile(new File(Environment
								.getExternalStorageDirectory(),
								Configs.UPDATA_SAVEAPK)),
						"application/vnd.android.package-archive");
				PendingIntent contentIntent = PendingIntent.getActivity(
						(Context) msg.obj, 0, intent, 0);
				notification.setLatestEventInfo((Context) msg.obj, "下载完成",
						"文件已下载完毕", contentIntent);
				// downFinish((Context) msg.obj);
			}

			// 通知更新
			notificationManager.notify(NOTIFY_ID, notification);

		}

	};

	/**
	 * 下载完成
	 * 
	 * @param context
	 */
	public void downFinish(final Context context) {
		handler.post(new Runnable() {

			@Override
			public void run() {
				// pDialog.cancel();
				// pDialog.dismiss();
				// install(context);

			}
		});

	}

	/**
	 * 安装应用
	 * 
	 * @param context
	 */
	public void install(Context context) {

		Intent intent = new Intent(Intent.ACTION_VIEW);
		intent.setDataAndType(Uri.fromFile(new File(Environment
				.getExternalStorageDirectory(), Configs.UPDATA_SAVEAPK)),
				"application/vnd.android.package-archive");
		context.startActivity(intent);

	}
}
