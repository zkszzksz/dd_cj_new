package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.NewsBean;
import com.inwhoop.ddcj.util.TimeUtils;
import com.inwhoop.ddcj.view.CircleImageview;
import com.inwhoop.ddcj.view.MyLayout;
import com.inwhoop.ddcj.xmpp.FaceConversionUtil;

import java.util.List;

import net.tsz.afinal.FinalBitmap;


/**
 * Created by Administrator on 2014/10/21.
 */
public class QingbaoListAdapter extends BaseAdapter {

	private List<NewsBean> list = null;

	private Context context = null;

	private LayoutInflater inflater = null;

	
	private FinalBitmap fb = null;

	public QingbaoListAdapter(Context context, List<NewsBean> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.qingbao_loading_img);
		fb.configLoadfailImage(R.drawable.qingbao_loading_img);
	}

	public void addList(List<NewsBean> addlist) {
		for (int i = 0; i < addlist.size(); i++) {
			list.add(addlist.get(i));
		}
	}

	public List<NewsBean> getAll() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unused")
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == holder) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.qb_list_item_layout, null);
			holder.name = (TextView) convertView.findViewById(R.id.name);
			holder.comment = (TextView) convertView
					.findViewById(R.id.comment_count);
			holder.share = (TextView) convertView
					.findViewById(R.id.share_count);
			holder.time = (TextView) convertView.findViewById(R.id.qb_time);
			holder.content = (TextView) convertView.findViewById(R.id.qb_title);
			holder.head = (CircleImageview) convertView
					.findViewById(R.id.headimg);
			holder.layout = (MyLayout) convertView.findViewById(R.id.imglayout);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		if (null != list.get(position)) {
			if (null != list.get(position)
					&& list.get(position).imgpath.size() > 0) {
				for (int i = 0; i < list.get(position).imgpath.size(); i++) {
					View imgView = inflater.inflate(
							R.layout.qb_img_item_layout, null);
					ImageView img = (ImageView) imgView.findViewById(R.id.img);
					fb.display(img,list.get(position).imgpath.get(i));
					holder.layout.addView(imgView);
				}
			}
			holder.name.setText(list.get(position).name);
			holder.comment.setText("" + list.get(position).commnetnum);
			holder.share.setText("" + list.get(position).sharenum);
			SpannableString spannableString = FaceConversionUtil.getInstace()
					.getExpressionString(context, list.get(position).content);
			holder.content.setText(spannableString);
			holder.time.setText(""
					+ TimeUtils.twoDateDistance(list.get(position).createtime,
							TimeUtils.getStrTime(System.currentTimeMillis())));
			fb.display(holder.head, list.get(position).img);
		}
		return convertView;
	}

	class Holder {
		public TextView name, comment, share, time, content;
		public CircleImageview head;
		public MyLayout layout;
	}
}
