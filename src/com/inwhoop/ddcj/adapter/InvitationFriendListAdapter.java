package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ContactsInfo;
import com.inwhoop.ddcj.bean.InviteJoinBean;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.TimeUtils;
import com.inwhoop.ddcj.view.CircleImageview;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/14.
 */
public class InvitationFriendListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    List<InviteJoinBean> mData = new ArrayList<InviteJoinBean>();
    private FinalBitmap fb;

    public InvitationFriendListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.home_head);
        fb.configLoadfailImage(R.drawable.home_head);
        System.out.println("--------------适配器" + this.mData);
    }

    public void addList(List<InviteJoinBean> mData) {
        this.mData = mData;
    }

    public List<InviteJoinBean> getList() {
        return mData;
    }

    @Override
    public int getCount() {
        // TODO Auto-ContactsInfo method stub
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder = null;
        if (null == holder) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(
                    R.layout.invitation_friend_list_item, null);
            holder.civ_headView = (CircleImageview) convertView
                    .findViewById(R.id.headimg);
            holder.tv_name = (TextView) convertView.findViewById(R.id.nametv);
            holder.tv_info = (TextView) convertView.findViewById(R.id.content);
            holder.checkBox = (CheckBox) convertView.findViewById(R.id.check_friend);
            holder.checkBox.setTag(position);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (mData.get(position).checkTag) {
            holder.checkBox.setChecked(true);
        } else {
            holder.checkBox.setChecked(false);
        }
        if (null != mData && mData.size() > 0 && position < mData.size()) {
            InviteJoinBean contactsInfo = mData.get(position);
            if (null != contactsInfo) {
                fb.display(holder.civ_headView,
                        Configs.HOST + mData.get(position).img);
                holder.tv_name.setText(mData.get(position).name);
                holder.tv_info.setText(mData.get(position).otherinfo);
                holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        int pos = (Integer) compoundButton.getTag();
                        mData.get(pos).checkTag = b;
                    }
                });
            }
        }
        // mData.get(position).state
        return convertView;
    }

    private class ViewHolder {
        CircleImageview civ_headView;
        TextView tv_name;
        TextView tv_info;
        TextView tv_otherInfo;
        public CheckBox checkBox;
    }
}
