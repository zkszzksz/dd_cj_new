package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.inwhoop.R;
import com.inwhoop.ddcj.util.ScreenSizeUtil;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @blog http://blog.csdn.net/xiaanming
 * 
 * @author xiaanming
 * 
 *         create by ZK 目前用于频道拖动管理
 */
public class DragAdapter extends BaseAdapter implements DragGridBaseAdapter {
	private final int screenW;
	private List<HashMap<String, Object>> list;
	private LayoutInflater mInflater;
	private int mHidePosition = -1;

	public DragAdapter(Context context, List<HashMap<String, Object>> list) {
		this.list = list;
		mInflater = LayoutInflater.from(context);
		screenW = ScreenSizeUtil.getScreenSize(context).screenW / 3 - 2;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	/**
	 * 由于复用convertView导致某些item消失了，所以这里不复用item，
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		convertView = mInflater.inflate(R.layout.item_subscribe, null);
		ImageView mImageView = (ImageView) convertView
				.findViewById(R.id.item_center_img);
		TextView mTextView = (TextView) convertView
				.findViewById(R.id.item_title);
		ImageView checkView = (ImageView) convertView
				.findViewById(R.id.item_check_view);

		mImageView.setImageResource((Integer) list.get(position).get(
				"item_image"));
		mTextView.setText((CharSequence) list.get(position).get("item_text"));

		convertView.setLayoutParams(new AbsListView.LayoutParams(screenW,
				screenW));

		if (position == mHidePosition) {
			convertView.setVisibility(View.INVISIBLE);
		}

		return convertView;
	}

	@Override
	public void reorderItems(int oldPosition, int newPosition) {
		HashMap<String, Object> temp = list.get(oldPosition);
		if (oldPosition < newPosition) {
			for (int i = oldPosition; i < newPosition; i++) {
				Collections.swap(list, i, i + 1);
			}
		} else if (oldPosition > newPosition) {
			for (int i = oldPosition; i > newPosition; i--) {
				Collections.swap(list, i, i - 1);
			}
		}

		list.set(newPosition, temp);
	}

	@Override
	public void setHideItem(int hidePosition) {
		this.mHidePosition = hidePosition;
		notifyDataSetChanged();
	}

}
