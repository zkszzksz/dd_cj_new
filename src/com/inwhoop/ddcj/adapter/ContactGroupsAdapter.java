package com.inwhoop.ddcj.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.ContactGroupsInfo;
import com.inwhoop.ddcj.bean.UserBean;
import net.tsz.afinal.FinalBitmap;

/**
 * 联系人:群组fragment的适配器
 *
 * @author ZOUXU
 */
public class ContactGroupsAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private Context context;
    private List<CateChannel> mData = new ArrayList<CateChannel>();
    private FinalBitmap fb;

    private class ViewHolder {
        ImageView iv_headView;
        TextView tv_name;
        TextView tv_info;
        TextView tv_otherInfo;
    }

    public ContactGroupsAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        this.context = context;
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.home_head);
        fb.configLoadfailImage(R.drawable.home_head);
    }

    public void addList(List<CateChannel> list) {
        this.mData = list;
    }

    public List<CateChannel> getAllList() {
        return mData;
    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder = null;
        if (null == convertView) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(
                    R.layout.contacts_groups_list_item_layout, null);
            holder.iv_headView = (ImageView) convertView
                    .findViewById(R.id.headimg);
            holder.tv_name = (TextView) convertView.findViewById(R.id.nametv);
            holder.tv_info = (TextView) convertView.findViewById(R.id.content);
            holder.tv_otherInfo = (TextView) convertView
                    .findViewById(R.id.tv_other_info);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        fb.display(holder.iv_headView, mData.get(position).img);
        holder.tv_name.setText(mData.get(position).name);
        holder.tv_info.setText(mData.get(position).des);
        holder.tv_otherInfo.setText(mData.get(position).groupnum);
        return convertView;
    }
}