package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CommentMeList;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.TimeUtils;
import com.inwhoop.ddcj.view.CircleImageview;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;


/**
 * 
 * 评论我的
 * 
 * @Project: DDCJ
 * @Title: CommentMeListAdapter.java
 * @Package com.inwhoop.ddcj.adapter
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-12-16 上午10:14:18
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class CommentMeListAdapter extends BaseAdapter {

	private List<CommentMeList> list = new ArrayList<CommentMeList>();

	private Context context = null;

	private LayoutInflater inflater = null;

	private FinalBitmap fb = null;

	@SuppressWarnings("static-access")
	public CommentMeListAdapter(Context context) {
		this.context = context;
		inflater = LayoutInflater.from(context);
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.home_head);
		fb.configLoadfailImage(R.drawable.home_head);
	}

	public void addList(List<CommentMeList> addlist) {
		for (int i = 0; i < addlist.size(); i++) {
			list.add(addlist.get(i));
		}
	}

	public List<CommentMeList> getAll() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings("unused")
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == holder) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.comment_me_item_layout,
					null);
			holder.name = (TextView) convertView.findViewById(R.id.name);
			holder.comment = (TextView) convertView
					.findViewById(R.id.commentcontent);
			holder.time = (TextView) convertView.findViewById(R.id.time);
			holder.content = (TextView) convertView
					.findViewById(R.id.qingbaocontent);
			holder.head = (CircleImageview) convertView
					.findViewById(R.id.headimg);
			holder.head.setBackground(context.getResources().getColor(
					R.color.stock_bg));
			holder.img = (ImageView) convertView.findViewById(R.id.img);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.name.setText(list.get(position).name);
		holder.comment.setText("@" + list.get(position).tousername + ": "
				+ list.get(position).tocontent);
		holder.time.setText(TimeUtils.twoDateDistance(TimeRender
				.getStandardDate(list.get(position).createtime * 1000),
				TimeRender.getStandardDate()));
		fb.display(holder.head, list.get(position).img);
		holder.content.setText(list.get(position).content);
		if (list.get(position).touserimg.startsWith("http://")) {
			fb.display(holder.img, list.get(position).touserimg);
		} else {
			fb.display(holder.img, Configs.HOST + list.get(position).touserimg);
		}
		return convertView;
	}

	class Holder {
		TextView name, time, comment, content;
		CircleImageview head;
		ImageView img;
	}
}
