package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.NewsBean;
import com.inwhoop.ddcj.bean.SearchFriendBean;
import com.inwhoop.ddcj.view.CircleImageview;

import java.util.ArrayList;
import java.util.List;

/**
 * package_name: com.inwhoop.ddcj.adapter Created with IntelliJ IDEA User:
 * Administrator Date: 2014/12/9 Time: 14:25
 */
public class SearchFriendListAdapter extends BaseAdapter {

	private List<SearchFriendBean> list = new ArrayList<SearchFriendBean>();

	private Context context = null;

	private LayoutInflater inflater = null;

	public SearchFriendListAdapter(Context context) {
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	public void addList(List<SearchFriendBean> addlist) {
		list.clear();
		this.list = addlist;
	}

	public List<SearchFriendBean> getAll() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == holder) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.friend_search_list_item,
					null);
			holder.tag = (TextView) convertView
					.findViewById(R.id.search_list_item_tag);
			holder.headImg = (CircleImageview) convertView
					.findViewById(R.id.headimg);
			holder.name = (TextView) convertView
					.findViewById(R.id.friend_search_name);
			holder.addBtn = (LinearLayout) convertView
					.findViewById(R.id.add_btn);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		return convertView;
	}

	class Holder {
		public TextView tag;
		public CircleImageview headImg;
		public TextView name;
		public LinearLayout addBtn;
	}
}