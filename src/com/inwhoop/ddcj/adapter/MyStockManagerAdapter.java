package com.inwhoop.ddcj.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.LoginActivity;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.impl.DetelStockLinsener;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressLint({ "UseSparseArrays", "HandlerLeak" })
public class MyStockManagerAdapter extends ArrayAdapter<StockInfoBean> {

	private Context mContext;
	private List<StockInfoBean> objects;
	private HashMap<Integer, Boolean> isAdd = new HashMap<Integer, Boolean>();
	private List<Boolean> isCheckedList = new ArrayList<Boolean>();
	private List<StockInfoBean> deleteList = new ArrayList<StockInfoBean>();
	private List<StockInfoBean> list = new ArrayList<StockInfoBean>();
	private DetelStockLinsener detelStockLinsener;

	public MyStockManagerAdapter(Context context, int resource,
			int textViewResourceId, List<StockInfoBean> objects) {
		super(context, resource, textViewResourceId, objects);
		this.objects = objects;
		mContext = context;
		init();
		initChecked(objects);
	}

	private void init() {
		for (int i = 0; i < objects.size(); i++) {
			isAdd.put(i, false);
		}
	}

	public void initChecked(List<StockInfoBean> list) {
		isCheckedList.clear();
		for (int i = 0; i < list.size(); i++) {
			isCheckedList.add(false);
		}
	}

	public void setData(List<StockInfoBean> list) {
		this.list = list;
	}

	public void setDetelStockLinsener(DetelStockLinsener detelStockLinsener) {
		this.detelStockLinsener = detelStockLinsener;
	}

	public List<Boolean> getisCheckedList() {
		return isCheckedList;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		StockInfoBean stock = getItem(position);
		if (null == convertView) {
			viewHolder = new ViewHolder();
			convertView = super.getView(position, convertView, parent);
			viewHolder.title = (TextView) convertView.findViewById(R.id.title);
			viewHolder.num = (TextView) convertView.findViewById(R.id.num);
			viewHolder.addView = (ImageView) convertView
					.findViewById(R.id.click_add);
			viewHolder.cheView = (CheckBox) convertView
					.findViewById(R.id.chechbox);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		viewHolder.title.setText(stock.stockname);
		viewHolder.num.setText(stock.stockcode);
		viewHolder.cheView.setChecked(isCheckedList.get(position));
		if (objects.get(position).notice == 0) {
			viewHolder.addView.setBackgroundResource(R.drawable.ico_track);
			isAdd.put(position, false);
		} else {
			viewHolder.addView
					.setBackgroundResource(R.drawable.ico_track_checked);
			isAdd.put(position, true);
		}

		viewHolder.cheView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isCheckedList.get(position)) {
					isCheckedList.remove(position);
					isCheckedList.add(position, false);
					for (int i = 0; i < deleteList.size(); i++) {
						if (deleteList.get(i).stockcode.equals(list
								.get(position).stockcode)) {
							deleteList.remove(deleteList.get(i));
							// for循环是先根据中间的值判断是否为true，然后再执行后面的i++
							i--;
						}
					}
				} else {
					isCheckedList.remove(position);
					isCheckedList.add(position, true);
					deleteList.add(list.get(position));
				}
				detelStockLinsener.detelStock(deleteList);
			}
		});
		viewHolder.addView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
                if (TextUtils.isEmpty(UserInfoUtil.getUserInfo(mContext).userid)) {
                    Toast.makeText(mContext, "请登录...", Toast.LENGTH_SHORT)
							.show();
                    Act.toAct(mContext, LoginActivity.class);
				} else {
					if (!isAdd.get(position)) {

						new Thread(new Runnable() {

							@Override
							public void run() {
								try {
									Message msg = handler.obtainMessage();
									Object[] mObjects = JsonUtils.addFocusStock(
											UserInfoUtil.getUserInfo(mContext).userid,
											objects.get(position).stockcode);
									if ((Boolean) mObjects[0]) {
										msg.what = 1;
										msg.obj = viewHolder.addView;
										isAdd.put(position, true);
										objects.get(position).notice = 1;
										handler.sendMessage(msg);
									} else {
										msg.what = 2;
										msg.obj = mObjects[1];
										handler.sendMessage(msg);
									}
								} catch (Exception e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						}).start();

					} else {

						new Thread(new Runnable() {

							@Override
							public void run() {
								try {
									Message msg = handler.obtainMessage();
									Object[] mObjects = JsonUtils.delFoucsStock(
											UserInfoUtil.getUserInfo(mContext).userid,
											objects.get(position).stockcode);
									if ((Boolean) mObjects[0]) {
										msg.what = 3;
										msg.obj = viewHolder.addView;
										isAdd.put(position, false);
										objects.get(position).notice = 0;
										handler.sendMessage(msg);
									} else {
										msg.what = 4;
										msg.obj = mObjects[1];
										handler.sendMessage(msg);
									}
								} catch (Exception e) {
									e.printStackTrace();
								}

							}
						}).start();

					}

				}

			}
		});

		return convertView;

	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case 1:
				Toast.makeText(mContext, "追踪成功...", Toast.LENGTH_SHORT).show();
				((ImageView) msg.obj)
						.setBackgroundResource(R.drawable.ico_track_checked);
				break;
			case 2:
				Toast.makeText(mContext, (String) msg.obj, Toast.LENGTH_SHORT)
						.show();
				break;
			case 3:
				Toast.makeText(mContext, "取消追踪...", Toast.LENGTH_SHORT).show();
				((ImageView) msg.obj)
						.setBackgroundResource(R.drawable.ico_track);
				break;
			case 4:
				Toast.makeText(mContext, (String) msg.obj, Toast.LENGTH_SHORT)
						.show();
				break;
			default:
				break;
			}
		};
	};

	private class ViewHolder {
		private TextView title;
		public TextView num;
		private ImageView addView;
		private CheckBox cheView;
	}
}
