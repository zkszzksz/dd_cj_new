package com.inwhoop.ddcj.adapter;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;

import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ChannelList;
import com.inwhoop.ddcj.util.BitmapManager;
import com.inwhoop.ddcj.util.TimeUtils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 频道资讯适配器
 * 
 * @Project: DDCJ
 * @Title: ChannelAdapter.java
 * @Package com.inwhoop.ddcj.adapter
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-20 下午2:32:08
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class ChannelAdapter extends BaseAdapter {
	private Context context;
	private List<ChannelList> dataLists;
	// private KJBitmap kjb;
	// private FinalBitmap fb;
	private int mPosition;

	public ChannelAdapter(Context context) {
		this.context = context;
		// kjb = KJBitmap.create();
		// fb = FinalBitmap.create(context);
		// fb.configLoadingImage(R.drawable.qingbao_loading_img);
		// fb.configLoadfailImage(R.drawable.qingbao_loading_img);
		dataLists = new ArrayList<ChannelList>();
	}

	public void addChannelList(List<ChannelList> data, boolean isLoadMore) {
		if (!isLoadMore) {
			dataLists.clear();
		}
		for (int i = 0; i < data.size(); i++) {
			dataLists.add(data.get(i));
		}
	}

	public void setPosition(int position) {
		mPosition = position;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return dataLists.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return dataLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (null == convertView) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.channel_list_item, null);
			viewHolder.img = (ImageView) convertView.findViewById(R.id.image);
			viewHolder.title = (TextView) convertView.findViewById(R.id.title);
			viewHolder.dec = (TextView) convertView.findViewById(R.id.dec_text);
			viewHolder.time = (TextView) convertView
					.findViewById(R.id.time_text);
			viewHolder.category = (TextView) convertView
					.findViewById(R.id.category_text);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (position < dataLists.size()) {
			if (dataLists.get(position).thumbnail.startsWith("http://")) {
				BitmapManager.INSTANCE.loadBitmap(
						dataLists.get(position).thumbnail, viewHolder.img,
						R.drawable.qingbao_loading_img, true);
				// fb.display(viewHolder.img,
				// dataLists.get(position).thumbnail);
			} else {
				BitmapManager.INSTANCE.loadBitmap(
						Configs.HOST + dataLists.get(position).thumbnail,
						viewHolder.img, R.drawable.qingbao_loading_img, true);
				// fb.display(viewHolder.img,
				// Configs.HOST + dataLists.get(position).thumbnail);
			}
			viewHolder.title.setText(dataLists.get(position).title);
			viewHolder.dec.setText(dataLists.get(position).des);
			viewHolder.time.setText(TimeUtils.twoDateDistanceToday(
					dataLists.get(position).createtime,
					TimeUtils.getStrTime(System.currentTimeMillis())));
			if (mPosition == 0) {
				viewHolder.category.setText("["
						+ dataLists.get(position).channelname + "]");
			} else {
				if (dataLists.get(position).top == 1) {
					viewHolder.category.setText("[" + "置顶" + "]");
				} else {
					viewHolder.category.setText("");
				}

			}

		}
		return convertView;
	}

	private class ViewHolder {
		private TextView title;
		public TextView dec;
		private TextView time;
		private TextView category;
		private ImageView img;

	}
}
