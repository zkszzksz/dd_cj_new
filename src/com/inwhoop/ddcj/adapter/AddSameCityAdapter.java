package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.impl.AddFriendLinsener;
import com.inwhoop.ddcj.view.CircleImageview;

import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: 雷达添加同城好友适配器
 * @Package com.inwhoop.ddcj.adapter
 * @Date: 2014/12/18 16:20
 * @version: 1.0
 */
public class AddSameCityAdapter extends BaseAdapter {
    private Context context;
    private List<UserBean> dataLists = new ArrayList<UserBean>();
    private FinalBitmap fb;
    private AddFriendLinsener addFriendLinsener;

    public AddSameCityAdapter(Context mContext) {
        this.context = mContext;
        fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.home_head);
		fb.configLoadfailImage(R.drawable.home_head);
        dataLists = new ArrayList<UserBean>();
    }

    public void add(List<UserBean> data) {
        for (int i = 0; i < data.size(); i++) {
            dataLists.add(data.get(i));
        }
        notifyDataSetChanged();
    }

    public void add(UserBean data) {
        dataLists.add(data);
    }

    public List<UserBean> getlist() {
        return dataLists;
    }

    @Override
    public int getCount() {
        return dataLists.size();
    }

    @Override
    public Object getItem(int position) {
        return dataLists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        if (null == convertView) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(context).inflate(
                    R.layout.add_same_city_list_item, null);
            viewHolder.head_img = (CircleImageview) convertView
                    .findViewById(R.id.item_img);
            viewHolder.name = (TextView) convertView
                    .findViewById(R.id.item_name);
            viewHolder.location = (TextView) convertView
                    .findViewById(R.id.item_location);
            viewHolder.addBtn = (TextView) convertView
                    .findViewById(R.id.tv_add_guanzhu);

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (position < dataLists.size()) {
            final UserBean bean = dataLists.get(position);
            if (bean.isattention.equals("yes")) {
                viewHolder.addBtn.setText("已关注");
                viewHolder.addBtn.setEnabled(false);
                viewHolder.addBtn.setTextColor(context.getResources().getColor(R.color.tab_text_color));
            } else {
                viewHolder.addBtn.setText("+关注");
                viewHolder.addBtn.setTextColor(context.getResources().getColor(R.color.blue));
            }
            viewHolder.name.setText("" + bean.name);
            viewHolder.location.setText("" + bean.address);
            fb.display(viewHolder.head_img, bean.img);

            viewHolder.addBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addFriendLinsener.addFriend(bean, position);
                }
            });
        }
        return convertView;
    }

    public void setFollowLinsener(AddFriendLinsener addFriendLinsener) {
        this.addFriendLinsener = addFriendLinsener;
    }

    private class ViewHolder {
        private CircleImageview head_img;
        private TextView name;
        private TextView location;
        private TextView addBtn;
    }

}
