package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ContactsInfo;
import com.inwhoop.ddcj.bean.FansBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.TimeUtils;
import com.inwhoop.ddcj.view.CircleImageview;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @Project: DDCJ
 * @Title: SearchAllItemUserlistAdapter.java
 * @Package com.inwhoop.ddcj.adapter
 * @Description: TODO
 *
 * @author dyong199046@163.com 代勇
 * @date 2015-1-30 下午4:01:04
 * @Copyright: 2015 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class SearchAllItemUserlistAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    List<UserBean> mData = new ArrayList<UserBean>();
    private FinalBitmap fb;


    public SearchAllItemUserlistAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        fb=FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.home_head);
        fb.configLoadfailImage(R.drawable.home_head);
    }

    public void addList(List<UserBean> addlist) {
        for (int i = 0; i < addlist.size(); i++) {
        	mData.add(addlist.get(i));
		}
    }

    public List<UserBean> getList() {
        return mData;
    }

    @Override
    public int getCount() {
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unused")
	@Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (null == holder) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(
                    R.layout.seacher_user_list_item, null);
            holder.head = (CircleImageview) convertView
                    .findViewById(R.id.pheadimg);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (null != mData && mData.size() > 0 && position < mData.size()) {
            UserBean bean = mData.get(position);
            if (null != bean) {
                fb.display(holder.head,mData.get(position).img);
                holder.name.setText(mData.get(position).name);
            }
        }
        return convertView;
    }


    private class ViewHolder {
        CircleImageview head;
        TextView name;
    }
}
