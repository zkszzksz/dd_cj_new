package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.SearchStock;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.StockUtil;
import com.inwhoop.ddcj.util.SysPrintUtil;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.List;

/**
 * Created by Administrator on 2014/10/28.
 */
public class SearchListAdapter extends BaseAdapter {

	private List<SearchStock> list = null;

	private Context context = null;

	private LayoutInflater inflater = null;
	private String address = "";
	private UserBean userBean;

	public SearchListAdapter(Context context, List<SearchStock> list) {
		this.context = context;
		this.list = list;

		userBean = UserInfoUtil.getUserInfo(context);
		inflater = LayoutInflater.from(context);
	}

	public void add(List<SearchStock> addlist, String address) {
		list = addlist;
		this.address = address;
	}

	public List<SearchStock> getAll() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == holder) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.search_list_item, null);
			holder.stockName = (TextView) convertView
					.findViewById(R.id.search_list_item_name);
			holder.stockNum = (TextView) convertView
					.findViewById(R.id.search_list_item_num);
			holder.stockPrompt = (TextView) convertView
					.findViewById(R.id.search_list_item_tishi);
			holder.stockAdd = (ImageView) convertView
					.findViewById(R.id.search_list_item_add);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		if (null != list && list.size() > 0) {
			SysPrintUtil.pt("股票代码为", list.get(position).stockname);
			holder.stockName.setText(list.get(position).stockname);
			holder.stockNum.setText(list.get(position).stockcode);
			if (list.get(position).focus.equals("1")) {
				holder.stockPrompt.setVisibility(View.VISIBLE);
				holder.stockAdd.setBackgroundResource(R.drawable.btn_minus);
			} else {
				holder.stockPrompt.setVisibility(View.INVISIBLE);
				holder.stockAdd.setBackgroundResource(R.drawable.btn_plus);
			}
			final Holder finalHolder = holder;
			holder.stockAdd.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View view) {
					SysPrintUtil.pt("是否添加", list.get(position).focus);
					if (list.get(position).focus.equals("1")) {
						setStockData("1", list.get(position).stockcode,
								list.get(position).stockname, finalHolder,
								position);
					} else {
						setStockData("0", list.get(position).stockcode,
								list.get(position).stockname, finalHolder,
								position);
					}
				}
			});
		}

		return convertView;
	}

	class Holder {
		public TextView stockName;
		public TextView stockNum;
		public TextView stockPrompt;
		public ImageView stockAdd;
	}

	public void setStockData(final String type, final String stockcode,
			final String stockname, Holder holder, int position) {
		StockInfoBean bean = new StockInfoBean();
		bean.stockcode = stockcode;
		bean.stockname = stockname;
		bean.position = address;
		if (type.equals("1")) {
			StockUtil.deleteStockInfoToDB(bean, mmHandler, context,
					holder.stockPrompt, holder.stockAdd, "1", position);
		} else if (type.equals("0")) {
			StockUtil.addStockInfoToDB(bean, mmHandler, context,
					holder.stockPrompt, holder.stockAdd, "0", position);
		}
	}

	public Handler mmHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			Object[] objects = (Object[]) msg.obj;
			SysPrintUtil.pt("结果为+++==11", (Boolean) objects[0] + "");
			TextView textView = (TextView) objects[2];
			ImageView imageView = (ImageView) objects[3];
			String type = (String) objects[4];
			int position = (Integer) objects[5];
			if ((Boolean) objects[0] && type.equals("1")) { // (Boolean)
															// objects[0] &&
				list.get(position).focus = "0";
				textView.setVisibility(View.GONE);
				imageView.setBackgroundResource(R.drawable.btn_plus);
			} else if ((Boolean) objects[0] && type.equals("0")) {
				list.get(position).focus = "1";
				textView.setVisibility(View.VISIBLE);
				imageView.setBackgroundResource(R.drawable.btn_minus);
			}
			// notifyDataSetChanged();
		}
	};
}
