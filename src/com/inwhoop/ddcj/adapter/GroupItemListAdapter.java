package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ContactsInfo;
import com.inwhoop.ddcj.bean.FansBean;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.TimeUtils;
import com.inwhoop.ddcj.view.CircleImageview;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/13.
 */
public class GroupItemListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    List<FansBean> mData = new ArrayList<FansBean>();
    private FinalBitmap fb;


    public GroupItemListAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        fb=FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.home_head);
        fb.configLoadfailImage(R.drawable.home_head);
//        System.out.println("--------------适配器" + this.mData);
    }

    public void addList(List<FansBean> mData) {
            this.mData = mData;
    }

    public List<FansBean> getList() {
        return mData;
    }

    @Override
    public int getCount() {
        // TODO Auto-ContactsInfo method stub
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder = null;
        if (null == holder) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(
                    R.layout.contacts_friends_list_item_layout, null);
            holder.civ_headView = (CircleImageview) convertView
                    .findViewById(R.id.headimg);
            holder.tv_name = (TextView) convertView.findViewById(R.id.nametv);
            holder.tv_info = (TextView) convertView.findViewById(R.id.content);
            holder.tv_otherInfo = (TextView) convertView
                    .findViewById(R.id.tv_other_info);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        if (null != mData && mData.size() > 0 && position < mData.size()) {
            FansBean bean = mData.get(position);
            if (null != bean) {
                fb.display(holder.civ_headView,mData.get(position).img);
                holder.tv_name.setText(mData.get(position).name);
                holder.tv_info.setText(mData.get(position).des);
                holder.tv_otherInfo.setText(mData.get(position).location
                        + "|"
                        + TimeUtils.twoDateDistance(TimeRender.getStandardDate(Long
                                .valueOf(mData.get(position).lastTime) * 1000),
                        TimeRender.getStandardDate())); // +
            /**/
            }
        }
        // mData.get(position).state
        return convertView;
    }


    private class ViewHolder {
        CircleImageview civ_headView;
        TextView tv_name;
        TextView tv_info;
        TextView tv_otherInfo;
    }
}
