package com.inwhoop.ddcj.adapter;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;


import com.inwhoop.R;
import com.inwhoop.ddcj.activity.PersonInfoActivity;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CommentInfo;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.TimeUtils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class CommentInfoAdapter extends BaseAdapter {
	private Context context;
	private List<CommentInfo> commentInfos;
	private FinalBitmap fb;
	private Bitmap bitmap;

	public CommentInfoAdapter(Context context) {
		this.context = context;
		commentInfos = new ArrayList<CommentInfo>();
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.pic_portrait);
		fb.configLoadfailImage(R.drawable.pic_portrait);
	}

	public void addChannelList(List<CommentInfo> data, boolean isLoadMore) {
		if (!isLoadMore) {
			commentInfos.clear();
		}
		for (int i = 0; i < data.size(); i++) {
			commentInfos.add(data.get(i));
		}
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return commentInfos.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return commentInfos.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (null == convertView) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.channel_comment_item, null);
			viewHolder.img = (ImageView) convertView.findViewById(R.id.image);
			viewHolder.title = (TextView) convertView.findViewById(R.id.text);
			viewHolder.content = (TextView) convertView
					.findViewById(R.id.content);
			viewHolder.time = (TextView) convertView.findViewById(R.id.time);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}

		if (position < commentInfos.size()
				&& null != commentInfos.get(position)) {
			convertView.setVisibility(View.VISIBLE);
			fb.display(viewHolder.img, commentInfos.get(position).img);
			viewHolder.title.setText(commentInfos.get(position).name + "-"
					+ commentInfos.get(position).location);
			viewHolder.content.setText(commentInfos.get(position).content);
			viewHolder.time.setText(TimeUtils.twoDateDistance(
					commentInfos.get(position).createtime,
					TimeUtils.getStrTime(System.currentTimeMillis())));
			viewHolder.img.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Bundle bundle = new Bundle();
					bundle.putString("userid",
							commentInfos.get(position).userid);
					Act.toAct(context, PersonInfoActivity.class, bundle);
				}
			});
		}
		return convertView;
	}

	private class ViewHolder {
		private TextView title;
		public TextView content;
		private TextView time;
		private ImageView img;

	}

}
