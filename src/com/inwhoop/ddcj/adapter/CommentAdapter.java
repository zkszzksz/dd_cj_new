package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.PersonInfoActivity;
import com.inwhoop.ddcj.bean.CommentBean;
import com.inwhoop.ddcj.view.CircleImageview;

import net.tsz.afinal.FinalBitmap;


import java.util.ArrayList;
import java.util.List;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: 评论列表适配器 。目前有邀请函里的评论列表用到
 * @Package com.inwhoop.ddcj.adapter
 * @Date: 2014/11/27 17:28
 * @version: 1.0
 */
public class CommentAdapter extends BaseAdapter {
	private Context context;
	private List<CommentBean> dataLists;
	private FinalBitmap fb;

	public CommentAdapter(Context mContext) {
		this.context = mContext;
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.home_head);
		fb.configLoadfailImage(R.drawable.home_head);
		dataLists = new ArrayList<CommentBean>();
	}

	public void add(List<CommentBean> data) {
		for (int i = 0; i < data.size(); i++) {
			dataLists.add(data.get(i));
		}
		notifyDataSetChanged();
	}

	public void add(CommentBean data) {
		List<CommentBean> newlist = new ArrayList<CommentBean>();
		newlist.add(data);
		for (int i = 0; i < dataLists.size(); i++) {
			newlist.add(dataLists.get(i));
		}
		dataLists.clear();
		dataLists = newlist;
	}

	public List<CommentBean> getlist() {
		return dataLists;
	}

	@Override
	public int getCount() {
		return dataLists.size();
	}

	@Override
	public Object getItem(int position) {
		return dataLists.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (null == convertView) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.comment_list_item, null);
			viewHolder.head_img = (CircleImageview) convertView
					.findViewById(R.id.item_img);
			viewHolder.name = (TextView) convertView
					.findViewById(R.id.item_name);
			viewHolder.time = (TextView) convertView
					.findViewById(R.id.item_time);
			viewHolder.content = (TextView) convertView
					.findViewById(R.id.item_content);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (position < dataLists.size()) {
			viewHolder.name.setText("" + dataLists.get(position).name);
			viewHolder.time.setText("" + dataLists.get(position).createtime);
			viewHolder.content.setText("" + dataLists.get(position).content);
			fb.display(viewHolder.head_img, dataLists.get(position).img);
			viewHolder.head_img.setTag(position);
			viewHolder.head_img.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					int pos = (Integer) v.getTag();
					Intent intent = new Intent(context, PersonInfoActivity.class);
					intent.putExtra("userid", dataLists.get(pos).userid);
					context.startActivity(intent);
				}
			});
		}
		return convertView;
	}

	private class ViewHolder {
		private CircleImageview head_img;
		private TextView name;
		public TextView time;
		private TextView content;

	}
}
