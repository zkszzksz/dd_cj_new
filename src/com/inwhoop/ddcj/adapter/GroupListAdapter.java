package com.inwhoop.ddcj.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.ScreenSizeUtil;
import com.inwhoop.ddcj.util.UserInfoUtil;

import net.tsz.afinal.FinalBitmap;


import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2014/12/29.
 */
@SuppressLint("Instantiatable")
public class GroupListAdapter extends BaseAdapter {
	private int imgW;
	private Context context;
	private List<CateChannel> myList = new ArrayList<CateChannel>();
	private FinalBitmap fb;
	private UserBean userBean;

	public GroupListAdapter(Context mContext) {
		this.context = mContext;
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.qingbao_loading_img);
		fb.configLoadfailImage(R.drawable.qingbao_loading_img);
		userBean = UserInfoUtil.getUserInfo(context);
		imgW = ScreenSizeUtil.getScreenSize(mContext).screenW * 94 / 388; // 388：94
	}

	public int getCount() {
		return myList.size();
	}

	public CateChannel getItem(int position) {
		return myList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			holder = new Holder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.item_my_setting_group, null);
			holder.head_img = (ImageView) convertView
					.findViewById(R.id.item_head_img);
			holder.head_img.setLayoutParams(new LinearLayout.LayoutParams(imgW,
					imgW));

			holder.title = (TextView) convertView.findViewById(R.id.item_title);
			holder.isAdmin = (TextView) convertView
					.findViewById(R.id.item_is_head);
			holder.groupName = (TextView) convertView
					.findViewById(R.id.item_group_head);
			holder.newsNum = (TextView) convertView
					.findViewById(R.id.item_qingbao_num);
			holder.groupNum = (TextView) convertView
					.findViewById(R.id.item_group_num);
			holder.fansNum = (TextView) convertView
					.findViewById(R.id.item_fensi_num);
			holder.item_num = (TextView) convertView
					.findViewById(R.id.item_num);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		if (null != myList && myList.size() > 0) {
			if (myList.get(position).img.startsWith("http://")) {
				fb.display(holder.head_img, myList.get(position).img);
			} else {
				fb.display(holder.head_img,
						Configs.HOST + myList.get(position).img);
			}
			holder.title.setText(myList.get(position).name);
			holder.groupName.setText(myList.get(position).groupmanager);
			holder.newsNum.setText(myList.get(position).newscount);
			holder.groupNum.setText(myList.get(position).groupnum);
			holder.fansNum.setText(myList.get(position).fanscount);
			holder.item_num.setText(myList.get(position).des);
			if (userBean.ddid.equals(myList.get(position).adminid)) {
				holder.isAdmin.setVisibility(View.VISIBLE);
			} else {
				holder.isAdmin.setVisibility(View.GONE);
			}
		}

		// final GroupBean myListItem = myList.get(position);

		// holder.name.setText(myListItem.getName());
		return convertView;
	}

	public void add(List<CateChannel> groupBean) {
		this.myList = groupBean;
	}

	class Holder {
		ImageView head_img;
		TextView title;
		public TextView item_num;
		public TextView groupName;
		public TextView newsNum;
		public TextView groupNum;
		public TextView fansNum;
		public TextView isAdmin;
	}
}