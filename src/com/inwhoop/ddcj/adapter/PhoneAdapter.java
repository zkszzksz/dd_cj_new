package com.inwhoop.ddcj.adapter;

import java.util.List;

import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.PhoneInfo;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * 本地通讯录适配器
 * 
 * @Project: Dream
 * @Title: MailAdapter.java
 * @Package com.inwhoop.dream.adapter
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-9-14 下午12:59:21
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class PhoneAdapter extends BaseAdapter {
	private Context context;
	private List<PhoneInfo> allflist;
	private String flag[];
	public ProgressDialog progressDialog;// 加载进度框
	private String uid;

	public PhoneAdapter(Context context, List<PhoneInfo> allflist) {
		this.context = context;
		this.allflist = allflist;
		flag = context.getResources().getStringArray(R.array.relation);
		uid = UserInfoUtil.getUserInfo(context).userid;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return allflist.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return allflist.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (null == convertView) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.phone_list_item, null);
			viewHolder.name = (TextView) convertView.findViewById(R.id.name);
			viewHolder.handle = (TextView) convertView
					.findViewById(R.id.handle);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (position < allflist.size()) {
			viewHolder.name.setText(allflist.get(position).name);
			int index = allflist.get(position).isGf;
			if (index < flag.length) {
				setText(index, viewHolder.handle, position);
			}
		}
		return convertView;
	}

	private void setText(int index, TextView textView, int position) {
		switch (index) {
		case 0:
			textView.setTextColor(0xff2f618e);
			sendMessage(textView, position);
			break;

		case 1:
			textView.setTextColor(0xff888888);
			break;
		case 2:
			textView.setTextColor(0xffd70c19);
			addFriendView(textView, position);
			break;

		}
		textView.setText(flag[index]);
	}

	/**
	 * 添加好友
	 * 
	 * @Title: addFriend
	 * @Description: TODO
	 * @param @param view
	 * @param @param position
	 * @return void
	 */
	private void addFriendView(final TextView view, final int position) {
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				new AlertDialog.Builder(context)
						.setTitle("关注好友")
						.setMessage("是否关注" + allflist.get(position).name)
						.setNegativeButton("关注",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
										addFriend(view, position);
									}

								})
						.setPositiveButton("取消",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								}).show();

			}
		});
	}

	private void addFriend(final TextView view, final int position) {
		showProgressDialog("正在添加...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					Message msg = handler.obtainMessage();
					String uid = UserInfoUtil.getUserInfo(context).userid;
					Object data[] = JsonUtils.attentionUser(uid,
							allflist.get(position).id, "通讯录");
					if ((Boolean) data[0]) {
						msg.what = Configs.READ_SUCCESS;
						msg.obj = view;
						handler.sendMessage(msg);
					} else {
						handler.sendEmptyMessage(Configs.READ_FAIL);
					}
				} catch (Exception e) {
					handler.sendEmptyMessage(Configs.READ_FAIL);
					e.printStackTrace();
				}

			}
		}).start();

	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				((TextView) (msg.obj)).setTextColor(0xff888888);
				((TextView) (msg.obj)).setText(flag[1]);
				Toast.makeText(context, "添加成功", Toast.LENGTH_SHORT).show();
				break;
			case Configs.READ_FAIL:
				Toast.makeText(context, "添加失败", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	protected void showProgressDialog(String msg) {
		if (null == progressDialog) {
			progressDialog = new ProgressDialog(context);
		}
		progressDialog.setMessage(msg);
		progressDialog.setCancelable(true);
		progressDialog.show();
	}

	protected void dismissProgressDialog() {
		if (null != progressDialog && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

	/**
	 * 发送邀请短信
	 * 
	 * @Title: sendMessage
	 * @Description: TODO
	 * @param @param view
	 * @param @param position
	 * @return void
	 */
	private void sendMessage(TextView view, final int position) {
		view.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Uri uri = Uri.parse("smsto://" + allflist.get(position).phone);
				Intent intent = new Intent(Intent.ACTION_SENDTO, uri);
				intent.putExtra("sms_body", "大大财经");
				context.startActivity(intent);

			}
		});
	}

	class ViewHolder {
		public TextView name;
		public TextView handle;
	}
}
