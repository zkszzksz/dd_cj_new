package com.inwhoop.ddcj.adapter;

import java.util.List;

import com.inwhoop.R;
import com.inwhoop.ddcj.bean.InvitationListInfo;
import com.inwhoop.ddcj.util.TimeRender;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 邀请函
 * 
 * @Project: DDCJ
 * @Title: InvitationListAdapter.java
 * @Package com.inwhoop.ddcj.adapter
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-11-26 下午7:23:37
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class InvitationListAdapter extends BaseAdapter {
	private List<InvitationListInfo> list;
	private LayoutInflater inflater;

	public InvitationListAdapter(Context context, List<InvitationListInfo> list) {
		this.list = list;
		inflater = LayoutInflater.from(context);
	}

	public void addChannelList(List<InvitationListInfo> data) {
		for (int i = 0; i < data.size(); i++) {
			list.add(data.get(i));
		}
	}

	public List<InvitationListInfo> getAll() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder viewHolder;
		if (null == convertView) {
			viewHolder = new ViewHolder();
			convertView = inflater.inflate(
					R.layout.invitation_list_item_layout, null);
			viewHolder.time = (TextView) convertView.findViewById(R.id.time);
			viewHolder.nowday = (TextView) convertView
					.findViewById(R.id.nowday);
			viewHolder.week = (TextView) convertView.findViewById(R.id.week);
			viewHolder.hourmin = (TextView) convertView
					.findViewById(R.id.hourmin);
			viewHolder.name = (TextView) convertView.findViewById(R.id.name);
			viewHolder.username = (TextView) convertView
					.findViewById(R.id.username);
			viewHolder.location = (TextView) convertView
					.findViewById(R.id.address);
			viewHolder.title = (LinearLayout) convertView
					.findViewById(R.id.titlelayout);
			viewHolder.line = convertView.findViewById(R.id.line);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		if (position >= 1) {
			if (list.get(position).time.equals(list.get(position - 1).time)) {
				viewHolder.title.setVisibility(View.GONE);
			} else {
				viewHolder.title.setVisibility(View.VISIBLE);
				viewHolder.time.setText(list.get(position).time);
				if (list.get(position).time.equals(TimeRender
						.getStandardDateriqiBylongtime(System
								.currentTimeMillis()))) {
					viewHolder.nowday.setVisibility(View.VISIBLE);
				} else {
					viewHolder.nowday.setVisibility(View.GONE);
				}
				viewHolder.week.setText(list.get(position).week);
			}
		} else {
			viewHolder.title.setVisibility(View.VISIBLE);
			viewHolder.time.setText(list.get(position).time);
			if (list.get(position).time.equals(TimeRender
					.getStandardDateriqiBylongtime(System.currentTimeMillis()))) {
				viewHolder.nowday.setVisibility(View.VISIBLE);
			} else {
				viewHolder.nowday.setVisibility(View.GONE);
			}
			viewHolder.week.setText(list.get(position).week);
		}
		if (position == (list.size() - 1)) {
			viewHolder.line.setVisibility(View.VISIBLE);
		} else {
			if (list.get(position).time.equals(list.get(position + 1).time)) {
				viewHolder.line.setVisibility(View.VISIBLE);
			} else {
				viewHolder.line.setVisibility(View.GONE);
			}
		}
		viewHolder.hourmin.setText(list.get(position).hourmin);
		viewHolder.name.setText(list.get(position).title);
		viewHolder.username.setText(list.get(position).name);
		viewHolder.location.setText(list.get(position).location);
		return convertView;
	}

	class ViewHolder {
		LinearLayout title;
		TextView time;
		TextView nowday;
		TextView week;
		TextView hourmin;
		TextView name;
		TextView username;
		TextView location;
		View line;
	}
}
