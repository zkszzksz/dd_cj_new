package com.inwhoop.ddcj.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.tsz.afinal.FinalBitmap;


import com.inwhoop.R;
import com.inwhoop.ddcj.bean.OrderChannel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * 频道订阅适配器
 * 
 * @Project: DDCJ
 * @Title: OrderChannelAdapter.java
 * @Package com.inwhoop.ddcj.adapter
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-6 下午7:30:24
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class OrderChannelAdapter extends BaseAdapter {
	private Context context;
	private List<OrderChannel> list = new ArrayList<OrderChannel>();
	private Map<Integer, CheckBox> map = new HashMap<Integer, CheckBox>();
	private FinalBitmap fb;

	public OrderChannelAdapter(Context context, List<OrderChannel> list) {
		this.context = context;
		this.list = list;
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.qingbao_loading_img);
		fb.configLoadfailImage(R.drawable.qingbao_loading_img);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final ViewHolder viewHolder;
		if (null == convertView) {
			viewHolder = new ViewHolder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.order_channel_item, null);
			viewHolder.title = (TextView) convertView.findViewById(R.id.title);
			viewHolder.checkBox = (CheckBox) convertView
					.findViewById(R.id.m_checkbox);
			viewHolder.imageView = (ImageView) convertView
					.findViewById(R.id.image);
			convertView.setTag(viewHolder);
		} else {
			viewHolder = (ViewHolder) convertView.getTag();
		}
		map.put(position, viewHolder.checkBox);
		viewHolder.title.setText(list.get(position).title);
		fb.display(viewHolder.imageView, list.get(position).imgUrl);
		return convertView;
	}

	public List<String> checkeChecked() {
		List<String> delList = new ArrayList<String>();
		for (Map.Entry<Integer, CheckBox> entry : map.entrySet()) {
			CheckBox checkBox = entry.getValue();
			if (checkBox.isChecked()) {
				int postition = entry.getKey();
				map.put(postition, checkBox);
				delList.add(list.get(postition).id);
			}
		}
		return delList;
	}

	private class ViewHolder {
		private TextView title;
		private CheckBox checkBox;
		private ImageView imageView;

	}
}
