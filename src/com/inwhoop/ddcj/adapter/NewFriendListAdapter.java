package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.SearchFriendBean;

import java.util.ArrayList;
import java.util.List;

/**
 * package_name: com.inwhoop.ddcj.adapter Created with IntelliJ IDEA User:
 * Administrator Date: 2014/12/9 Time: 17:44
 */
public class NewFriendListAdapter extends BaseAdapter {

	private List<SearchFriendBean> list = new ArrayList<SearchFriendBean>();

	private Context context = null;

	private LayoutInflater inflater = null;

	public NewFriendListAdapter(Context context) {
		this.context = context;
		inflater = LayoutInflater.from(context);
	}

	public void addList(List<SearchFriendBean> addlist) {
		list.clear();
		this.list = addlist;
	}

	public List<SearchFriendBean> getAll() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == holder) {
			holder = new Holder();
			convertView = inflater.inflate(R.layout.new_friend_list_item, null);
			holder.name = (TextView) convertView
					.findViewById(R.id.new_friend_list_item_name);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		if (null != list && list.size() > 0) {
			holder.name.setText(list.get(position).name);
		}

		return convertView;
	}

	class Holder {
		public TextView name;
	}
}