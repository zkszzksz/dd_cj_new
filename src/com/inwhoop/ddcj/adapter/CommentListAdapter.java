package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.PersonInfoActivity;
import com.inwhoop.ddcj.activity.ShowWebImageActivity;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.QingbaoComment;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.TimeUtils;
import com.inwhoop.ddcj.view.CircleImageview;

import net.tsz.afinal.FinalBitmap;


import java.util.List;

/**
 * Created by Administrator on 2014/10/21.
 */
public class CommentListAdapter extends BaseAdapter {

	private List<QingbaoComment> list = null;

	private Context context = null;

	private LayoutInflater inflater = null;

	private FinalBitmap fb = null;
	
	public CommentListAdapter(Context context, List<QingbaoComment> list) {
		this.context = context;
		this.list = list;
		inflater = LayoutInflater.from(context);
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.home_head);
		fb.configLoadfailImage(R.drawable.home_head);
	}

	public void addlist(List<QingbaoComment> addlist) {
		for (int i = 0; i < addlist.size(); i++) {
			list.add(addlist.get(i));
		}
	}

	public List<QingbaoComment> getAll() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressWarnings({ "unused", "unused" })
	@Override
	public View getView(int position, View convertView, ViewGroup arg2) {
		Holder holder = null;
		if (null == holder) {
			holder = new Holder();
			convertView = inflater.inflate(
					R.layout.qingbao_comment_item_layout, null);
			holder.name = (TextView) convertView.findViewById(R.id.name);
			holder.time = (TextView) convertView.findViewById(R.id.time);
			holder.content = (TextView) convertView.findViewById(R.id.content);
			holder.head = (CircleImageview) convertView
					.findViewById(R.id.headimg);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}
		holder.name.setText(list.get(position).name);
		holder.time.setText(""
				+ TimeUtils.twoDateDistance(TimeRender.getStandardDate(list
						.get(position).createtime * 1000), TimeRender
						.getStandardDate()));
		holder.content.setText(list.get(position).content);
		fb.display(holder.head, list.get(position).img);
		holder.head.setTag(position);
		holder.head.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				int pos = (Integer) v.getTag();
				Intent intent = new Intent(context, PersonInfoActivity.class);
				intent.putExtra("userid", list.get(pos).userid);
				context.startActivity(intent);
			}
		});
		return convertView;
	}

	class Holder {
		TextView name, time, content;
		CircleImageview head;
	}
}
