package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ChangeThemeBean;

import net.tsz.afinal.FinalBitmap;


import java.util.List;

/**
 * Created by Administrator on 2014/9/19.
 */
public class ChBgGridAdapter extends BaseAdapter {
	private LayoutInflater inflater;
	private Context context;
	private List<ChangeThemeBean> list = null;
	private int srceenWidth;
	private String id;
	private int mPosition = 0;

	private FinalBitmap imgfb = null;

	public ChBgGridAdapter(Context context, List<ChangeThemeBean> list,
			int SrceenWidth) {
		this.context = context;
		this.list = list;
		this.srceenWidth = SrceenWidth;
		inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		imgfb = FinalBitmap.create(context);
		imgfb.configLoadingImage(R.drawable.qingbao_loading_img);
		imgfb.configLoadfailImage(R.drawable.qingbao_loading_img);
	}

	public int getPosition() {
		return mPosition;
	}

	public int getCount() {
		return list.size();
	}

	public Object getItem(int position) {
		return list.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		convertView = inflater.inflate(R.layout.grid_img_item, null);
		ImageView img = (ImageView) convertView
				.findViewById(R.id.grid_img_item_img);

		RelativeLayout.LayoutParams linear = (RelativeLayout.LayoutParams) img
				.getLayoutParams();
		linear.width = (srceenWidth / 3);
		linear.height = linear.width - 25;
		img.setLayoutParams(linear);

		if (null != list && list.size() > 0) {
			imgfb.display(img, list.get(position).imgs);
		}
		return convertView;
	}

}
