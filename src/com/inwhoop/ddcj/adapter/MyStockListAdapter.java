package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.StockInfoActivity;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.view.HVListView;
import com.inwhoop.ddcj.view.XListView;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO * * * ****** Created by ZK ********
 * @Date: 2014/10/21 11:55
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class MyStockListAdapter extends BaseAdapter {
	// private Typeface textType;
	private HVListView pListview;
	private Context mContext;
	// private KJBitmap kjb;
	private ArrayList<StockInfoBean> list = new ArrayList();
	private boolean isTouch = false;
	private float down_x, up_x;
	private int scPosition = 0;// 记录第几个item
	private int tag;

	public MyStockListAdapter(Context mContext, HVListView listview) {
		this.mContext = mContext;
		this.pListview = listview;
		// textType = Typeface.createFromAsset(mContext.getAssets(),
		// "kai_ti.ttf");

		// kjb = KJBitmap.create();
		// KJBitmap.config.width = 520;
		// KJBitmap.config.height = (int) (520 * 0.45);
		// KJBitmap.config.loadingBitmap = BitmapCreate
		// .bitmapFromResource(mContext.getResources(),
		// R.drawable.ic_launcher,
		// KJBitmap.config.width, KJBitmap.config.height);
		pListview.setOnScrollListener(new XListView.OnXScrollListener() {
			@Override
			public void onXScrolling(View view) {

			}

			@Override
			public void onScrollStateChanged(AbsListView view, int scrollState) {
				// 不滚动时保存当前滚动到的位置
				if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
					// if (currentMenuInfo != null) {
					if (list.size() != 0) {
						scPosition = pListview.getFirstVisiblePosition();
					}
				}
			}

			@Override
			public void onScroll(AbsListView view, int firstVisibleItem,
					int visibleItemCount, int totalItemCount) {
				// 滚动时调用
			}
		});
	}

	// kjb.display(image, "/storage/sdcard0/1.png");

	public void add(List<StockInfoBean> addList) {
		for (StockInfoBean b : addList)
			list.add(b);
		refresh();
	}

	public void setTag(int tag) {
		this.tag = tag;

	}

	public ArrayList<StockInfoBean> getList() {
		return list;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public void notifyDataSetChanged() {
		super.notifyDataSetChanged();
		// 在恢复位置时调用
		pListview.setSelection(scPosition);
	}

	public void refresh() {
		notifyDataSetChanged();
		notifyDataSetInvalidated();
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(final int position, View v, ViewGroup parent) {
		ViewHolder holder = null;
		if (v == null) {
			v = ((LayoutInflater) mContext
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
					.inflate(R.layout.item_my_stock_lisst, null);
			holder = new ViewHolder();
			holder.item_name = (TextView) v.findViewById(R.id.item1);
			holder.item_num = (TextView) v.findViewById(R.id.item_num);

			holder.items = new TextView[] {
					(TextView) v.findViewById(R.id.item2), // 最新 0
					(TextView) v.findViewById(R.id.item3), // 涨跌 1
					(TextView) v.findViewById(R.id.item4), // 振幅 2
					(TextView) v.findViewById(R.id.item5), // 今日开盘价 3
					(TextView) v.findViewById(R.id.item6), // 昨日收盘价 4
					(TextView) v.findViewById(R.id.item7), // 最高价 5
					(TextView) v.findViewById(R.id.item8), // 最低价 6
					(TextView) v.findViewById(R.id.item9), // 成交量 7
					(TextView) v.findViewById(R.id.item10), // 成交额 8
			};
			// holder.item_zuixin.setTypeface(textType);
			// holder.item_zuixin.setTextSize(14);
			// holder.item_zuixin.setTextColor(mContext.getResources().getColor(R.color.black));
			// holder.item_zhangdie.setTypeface(textType);
			// holder.item_zhenfu.setTypeface(textType);
			// holder.item_xx5.setTypeface(textType);
			// holder.item_xx6.setTypeface(textType);
			v.setTag(holder);
		} else {
			holder = (ViewHolder) v.getTag();
		}

		final StockInfoBean b = list.get(position);

		holder.item_name.setText("" + b.stockname);
		holder.item_num.setText("" + b.stockcode);
		holder.items[0].setText("" + b.nowPrice);
		if (b.addPrice > 0)
			holder.items[1].setText("+" + b.addPrice);
		else
			holder.items[1].setText("" + b.addPrice);
		holder.items[2].setText("" + b.swing + "%");
		holder.items[3].setText("" + b.nowDayOP);
		holder.items[4].setText("" + b.yestprice);
		holder.items[5].setText("" + b.highPrice);
		holder.items[6].setText("" + b.lowPrice);
		holder.items[7].setText("" + b.volume + "万");
		holder.items[8].setText("" + b.total + "万");

		if (b.nowPrice == 0)
			for (int i = 0; i < holder.items.length; i++) {
				holder.items[i].setText("---");
			}

		if (b.addRate > 0) {
			setTextColour(holder,
					mContext.getResources().getColor(R.color.red_ff3030));
		} else if (b.addRate < 0) {
			setTextColour(holder,
					mContext.getResources().getColor(R.color.green_3fbf06));
		} else
			setTextColour(holder,
					mContext.getResources().getColor(R.color.black));

		if (position % 2 == 0) {
			v.setBackgroundColor(mContext.getResources()
					.getColor(R.color.white));
		} else {
			v.setBackgroundColor(mContext.getResources().getColor(
					R.color.white_f2f2f2));
		}

		// 校正（处理同时上下和左右滚动出现错位情况）
		View child = ((ViewGroup) v).getChildAt(1);
		int head = pListview.getHeadScrollX();
		if (child.getScrollX() != head) {
			child.scrollTo(pListview.getHeadScrollX(), 0);
		}

		// v.setOnTouchListener(new View.OnTouchListener() {
		// @Override
		// public boolean onTouch(View v, MotionEvent event) {
		// float this_x = event.getX();
		// if (event.getAction() == MotionEvent.ACTION_DOWN) {// 获取按下时的x轴坐标
		// // 当按下时处理
		// down_x = event.getX();
		// } else if (event.getAction() == MotionEvent.ACTION_UP) {// 松开处理
		// // up_x = event.getX();
		// } else if (event.getAction() == MotionEvent.ACTION_MOVE) {//
		// 当滑动时背景为选中状态
		// if (Math.abs(down_x - this_x) > 5) {
		// return false;
		// } else {
		// // Bundle bundle = new Bundle();
		// // bundle.putSerializable("bean", b);
		// // Act.toAct(mContext, StockInfoActivity.class, bundle);
		// return true;
		// }
		// } else {// 其他模式
		// // 设置背景为未选中正常状态
		// // v.setBackgroundColor(000);
		// }
		// return true;
		// }
		// });
		v.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Bundle bundle = new Bundle();
				bundle.putSerializable("bean", b);
				bundle.putInt("tag", tag);
				Act.toAct(mContext, StockInfoActivity.class, bundle);
			}
		});
		return v;
	}

	// 设置文字颜色
	private void setTextColour(ViewHolder holder, int resColour) {
		for (TextView tv : holder.items)
			tv.setTextColor(resColour);

	}

	public boolean isTouch() {
		return isTouch;
	}

	class ViewHolder {
		TextView item_name;
		TextView item_num;

		public TextView[] items;
	}

}
