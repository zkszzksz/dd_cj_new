package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.JpushBean;
import com.inwhoop.ddcj.bean.SearchFriendBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.bean.ValidateBean;
import com.inwhoop.ddcj.db.PushMsgDb;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dev1 on 2015/1/7.
 */
public class ValidateListAdapter extends BaseAdapter {

    private List<JpushBean> list = new ArrayList<JpushBean>();

    private Context context = null;

    private LayoutInflater inflater = null;
    private FinalBitmap fb;
    private Object[] obj;
    private UserBean userBean;

    public ValidateListAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        userBean = UserInfoUtil.getUserInfo(context);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.home_head);
        fb.configLoadfailImage(R.drawable.home_head);
    }

    public void addList(List<JpushBean> addlist) {
        list.clear();
        this.list = addlist;
    }

    public List<JpushBean> getAll() {
        return list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        Holder holder = null;
        final JpushBean bean = list.get(position);
        if (null == holder) {
            holder = new Holder();
            if (null != bean && bean.type == 6) {
                //申请加群
                convertView = inflater.inflate(R.layout.validate_item1, null);
                holder.headImg = (ImageView) convertView.findViewById(R.id.push_head_img);
                holder.name = (TextView) convertView.findViewById(R.id.push_name);
                holder.content = (TextView) convertView.findViewById(R.id.push_content);
                holder.agree = (TextView) convertView.findViewById(R.id.push_agree);
                holder.deny = (TextView) convertView.findViewById(R.id.push_deny);
                holder.text = (TextView) convertView.findViewById(R.id.push_text);
                holder.agree.setTag(holder);
                holder.deny.setTag(holder);
                if (bean.isAgree.equals("0")) {
                    holder.agree.setVisibility(View.VISIBLE);
                    holder.deny.setVisibility(View.VISIBLE);
                    holder.text.setVisibility(View.GONE);
                } else {
                    holder.agree.setVisibility(View.GONE);
                    holder.deny.setVisibility(View.GONE);
                    holder.text.setVisibility(View.VISIBLE);
                    if (bean.isAgree.equals("1")) {
                        holder.text.setText("已同意");
                    } else if (bean.isAgree.equals("2")) {
                        holder.text.setText("已拒绝");
                    }
                }

                fb.display(holder.headImg, Configs.HOST + bean.headImg);
                holder.name.setText(bean.name);
                holder.content.setText(bean.content);

                holder.agree.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Holder holder1 = (Holder) view.getTag();
                        holder1.agree.setVisibility(View.GONE);
                        holder1.deny.setVisibility(View.GONE);
                        holder1.text.setVisibility(View.VISIBLE);
                        holder1.text.setText("已同意");
                        getJoinGp("0", bean.sender, bean.id);
                    }
                });

                holder.deny.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Holder holder2 = (Holder) view.getTag();
                        holder2.agree.setVisibility(View.GONE);
                        holder2.deny.setVisibility(View.GONE);
                        holder2.text.setVisibility(View.VISIBLE);
                        holder2.text.setText("已拒绝");
                        getJoinGp("1", bean.sender, bean.id);
                    }
                });


            } else if (bean.type == 8) {
                // 同意加群
                convertView = inflater.inflate(R.layout.validate_item2, null);
                holder.headImg = (ImageView) convertView.findViewById(R.id.push_head_img);
                holder.name = (TextView) convertView.findViewById(R.id.push_name);
                holder.content = (TextView) convertView.findViewById(R.id.push_content);
                fb.display(holder.headImg, Configs.HOST + bean.headImg);
                holder.name.setText(bean.name);
                holder.content.setText(bean.content);
            } else if (bean.type == 7) {
                // 建群成功通知
                convertView = inflater.inflate(R.layout.validate_item3, null);
                holder.headImg = (ImageView) convertView.findViewById(R.id.push_head_img);
                holder.name = (TextView) convertView.findViewById(R.id.push_name);
                holder.content = (TextView) convertView.findViewById(R.id.push_content);
                fb.display(holder.headImg, Configs.HOST + bean.headImg);
                holder.name.setText(bean.name);
                holder.content.setText(bean.content);
            }

            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }
        if (null != list && list.size() > 0) {
            //holder.name.setText(list.get(position).name);
        }

        return convertView;
    }

    class Holder {
        public TextView name;
        public ImageView headImg;
        public TextView content;
        public TextView agree;
        public TextView deny;
        public TextView text;
    }


    private void getJoinGp(final String type, final String userid, final String groupid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.handleJoinGroup(type, userid, groupid);
                    System.out.println("处理群消息结果" + obj[0] + "===" + obj[1]);
                    if (obj[0].equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                        String[] str = {type, groupid};
                        msg.obj = str;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("处理群消息异常" + e.toString());
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            System.out.println("处理群结果222==" + msg.what);
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    PushMsgDb db = new PushMsgDb(context);
                    String[] strings = (String[]) msg.obj;
                    System.out.println("处理群结果1111===" + strings[0] + "===" + strings[1]);
                    if (strings[0].equals("0")) {
                        db.updatePushData("1", "6", userBean.userid, strings[1]);
                    } else {
                        db.updatePushData("2", "6", userBean.userid, strings[1]);
                    }
                    db = null;
                    break;
                case Configs.READ_FAIL:
                    break;
                case Configs.READ_ERROR:
                    break;
            }
        }
    };
}
