package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.AreaListItem;

import java.util.List;

/**
 * @Describe: TODO * * * ****** Created by ZK ********
 * @Date: 2014/11/07 14:06
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class AreaAdapter extends BaseAdapter {
	private Context context;
	private List<AreaListItem> myList;
	public int selectPosition = -1;

	public AreaAdapter(Context mContext, List<AreaListItem> list) {
		this.context = mContext;
		this.myList = list;
	}

	public int getCount() {
		return myList.size();
	}

	public AreaListItem getItem(int position) {
		return myList.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		Holder holder = null;
		if (convertView == null) {
			holder = new Holder();
			convertView = LayoutInflater.from(context).inflate(
					R.layout.invite_member_item, null);
			holder.name = (TextView) convertView
					.findViewById(R.id.invite_member_item_name);
			holder.checkBox = convertView
					.findViewById(R.id.invite_member_item_checkbox);
			convertView.setTag(holder);
		} else {
			holder = (Holder) convertView.getTag();
		}

		final AreaListItem myListItem = myList.get(position);

		if (position == selectPosition)
			holder.checkBox.setBackgroundResource(R.drawable.checkbox_checked);
		else
			holder.checkBox
					.setBackgroundResource(R.drawable.checkbox_unchecked);

		holder.name.setText(myListItem.getName());
		convertView.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				selectPosition = position;
				notifyDataSetChanged();
			}
		});
		return convertView;
	}

	class Holder {
		TextView name;
		public View checkBox;
	}
}
