package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ContactsInfo;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.TimeUtils;
import com.inwhoop.ddcj.view.CircleImageview;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * 联系人：好友和财迷适配器
 *
 * @author ZOUXU
 */
public class ContactsAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    List<ContactsInfo> mData = new ArrayList<ContactsInfo>();
    private FinalBitmap fb;

    private class ViewHolder {
        CircleImageview civ_headView;
        TextView tv_name;
        TextView tv_info;
        TextView tv_otherInfo;
    }

    public ContactsAdapter(Context context) {
        this.mInflater = LayoutInflater.from(context);
        fb=FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.home_head);
        fb.configLoadfailImage(R.drawable.home_head);
    }

    public void addList(List<ContactsInfo> mData, boolean isLoad) {
        if (isLoad) {
            for (int i = 0; i < mData.size(); i++) {
                this.mData.add(mData.get(i));
            }
        } else {
            this.mData = mData;
        }
    }

    public int getAllSize(){
        if (mData.size()>0)
            return Integer.parseInt(mData.get(0).count);
        else
            return 0;
    }

    public List<ContactsInfo> getList() {
        return mData;
    }

    @Override
    public int getCount() {
        // TODO Auto-ContactsInfo method stub
        return mData.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return mData.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        ViewHolder holder = null;
        if (null == holder) {
            holder = new ViewHolder();
            convertView = mInflater.inflate(
                    R.layout.contacts_friends_list_item_layout, null);
            holder.civ_headView = (CircleImageview) convertView
                    .findViewById(R.id.headimg);
            holder.tv_name = (TextView) convertView.findViewById(R.id.nametv);
            holder.tv_info = (TextView) convertView.findViewById(R.id.content);
            holder.tv_otherInfo = (TextView) convertView
                    .findViewById(R.id.tv_other_info);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        try {
        	if(mData.get(position).img.startsWith("http://")){
        		fb.display(holder.civ_headView,
                        mData.get(position).img);
        	}else{
            fb.display(holder.civ_headView,
                    Configs.HOST + mData.get(position).img);
        	}
        } catch (Exception e) {
            e.printStackTrace();
        }
        // holder.civ_headView
        // .setBackgroundResource(mData.get(position).imageResId);
        if (null != mData && mData.size() > 0 && position < mData.size()) {
            ContactsInfo contactsInfo = mData.get(position);
            if (null != contactsInfo) {
                holder.tv_name.setText(mData.get(position).name);
                holder.tv_info.setText(mData.get(position).otherinfo);
                holder.tv_otherInfo.setText(mData.get(position).location
                        + "|"
                        + TimeUtils.twoDateDistance(TimeRender.getStandardDate(Long
                                .valueOf(mData.get(position).state) * 1000),
                        TimeRender.getStandardDate())); // +
            /**/
            }
        }
        // mData.get(position).state
        return convertView;
    }
}