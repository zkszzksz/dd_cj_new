package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.PersonInfoActivity;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.InviterListContactBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.ToastUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.Utils;

import net.tsz.afinal.FinalBitmap;

import java.util.List;

/**
 * 适配器 发现-邀请函-邀请人列表
 * 
 * @author ZOUXU
 */
public class InvitationListActivityAdapter extends BaseAdapter {
	private UserBean userBean;// 当前app用户对象
	private LayoutInflater mInflater;
	List<InviterListContactBean> mData = null;
	private Context context;
	private AttentionUserOrCancleListener attentionListener;
	private String userId;
	/**
	 * 图像缓存
	 */
	private FinalBitmap fb;

	private class ViewHolder {
		ImageView iv_headView;
		TextView tv_name;
		TextView tv_info;
		TextView tv_addWhatch;
	}

	public InvitationListActivityAdapter(Context context,
			List<InviterListContactBean> mData) {
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.home_head);
		fb.configLoadfailImage(R.drawable.home_head);
		this.context = context;
		this.mInflater = LayoutInflater.from(context);
		this.mData = mData;
		this.userBean = UserInfoUtil.getUserInfo(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	public List<InviterListContactBean> getList() {
		return mData;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder = null;
		if (null == convertView) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(
					R.layout.contacts_recomends_list_item_layout, null);
			holder.iv_headView = (ImageView) convertView
					.findViewById(R.id.headimg);
			holder.tv_name = (TextView) convertView.findViewById(R.id.nametv);
			holder.tv_info = (TextView) convertView.findViewById(R.id.content);
			holder.tv_addWhatch = (TextView) convertView
					.findViewById(R.id.tv_other_info);
			holder.tv_addWhatch.setTextColor(context.getResources().getColor(
					android.R.color.white));
			holder.tv_addWhatch
					.setBackgroundResource(R.drawable.btn_jiaguanzhu_selector);
			holder.tv_addWhatch.setGravity(Gravity.CENTER);

			holder.tv_addWhatch.setClickable(true);

			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		if (mData.get(position).imgUrl.startsWith("http://")) {
			fb.display(holder.iv_headView, mData.get(position).imgUrl);
		} else {
			fb.display(holder.iv_headView,
					Configs.HOST + mData.get(position).imgUrl);
		}
		holder.tv_name.setText(mData.get(position).name);
		holder.iv_headView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent intent = new Intent(context, PersonInfoActivity.class);
				intent.putExtra("userid", mData.get(position).userId);
				context.startActivity(intent);
			}
		});

		if (userBean.userid.equals(mData.get(position).userId))
			holder.tv_addWhatch.setVisibility(View.INVISIBLE);
		else
			holder.tv_addWhatch.setVisibility(View.VISIBLE);

		// 添加关注和取消关注
		holder.tv_addWhatch.setTag(position);
		holder.tv_addWhatch.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final TextView tv = (TextView) v;
				final int pos = Integer.valueOf((Integer) tv.getTag());
				if (mData.get(pos).attentionState == 1) {
					tv.setBackgroundResource(R.drawable.btn_jiaguanzhu_selector);
					tv.setText("加关注");
					mData.get(pos).attentionState = 0;
					// 取消关注
					new AsyncTask<Void, Integer, Boolean>() {

						@Override
						protected Boolean doInBackground(Void... params) {
							// TODO Auto-generated method stub
							boolean result = attentionListener.cancelAttention(
									userId, mData.get(pos).userId);
							return result;
						}

						@Override
						protected void onPostExecute(Boolean result) {
							// TODO Auto-generated method stub
							if (result)
								ToastUtils.showShort(context, "取消关注");
						}
					}.execute();

				} else if (mData.get(pos).attentionState == 0) {
					tv.setBackgroundResource(R.drawable.btn_quxiaoguanzhu_selector);
					tv.setText("取消关注");
					// 取消关注
					new AsyncTask<Void, Integer, Boolean>() {

						@Override
						protected Boolean doInBackground(Void... params) {
							// TODO Auto-generated method stub
							boolean result = attentionListener.attention(
									userId, mData.get(pos).userId);
							return result;
						}

						@Override
						protected void onPostExecute(Boolean result) {
							// TODO Auto-generated method stub
							if (result) {
								ToastUtils.showShort(context, "成功关注");
								mData.get(pos).attentionState = 1;
							} else {
								tv.setBackgroundResource(R.drawable.btn_jiaguanzhu_selector);
								tv.setText("加关注");
							}
						}
					}.execute();
				}
			}
		});
		if (mData.get(position).attentionState == 0) {
			holder.tv_addWhatch
					.setBackgroundResource(R.drawable.btn_jiaguanzhu_selector);
			holder.tv_addWhatch.setText("加关注");
		} else if (mData.get(position).attentionState == 1) {
			holder.tv_addWhatch
					.setBackgroundResource(R.drawable.btn_quxiaoguanzhu_selector);
			holder.tv_addWhatch.setText("取消关注");
		}
		System.out.println("---------" + mData.get(position).isFriend);
		// 联系人信息：好友 OR 可能认识的人
		holder.tv_info.setText(setTvInfo(position));
		return convertView;
	}

	/**
	 * 设置其他信息：联系人好友姓名 ， 可能认识的人<br>
	 * (理论上服务器端返回的电话号码和地址参数不能同时存在非空字符串)
	 * 
	 * @param pos
	 * @return
	 */
	public String setTvInfo(int pos) {
		// 初始化为陌生人
		String info = "陌生人";

		if (!TextUtils.isEmpty(mData.get(pos).location)) {
			// 可能认识的人以及来自的省市
			info = mData.get(pos).location.replaceAll(" ", "");// 去掉空格
			if (mData.get(pos).maybeknow == 1){
				info += "-" + context.getString(R.string.maybeKnow);
			}
		} else if (!TextUtils.isEmpty(mData.get(pos).tel)
				&& mData.get(pos).isFriend == 1) {
			// TODO 通过手机号码获得联系人姓名
			List<String> contactNames = Utils.searchPhoneToName(context,
					mData.get(pos).tel);
			int size = contactNames.size();
			switch (size) {
			case 0:
				info = "未知联系人";
				break;
			case 1:
				info = String.format("联系人好友%1$s", contactNames.get(0));
				break;
			case 2:
				info = String.format("联系人好友%1$s和%2$s", contactNames.get(0),
						contactNames.get(1));
				break;
			default:
				info = String.format("联系人好友%1$s和%2$s等", contactNames.get(0),
						contactNames.get(1));
				break;
			}
		}
		return info;
	}

	/**
	 * 设置关注操作监听器，并传入当期用户的userid
	 * 
	 * @param attentionListener
	 * @param userId
	 *            用于关注用户等接口操作 当前用户ID
	 */
	public void setAttentionListener(
			AttentionUserOrCancleListener attentionListener, String userId) {
		this.attentionListener = attentionListener;
		this.userId = userId;
	}

	/**
	 * 执行关注和取消关注操作的回调接口，在{@link InvitationListActivityAdapter}中定义的接口
	 * 
	 * @author ZOUXU
	 */
	public interface AttentionUserOrCancleListener {
		/**
		 * 添加关注，在{@link InvitationListActivityAdapter}中定义的接口
		 * 
		 * @param userid
		 * @param touserid
		 *            被关注用户ID
		 * @return
		 */
		public boolean attention(String userid, String touserid);

		/**
		 * 取消关注，在{@link InvitationListActivityAdapter}中定义的接口
		 * 
		 * @param userid
		 * @param touserid
		 * @return
		 */
		public boolean cancelAttention(String userid, String touserid);
	}

}