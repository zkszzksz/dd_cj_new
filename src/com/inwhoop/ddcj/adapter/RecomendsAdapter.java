package com.inwhoop.ddcj.adapter;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.RecomendsInfo;
import com.inwhoop.ddcj.bean.RecommenBean;
import net.tsz.afinal.FinalBitmap;

public class RecomendsAdapter extends BaseAdapter {
	private LayoutInflater mInflater;
	List<RecommenBean> mData = new ArrayList<RecommenBean>();
	private Context context;
	private FinalBitmap fb;

	public RecomendsAdapter(Context context) {
		this.context = context;
		this.mInflater = LayoutInflater.from(context);
		fb = FinalBitmap.create(context);
		fb.configLoadingImage(R.drawable.home_head);
		fb.configLoadfailImage(R.drawable.home_head);
	}

	public void addList(List<RecommenBean> list, boolean isLoad) {
		if (isLoad) {
			for (int i = 0; i < list.size(); i++) {
				mData.add(list.get(i));
			}
		} else {
			this.mData = list;
		}
	}

	public List<RecommenBean> getList() {
		return mData;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mData.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return mData.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub

		ViewHolder holder = null;
		if (null == convertView) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(
					R.layout.contacts_recomends_list_item_layout, null);
			holder.iv_headView = (ImageView) convertView
					.findViewById(R.id.headimg);
			holder.tv_name = (TextView) convertView.findViewById(R.id.nametv);
			holder.tv_info = (TextView) convertView.findViewById(R.id.content);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		fb.display(holder.iv_headView, Configs.HOST + mData.get(position).img);
		holder.tv_name.setText(mData.get(position).name);
		if (mData.get(position).otherinfo.contains("通讯录好友")) {
			holder.tv_info.setText(mData.get(position).otherinfo
					+ mData.get(position).bookname);
		} else {
			holder.tv_info.setText(mData.get(position).location + "-"
					+ mData.get(position).otherinfo);
		}

		return convertView;
	}

	private class ViewHolder {
		ImageView iv_headView;
		TextView tv_name;
		TextView tv_info;
	}
}