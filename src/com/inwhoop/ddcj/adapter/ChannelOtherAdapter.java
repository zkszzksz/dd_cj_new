package com.inwhoop.ddcj.adapter;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.inwhoop.R;
import com.inwhoop.ddcj.bean.ChannelItem;

public class ChannelOtherAdapter extends BaseAdapter {

	private Context context;
	public List<ChannelItem> channelList;
	private TextView item_text;
	/** 是否可见 */
	boolean isVisible = true;
	/** 要删除的position */
	public int remove_position = -1;

	/**
	 * 是否全显
	 */
	public boolean isShowAllChannel = false;
	/**
	 * 要显示的Channel的最大数量
	 */
	private static final int MAXSIZE = 4;

	public ChannelOtherAdapter(Context context, List<ChannelItem> channelList) {
		this.context = context;
		this.channelList = channelList;
	}

	@Override
	public int getCount() {
		if (channelList == null) {
			return 0;
		} else if (!isShowAllChannel && channelList.size() <= MAXSIZE) {
			return channelList.size();
		} else if (!isShowAllChannel && channelList.size() > MAXSIZE) {
			return MAXSIZE;
		} else if (isShowAllChannel) {
			return channelList.size();
		}
		return 0;

	}

	@Override
	public ChannelItem getItem(int position) {
		// 可以加入显示数量的控制
		if (channelList != null && channelList.size() != 0) {
			return channelList.get(position);
		}
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = LayoutInflater.from(context).inflate(
				R.layout.subscribe_category_item, null);
		item_text = (TextView) view.findViewById(R.id.text_item);
		ChannelItem channel = getItem(position);
		item_text.setText(channel.getName());
		// 点击事件中获取
		item_text.setTag(channel.getId());
		if (!isVisible && (position == -1 + channelList.size())) {
			item_text.setText("");
		}
		if (remove_position == position) {
			item_text.setText("");
		}

		// // 最多显示8项
		// if (isShowAllChannel)
		// view.setVisibility(View.VISIBLE);
		// else if (position >= 8)
		// view.setVisibility(View.GONE);

		return view;
	}

	/** 获取频道列表 */
	public List<ChannelItem> getChannnelLst() {
		return channelList;
	}

	/** 添加频道列表 */
	public void addItem(ChannelItem channel) {
		channelList.add(channel);
		notifyDataSetChanged();
	}

	/** 设置删除的position */
	public void setRemove(int position) {
		remove_position = position;
		notifyDataSetChanged();
		// notifyDataSetChanged();
	}

	/** 删除频道列表 */
	public void remove() {
		channelList.remove(remove_position);
		remove_position = -1;
		notifyDataSetChanged();
	}

	/** 设置频道列表 */
	public void setListDate(List<ChannelItem> list) {
		channelList = list;
	}

	/** 获取是否可见 */
	public boolean isVisible() {
		return isVisible;
	}

	/** 设置是否可见 */
	public void setVisible(boolean visible) {
		isVisible = visible;
	}
}