package com.inwhoop.ddcj.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.UserMessage;
import com.inwhoop.ddcj.view.CircleImageview;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: ChatRecordlistAdapter.java
 * @Package com.inwhoop.ddcj.adapter
 * @Description: TODO
 * @date 2014-11-6 下午3:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class ChatRecordlistAdapter extends BaseAdapter {

    private List<UserMessage> list = new ArrayList<UserMessage>();

    private Context context = null;

    private LayoutInflater inflater = null;
    private FinalBitmap fb;

    public ChatRecordlistAdapter(Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.home_head);
        fb.configLoadfailImage(R.drawable.home_head);
    }

    public void addList(List<UserMessage> list) {
        this.list = list;
    }

    public List<UserMessage> getAll() {
        return list;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressWarnings("unused")
    @Override
    public View getView(int position, View convertView, ViewGroup arg2) {
        Holder holder = null;
        if (null == holder) {
            holder = new Holder();
            convertView = inflater
                    .inflate(R.layout.chat_record_list_item, null);
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.time = (TextView) convertView.findViewById(R.id.time);
            holder.content = (TextView) convertView.findViewById(R.id.content);
            holder.count = (TextView) convertView.findViewById(R.id.count);
            holder.pimg = (CircleImageview) convertView
                    .findViewById(R.id.pheadimg);
            holder.gimg = (ImageView) convertView.findViewById(R.id.gheadimg);
            convertView.setTag(holder);
        } else {
            holder = (Holder) convertView.getTag();
        }

        UserMessage bb = list.get(position);
        if (bb.type.equals("1")) {
            holder.pimg.setVisibility(View.VISIBLE);
            holder.gimg.setVisibility(View.GONE);
            System.out.println("个人头像地址为"+bb.logo_url);
            fb.display(holder.pimg, bb.logo_url);

        } else {
            holder.pimg.setVisibility(View.GONE);
            holder.gimg.setVisibility(View.VISIBLE);
            System.out.println("群组头像地址为"+bb.logo_url);
            fb.display(holder.gimg, bb.logo_url);
        }
        holder.name.setText(bb.title);
        holder.content.setText(bb.info);
        holder.time.setText(bb.time);
        if (bb.number == 0)
            holder.count.setVisibility(View.INVISIBLE);
        else {
            holder.count.setVisibility(View.VISIBLE);
            holder.count.setText(bb.number + "");
        }
        return convertView;
    }

    class Holder {
        TextView name, time, content;
        CircleImageview pimg;
        ImageView gimg;
        public TextView count;
    }
}
