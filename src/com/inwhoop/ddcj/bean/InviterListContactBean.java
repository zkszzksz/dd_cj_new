package com.inwhoop.ddcj.bean;

import com.inwhoop.R;

/**
 * 发现-邀请函-邀请人列表界面
 * 
 * @author ZOUXU
 * 
 */
public class InviterListContactBean {
	public String userId;
	public String imgUrl;
	public String name;
	/**
	 * 是否关注 (1关注---- 0没关注)
	 */
	public int isFriend;

	public int maybeknow;
	public String location;

	public String tel;

	public int attentionState = 0;
	/**
	 * 作假数据用途：头像
	 */
	public int imageResId = R.drawable.home_head;
}