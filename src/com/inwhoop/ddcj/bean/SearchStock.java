package com.inwhoop.ddcj.bean;

import java.io.Serializable;

/**
 * 
 * @Project: DDCJ
 * @Title: SearchStock.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-10-22 下午8:07:24
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class SearchStock extends StockInfoBean implements Serializable {
	public String stockname;
	public String stockcode;

	public String aliasname;
	public String count;
	public String focus;
	public boolean isAdd;

	public String getFocus() {
		return "" + focus;
	}

	public void setFocus(String focus) {
		this.focus = focus;
	}

	public String getStockname() {
		return stockname;
	}

	public void setStockname(String stockname) {
		this.stockname = stockname;
	}

	public String getStockcode() {
		return stockcode;
	}

	public void setStockcode(String stockcode) {
		this.stockcode = stockcode;
	}

	public String getAliasname() {
		return aliasname;
	}

	public void setAliasname(String aliasname) {
		this.aliasname = aliasname;
	}

	public String getCount() {
		return count;
	}

	public void setCount(String count) {
		this.count = count;
	}

}
