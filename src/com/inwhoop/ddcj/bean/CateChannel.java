package com.inwhoop.ddcj.bean;

/**
 * package_name: com.inwhoop.ddcj.bean Created with IntelliJ IDEA User:
 * Administrator Date: 2014/12/17 Time: 16:47   群组对象
 */
public class CateChannel extends BaseBean{
    public String id="";//
    public String name="";//群名称
    public String des="";//群介绍
//    public String img="";//群图片
    public String groupmanager="";//群主昵称
    public String newscount="";//资讯数
    public String fanscount="";//粉丝数
    public String groupnum="";//群组人数
    public String adminid="";
    public String adminattention="";
    public String isgroup="";//是否加入群组
    public String ischannel="";//是否订阅资讯
    public String channelid="";//频道id
    public boolean isselect = false;
}