package com.inwhoop.ddcj.bean;

/**
 * 上传通讯录返回结果
 * 
 * @Project: DDCJ
 * @Title: UserRelation.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-28 下午7:05:13
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class UserRelation extends BaseBean {
	// public String number;
	public SendPhone phone;
	public String userid;
	public int relative;
}
