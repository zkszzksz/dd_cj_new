package com.inwhoop.ddcj.bean;

/**
 * 小纸条列表
 * 
 * @Project: DDCJ
 * @Title: ChatRecList.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-11-6 下午3:35:42
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ChatRecList extends BaseBean {

	public int id;
	public int role;
	public String titlename;
	public String picpath;
	public String time;
	public String content;
	public int noread;

}
