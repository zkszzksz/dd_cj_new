package com.inwhoop.ddcj.bean;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: TODO
 * @Package com.inwhoop.ddcj.bean
 * @Date: 2014/11/27 18:01
 * @version: 1.0
 */
public class FriendBean extends BaseBean {
	public String frienduserid = "";
	public String name = "";
	// public String img 父类
	public String location = "";
	public String otherinfo = "";

	/*
	 * ":[{"frienduserid":"MczxIpxhMpzdIaxsMdzIxMzIxMywxLDE0MTEwNjIwMTQwOAO0O0OO0O0O
	 * ", "name":"1", "img":"\/upload\/header\/1", "location":"1",
	 * "otherinfo":"1"},
	 * {"frienduserid":"MczxUp3hMpTdQazsMdDQ0NjQ2NTU5LDgsMTQxMTExMTUwMjU2"
	 * ,"name":"\u6d4b\u8bd5111","img":
	 * "\/upload\/header\/","location":null,"otherinfo":null},{"frienduserid":"OcDxYpyhMpDdka4sMdDI2MDg2MDU5LDE3LDE0MTEyNzE0MDA0NgO0O0OO0O0O","name":"\u6d4b\u8bd5","img":"\/upload\/header\/","location":"\u5c71\u897f\u7701\u0000
	 * \u664b\u57ce\u5e02","otherinfo":""},{"frienduserid":"
	 * MczxUp3hMpTdQazsMdDQ0NjQ2NTU5LDksMTQxMTEyMTM0MzAy
	 * ","name":"ganyuchuan","img
	 * ":"\/upload\/header\/","location":null,"otherinfo":""},{"frienduserid":"
	 * OcDxYpyhMpDdka4sMdDI2MDg2MDU5LDE3LDE0MTEyNzE0MDA0NgO0O0OO0O0O
	 * ","name":"\u6d4b\u8bd5
	 * ","img":"\/upload\/header\/","location":"\u5c71\u897f\u7701\u0000
	 * \u664b\u57ce\u5e02","otherinfo":""}]}
	 */
}
