package com.inwhoop.ddcj.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @Project: DDCJ
 * @Title: CommentMeList.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-12-16 上午10:07:45
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class CommentMeList extends BaseBean {

	public int id;
	public int newsid;
	public String content;
	public long createtime;
	public String name;
	public String userid;
	public String tousername;
	public String touseruserid;
	public String touserimg;
	public String tocontent;

}
