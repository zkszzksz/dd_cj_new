package com.inwhoop.ddcj.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 频道列表
 * 
 * @Project: DDCJ
 * @Title: ChannelInfo.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-29 下午6:57:19
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ChannelInfo extends BaseBean {

	public int id;
	public String name;
	public String des;
	public int foucs;
	/**
	 * 栏目在整体中的排序顺序 rank
	 * */
	public int orderId;
	public List<ChannelInfo> alllist=new ArrayList<ChannelInfo>();
	public List<ChannelInfo> foucslist=new ArrayList<ChannelInfo>();
	// 有个img 在父类里
}
