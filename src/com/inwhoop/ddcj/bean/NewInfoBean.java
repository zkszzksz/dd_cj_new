package com.inwhoop.ddcj.bean;

/**
 * @Project: DDCJ
 * @Title: NewInfoBean.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-22 下午7:57:48
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 * 
 *          个股关联最新情报
 * 
 */
public class NewInfoBean extends BaseBean {

	public String name;
	public String userid;
	public int id;
	public long createtime;
	public String content;
	public int sharenum;
	public int commnetnum;

}
