package com.inwhoop.ddcj.bean;

/**
 * 是否关注股票的实体
 * 
 * @Project: DDCJ
 * @Title: Fouces.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-12-16 上午11:13:06
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class Fouces extends BaseBean {
	public int foucs;// 0 没有关注 1 已经关注
}
