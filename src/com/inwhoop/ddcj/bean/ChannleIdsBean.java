package com.inwhoop.ddcj.bean;

/**
 * Created by Administrator on 2014/10/30. 【用户通过传入用户的Id，获取当前用户关注的所有频道下的资讯Id】对象
 */
public class ChannleIdsBean extends BaseBean {
	public String id = "";
	public String channelid = "";
	public String userid = "";
	public String channelname = "";
	public String count = "";
}
