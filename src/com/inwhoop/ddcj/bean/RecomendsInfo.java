package com.inwhoop.ddcj.bean;

import com.inwhoop.R;

public class RecomendsInfo {
	public String userId;
	public String imgUrl;
	public int imageResId = R.drawable.home_head;
	public String name;
	public String info;
	/**
	 * 是否关注 (0关注---- 1取消关注)
	 */
	public int isWhatch;
}