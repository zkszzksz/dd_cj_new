package com.inwhoop.ddcj.bean;

/**
 * @Describe: TODO 上传的用户跟帖对象//直接发表的帖子对象 ， * * * ****** Created by ZK ********
 * @Date: 2014/11/11 19:52
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class NetInvitationBean extends BaseBean {
	public String content = "";// :（发帖内容）
	public String title = "";// :发帖标题
	public String imgpath = "";// ：发送内容中包含的图片（可以为空）
	public String touserid = "";// :被跟帖人id（不可以为空）【此项跟帖需要，发帖接口不传这个】
	public String userid = "";// :跟帖人id
	public String channelid = "";// ：频道id
	public String newsid = "";// （资讯id）。

	public NetInvitationBean(String content, String title, String imgpath,
			String touserid, String userid, String channelid, String newsid) {
		this.content = content;
		this.title = title;
		this.imgpath = imgpath;
		this.touserid = touserid;
		this.userid = userid;
		this.channelid = channelid;
		this.newsid = newsid;
	}

	public NetInvitationBean(String content, String title, String imgpath,
			String userid, String channelid, String newsid) {
		this.content = content;
		this.title = title;
		this.imgpath = imgpath;
		this.userid = userid;
		this.channelid = channelid;
		this.newsid = newsid;
	}
}
