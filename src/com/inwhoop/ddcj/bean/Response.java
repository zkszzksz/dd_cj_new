package com.inwhoop.ddcj.bean;

/**
 * 响应实体
 * 
 * @Project: DDCJ
 * @Title: Response.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-10-28 下午8:22:46
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class Response {
	public int code;
	public String msg;
}
