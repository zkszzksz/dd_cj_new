package com.inwhoop.ddcj.bean;

//目前用于 注册选择城市里，省市的item的数据
public class AreaListItem {
	private String name;
	private String pcode;

	public String getName() {
		return name;
	}

	public String getPcode() {
		return pcode;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setPcode(String pcode) {
		this.pcode = pcode;
	}
}
