package com.inwhoop.ddcj.bean;

import com.inwhoop.R;

/**
 * 联系人:群组
 * 
 * @author ZOUXU
 * 
 */
public class ContactGroupsInfo {
	public int imageResId = R.drawable.btn_photo1;
	public String name;
	public String info;
	public int curNum;
	// 容量
	public int totalNum;
}
