package com.inwhoop.ddcj.bean;

/**
 * @Project: DDCJ
 * @Title: EditQBChannellist.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-12-17 上午10:00:47
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class EditQBChannellist extends BaseBean {

	public int id;
	public String name;
	public boolean isselect = false;

}
