package com.inwhoop.ddcj.bean;

/**  
 * @Project: DDCJ
 * @Title: GroupInfomation.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO 接口返回的群组对对象
 *
 * @author dyong199046@163.com 代勇
 * @date 2015-1-14 上午11:03:42
 * @Copyright: 2015 呐喊信息技术 All rights reserved.
 * @version V1.0  
 */
public class GroupInfomation extends BaseBean{
	public int id;
	public long createtime;
	public String des;
	public String groupnum;
	public int fanscount;
	public String groupmanager;
	public String adminattention;
	public String name;
	public String adminid;
	public int newscount;
	public int ismeeting;
	public String tel;
	
}
