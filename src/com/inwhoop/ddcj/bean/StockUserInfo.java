package com.inwhoop.ddcj.bean;

/**
 * @Project: DDCJ
 * @Title: StockUserInfo.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-22 下午8:57:53
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class StockUserInfo extends BaseBean {

	public int id;
	public String othreinfo;
	public String userid;
	public String name;
	public int count;
	public String createtime;

}
