package com.inwhoop.ddcj.bean;

/**
 * TODO 评论对象 Created by zk
 */
public class CommentBean extends BaseBean {
	public int newsid = 0;
	public int channelid = 0;
	public String title = "";
	public String imgpath = "";
	public int touserid = 0; // 评论谁的id
	public int count = 0;

	public int id;
	public String userid;
	public int invitationid;
	public String createtime;
	public int status;
	public String content;
	public String name;
   //img
	// {"code":200,"msg":[{"id":"2","newsid":"2","channelid":"2","userid":"2","content":"2","createtime":null,"title":null,"imgpath":null,"touserid":"1","status":"0","type":null,"count":"1"}]}
}
