package com.inwhoop.ddcj.bean;

import com.inwhoop.R;

/**
 * 联系人：好友和财迷
 * 
 * @author ZOUXU
 * 
 */
public class ContactsInfo extends BaseBean {
	public String userid;
	public int imageResId = R.drawable.home_head;
	public String img;
	// 昵称
	public String name;
	// 描述
	public String otherinfo;
	public String location;
	// 上线状态
	public String state;
	public String tel = "";

	public boolean checkTag=false;
    public String count="0";
}