package com.inwhoop.ddcj.bean;

/**
 * @Project: DDCJ
 * @Title: QingbaoComment.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-12-15 上午10:15:16
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class QingbaoComment extends BaseBean {

	public int id;
	public int newsid;
	public String content;
	public long createtime;
	public int status;
	public String userid;
	public String name;
}
