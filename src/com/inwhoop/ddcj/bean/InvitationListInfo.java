package com.inwhoop.ddcj.bean;

/**
 * 
 * 邀请函列表信息
 * 
 * @Project: DDCJ
 * @Title: InvitationListInfo.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-11-26 下午7:24:54
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class InvitationListInfo extends BaseBean {

	public int id;
	public String userid;
	public String name;
	public String title;
	public String begintime;
	public int count;
	public String location;
	public String time;
	public String hourmin;
	public String week;

}
