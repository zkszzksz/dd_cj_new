package com.inwhoop.ddcj.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @Project: DDCJ
 * @Title: QingbaoInfo.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-11-11 下午6:51:49
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class QingbaoInfo {

	public int id;
	public String headpath;
	public String username;
	public String content;
	public int commentcount;
	public int sharecount;
	public String time;
	public List<String> imglist = new ArrayList<String>();

}
