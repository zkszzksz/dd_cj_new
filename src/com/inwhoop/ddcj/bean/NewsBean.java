package com.inwhoop.ddcj.bean;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO 新闻对象 ，情报详情 * * * ****** Created by ZK ********
 * @Date: 2014/11/03 17:19
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class NewsBean extends BaseBean {
	public int id = 0;
	public int type = 0; // 0默认草根爆料，1资讯情报所，2资讯专栏
	public String content = "";
	public String createtime;
	public String userid;
	public String userimg; // 注意这个img需要
	public String location = "";
	public int count = 0;
	public String orderid;
	// public List<String> stockcode=new ArrayList<String>();
	public String name;
	public int sharenum;
	public int lovenum;
	public int commnetnum;
	public int commentcount;
	public String sharecontent = "";    //分享需要的 地址url

	public List<String> imgpath = new ArrayList<String>();
}
