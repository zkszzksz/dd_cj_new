package com.inwhoop.ddcj.bean;

import java.util.List;

/**
 * package_name: com.inwhoop.ddcj.bean Created with IntelliJ IDEA User:
 * Administrator Date: 2014/12/15 Time: 10:11
 */
public class OtherFriendInfo {
	public String userid = "";
	public String name = "";
	public String location = "";
	public String otherinfo = "";
	public String ddid = "";
	public String img = "";
	public String createtime = "";
	public String relation = "";
	public List<GroupInfo> groupInfoList = null;
	public String tel = "";
}