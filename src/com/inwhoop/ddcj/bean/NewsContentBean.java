package com.inwhoop.ddcj.bean;

import java.util.List;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @ClassName: 具体的新闻/资讯详情
 * @Description: TODO
 * @Package com.inwhoop.ddcj.bean
 * @Date: 2014/11/27 11:53
 * @version: 1.0
 */
public class NewsContentBean extends BaseBean {
	public String title = "";
	public String des = ""; // ?
	public String createtime = ""; // 2014-11-25 12:36:48
	public String sharenum = ""; // null
	public String lovenum = ""; // null
	public String content = ""; // html 代码
	public String sharecontent = ""; // 分享url
	public List<Theme> themelist;
	public List<Stock> stocklist;
	public int collect;// 是否收藏
	public int subscribe;// 订阅标示

}
