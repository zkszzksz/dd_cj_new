package com.inwhoop.ddcj.bean;

/**
 * 通讯录实体
 * 
 * @Project: DDCJ
 * @Title: PhoneInfo.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-28 下午6:59:36
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
@SuppressWarnings("serial")
public class PhoneInfo extends BaseBean {
	public String id;
	public String name;
	public String phone;
	public int isGf;
}
