package com.inwhoop.ddcj.bean;

/**
 * @Describe: TODO 用户对象 * * * ****** Created by ZK ********
 * @Date: 2014/10/20 19:11
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class UserBean extends BaseBean {
	public int id;
	public String username = ""; // 暂时没用
	public String pwd = "";     //登录前是普通密码，登录后被保存了返回后的加密的密码

	public String userid = "";
	public String chatid = "";
	public String name = ""; // 昵称
	public String tel = ""; // openfire账号
	public int sex = 0;//
	public String ddid = "";
	// public String img = ""; //父类里，目前是头像

	public String type = ""; // 登录传的，普通可不传或乱传； 0：qq；1：sina；2：wechat
	public String yzm = "";// 验证码
	public String openid = "";// 第三方登录的id
	public String sinaid = "";
	public String qqid = "";
	public String wxid = "";// 微信id
	public String udid = ""; // 目前是获取的手机IMEI码
	public String email = "";
	public String location = ""; // 地区
	public String otherinfo = "";// 个性签名，

	public String address = "";// 登录时传的当前地址
    public String oldNormalPwd ="";//保存的登录时的普通密码
    public String isattention ="";

    public UserBean(String username, String pwd, String type) {
		this.username = username;
		this.pwd = pwd;
		this.type = type;
	}

	public UserBean() {
	}

	/*
	 * 修改个人信息接口 http://hostURL/index.php/userapi/updatemsg?userid=1
	 * 
	 * name 用户昵称（不可为空） tel 手机号（不可为空） sinaid 新浪id（可以为空） qqid QQid（可以为空） ddid
	 * （可以为空） img 头像（可以为空） sex 性别 （0 女 1 男） email 邮箱（可以为空） otherinfo 其他信息（可以为空）
	 * location 用户地址（不可为空）
	 */

}
