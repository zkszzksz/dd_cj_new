package com.inwhoop.ddcj.bean;

/**
 * @Describe: TODO [通过 subcategid 去查询 relateid .只返回relateid和type]接口返回对象 * * *
 *            ****** Created by ZK ********
 * @Date: 2014/11/03 17:46
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SubcategBean extends BaseBean {
	public int id = 0;
	public String relateid = "0";
	public String subcategid = "0";
	public int type = 0;
	public int count = 0;
}
