package com.inwhoop.ddcj.bean;

/**
 * 
 * 群组列表
 * 
 * @Project: DDCJ
 * @Title: GroupList.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-11-10 下午7:39:45
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GroupList extends BaseBean {

	public int groupId;
	public String groupName;
	public String groupPath;

}
