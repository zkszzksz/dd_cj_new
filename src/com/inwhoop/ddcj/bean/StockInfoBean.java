package com.inwhoop.ddcj.bean;

/**
 * @Project: DDCJ
 * @Title: StockInfo.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO 股票详情
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-20 下午8:29:45
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class StockInfoBean extends BaseBean {

	public int id;
	public int stockId = 0;
	public int notice = 0;
	public String stockname = ""; // 股票名称
	public String stockcode = ""; // 股票编码 ,服务器获取我的自选股有返回
	public double nowPrice = 0.0; // 现在价格
	public double swing; // 振幅
	public double addPrice = 0.0; // 涨跌
	public double addRate = 0.0; // 振幅
	public double nowDayOP = 0.0; // 今日价格
	public double highPrice = 0.0; // 最高价格
	public double yestprice = 0.0; // 昨日价格
	public double lowPrice = 0.0; // 最低价格
	public double volume = 0; // 成交量 //万
	public double total = 0; // 成交额 //万

	public String count = ""; // 数量？
	public int focus = 0;// 焦点？
	public String aliasname = "";// 别名
	public String position;
}
