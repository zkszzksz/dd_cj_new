package com.inwhoop.ddcj.bean;

/**
 * @Describe: TODO 群组对象，目前有排行榜返回 * * * ****** Created by ZK ********
 * @Date: 2014/11/03 15:57
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class GroupBean extends BaseBean {
//	public String img = "";
	public int id = 0;
	public String cate = ""; // 整体分类，最大的分类。接口返回是 群组排行榜 几个字；
	public String categid = "0";
	public String name = "";

    public int groupid =0;      //此俩项是获取群组列表返回的
    public String groupname ="";

    public boolean isselect =false;//在群组列表界面是否被选中
}
