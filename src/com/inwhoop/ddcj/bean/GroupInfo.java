package com.inwhoop.ddcj.bean;

/**
 * package_name: com.inwhoop.ddcj.bean Created with IntelliJ IDEA User:
 * Administrator Date: 2014/12/15 Time: 10:13  群组对象
 */
public class GroupInfo {
	public String groupid = "";
	public String name = "";
	public String des = "";
	public String image = "";
	public String level = "";
	public String channelid="";
}