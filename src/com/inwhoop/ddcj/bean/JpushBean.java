package com.inwhoop.ddcj.bean;

import java.io.Serializable;

/**
 * package_name: com.inwhoop.ddcj.bean Created with IntelliJ IDEA User:
 * Administrator Date: 2014/12/11 Time: 10:57
 */
public class JpushBean implements Serializable {
	public String sender = "";
	public String id = "";
	public String content = "";
	public String time = "";
	public String headImg = "";
	public int type = 0;
	public String name = "";
	public String isAgree="";      //0 未处理该消息 1 同意 2 拒绝
	public String isread=""; //0 未读 1 已读
}