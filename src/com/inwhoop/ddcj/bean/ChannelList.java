package com.inwhoop.ddcj.bean;

import java.util.List;

/**
 * 资讯列表
 * 
 * @Project: DDCJ
 * @Title: ChannelList.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-27 下午5:44:40
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class ChannelList extends BaseBean {
	public String id;
	public String title;
	public String des;
	public String createtime;
	public String stockcode;
	public String sharenum;
	public String lovenum;
	public String refnews;
	public String orderchannel;
	public String orderstockcode;
	public String channelname;
	public String channelid;
	public int top;
	public String thumbnail;
}
