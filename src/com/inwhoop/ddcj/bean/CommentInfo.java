package com.inwhoop.ddcj.bean;

/**
 * 资讯评论信息实体
 * 
 * @Project: DDCJ
 * @Title: CommentInfo.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-7 上午10:50:01
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class CommentInfo extends BaseBean {
	public String id;
	public String userid;
	public String name;
	public String location;
	public String createtime;
	public String content;
	public String count;

}
