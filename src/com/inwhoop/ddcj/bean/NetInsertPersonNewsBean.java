package com.inwhoop.ddcj.bean;

/**
 * @Describe: TODO 往personnews 插入新闻数据Post对象 * * * ****** Created by ZK ********
 * @Date: 2014/11/03 14:59
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class NetInsertPersonNewsBean extends BaseBean {
	public String uid;
	public int type;
	public String content;
	public String location;
	public String pic;

	public NetInsertPersonNewsBean(String uid, int type, String content,
			String location, String pic) {
		this.uid = uid;
		this.type = type;
		this.content = content;
		this.location = location;
		this.pic = pic;
	}
}
