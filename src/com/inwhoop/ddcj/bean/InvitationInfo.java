package com.inwhoop.ddcj.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Project: DDCJ
 * @Title: InvitationInfo.java
 * @Package com.inwhoop.ddcj.bean
 * @Description: TODO     邀请函详情接口返回对象
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-12-5 上午10:46:58
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class InvitationInfo implements Serializable {
	public int id = 0; // 此邀请函的id

	public String title;
	public String content;
	public String location;
	public String begintime;
	public int commentcount;
	public List<InvitationUserlist> user = new ArrayList<InvitationUserlist>();
	public List<String> img = new ArrayList<String>();

    public int ifinvitation =0;//接口返回的，是否已经报名
    public String createuserid ="";//创建人的id
}
