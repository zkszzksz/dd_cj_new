package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.*;

/**
 * @Describe: TODO 注册第一个界面，输入手机号、密码、邮箱（可选） * * * ****** Created by ZK ********
 * @Date: 2014/11/06 10:52
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class RegisterOneActivity extends BaseFragmentActivity {
	private UserBean user; // 此界面的数据user，只有点下一步的时候new对象

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_register_one);

		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.register_one);
		setRightLayout(R.string.next_step, true);
		title_right_tv.setTextColor(getResources()
				.getColor(R.color.blue_028efd));
		title_right_layout.setOnClickListener(this);

		findViewById(R.id.my_next_btn).setOnClickListener(this);

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.title_right_layout:
			user = new UserBean();
			user.tel = ((EditText) findViewById(R.id.phone_edit)).getText()
					.toString().trim();
			user.pwd = ((EditText) findViewById(R.id.pwd_edit)).getText()
					.toString().trim();
			user.email = ((EditText) findViewById(R.id.email_edit)).getText()
					.toString().trim();
			if (checkOK(user)) {
				showProgressDialog("请稍后...");
				registerCheckPhone(handler, user);
			}
			break;
		case R.id.my_next_btn: // 自定义跳转下关页面，隐藏了
			user = new UserBean();
			user.tel = ((EditText) findViewById(R.id.phone_edit)).getText()
					.toString().trim();
			user.pwd = ((EditText) findViewById(R.id.pwd_edit)).getText()
					.toString().trim();
			user.email = ((EditText) findViewById(R.id.email_edit)).getText()
					.toString().trim();
			Bundle bundle = new Bundle();
			bundle.putSerializable("bean", user);
			Act.toAct(mContext, RegisterTwoActivity.class, bundle);
			break;
		}
	}

	private boolean checkOK(UserBean b) {
		if ("".equals(b.tel)) {
			showToast("手机号码不能为空");
			return false;
		} else if (!Utils.isMobileNO(b.tel)) {
			showToast("请输入正确的手机号码");
			return false;
		} else if ("".equals(b.pwd)) {
			showToast("密码不能为空");
			return false;
		} else if (b.pwd.length() < 6) {
			showToast("密码必须大于6个字节");
			return false;
		} else if (!b.email.equals("") && !Utils.isEmail(b.email)) {
			showToast("请输入正确的邮箱");
			return false;
		}
		return true;
	}

	public static void registerCheckPhone(final Handler handler,
			final UserBean user) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.phoneVerfiy(user);
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("数据异常，请稍后再试~~");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					Bundle bundle = new Bundle();
					bundle.putSerializable("bean", user);
					Act.toActClearTop(mContext, RegisterTwoActivity.class,
							bundle);
				} else {
					showToast(obj[1].toString());
				}
				break;

			default:
				break;
			}
		}
	};
}
