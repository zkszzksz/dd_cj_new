package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.DialogShowStyle;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * 设置隐身模式
 *
 * @author ZOUXU
 */
public class SettingStealthActivity extends BaseFragmentActivity implements
        OnClickListener {

    private int checkedItme = 0;
    private Context ctx;
    private DialogShowStyle dialogShowStyle;
    final Integer[] strs = new Integer[]{R.string.duisuoyourenkejian,
            R.string.duimoshengrenyinshen, R.string.guanbidiqu,};

    final Integer[] belowStrs = new Integer[]{
            R.string.duisuoyourenxianshizaixian,
            R.string.duimoshengrenyinshen_jianshaomoshengrenxiaoxi,
            R.string.duisuoyourenyinshen,};
    private ImageView img1;
    private ImageView img2;
    private ImageView img3;
    private RelativeLayout linear1;
    private RelativeLayout linear2;
    private RelativeLayout linear3;
    private UserBean userBean;
    private Object[] obj;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.setting_stealth_activity);
        userBean = UserInfoUtil.getUserInfo(ctx);
        initData();
    }

    @Override
    public void initData() {
        super.initData();
        findViewById(R.id.title_left_tv).setOnClickListener(this);
        checkedItme = UserInfoUtil.getStealthModeInfo(mContext);
        setLeftLayout(R.drawable.back_btn_selector, false);
        setTitleResText(R.string.yinshenmoshi);
        linear1 = (RelativeLayout) findViewById(R.id.linear1);
        linear2 = (RelativeLayout) findViewById(R.id.linear2);
        linear3 = (RelativeLayout) findViewById(R.id.linear3);
        img1 = (ImageView) findViewById(R.id.checkBox);
        img2 = (ImageView) findViewById(R.id.checkBox1);
        img3 = (ImageView) findViewById(R.id.checkBox2);

        linear1.setOnClickListener(this);
        linear2.setOnClickListener(this);
        linear3.setOnClickListener(this);
        setpos(UserInfoUtil.getStealthModeInfo(mContext));
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.title_left_tv:
                finish();
                break;
            case R.id.linear1:
                setStatData(0);
                break;
            case R.id.linear2:
                setStatData(1);
                break;
            case R.id.linear3:
                setStatData(2);
                break;
        }

    }

    private void setpos(int pos) {
        ImageView[] views = {img1, img2, img3};
        for (int i = 0; i < views.length; i++) {
            views[i].setBackgroundColor(getResources()
                    .getColor(android.R.color.transparent));
        }
        views[pos].setBackgroundResource(R.drawable.ico_checked);
        UserInfoUtil.setStealthModeInfo(mContext, pos);
    }

    private void setStatData(final int stat) {
        if (!isRun) isRun = true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.setStealthMode(userBean.userid, stat);
                    if ((Boolean) obj[0]) {
                        msg.what = Configs.READ_SUCCESS;
                        msg.obj = stat;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private boolean isRun=false;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            isRun=false;
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    int posi = (Integer) msg.obj;
                    setpos(posi);
                    showToast("设置成功");
                    break;
                case Configs.READ_FAIL:
                    showToast("设置失败");
                    break;
                case Configs.READ_ERROR:
                    showToast("数据异常");
                    break;
            }
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
        cancelToast();
    }
}
