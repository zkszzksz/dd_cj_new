package com.inwhoop.ddcj.activity;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.inwhoop.R;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * 设置界面-账号绑定
 * 
 * @author ZOUXU
 * 
 */
public class SettingAccountActivity extends Activity implements OnClickListener {

	private Context ctx;
	private LinearLayout accountLayout;
	private UserBean userBean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ctx = this;
		setContentView(R.layout.setting_account_acvtivity);

		findViewById(R.id.title_left_tv).setOnClickListener(this);
		findViewById(R.id.SettingAccountActivity_binding).setOnClickListener(
				this);
		findViewById(R.id.SettingAccountActivity_myinfo).setOnClickListener(
				this);
		findViewById(R.id.SettingAccountActivity_modify_pwd)
				.setOnClickListener(this);
		accountLayout = (LinearLayout) findViewById(R.id.SettingAccountActivity_account_container);
		userBean = UserInfoUtil.getUserInfo(ctx);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		userBean = UserInfoUtil.getUserInfo(ctx);
		setOtherBindIcon();
	}

	private void initView() {

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_left_tv:
			finish();
			break;
		case R.id.SettingAccountActivity_binding:
			Act.toAct(ctx, BindAccountActivity.class);
			break;
		case R.id.SettingAccountActivity_myinfo:
			Act.toAct(ctx, MyInfoActivity.class);
			break;
		case R.id.SettingAccountActivity_modify_pwd:
			Act.toAct(ctx, ChangeUserPwdActivity.class);
			break;

		default:
			break;
		}
	}

	// 设置是否有第三方登录的图标
	private void setOtherBindIcon() {
		accountLayout.removeAllViews();
		if (!TextUtils.isEmpty(userBean.qqid)) {
			View acView = LayoutInflater.from(ctx).inflate(
					R.layout.account_item_img, null);
			ImageView aimg = (ImageView) acView.findViewById(R.id.accountimg);
			aimg.setBackgroundResource(R.drawable.login_ico_qq);
			accountLayout.addView(acView);
		}
		if (!TextUtils.isEmpty(userBean.sinaid)) {
			View acView = LayoutInflater.from(ctx).inflate(
					R.layout.account_item_img, null);
			ImageView aimg = (ImageView) acView.findViewById(R.id.accountimg);
			aimg.setBackgroundResource(R.drawable.login_ico_wb);
			accountLayout.addView(acView);
		}
		if (!TextUtils.isEmpty(userBean.wxid)) {
			View acView = LayoutInflater.from(ctx).inflate(
					R.layout.account_item_img, null);
			ImageView aimg = (ImageView) acView.findViewById(R.id.accountimg);
			aimg.setBackgroundResource(R.drawable.login_ico_wechat);
			accountLayout.addView(acView);
		}
	}

}
