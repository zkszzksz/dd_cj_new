package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.*;

/**
 * 密码找回,界面1，输入手机号
 * 
 * @author ZOUXU
 */
public class ForgetPwdOnedActivity extends BaseFragmentActivity {
	private Context context;
	private UserBean userBean = new UserBean();// 传递的用户对象

	private DialogShowStyle dialogShowStyle;
	private EditText et_userTel;

	public final static int SUCCESS = 1001;
	public final static int FAIL = 1002;
	public final static int ERROR = 1003;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_forget_pwd_one);
		context = this;
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.forget_pwd_one);
		setRightLayout(R.string.next_step, true);
		title_right_tv.setTextColor(getResources()
				.getColor(R.color.blue_028efd));
		title_right_layout.setOnClickListener(this);

		et_userTel = (EditText) findViewById(R.id.phone_edit);
//		et_userTel.setText("13709066393");

		findViewById(R.id.nextPage).setOnClickListener(this);

	}

	@Override
	public void onClick(View view) {
		super.onClick(view);
		switch (view.getId()) {
		case R.id.title_right_layout:
			if (et_userTel.getText().toString().trim().equals("")) {
				Toast.makeText(context, "请先填写手机号", Toast.LENGTH_SHORT).show();
			} else if (!Utils
					.isMobileNO(et_userTel.getText().toString().trim())) {
				Toast.makeText(context, "请核对手机号", Toast.LENGTH_SHORT).show();
			} else {
				userBean.tel = et_userTel.getText().toString().trim();
				getPhoneVerfic();
			}
			break;
		case R.id.nextPage:
			Bundle bundle = new Bundle();
			bundle.putSerializable("bean", userBean);
			Act.toAct(mContext, ForgetPwdTwoActivity.class, bundle);
			break;
		default:
			break;
		}
	}

	private void getPhoneVerfic() {
		dialogShowStyle = new DialogShowStyle(context, "正在发送验证码获取请求");
		dialogShowStyle.dialogShow();
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					SysPrintUtil.pt("json==上传的json数据为", userBean.tel);
					Object[] res = JsonUtils.fogetPhoneVerfiy(userBean.tel);
					SysPrintUtil.pt("json==获取到的服务器的结果码数据为:", res[0].toString()
							+ "---" + res[1].toString());
					boolean resultCode = (Boolean) res[0];
					if (resultCode) {
						msg.what = SUCCESS;
					} else {
						msg.what = FAIL;
						msg.obj = res[1];
					}

				} catch (Exception e) {
					e.printStackTrace();
					SysPrintUtil.pt("数据异常1111", e.toString());
					msg.what = ERROR;
				}
				mmHandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler mmHandler = new Handler() {
		public void handleMessage(Message msg) {
			if (null != dialogShowStyle) {
				dialogShowStyle.dialogDismiss();
				dialogShowStyle = null;
			}
			switch (msg.what) {
			case SUCCESS:
				Bundle bundle = new Bundle();
				bundle.putSerializable("bean", userBean);
				Act.toAct(mContext, ForgetPwdTwoActivity.class, bundle);
				Toast.makeText(context, "请求发送成功，请注意接收短信", Toast.LENGTH_SHORT)
						.show();
				break;
			case ERROR:
				Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
				break;
			case FAIL:
				Toast.makeText(context, msg.obj.toString(), Toast.LENGTH_SHORT)
						.show();
				break;
			}
		}
	};
}
