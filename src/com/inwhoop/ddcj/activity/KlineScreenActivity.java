package com.inwhoop.ddcj.activity;

import com.ant.liao.GifView;
import com.inwhoop.R;
import com.inwhoop.ddcj.util.GifViewUtil;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * @Project: DDCJ
 * @Title: KlineScreenActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-23 上午11:38:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class KlineScreenActivity extends BaseFragmentActivity {

	private int space = 0, sw, sh;

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.kline_screen_layout);
		space = (int) getResources().getDimension(R.dimen.k_line_space);
		sw = getWindowManager().getDefaultDisplay().getWidth();
		sh = getWindowManager().getDefaultDisplay().getHeight();
		init();
	}

	private void init() {
		GifView gifView = (GifView) findViewById(R.id.itemimg);
		gifView.setShowDimension(sw - 2 * space, sh - 2 * space);
		GifViewUtil.setGifViewNetUrl(gifView,
				getIntent().getStringExtra("strurl"));
		gifView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
	}

}
