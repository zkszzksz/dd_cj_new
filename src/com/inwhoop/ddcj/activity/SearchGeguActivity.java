package com.inwhoop.ddcj.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.*;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.SearchStock;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.Utils;
import com.inwhoop.ddcj.view.KeybordPopupWindow;
import com.inwhoop.ddcj.view.KeybordPopupWindow.OnSureListener;
import com.inwhoop.ddcj.view.XListView;

import java.util.ArrayList;

/**
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class SearchGeguActivity extends BaseFragmentActivity implements
		OnSureListener {

	private LayoutInflater inflater = null;

	private EditText searchEditText = null;

	private String searchstr = "";

	private LinearLayout addlayout, contentlayout;

	private TextView searchTextView, cancelTextView;

	private XListView listView = null;

	private ArrayList<SearchStock> resultlist = new ArrayList<SearchStock>();

	private ArrayList<SearchStock> searchtlist = new ArrayList<SearchStock>();

	private ArrayList<SearchStock> addlist = new ArrayList<SearchStock>();

	private View headView = null;

	private MyAdapter adapter = null;

	private boolean isSearch = true;

	private ProgressBar progressBar;

	private int type = 0;

	private KeybordPopupWindow keybordPopupWindow = null;

	private int sw = 0;

	@SuppressWarnings({ "unchecked", "deprecation" })
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_gegu_layout);
		inflater = LayoutInflater.from(mContext);
		addlist = (ArrayList<SearchStock>) getIntent().getSerializableExtra(
				"stocklist");
		sw = getWindowManager().getDefaultDisplay().getWidth();
		keybordPopupWindow = new KeybordPopupWindow(mContext, sw);
		keybordPopupWindow.setOnSureListener(this);
		initData();
	}

	public void initData() {
		searchEditText = (EditText) findViewById(R.id.search_view);
		searchEditText.addTextChangedListener(new MyTextSwicher());
		searchEditText.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED) {
					searchEditText.setInputType(InputType.TYPE_NULL);
					keybordPopupWindow.setEdittext(searchEditText);
					keybordPopupWindow.showkeybordPopupWindow();
				}
				return false;
			}
		});
		progressBar = (ProgressBar) findViewById(R.id.my_stock_search_bar);
		progressBar.setVisibility(View.GONE);
		cancelTextView = (TextView) findViewById(R.id.search_cancle);
		cancelTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent();
				Bundle bundle = new Bundle();
				bundle.putSerializable("stocklist", addlist);
				intent.putExtras(bundle);
				setResult(1002, intent);
				finish();
			}
		});
		contentlayout = (LinearLayout) findViewById(R.id.contentlayout);
		listView = (XListView) findViewById(R.id.contentlistview);
		listView.setPullLoadEnable(false);
		listView.setPullRefreshEnable(false);
		adapter = new MyAdapter();
		listView.setAdapter(adapter);
		headView = inflater.inflate(R.layout.search_gegu_head_layout, null);
		addlayout = (LinearLayout) headView.findViewById(R.id.addlayout);
		searchTextView = (TextView) headView.findViewById(R.id.search_tv);
		if (addlist.size() > 0) {
			contentlayout.setVisibility(View.VISIBLE);
		} else {
			contentlayout.setVisibility(View.GONE);
		}
		setHeadView();
		handler.sendEmptyMessageDelayed(1001, 120);
	}

	private void setHeadView() {
		listView.removeHeaderView(headView);
		addlayout.removeAllViews();
		for (int i = 0; i < addlist.size(); i++) {
			View itemView = inflater.inflate(R.layout.search_gegu_item_layout,
					null);
			TextView nameTextView = (TextView) itemView.findViewById(R.id.name);
			nameTextView.setText(addlist.get(i).stockname);
			TextView codeTextView = (TextView) itemView.findViewById(R.id.code);
			codeTextView.setText(addlist.get(i).stockcode);
			ImageView img = (ImageView) itemView.findViewById(R.id.img);
			img.setTag(i);
			img.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					int pos = (Integer) v.getTag();
					for (int j = 0; j < resultlist.size(); j++) {
						if (addlist.get(pos).stockcode.equals(resultlist.get(j).stockcode)) {
							resultlist.get(j).isAdd = false;
							adapter.notifyDataSetChanged();
							break;
						}
					}
					addlist.remove(pos);
					setHeadView();
				}
			});
			addlayout.addView(itemView);
		}
		listView.addHeaderView(headView);
		if (!isSearch && searchtlist.size() > 0) {
			searchTextView.setVisibility(View.VISIBLE);
		} else {
			searchTextView.setVisibility(View.GONE);
		}
	}

	class MyTextSwicher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable arg0) {
			searchEditText.setSelection(searchEditText.getText().toString()
					.trim().length());
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {

		}

		@Override
		public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
			searchstr = s.toString().trim();
			if (searchstr.length() == 0) {
				isSearch = true;
				resultlist.clear();
				adapter.notifyDataSetChanged();
			} else {
				if (isSearch) {
					search();
				} else {
					setListviewData();
				}
			}
		}
	}

	private void search() {
		progressBar.setVisibility(View.VISIBLE);
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					if (Utils.isNumeric(searchstr.substring(0, 1))) {
						searchtlist = (ArrayList<SearchStock>) JsonUtils
								.getSearchMyStock("0", "0",
										searchstr.substring(0, 1));
						type = 0;
					} else if (Utils.isEnglish(searchstr.substring(0, 1))) {
						searchtlist = (ArrayList<SearchStock>) JsonUtils
								.getSearchMyStock("0", "2",
										searchstr.substring(0, 1));
						type = 2;
					} else {
						searchtlist = (ArrayList<SearchStock>) JsonUtils
								.getSearchMyStock("0", "1",
										searchstr.substring(0, 1));
						type = 1;
					}
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			progressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				isSearch = false;
				if (searchtlist.size() == 0) {
					showToast(getResources().getString(R.string.no_search_data));
				} else {
					setListviewData();
				}
				break;

			case Configs.READ_FAIL:
				showToast(getResources().getString(R.string.no_search_data));
				break;

			case 1001:
				keybordPopupWindow.setEdittext(searchEditText);
				keybordPopupWindow.showkeybordPopupWindow();
				break;

			default:
				break;
			}
		};
	};

	private void setListviewData() {
		resultlist.clear();
		if (type == 0) {
			for (int i = 0; i < searchtlist.size(); i++) {
				if (searchtlist.get(i).stockcode.contains(searchstr)) {
					resultlist.add(searchtlist.get(i));
				}
			}
			adapter.notifyDataSetChanged();
		} else if (type == 1) {
			for (int i = 0; i < searchtlist.size(); i++) {
				if (searchtlist.get(i).stockname.contains(searchstr)) {
					resultlist.add(searchtlist.get(i));
				}
			}
			adapter.notifyDataSetChanged();
		}

		for (int i = 0; i < resultlist.size(); i++) {
			for (int j = 0; j < addlist.size(); j++) {
				if (addlist.get(j).stockcode
						.equals(resultlist.get(i).stockcode)) {
					resultlist.get(i).isAdd = true;
				}
			}
		}
		if (addlist.size() > 0 || resultlist.size() > 0) {
			contentlayout.setVisibility(View.VISIBLE);
		} else {
			contentlayout.setVisibility(View.GONE);
		}
		setHeadView();
	}

	public class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return resultlist.size();
		}

		@Override
		public Object getItem(int position) {
			return resultlist.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = inflater.inflate(
						R.layout.search_gegu_item_two_layout, null);
				viewHolder.name = (TextView) convertView
						.findViewById(R.id.name);
				viewHolder.code = (TextView) convertView
						.findViewById(R.id.code);
				viewHolder.ytj = (TextView) convertView.findViewById(R.id.ytj);
				viewHolder.add = (ImageView) convertView.findViewById(R.id.img);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.name.setText(resultlist.get(position).stockname);
			viewHolder.code.setText(resultlist.get(position).stockcode);
			if (resultlist.get(position).isAdd) {
				viewHolder.ytj.setVisibility(View.VISIBLE);
				viewHolder.add.setVisibility(View.GONE);
			} else {
				viewHolder.ytj.setVisibility(View.GONE);
				viewHolder.add.setVisibility(View.VISIBLE);
				viewHolder.add.setTag(position);
				viewHolder.add.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						int pos = (Integer) v.getTag();
						addlist.add(resultlist.get(pos));
						setHeadView();
						resultlist.get(pos).isAdd = true;
						adapter.notifyDataSetChanged();
					}
				});
			}
			return convertView;
		}

		class ViewHolder {
			TextView name;
			TextView code;
			TextView ytj;
			ImageView add;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (null != keybordPopupWindow.keybordPopupWindow
					&& keybordPopupWindow.keybordPopupWindow.isShowing()) {
				keybordPopupWindow.keybordPopupWindow.dismiss();
				keybordPopupWindow.keybordPopupWindow = null;
			} else {
				Intent intent = new Intent();
				Bundle bundle = new Bundle();
				bundle.putSerializable("stocklist", addlist);
				intent.putExtras(bundle);
				setResult(1002, intent);
				finish();
			}
		}
		return false;
	}

	@Override
	public void onSure() {
		Intent intent = new Intent();
		Bundle bundle = new Bundle();
		bundle.putSerializable("stocklist", addlist);
		intent.putExtras(bundle);
		setResult(1002, intent);
		finish();
	}
}
