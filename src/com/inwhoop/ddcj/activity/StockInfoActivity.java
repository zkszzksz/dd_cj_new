package com.inwhoop.ddcj.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.support.v4.widget.EdgeEffectCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;
import com.ant.liao.GifView;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.Fouces;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.fragment.StockFollowFragment;
import com.inwhoop.ddcj.fragment.StockNewInfoFragment;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.GifViewUtil;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.StockUtil;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.MyViewPager;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * 股票详情界面
 * 
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class StockInfoActivity extends BaseFragmentActivity implements
		IXListViewListener, MLocationLinsener {
	public final static int READ_MY_SUCCESS = 20; // 读取网络数据成功
	public final static int READ_MY_FAIL = 21; // 读取网络为空

	public XListView kjListView = null;

	private LayoutInflater inflater = null;

	private OnLoadMoreListener loadMoreListener = null;

	private Fouces fouces;

	/**
	 * 股票详情
	 */
	private TextView nowpTextView = null;
	private TextView increaceTextView = null;
	private TextView increadateTextView = null;
	private TextView nowdTextView = null;
	private TextView yesTextView = null;
	private TextView countTextView = null;
	private TextView highTextView = null;
	private TextView lowTextView = null;
	private TextView totalTextView = null;
	private TextView leftkTextView = null;
	private TextView rightkTextView = null;

	/**
	 * k线图
	 */
	private MyViewPager viewPager = null;
	private List<View> viewList = new ArrayList<View>();
	private List<TextView> linetextviews = new ArrayList<TextView>();
	private List<View> lineviews = new ArrayList<View>();

	/**
	 * 个股关联关注
	 */
	private List<TextView> infotextviews = new ArrayList<TextView>();
	private List<View> infoviews = new ArrayList<View>();
	
	private LinearLayout bottomLayout = null;

	/**
	 * 个股关联最新情报
	 */
	private StockNewInfoFragment infoFragment = null;
	private FragmentTransaction mFt;

	private StockFollowFragment followFragment = null;

	private int space = 0, sw, sh;
	private View view = null;

	private StockInfoBean info = null;
	public StockInfoBean stockBean;// 传递过来的股票对象

	private Button cancelButton = null;

	private boolean isSelect = false;

	private boolean isGo = true;

	private int tag;   //从自选股适配器传过来的tag，标识已经是自选股。但是他的自选股就被写成“取消自选”按钮了

	@SuppressWarnings("deprecation")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.stock_info_layout);
		stockBean = (StockInfoBean) getIntent().getSerializableExtra("bean");
		tag = getIntent().getIntExtra("tag", 0);
		/*
		 * StockInfoBean info = new StockInfoBean(); info.stockcode =
		 * "sh601006"; info.stockname = "大秦铁路"; info.focus = 1; stockBean =
		 * info;
		 */
		fouces = new Fouces();
		inflater = LayoutInflater.from(mContext);
		space = (int) getResources().getDimension(R.dimen.k_line_space);
		sw = getWindowManager().getDefaultDisplay().getWidth();
		sh = getWindowManager().getDefaultDisplay().getHeight();
		initData();
		StockUtil.isStockFouces(stockBean.stockcode, foucehandler, mContext);
	}

	@Override
	public void initData() {
		super.initData();
		setTitleStrText("" + stockBean.stockname + "(" + stockBean.stockcode
				+ ")");
		setLeftLayout(R.drawable.back_btn_selector, false);
		setRightLayout(R.drawable.search_btn_selector, false);
		title_right_layout.setOnClickListener(this);
		kjListView = (XListView) findViewById(R.id.listview);
		kjListView.addHeaderView(setStockInfoView(null));
		kjListView.setAdapter(null);
		kjListView.setPullRefreshEnable(true);
		if("".equals(UserInfoUtil.getUserInfo(mContext).userid)){
			kjListView.setPullLoadEnable(false);
		}else{
			kjListView.setPullLoadEnable(true);
		}
		kjListView.setXListViewListener(this);
	}

	private View setStockInfoView(StockInfoBean bean) {
		view = inflater.inflate(R.layout.stock_real_content_layout, null);
		initStockinfoView();
		setklineTextview(view);
		setViewpager(view);
		setBottomLayout(view);
		setNowPostionKLine(0);
		if(!"".equals(UserInfoUtil.getUserInfo(mContext).userid)){
			setNowPostionInfos(0);
		}
		readStockInfoFromSina();
		readForever();
		return view;
	}

	private void readForever() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isGo) {
					try {
						Thread.sleep(5000);
						readStockInfoFromSina();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

				}
			}
		}).start();
	}

	/**
	 * init view
	 * 
	 * @Title: initStockinfoView
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void initStockinfoView() {
		leftkTextView = (TextView) view.findViewById(R.id.leftk);
		rightkTextView = (TextView) view.findViewById(R.id.rightk);
		nowpTextView = (TextView) view.findViewById(R.id.nowprice_tv);
		increaceTextView = (TextView) view.findViewById(R.id.increace_tv);
		increadateTextView = (TextView) view
				.findViewById(R.id.increace_date_tv);
		nowdTextView = (TextView) view.findViewById(R.id.nowday_tv);
		yesTextView = (TextView) view.findViewById(R.id.yesday_tv);
		countTextView = (TextView) view.findViewById(R.id.cjcount_tv);
		highTextView = (TextView) view.findViewById(R.id.highprice_tv);
		lowTextView = (TextView) view.findViewById(R.id.lowprice_tv);
		totalTextView = (TextView) view.findViewById(R.id.total_tv);
		cancelButton = (Button) view.findViewById(R.id.cancelfree);
		cancelButton.setOnClickListener(this);
		if (tag != 1) {
			if (stockBean.focus == 1) {
				cancelButton.setText("取消自选");
				isSelect = true;
			} else {
				cancelButton.setText("添加自选");
				isSelect = false;
			}
		}

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.cancelfree:
			showProgressDialogFalse("操作中，请稍后...");
			if (!isSelect) {
				startLocation(this);
			} else {
				addOrCancelStock("");
			}
			break;

		case R.id.title_right_layout:
			Act.toAct(mContext, MyStockSearchActivity.class);
			break;

		default:
			break;
		}
	}

	private void readStockInfoFromSina() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					info = JsonUtils.getStockInfo("" + stockBean.stockcode);
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	@SuppressLint("HandlerLeak")
	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			kjListView.stopRefresh();
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				break;
			case Configs.READ_SUCCESS:
				setStockView();
				break;

			case READ_MY_SUCCESS:
				fouces = (Fouces) msg.obj;
				if (fouces.foucs == 1) {
					cancelButton.setText("取消自选");
					isSelect = true;
				} else {
					cancelButton.setText("添加自选");
					isSelect = false;
				}
				break;
			}
		}
	};

    @SuppressLint("HandlerLeak")
    private Handler foucehandler = new Handler() {
        public void handleMessage(Message msg) {
            kjListView.stopRefresh();
            dismissProgressDialog();
            switch (msg.what) {
                case READ_MY_SUCCESS:
                    fouces = (Fouces) msg.obj;
                    if (fouces.foucs == 1) {
                        cancelButton.setText("取消自选");
                        isSelect = true;
                    } else {
                        cancelButton.setText("添加自选");
                        isSelect = false;
                    }
                    break;
            }
        }
    };

	private void setStockView() {
		if (isGo) {
			if(null==info){
				return;
			}
			setTitleStrText("" + info.stockname + "(" + stockBean.stockcode
					+ ")");
			nowpTextView.setText("" + info.nowPrice);
			if (info.addPrice > 0) {
				increaceTextView.setTextColor(getResources().getColor(
						R.color.stock_price_color));
				increadateTextView.setTextColor(getResources().getColor(
						R.color.stock_price_color));
				leftkTextView.setTextColor(getResources().getColor(
						R.color.stock_price_color));
				rightkTextView.setTextColor(getResources().getColor(
						R.color.stock_price_color));
				nowpTextView.setTextColor(getResources().getColor(
						R.color.stock_price_color));
			} else if (info.addPrice < 0) {
				increaceTextView.setTextColor(getResources().getColor(
						R.color.stock_increa_date_color));
				increadateTextView.setTextColor(getResources().getColor(
						R.color.stock_increa_date_color));
				leftkTextView.setTextColor(getResources().getColor(
						R.color.stock_increa_date_color));
				rightkTextView.setTextColor(getResources().getColor(
						R.color.stock_increa_date_color));
				nowpTextView.setTextColor(getResources().getColor(
						R.color.stock_increa_date_color));
			} else {
				increaceTextView.setTextColor(getResources().getColor(
						R.color.white));
				increadateTextView.setTextColor(getResources().getColor(
						R.color.white));
				leftkTextView.setTextColor(getResources().getColor(
						R.color.white));
				rightkTextView.setTextColor(getResources().getColor(
						R.color.white));
				nowpTextView.setTextColor(getResources()
						.getColor(R.color.white));
			}
			increaceTextView.setText("" + info.addPrice);
			increadateTextView.setText("" + info.addRate + "%");
			nowdTextView.setText("" + info.nowDayOP);
			yesTextView.setText("" + info.yestprice);
			countTextView.setText("" + info.volume + "万手");
			highTextView.setText("" + info.highPrice);
			lowTextView.setText("" + info.lowPrice);
			totalTextView.setText("" + info.total + "万");
		}
	}

	private void setNowPostionKLine(int position) {
		for (int i = 0; i < linetextviews.size(); i++) {
			linetextviews.get(i).setTextColor(
					getResources().getColor(R.color.k_line_tv));
			lineviews.get(i).setBackgroundDrawable(null);
		}
		linetextviews.get(position).setTextColor(
				getResources().getColor(R.color.k_line_tv_select));
		lineviews.get(position).setBackgroundResource(R.drawable.pic_line12x);
	}

	private void setklineTextview(View view) {
		TextView hourtv = (TextView) view.findViewById(R.id.fhour);
		TextView daytv = (TextView) view.findViewById(R.id.fday);
		TextView weektv = (TextView) view.findViewById(R.id.fweek);
		TextView monthtv = (TextView) view.findViewById(R.id.fmonth);
		linetextviews.add(hourtv);
		linetextviews.add(daytv);
		linetextviews.add(weektv);
		linetextviews.add(monthtv);
		View hourline = view.findViewById(R.id.line_hour);
		View dayline = view.findViewById(R.id.line_day);
		View weekline = view.findViewById(R.id.line_week);
		View monthline = view.findViewById(R.id.line_month);
		lineviews.add(hourline);
		lineviews.add(dayline);
		lineviews.add(weekline);
		lineviews.add(monthline);
		for (int i = 0; i < linetextviews.size(); i++) {
			linetextviews.get(i).setTag(i);
			linetextviews.get(i).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					int pos = (Integer) v.getTag();
					viewPager.setCurrentItem(pos);
					setNowPostionKLine(pos);
				}
			});
		}
	}

	@SuppressWarnings("deprecation")
	private void setViewpager(View view) {
		viewList = new ArrayList<View>();
		viewPager = (MyViewPager) view.findViewById(R.id.kline_viewpager);
		LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, getWindowManager()
						.getDefaultDisplay().getHeight() / 3);
		viewPager.setLayoutParams(parm);
		for (int i = 0; i < 4; i++) {
			View itemview = inflater.inflate(
					R.layout.kline_viewpager_item_layout, null);
			viewList.add(itemview);
			GifView img = (GifView) itemview.findViewById(R.id.itemimg);
			img.setShowDimension(sw - 2 * space, sh / 3 - 2 * space);
			final String url = GifViewUtil.getKLineUrl(i, stockBean.stockcode
					+ ".gif");
			GifViewUtil.setGifViewNetUrl(img, url);
			img.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Intent intent = new Intent(mContext,
							KlineScreenActivity.class);
					intent.putExtra("strurl", url);
					startActivity(intent);
				}
			});
		}
		viewPager.setAdapter(new MyPageAdapter());
		viewPager.setOnPageChangeListener(new MyOnPageChangeListener());
	}

	private void setBottomLayout(View view) {
		bottomLayout = (LinearLayout) view.findViewById(R.id.stock_bottomlayout);
		if("".equals(UserInfoUtil.getUserInfo(mContext).userid)){
			bottomLayout.setVisibility(View.GONE);
			return;
		}
		TextView infoTextView = (TextView) view.findViewById(R.id.infotv);
		TextView followTextView = (TextView) view.findViewById(R.id.followtv);
		infotextviews.add(infoTextView);
		infotextviews.add(followTextView);
		View infoline = view.findViewById(R.id.line_info);
		View followline = view.findViewById(R.id.line_follow);
		infoviews.add(infoline);
		infoviews.add(followline);
		for (int i = 0; i < infotextviews.size(); i++) {
			infotextviews.get(i).setTag(i);
			infotextviews.get(i).setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					int pos = (Integer) v.getTag();
					setNowPostionInfos(pos);
				}
			});
		}
	}

	private void setNowPostionInfos(int position) {
		for (int i = 0; i < infotextviews.size(); i++) {
			infotextviews.get(i).setTextColor(
					getResources().getColor(R.color.k_line_tv));
			infoviews.get(i).setBackgroundDrawable(null);
		}
		infotextviews.get(position).setTextColor(
				getResources().getColor(R.color.k_line_tv_select));
		infoviews.get(position).setBackgroundResource(R.drawable.pic_line12x);
		mFt = getSupportFragmentManager().beginTransaction();
		if (null != infoFragment) {
			mFt.hide(infoFragment);
		}
		if (null != followFragment) {
			mFt.hide(followFragment);
		}
		if (position == 0) {
			if (null == infoFragment) {
				infoFragment = new StockNewInfoFragment();
				mFt.add(R.id.stockframe, infoFragment);
			} else {
				mFt.show(infoFragment);
				infoFragment.setOnloadMorelistener();
			}
		} else {
			if (null == followFragment) {
				followFragment = new StockFollowFragment();
				mFt.add(R.id.stockframe, followFragment);
			} else {
				mFt.show(followFragment);
				followFragment.setNowPostionKLine(followFragment.nowposition);
			}
		}
		mFt.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		mFt.commit();
	}

	@Override
	public void onRefresh() {
		readStockInfoFromSina();
	}

	@Override
	public void onLoadMore() {
		loadMore();
	}

	private class MyOnPageChangeListener implements OnPageChangeListener {
		private EdgeEffectCompat leftEdge;
		private EdgeEffectCompat rightEdge;

		public MyOnPageChangeListener() {
			try {
				Field leftEdgeField = viewPager.getClass().getDeclaredField(
						"mLeftEdge");
				Field rightEdgeField = viewPager.getClass().getDeclaredField(
						"mRightEdge");
				if (leftEdgeField != null && rightEdgeField != null) {
					leftEdgeField.setAccessible(true);
					rightEdgeField.setAccessible(true);
					leftEdge = (EdgeEffectCompat) leftEdgeField.get(viewPager);
					rightEdge = (EdgeEffectCompat) rightEdgeField
							.get(viewPager);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		@Override
		public void onPageScrollStateChanged(int arg0) {

		}

		@Override
		public void onPageScrolled(int arg0, float arg1, int arg2) {
			if (leftEdge != null && rightEdge != null) {
				leftEdge.finish();
				rightEdge.finish();
				leftEdge.setSize(0, 0);
				rightEdge.setSize(0, 0);
			}
		}

		@Override
		public void onPageSelected(int position) {
			setNowPostionKLine(position);
		}
	}

	class MyPageAdapter extends PagerAdapter {

		@Override
		public void destroyItem(ViewGroup container, int position, Object object) {
			container.removeView((View) object);
		}

		@Override
		public int getCount() {
			return viewList.size();
		}

		@Override
		public boolean isViewFromObject(View arg0, Object arg1) {
			return (arg0 == arg1);
		}

		@Override
		public int getItemPosition(Object object) {
			return POSITION_NONE;
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			((ViewPager) container).addView(viewList.get(position));
			return viewList.get(position);
		}
	}

	@Override
	public void mLocationSucess(String address) {
		addOrCancelStock(address);
	}

	@Override
	public void mLocationFaile(String errorInfo) {

	}

	private void addOrCancelStock(final String address) {
		StockInfoBean info = new StockInfoBean();
		info.stockcode = stockBean.stockcode;
		info.stockname = stockBean.stockname;
		info.position = address;
		if (isSelect) {
			StockUtil.deleteStockInfoToDB(info, addHandler, mContext, null,
					null, "", 0);
		} else {
			StockUtil.addStockInfoToDB(info, addHandler, mContext, null, null,
					"", 0);
		}
	}

	@SuppressLint("HandlerLeak")
	private Handler addHandler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("操作失败，请稍后再试");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if (!(Boolean) obj[0]) {
					showToast(obj[1].toString());
				} else {
					if (isSelect) {
						isSelect = false;
						cancelButton.setText("添加自选");
					} else {
						isSelect = true;
						cancelButton.setText("取消自选");
					}
				}
				break;

			default:
				break;
			}
		};
	};

	public interface OnLoadMoreListener {
		public void onLoadMore();
	}

	public void setLoadMoreListener(OnLoadMoreListener loadMoreListener) {
		this.loadMoreListener = loadMoreListener;
	}

	public void loadMore() {
		if (null != loadMoreListener) {
			loadMoreListener.onLoadMore();
		}
	}

}
