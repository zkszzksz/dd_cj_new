package com.inwhoop.ddcj.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.Utils;

/**
 * 申请建群资格
 * 
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class ApplySetGroupActivity extends BaseFragmentActivity {
    private TextView userAgree;
	private ImageView imgBg;
	private EditText gname;
	private RadioGroup radioGroup;
	private RadioButton radioZcrb;
	private RadioButton radioMtrb;
	private RadioButton radioHotrb;
	private EditText gintro;
	private EditText introduce;
	private Context context;
	private UserBean userBean;
	private String imageCode = "";
	private int radioTag = 1;
	private Object[] objects;
	private RelativeLayout createBtn;
	private CheckBox check;
	private boolean checkTag = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.apply_creategroup_layout);
		context = ApplySetGroupActivity.this;
		initData();
	}

    @SuppressLint("WrongViewCast")
	@Override
    public void initData() {
        super.initData();
        setTitleStrText("申请建群资格");
        setLeftLayout(R.drawable.back_btn_selector, false);
        setRightLayout(R.string.create, true);
        createBtn = (RelativeLayout) findViewById(R.id.title_right_layout);
        createBtn.setOnClickListener(this);
        userBean = UserInfoUtil.getUserInfo(context);
        imgBg = (ImageView) findViewById(R.id.upimg);
        gname = (EditText) findViewById(R.id.gname);
        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioZcrb = (RadioButton) findViewById(R.id.zcrb);
        radioMtrb = (RadioButton) findViewById(R.id.mtrb);
        radioHotrb = (RadioButton) findViewById(R.id.hotrb);
        gintro = (EditText) findViewById(R.id.gintro);
        introduce = (EditText) findViewById(R.id.introduce);
        check = (CheckBox) findViewById(R.id.ty_box);
        userAgree = (TextView) findViewById(R.id.user_agree);
        userAgree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ApplySetGroupActivity.this, OtherWebviewActivity.class);
                i.putExtra("pos", 1);
                startActivity(i);
            }
        });

		imgBg.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showCameraDialog(handler, true, 210, 100, true);
			}
		});
		radioGroup
				.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
					@Override
					public void onCheckedChanged(RadioGroup radioGroup, int i) {
						switch (i) {
						case R.id.zcrb:
							radioTag = 1;
							break;
						case R.id.mtrb:
							radioTag = 2;
							break;
						case R.id.hotrb:
							radioTag = 3;
							break;
						}
					}
				});
		check.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton compoundButton,
					boolean b) {
				if (b) {
					checkTag = true;
				} else {
					checkTag = false;
				}
			}
		});

		createBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				createGroup();
			}
		});
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case RESULT_CUT_IMG:
				imgBg.setImageBitmap((Bitmap) msg.obj);
				imageCode = Utils.bitmapToString((Bitmap) msg.obj);
				break;
			}
		}
	};

	private void createGroup() {
		if (null == imageCode || imageCode.equals("")) {
			Toast.makeText(context, "群组背景图不能为空", Toast.LENGTH_SHORT).show();
		} else if (TextUtils.isEmpty(gname.getText().toString().trim())) {
			Toast.makeText(context, "群组名称不能为空", Toast.LENGTH_SHORT).show();
		} else if (TextUtils.isEmpty(introduce.getText().toString().trim())) {
			Toast.makeText(context, "群组介绍不能为空", Toast.LENGTH_SHORT).show();
		} else if (gname.getText().toString().trim().length() < 4
				|| gname.getText().toString().trim().length() > 6) {
			Toast.makeText(context, "请输入4-6长度的群组名称", Toast.LENGTH_SHORT)
					.show();
		} else if (!checkTag) {
			Toast.makeText(context, "请阅读大大财经用户协议", Toast.LENGTH_SHORT).show();
		} else {
			applyCreateGroup();
		}
	}

	private void applyCreateGroup() {
		showProgressDialog("正在上传数据，请稍后");
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					objects = JsonUtils.createGroupData(userBean.userid, gname
							.getText().toString().trim(), introduce.getText()
							.toString().trim(), imageCode, radioTag + "");
					if (objects[0].equals("200")) {
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
				} catch (Exception e) {
					e.printStackTrace();
					msg.what = Configs.READ_ERROR;
				}
				mHandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Toast.makeText(context, objects[1] + "", Toast.LENGTH_SHORT)
						.show();
				break;
			case Configs.READ_FAIL:
				Toast.makeText(context, objects[1] + "", Toast.LENGTH_SHORT)
						.show();
				break;
			case Configs.READ_ERROR:
				Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};
}
