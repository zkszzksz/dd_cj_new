package com.inwhoop.ddcj.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.style.ForegroundColorSpan;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.GroupChatinfo;
import com.inwhoop.ddcj.db.HandleGroupchatRecordDB;
import com.inwhoop.ddcj.view.CircleImageview;
import com.inwhoop.ddcj.view.XListView;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;

/**
 * 查看聊天记录
 * 
 * @Project: DDCJ
 * @Title: SearchChatRecordActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2015-1-14 下午2:16:48
 * @Copyright: 2015 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class SearchChatRecordActivity extends BaseFragmentActivity {

	private LayoutInflater inflater = null;

	private EditText searchEditText = null;

	private LinearLayout contentLayout = null;

	private String searchstr = "";

	private TextView cancelTextView;

	private XListView listView = null;

	private MyAdapter adapter = null;

	private ProgressBar progressBar;

	private List<GroupChatinfo> reslist = new ArrayList<GroupChatinfo>();

	private FinalBitmap imgfb = null;

	private HandleGroupchatRecordDB db = null;

	private String groupid = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_gegu_layout);
		inflater = LayoutInflater.from(mContext);
		db = new HandleGroupchatRecordDB(mContext);
		groupid = getIntent().getStringExtra("groupid");
		imgfb = FinalBitmap.create(mContext);
		imgfb.configLoadingImage(R.drawable.home_head);
		imgfb.configLoadfailImage(R.drawable.home_head);
		initData();
	}

	public void initData() {
		searchEditText = (EditText) findViewById(R.id.search_view);
		searchEditText.addTextChangedListener(new MyTextSwicher());
		contentLayout = (LinearLayout) findViewById(R.id.contentlayout);
		progressBar = (ProgressBar) findViewById(R.id.my_stock_search_bar);
		progressBar.setVisibility(View.GONE);
		cancelTextView = (TextView) findViewById(R.id.search_cancle);
		cancelTextView.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				finish();
			}
		});
		listView = (XListView) findViewById(R.id.contentlistview);
		listView.setPullLoadEnable(false);
		listView.setPullRefreshEnable(false);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				Intent intent = new Intent(mContext,
						GroupChatRecordActivity.class);
				intent.putExtra("groupid", reslist.get(position - 1).groupid);
				intent.putExtra("groupname",
						reslist.get(position - 1).groupname);
				intent.putExtra("id", "" + reslist.get(position - 1).id);
				startActivity(intent);
			}
		});
		adapter = new MyAdapter();
		listView.setAdapter(adapter);
		handler.sendEmptyMessageDelayed(1001, 120);
	}

	class MyTextSwicher implements TextWatcher {

		@Override
		public void afterTextChanged(Editable arg0) {
			searchEditText.setSelection(searchEditText.getText().toString()
					.trim().length());
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
				int arg3) {

		}

		@Override
		public void onTextChanged(CharSequence s, int arg1, int arg2, int arg3) {
			searchstr = s.toString().trim();
			if (searchstr.length() == 0) {
				reslist.clear();
				adapter.notifyDataSetChanged();
			} else {
				search();
			}
		}
	}

	private void search() {
		progressBar.setVisibility(View.VISIBLE);
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					reslist.clear();
					List<GroupChatinfo> flist = db.searchChatRec(groupid,
							searchstr, mContext);
					addReslist(flist);
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private void addReslist(List<GroupChatinfo> nlist) {
		for (int i = 0; i < nlist.size(); i++) {
			reslist.add(nlist.get(i));
		}
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			progressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				if (reslist.size() == 0) {
					showToast("没有匹配的聊天记录");
					contentLayout.setVisibility(View.GONE);
				} else {
					contentLayout.setVisibility(View.VISIBLE);
				}
				adapter.notifyDataSetChanged();
				break;

			case Configs.READ_FAIL:
				showToast(getResources().getString(R.string.no_search_data));
				break;

			default:
				break;
			}
		};
	};

	public class MyAdapter extends BaseAdapter {

		@Override
		public int getCount() {
			return reslist.size();
		}

		@Override
		public Object getItem(int position) {
			return reslist.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			final ViewHolder viewHolder;
			if (null == convertView) {
				viewHolder = new ViewHolder();
				convertView = inflater.inflate(
						R.layout.search_chatrec_item_layout, null);
				viewHolder.name = (TextView) convertView
						.findViewById(R.id.cname);
				viewHolder.contet = (TextView) convertView
						.findViewById(R.id.content);
				viewHolder.head = (CircleImageview) convertView
						.findViewById(R.id.headimg);
				convertView.setTag(viewHolder);
			} else {
				viewHolder = (ViewHolder) convertView.getTag();
			}
			viewHolder.name.setText("" + reslist.get(position).usernick);
			viewHolder.contet.setText(setSpan(reslist.get(position).content));
			imgfb.display(viewHolder.head, reslist.get(position).userheadpath);
			return convertView;
		}

		class ViewHolder {
			TextView name;
			TextView contet;
			CircleImageview head;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (getWindow().getAttributes().softInputMode == WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED) {
				// 隐藏软键盘
				getWindow().setSoftInputMode(
						WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
			} else {
				finish();
			}
		}
		return false;
	}

	public SpannableString setSpan(String str) {
		String seran = searchstr.toLowerCase();
		str = str.toLowerCase();
		String search = str;
		SpannableString ss = new SpannableString(str);
		String first = seran.substring(0, 1);
		int length = seran.length();
		int start = 0;
		int end = 0;
		String mm = "";
		while(!"".equals(search)&&search.indexOf(first)!=-1){
			end = search.indexOf(first);
			if(end!=0){
				search = search.substring(search.indexOf(first),search.length());
				start = start+end;
			}
			if(search.indexOf(first)+length>str.length()){
				search = "";
			}else{
				mm = search.substring(search.indexOf(first), search.indexOf(first)+length);
				if(mm.equals(seran)){
					ss.setSpan(new ForegroundColorSpan(Color.RED), start,start+length,
							Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
					start = start+search.indexOf(first)+length;
					search = search.substring(length,search.length());
				}else{
					search = search.substring(1,search.length());
					start = start+1;
				}
			}
		}
		return ss;
	}
}
