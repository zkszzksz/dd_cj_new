package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.*;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.SearchFriendListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.FriendMsgBean;
import com.inwhoop.ddcj.bean.SearchFriendBean;
import com.inwhoop.ddcj.util.JsonUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * package_name: com.inwhoop.ddcj.activity Created with IntelliJ IDEA User:
 * Administrator Date: 2014/12/9 Time: 13:35
 */
public class FriendSearchActivity extends Activity implements
		View.OnClickListener {
	private EditText inputEdit;
	private ProgressBar progressBar;
	private TextView cancelBtn;
	private ListView listView;
	private List<SearchFriendBean> list = new ArrayList<SearchFriendBean>();
	private Context context;
	private SearchFriendListAdapter adapter;
    private FriendMsgBean bean;

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.friend_search_activity);
		context = FriendSearchActivity.this;
		initView();
	}

	private void initView() {
		inputEdit = (EditText) findViewById(R.id.search_edit);
		progressBar = (ProgressBar) findViewById(R.id.search_progress);
		cancelBtn = (TextView) findViewById(R.id.cancel_btn);
		cancelBtn.setOnClickListener(this);
		listView = (ListView) findViewById(R.id.search_list);
		adapter = new SearchFriendListAdapter(context);
		listView.setAdapter(adapter);
		inputEdit.addTextChangedListener(new TextWatcher() {
			@Override
			public void beforeTextChanged(CharSequence charSequence, int i,
					int i1, int i2) {

			}

			@Override
			public void onTextChanged(CharSequence charSequence, int i, int i1,
					int i2) {
				String content = charSequence.toString();
				if (!TextUtils.isEmpty(content)) {
					getSearchData();
				}
			}

			@Override
			public void afterTextChanged(Editable editable) {

			}
		});
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.cancel_btn:
			finish();
			break;
		}
	}

	private void getSearchData() {
		progressBar.setVisibility(View.VISIBLE);
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					bean = JsonUtils.searchFriend(inputEdit.getText()
							.toString().trim());
                    if (null != bean && null!=bean.list&&bean.list.size() > 0) {
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("异常信息为" + e.toString());
					msg.what = Configs.READ_ERROR;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			progressBar.setVisibility(View.GONE);
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				adapter.addList(list);
				adapter.notifyDataSetChanged();
				break;
			case Configs.READ_FAIL:
                Toast.makeText(context, bean.msg, Toast.LENGTH_SHORT)
                        .show();
				break;
			case Configs.READ_ERROR:
				Toast.makeText(context, bean.msg, Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

}