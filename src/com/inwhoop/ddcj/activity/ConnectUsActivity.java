package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

import com.inwhoop.R;
import com.inwhoop.ddcj.fragment.ChannelFragment.MyPagerAdapter;
import com.inwhoop.ddcj.view.CircleImageview;
import com.inwhoop.ddcj.view.PagerSlidingTabStrip;
import com.inwhoop.ddcj.view.SlidButton;

/**
 * 
 * 联系我们
 * 
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ConnectUsActivity extends BaseFragmentActivity {

	private WebView webView; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.connect_us_layout);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleStrText("联系我们");
		setLeftLayout(R.drawable.back_btn_selector, false);
		webView=(WebView) findViewById(R.id.mapimg);
		WebSettings webSettings=webView.getSettings();
		webSettings.setJavaScriptEnabled(true);
		webView.loadUrl("file:///android_asset/about.html");
	}

}
