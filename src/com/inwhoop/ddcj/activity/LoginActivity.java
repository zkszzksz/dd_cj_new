package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformDb;
import cn.sharesdk.framework.utils.UIHandler;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.impl.AlertLoginLisenner;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.service.XmppService;
import com.inwhoop.ddcj.util.*;

import java.util.HashMap;
import java.util.Set;

/**
 * @Describe: TODO 登录 * * * ****** Created by ZK ********
 * @Date: 2014/10/20 18:18
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class LoginActivity extends BaseOtherLoginActivity implements
		MLocationLinsener {
	private static final int MY_LOGIN_OK = 6;
	private static final int MY_LOGIN_WRONG = 7;
	private static final String TAG = "JPUSH";

	private EditText userET, pwdET;
	private UserBean userInfoBean = new UserBean();
	private View[] loginBtns;
	private String whatLogin = "";
	private TextView loginBtn;
	private String bindStr = "绑定登录";
	private String openId = "";// 登录第三后，得到的
	private String act = "";

	@Override
	protected void onPause() {
		super.onPause();
		KeyBoard.HiddenInputPanel(mContext);
		dismissProgressDialog();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_log_in);
		act = getIntent().getStringExtra("act");
		initData();
		startLocation(this);
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.log_in);

		loginBtn = (TextView) findViewById(R.id.login_btn);
		loginBtn.setOnClickListener(this);

		findViewById(R.id.forget_layout).setOnClickListener(this);

		userET = (EditText) findViewById(R.id.email_et);
		pwdET = (EditText) findViewById(R.id.pwd_et);
		pwdET.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId,
					KeyEvent event) {
				if (actionId == EditorInfo.IME_ACTION_GO)
					login();
				return true;
			}
		});
		// userET.setText("15828101225");// 15900000008 790007147@qq.com
		// pwdET.setText("123456"); // 111111

		loginBtns = new View[] { findViewById(R.id.sinaweibo_btn),
				findViewById(R.id.qq_btn), findViewById(R.id.weixin_btn), };
		for (View v : loginBtns) {
			v.setOnClickListener(this);
		}

		TelephonyManager tm = (TelephonyManager) LoginActivity.this
				.getSystemService(Context.TELEPHONY_SERVICE);
		String id = tm.getDeviceId();
		// System.out.println("设备ID为=====" + id);
		JPushInterface.setAlias(this, id, mAliasCallback);
	}

	private final TagAliasCallback mAliasCallback = new TagAliasCallback() {
		public void gotResult(int code, String alias, Set<String> tags) {
			String logs;
			switch (code) {
			case 0:
				logs = "Set tag and alias success";
				Log.i(TAG, logs);
				// 建议这里往 SharePreference 里写一个成功设置的状态。成功设置一次后，以后不必再次设置了。
				break;
			case 6002:
				logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
				Log.i(TAG, logs);
				// 延迟 60 秒来调用 Handler 设置别名
				// mHandler.sendMessageDelayed(mHandler.obtainMessage(MSG_SET_ALIAS,
				// alias), 1000 * 60);
				break;
			default:
				logs = "Failed with errorCode = " + code;
				Log.e(TAG, logs);
			}
		}
	};

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.login_btn:
			login();
			break;
		case R.id.forget_layout: // 忘记密码
			startActivity(new Intent(mContext, ForgetPwdOnedActivity.class));
			break;
		case R.id.sinaweibo_btn:
			showProgressDialog("");
			whatLogin = TYPE_SINA;
			authorize(new SinaWeibo(this));
			break;
		case R.id.qq_btn:
			showProgressDialog("");
			whatLogin = TYPE_QQ;
			authorize(new QZone(this));
			break;
		case R.id.weixin_btn:
			showProgressDialog("");
			whatLogin = TYPE_WEIXIN;
			authorize(new Wechat(this));
			// showToast("微信登录暂未开放");
			break;
		}
	}

	// TODO 判断输入信息以及登录
	private void login() {
		String user = userET.getText().toString();
		String pwd = pwdET.getText().toString();
		if ("".equals(user)) {
			ToastUtils.showShort(mContext, R.string.please_put_username);
			return;
		} else if ("".equals(pwd)) {
			ToastUtils.showShort(mContext, R.string.please_put_pwd);
			return;
		}
		userInfoBean.tel = user;
		userInfoBean.pwd = pwd;
		userInfoBean.udid = Utils.getPhoneUdid(mContext);

		showProgressDialog("");
		if (bindStr.equals(loginBtn.getText())) { // 目前绑定在另外个界面
			userInfoBean.type = whatLogin;
			userInfoBean.openid = openId;
			bindUser(handle, userInfoBean);
		} else
			loginUser(handle, userInfoBean);

	}

	// TODO 登录接口
	public static void loginUser(final Handler handle, final UserBean bean) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.loginUser(bean);
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					LogUtil.e("exception,login" + e.toString());
					msg.what = Configs.READ_FAIL;
				}
				handle.sendMessage(msg);
			}
		}).start();
	}

	private Handler handle = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case BIND_OK: // 绑定
			case Configs.READ_SUCCESS: // 普通登录
				Object[] xx = (Object[]) msg.obj;
				if ((Boolean) xx[0]) {
					String oldNormalPwd = userInfoBean.pwd;
					if (null != xx[2])
						userInfoBean = (UserBean) xx[2];
					userInfoBean.oldNormalPwd = oldNormalPwd; // 目前直接用加密后返回的密码，用来登录openfire
					UserInfoUtil.rememberUserInfo(mContext, userInfoBean);
					ToastUtils.showShort(mContext, "登录成功");
					if (null != act && act.equals("startactivity")) {
						Intent intent1 = new Intent(LoginActivity.this, XmppService.class);
						startService(intent1);
						Act.toAct(mContext, MainActivity.class);
					}else{
						setResult(Configs.RESULT_CODE);
					}
					finish();
				} else
					ToastUtils.showLong(mContext, xx[1] + "");
				break;
			case Configs.READ_FAIL:
				ToastUtils.showShort(mContext, "数据异常");
				break;
			}
		}
	};

	@Override
	public void mLocationSucess(String address) {
		userInfoBean.address = address;
	}

	@Override
	public void mLocationFaile(String errorInfo) {
	}

	@Override
	public void onComplete(Platform platform, int action,
			HashMap<String, Object> res) {
		if (action == Platform.ACTION_USER_INFOR) {
			UIHandler.sendEmptyMessage(MSG_AUTH_COMPLETE, this);
			login(platform.getName(), platform.getDb().getUserId(), res);
			myLogin(platform);
		}
		// System.out.println("第三方登录数据：" + res);
	}

	private void myLogin(Platform plat) {
		Message msg = new Message();
		try {
			PlatformDb xx = plat.getDb(); // URLEncoder.encode(xx.getUserName(),
			// "utf-8")

			Bundle b = new Bundle(); // 用于保存type和openid到第三方的登陆
			UserBean bbb = new UserBean();
			bbb.type = whatLogin;
			bbb.openid = xx.getUserId();
			bbb.img = xx.getUserIcon();
			bbb.udid = Utils.getPhoneUdid(mContext);
			bbb.address = userInfoBean.address;

			b.putSerializable("bean", bbb);
			msg.setData(b);
			msg.obj = JsonUtils.loginUser(bbb);
			msg.what = MY_LOGIN_OK;
		} catch (Exception e) {
			msg.obj = e.toString();
			msg.what = MY_LOGIN_WRONG;
			LogUtil.e("MY_LOGIN_WRONG," + e.toString());
		}
		UIHandler.sendMessage(msg, this);
	}

	public boolean handleMessage(final Message msg) {
		super.handleMessage(msg);
		dismissProgressDialog();
		System.out.printf("otherlogin:" + msg.what + "@@@"
				+ System.currentTimeMillis());
		switch (msg.what) {
		case MSG_USERID_FOUND: // 用户信息已存在，正在跳转登录操作，子类必须实现
			// Toast.makeText(this, R.string.userid_found, Toast.LENGTH_SHORT)
			// .show();
			new Thread(new Runnable() {
				@Override
				public void run() {
					myLogin((Platform) msg.obj);
				}
			}).start();
			break;
		case MY_LOGIN_WRONG:
			LogUtil.e("exception,登录失败" + msg.obj);
			ToastUtils.showLong(this, "登录失败");
			break;
		case MY_LOGIN_OK:
			Object[] obs = (Object[]) msg.obj;
			if ((Boolean) obs[0]) {
				UserBean getBean = (UserBean) obs[2];
				// getBean.flag = whatLogin;
				UserInfoUtil.rememberUserInfo(this, getBean);
				ToastUtils.showShort(this, "登录成功");
				if (null != act && act.equals("startactivity")) {
					Act.toAct(mContext, MainActivity.class);
				}else{
					setResult(Configs.RESULT_CODE);
				}
				finish();
			} else if (obs[1].toString().indexOf("用户没有绑定") != -1) { // 未绑定
				ToastUtils.showLong(this, "" + obs[1]);
				// chooseWhatBindLogin(msg.getData());

				Bundle b = new Bundle();
				b.putSerializable("bean", (UserBean) msg.getData()
						.getSerializable("bean"));
				Act.toAct(mContext, LoginBindActivity.class, b);
			}
			break;
		}
		return true;
	}
}
