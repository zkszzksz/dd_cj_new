package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.CacheUtil;
import com.inwhoop.ddcj.util.DialogShowStyle;
import com.inwhoop.ddcj.util.ToastUtils;

/**
 * 功能设置界面
 *
 * @author ZOUXU
 */
public class SettingFuncationActivity extends Activity implements
        OnClickListener {

    private Context ctx;
    private DialogShowStyle dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.setting_function_acvtivity);

        findViewById(R.id.title_left_tv).setOnClickListener(this);
        findViewById(R.id.SettingFuncationActivity_clear_cache)
                .setOnClickListener(this);
        findViewById(R.id.SettingFuncationActivity_hiding).setOnClickListener(
                this);
        findViewById(R.id.SettingFuncationActivity_info_notify)
                .setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_left_tv:
                finish();
                break;
            case R.id.SettingFuncationActivity_hiding:
                Act.toAct(ctx, SettingStealthActivity.class);
                break;
            case R.id.SettingFuncationActivity_info_notify:
                Act.toAct(ctx, SettingWhatGetMessageActivity.class);
                break;
            case R.id.SettingFuncationActivity_clear_cache:
                dialog();
                break;
            default:
                break;
        }
    }

    protected void dialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setMessage("确认是否清除缓存！");
        builder.setTitle("提示");
        builder.setPositiveButton("确认", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog1, int which) {
                dialog1.dismiss();
                dialog = new DialogShowStyle(ctx, "缓存清理中,请稍候...");
                dialog.dialogShow();
                CacheUtil.clearWebViewCache(ctx);
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Message msg = new Message();
                        try {
                            Thread.sleep(1000);
                            msg.what = Configs.READ_SUCCESS;
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        handler.sendMessage(msg);
                    }
                }).start();

            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Toast.makeText(ctx, "缓存清理成功", Toast.LENGTH_SHORT).show();
                    dialog.dialogDismiss();
                    break;
            }
        }
    };

}
