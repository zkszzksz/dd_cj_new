package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inwhoop.R;
import com.inwhoop.ddcj.bean.MsgNoticeBean;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO 消息提醒 设置 * * * ****** Created by ZK 、 ZOUXU ********
 * @Date: 2014/11/11 16:15
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class SettingWhatGetMessageActivity extends BaseFragmentActivity
        implements OnCheckedChangeListener {
    private MsgNoticeBean settings;
    final Integer[] strs = new Integer[]{R.string.jieshouxinxiaoxitixing,
            R.string.dingyuegengxintixing, R.string.shengyin,
            R.string.zhendong, R.string.tixingshixianshixiaoxineirong,};
    private CheckBox checkBox1;
    private CheckBox checkBox2;
    private CheckBox checkBox3;
    private CheckBox checkBox4;
    private CheckBox checkBox5;
    private CheckBox checkBox6;
    private CheckBox checkBox7;
    private LinearLayout linear1;
    private LinearLayout linear2;

    class Holder {
        TextView title;
        CheckBox checkBox;
    }

    private List<Holder> list = new ArrayList<Holder>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_only_title_layout);
        initData();
    }

    @Override
    public void initData() {
        super.initData();
        // 获取持久化数据
        settings = UserInfoUtil.getInfoNotifyMode(mContext);

        setLeftLayout(R.drawable.back_btn_selector, false);
        setTitleResText(R.string.msg_tixing);

        linear1 = (LinearLayout) findViewById(R.id.linearLayout1);
        linear2 = (LinearLayout) findViewById(R.id.linearLayout2);

        checkBox1 = (CheckBox) findViewById(R.id.chechbox1);
        checkBox2 = (CheckBox) findViewById(R.id.chechbox2);
        checkBox3 = (CheckBox) findViewById(R.id.chechbox3);
        checkBox4 = (CheckBox) findViewById(R.id.chechbox4);
        checkBox5 = (CheckBox) findViewById(R.id.chechbox5);
        checkBox6 = (CheckBox) findViewById(R.id.chechbox6);
        checkBox7 = (CheckBox) findViewById(R.id.chechbox7);

        checkBox1.setOnCheckedChangeListener(this);
        checkBox2.setOnCheckedChangeListener(this);
        checkBox3.setOnCheckedChangeListener(this);
        checkBox4.setOnCheckedChangeListener(this);
        checkBox5.setOnCheckedChangeListener(this);
        checkBox6.setOnCheckedChangeListener(this);
        checkBox7.setOnCheckedChangeListener(this);

        if (settings.checkTag1 == 1) {
            checkBox1.setChecked(true);
            linear1.setVisibility(View.VISIBLE);
        } else {
            checkBox1.setChecked(false);
            linear1.setVisibility(View.GONE);
        }

        if (settings.checkTag2 == 1) {
            checkBox2.setChecked(true);
        } else {
            checkBox2.setChecked(false);
        }
        if (settings.checkTag3 == 1) {
            checkBox3.setChecked(true);
        } else {
            checkBox3.setChecked(false);
        }
        if (settings.checkTag4 == 1) {
            checkBox4.setChecked(true);
            linear2.setVisibility(View.VISIBLE);
        } else {
            checkBox4.setChecked(false);
            linear2.setVisibility(View.GONE);
        }
        if (settings.checkTag5 == 1) {
            checkBox5.setChecked(true);
        } else {
            checkBox5.setChecked(false);
        }
        if (settings.checkTag6 == 1) {
            checkBox6.setChecked(true);
        } else {
            checkBox6.setChecked(false);
        }
        if (settings.checkTag7 == 1) {
            checkBox7.setChecked(true);
        } else {
            checkBox7.setChecked(false);
        }

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        UserInfoUtil.setInfoNotifyMode(mContext, settings);
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        // TODO Auto-generated method stub
        switch (buttonView.getId()) {
            case R.id.chechbox1:
                if (checkBox1.isChecked()) {
                    settings.checkTag1 = 1;
                    linear1.setVisibility(View.VISIBLE);
                    settings.checkTag2 = 1;
                    settings.checkTag3 = 0;
                } else {
                    settings.checkTag1 = 0;
                    linear1.setVisibility(View.GONE);
                    settings.checkTag2 = 0;
                    settings.checkTag3 = 0;
                }
                break;
            case R.id.chechbox2:
                if (checkBox2.isChecked()) {
                    settings.checkTag2 = 1;
                } else {
                    settings.checkTag2 = 0;
                }
                break;
            case R.id.chechbox3:
                if (checkBox3.isChecked()) {
                    settings.checkTag3 = 1;
                } else {
                    settings.checkTag3 = 0;
                }
                break;
            case R.id.chechbox4:
                if (checkBox4.isChecked()) {
                    settings.checkTag4 = 1;
                    linear2.setVisibility(View.VISIBLE);
                    settings.checkTag5 = 1;
                    settings.checkTag6 = 0;
                } else {
                    settings.checkTag4 = 0;
                    linear2.setVisibility(View.GONE);
                    settings.checkTag5 = 0;
                    settings.checkTag6 = 0;
                }
                break;
            case R.id.chechbox5:
                if (checkBox5.isChecked()) {
                    settings.checkTag5 = 1;
                } else {
                    settings.checkTag5 = 0;
                }
                break;
            case R.id.chechbox6:
                if (checkBox6.isChecked()) {
                    settings.checkTag6 = 1;
                } else {
                    settings.checkTag6 = 0;
                }
                break;
            case R.id.chechbox7:
                if (checkBox7.isChecked()) {
                    settings.checkTag7 = 1;
                } else {
                    settings.checkTag7 = 0;
                }
                break;
        }
        UserInfoUtil.setInfoNotifyMode(mContext, settings);
    }
}
