package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.LinearLayout;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.*;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: TODO
 * @Package com.inwhoop.ddcj.activity
 * @Date: 2014/12/24 10:02
 * @version: 1.0
 */
public class WelcomeActivity extends BaseFragmentActivity implements MLocationLinsener {
    protected static final String TYPE_WEIXIN = "3"; // 用于标识何种方式登录
    protected static final String TYPE_QQ = "1";
    protected static final String TYPE_SINA = "2";

    private UserBean userBean;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_first_start);

        int sw = ScreenSizeUtil.getScreenSize(this).screenW;
        int topW = sw * 291 / 377; // 377:291
        findViewById(R.id.logo_view).setLayoutParams(
                new LinearLayout.LayoutParams(topW, topW * 13 / 48)); // 480*130

        userBean = UserInfoUtil.getUserInfo(mContext);
        userBean.udid = Utils.getPhoneUdid(mContext);
        if (!"".equals(userBean.oldNormalPwd))
            userBean.pwd = userBean.oldNormalPwd;
        startLocation(this);

        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                msg.what = 1;
                handler.sendMessageDelayed(msg, 1000);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            switch (msg.what) {
                case 1:
                    if ("".equals(userBean.userid)) {
                        gotoStartAct();
                    } else {
                        if (!"".equals(userBean.qqid)) {
                            userBean.openid = userBean.qqid;
                            userBean.type = TYPE_QQ;
                            userBean.pwd = "";
                        } else if (!"".equals(userBean.sinaid)) {
                            userBean.openid = userBean.sinaid;
                            userBean.type = TYPE_SINA;
                            userBean.pwd = "";
                        } else if (!"".equals(userBean.wxid)) {
                            userBean.openid = userBean.wxid;
                            userBean.type = TYPE_WEIXIN;
                            userBean.pwd = "";
                        }
                        LoginActivity.loginUser(handler, userBean);
                    }
                    break;
                case Configs.READ_SUCCESS:
                    Object[] xx = (Object[]) msg.obj;
                    if ((Boolean) xx[0]) {
                        if (null != xx[2]) {
                            UserBean newBean = (UserBean) xx[2];
                            userBean.pwd = newBean.pwd ; //目前直接用加密后返回的密码，用来登录openfire
                            UserInfoUtil.rememberUserInfo(mContext, userBean);
                            ToastUtils.showShort(mContext, "登录成功");
                            Act.toAct(mContext, MainActivity.class);
                            finish();
                        }
                    } else {
//                        ToastUtils.showLong(mContext, xx[1] + "");
                        gotoStartAct();
                    }
                    break;
                case Configs.READ_FAIL:
                    ToastUtils.showShort(mContext, "数据异常");
                    gotoStartAct();
                    break;
            }


        }
    };

    private void gotoStartAct() {
        Act.toActClearTop(this, StartActivity.class);
        this.finish();
    }

    @Override
    public void mLocationSucess(String address) {
        userBean.address=address;
    }

    @Override
    public void mLocationFaile(String errorInfo) {

    }
}
