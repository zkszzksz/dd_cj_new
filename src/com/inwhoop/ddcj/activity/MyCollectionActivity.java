package com.inwhoop.ddcj.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.ChannelAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ChannelList;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 我的收藏
 * 
 * @Project: DDCJ
 * @Title: MyCollectionActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2015-1-26 下午3:23:15
 * @Copyright: 2015 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class MyCollectionActivity extends BaseFragmentActivity implements
		IXListViewListener, OnItemClickListener {
	private XListView mListView;
	private ChannelAdapter adapter;
	private List<ChannelList> dataList = new ArrayList<ChannelList>();
	private List<ChannelList> tempDataList = new ArrayList<ChannelList>();
	private int index = 0;
	private LinearLayout progressLinear;
	private boolean isLoadMore = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.channel_list);
		initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.my_collect);
		initView();
		getData();
	}

	private void initView() {
		progressLinear = (LinearLayout) findViewById(R.id.progress_linear);
		progressLinear.setVisibility(View.VISIBLE);
		mListView = (XListView) findViewById(R.id.xListView);
		adapter = new ChannelAdapter(mContext);
		mListView.setPullLoadEnable(false);
		mListView.setPullRefreshEnable(true);
		mListView.setXListViewListener(this);
		mListView.setOnItemClickListener(this);
		mListView.setAdapter(adapter);
	}

	private void getData() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				try {
					String uid = UserInfoUtil.getUserInfo(mContext).userid;
					Object[] data = JsonUtils.getMyCollect(uid, Configs.COUNT,
							index);
					if ((Boolean) data[0]) {
						dataList = (List<ChannelList>) data[2];
						tempDataList.addAll(dataList);
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);

				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
            progressLinear.setVisibility(View.GONE);
            switch (msg.what) {
			case Configs.READ_SUCCESS:
				if (dataList.size() >= Configs.COUNT) {
					mListView.setPullLoadEnable(true);
				} else {
					mListView.setPullLoadEnable(false);
				}
				adapter.setPosition(0);
				adapter.addChannelList(dataList, isLoadMore);
				adapter.notifyDataSetChanged();
				break;

			case Configs.READ_FAIL:
				break;
			}
		}
	};

	private void onLoad() {
		mListView.stopRefresh();
		mListView.stopLoadMore();
		mListView.setRefreshTime("刚刚");
	}

	@Override
	public void onRefresh() {
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				isLoadMore = false;
				index = 0;
				tempDataList.clear();
				getData();
				onLoad();
			}
		}, 2000);
	}

	@Override
	public void onLoadMore() {
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				isLoadMore = true;
				try {
					index = Integer.valueOf(dataList.get(dataList.size() - 1).id);
				} catch (Exception e) {
				}
				getData();
				onLoad();
			}
		}, 2000);

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(mContext, NewsDetailActivity.class);
		position -= 1;
		if (position >= 0 && position < tempDataList.size()) {
			intent.putExtra("news_id", tempDataList.get(position).id);
			intent.putExtra("channel_id", tempDataList
					.get(position).channelid.trim());
			intent.putExtra("channel_name",
					tempDataList.get(position).channelname);
			intent.putExtra("news", tempDataList.get(position));
			startActivity(intent);
		}

	}

}