package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.os.Bundle;
import android.widget.ListView;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.NewFriendListAdapter;
import com.inwhoop.ddcj.bean.SearchFriendBean;

import java.util.ArrayList;
import java.util.List;

/**
 * package_name: com.inwhoop.ddcj.activity Created with IntelliJ IDEA User:
 * Administrator Date: 2014/12/9 Time: 17:40
 */
public class NewFriendActivity extends BaseFragmentActivity {
	private ListView listView;
	private Context context;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.new_friend_activity);
		context = NewFriendActivity.this;
		initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.new_friend);
		initView();
	}

	private void initView() {
		listView = (ListView) findViewById(R.id.new_friend_list);
		NewFriendListAdapter adapter = new NewFriendListAdapter(context);
		listView.setAdapter(adapter);

		List<SearchFriendBean> list = new ArrayList<SearchFriendBean>();
		SearchFriendBean bean = null;
		for (int i = 0; i < 5; i++) {
			bean = new SearchFriendBean();
			bean.name = "III" + i;
			list.add(bean);
		}

		adapter.addList(list);
		adapter.notifyDataSetChanged();
	}
}