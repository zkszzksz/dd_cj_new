package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.CircleImageview;
import net.tsz.afinal.FinalBitmap;

/**
 * 大大财经设置的主界面
 *
 * @author ZOUXU
 */
public class SettingActivity extends BaseFragmentActivity implements
        OnClickListener {

    private Context ctx;
    private CircleImageview headimg;
    private TextView userName;
    private TextView userID;
    private FinalBitmap fb;
    private UserBean userBean;
    private LinearLayout accout;
    private TextView userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.setting_acvtivity);
        fb = FinalBitmap.create(this);
        userBean = UserInfoUtil.getUserInfo(this);
        findViewById(R.id.title_left_layout).setOnClickListener(this);
        accout = (LinearLayout) findViewById(R.id.SettingActivity_account);
        accout.setOnClickListener(this);
        TextView textView = (TextView) findViewById(R.id.text_View);
        textView.setText("(用户ID: " + userBean.ddid + ")");
        findViewById(R.id.SettingActivity_funcation).setOnClickListener(this);
        findViewById(R.id.SettingActivity_my).setOnClickListener(this);
        findViewById(R.id.SettingActivity_other).setOnClickListener(this);
        findViewById(R.id.SettingActivity_myInfo).setOnClickListener(this);
        findViewById(R.id.ll_addWeibo).setOnClickListener(this);

        headimg = (CircleImageview) findViewById(R.id.headimg);
        headimg.setBackground(getResources().getColor(R.color.black_474747));
        userName = (TextView) findViewById(R.id.nametv);
        userInfo = (TextView) findViewById(R.id.tv_other_info);
        userID = (TextView) findViewById(R.id.content);

    }

    @Override
    protected void onResume() {
        super.onResume();
        userBean = UserInfoUtil.getUserInfo(this);
        if (null != userBean) {
            if (userBean.ddid.equals("")) {
                userInfo.setText("登录");
            } else {
                userInfo.setText("资料");
            }
            fb.display(headimg, userBean.img);

            if (userBean.ddid.equals("")) {
                userName.setText("未登录");
                userID.setVisibility(View.GONE);
            } else {
                userName.setText(userBean.name);
                userID.setVisibility(View.VISIBLE);
                userID.setText("用户的ID：" + userBean.ddid);
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_left_layout:
                finish();
                break;
            case R.id.SettingActivity_account:
                if (userBean.ddid.equals("")) {
                    Act.toAct(mContext, LoginActivity.class);
                } else {
                    Act.toAct(ctx, SettingAccountActivity.class);
                }
                break;
            case R.id.SettingActivity_funcation:
                if (userBean.ddid.equals("")) {
                    Act.toAct(mContext, LoginActivity.class);
                } else {
                    Act.toAct(ctx, SettingFuncationActivity.class);
                }
                break;
            case R.id.SettingActivity_my:
                if (userBean.ddid.equals("")) {
                    Act.toAct(mContext, LoginActivity.class);
                } else {
                    Act.toAct(ctx, SettingMyActivity.class);
                }
                break;
            case R.id.SettingActivity_other:
                Act.toAct(ctx, SettingOtherActivity.class);
                break;
            case R.id.SettingActivity_myInfo:
                if (userBean.ddid.equals("")) {
                    Act.toAct(mContext, LoginActivity.class);
                } else {
                    Act.toAct(ctx, MyInfoActivity.class);
                }
                break;
            case R.id.ll_addWeibo:
                Intent intent = new Intent(this, OtherWebviewActivity.class);
                intent.putExtra("pos", 4);
                startActivity(intent);
                break;
        }
    }
}
