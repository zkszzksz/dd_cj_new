package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.ValidateListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.JpushBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.bean.ValidateBean;
import com.inwhoop.ddcj.db.PushMsgDb;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dev1 on 2015/1/7.
 */
public class ValidateMsgActivity extends BaseFragmentActivity {
    private ListView listView;
    private Context context;
    private JpushBean jpushBean;
    private ReceiveBroadCast receiveBroadCast;
    private UserBean userBean;
    private List<JpushBean> list;
    private ValidateListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.validate_msg_activity);
        context = ValidateMsgActivity.this;
        initData();
        setLeftLayout(R.drawable.back_btn_selector, false);
        setTitleResText(R.string.yanz);
        initView();
    }

    private void initView() {
        userBean = UserInfoUtil.getUserInfo(context);
        receiveBroadCast = new ReceiveBroadCast();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Configs.PUSH_ACTION6);
        mContext.registerReceiver(receiveBroadCast, filter);
        listView = (ListView) findViewById(R.id.validate_msg_activity_list);
        adapter = new ValidateListAdapter(context);
        listView.setAdapter(adapter);

        list = new PushMsgDb(context).getQbData(userBean.userid);
        adapter.addList(list);
        adapter.notifyDataSetChanged();
    }

    public class ReceiveBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            jpushBean = (JpushBean) intent.getSerializableExtra("pushBean");
            if (null != jpushBean) {
                list.add(0, jpushBean);
                adapter.addList(list);
                adapter.notifyDataSetChanged();
            }
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiveBroadCast);
    }
}