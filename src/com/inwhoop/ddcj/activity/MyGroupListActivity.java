package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.GroupSettingAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.GroupBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.List;

/**
 * @Describe: TODO 设置里的 我的群组 * * * ****** Created by ZK ********
 * @Date: 2014/11/11 11:48
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class MyGroupListActivity extends BaseFragmentActivity {

    private GroupSettingAdapter adapter;
    private UserBean userBean;
    private Context context;
    private List<CateChannel> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_my_froup_list);
        context = MyGroupListActivity.this;
        initData();
    }

    @Override
    public void initData() {
        super.initData();
        userBean = UserInfoUtil.getUserInfo(context);
        setLeftLayout(R.drawable.back_btn_selector, false);
        setTitleResText(R.string.my_groups);

        ListView listview = (ListView) findViewById(R.id.listview);
        adapter = new GroupSettingAdapter(mContext);
        listview.setAdapter(adapter);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle bundle = new Bundle();
                bundle.putSerializable("CateChannel", list.get(i));
                Intent intent = new Intent(context, GroupInfoActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
        getMyGroup();

    }

    private void getMyGroup() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    list = JsonUtils.getUserGroupList(userBean.userid);
                    if (null != list && list.size() > 0) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    adapter.add(list,false);
                    adapter.notifyDataSetChanged();
                    break;
                case Configs.READ_FAIL:
                    break;
                case Configs.READ_ERROR:
                    break;
            }
        }
    };
}
