package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.LinearLayout;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.MyStockListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.LogUtil;
import com.inwhoop.ddcj.util.ToastUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.Utils;
import com.inwhoop.ddcj.view.HVListView;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO 他的自选股界面 * * * ****** Created by ZK ********
 * @Date: 2014/11/07 16:30
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class HisStockListActivity extends BaseFragmentActivity {
	private String touserid;

	private HVListView listview;
	private MyStockListAdapter adapter;
	private UserBean user;
	private String searchCodeStrs = "";
	private List<StockInfoBean> dbOrNetList = new ArrayList<StockInfoBean>();// 读取的本地或服务器下来的自选股，只有code和name
	private boolean isRunning = true; // 是否在resume里跑线程重新查询
	private boolean isUpdate = false;// 是否在重新拉取股票
	private boolean isSinaUpdate = false;// 是否在访问sina查询

	@Override
	public void onResume() {
		super.onResume();
		readData();
	}

	@Override
	public void onStop() {
		super.onStop();
		isRunning = false;
	}

	@Override
	public void onPause() {
		super.onPause();
		isRunning = false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_his_stock_list);
		touserid = getIntent().getStringExtra("touserid");
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.his_stock);
		listview = (HVListView) findViewById(R.id.listview);
		// 设置列头
		listview.mListHead = (LinearLayout) findViewById(R.id.head);
		adapter = new MyStockListAdapter(mContext, listview);
		listview.setAdapter(adapter);
	}

	private void readData() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.getNetMyStock(
							UserInfoUtil.getUserInfo(mContext).userid, touserid);
					msg.what = 1;
				} catch (Exception e) {
					msg.obj = e.toString();
					msg.what = 0;
				}
				nethandler.sendMessage(msg);
			}
		}).start();
	}

	private void readDataFromSina() {
		isRunning = true;
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (isRunning) {
					Message msg = new Message();
					try {
						// 先获取最新自选股后，再去xinna查询具体股票
						msg.obj = JsonUtils.getStockInfoList(searchCodeStrs);
						msg.what = Configs.READ_SUCCESS;
						handler.sendMessage(msg);
					} catch (Exception e) {
						// LogUtil.e("exception,tab0:" + e.toString()); //经常异常
						// java.lang.StringIndexOutOfBoundsException: length=0;
						// regionStart=0; regionLength=1
						msg.what = Configs.READ_FAIL;
						handler.sendMessage(msg);
					}
					try {
						Thread.sleep(3000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Configs.READ_FAIL:
				isSinaUpdate = false;
				break;

			case Configs.READ_SUCCESS:
				if ("".equals(searchCodeStrs)) {
					isSinaUpdate = false;
					return;
				}
				List<StockInfoBean> result = (List<StockInfoBean>) msg.obj;
				if (result.size() != 0) {
					for (int i = 0; i < result.size()
							&& i < adapter.getList().size(); i++) {
						// [{"userid":"6","id":"281","stockcode":"sh601007","orderid":"0","notice":1,"location":"成都市","aliasname":"jz"},{"userid":"6","id":"262","stockcode":"sh601006","orderid":"1","notice":1,"location":"成都市","aliasname":"dqtl"}]
						adapter.getList().get(i).stockname = result.get(i).stockname;
						adapter.getList().get(i).nowDayOP = result.get(i).nowDayOP;
						adapter.getList().get(i).yestprice = result.get(i).yestprice;
						adapter.getList().get(i).nowPrice = result.get(i).nowPrice;
						adapter.getList().get(i).highPrice = result.get(i).highPrice;
						adapter.getList().get(i).lowPrice = result.get(i).lowPrice;
						adapter.getList().get(i).addPrice = result.get(i).addPrice;
						adapter.getList().get(i).swing = result.get(i).swing;
						adapter.getList().get(i).addRate = result.get(i).addRate;
						adapter.getList().get(i).volume = result.get(i).volume;
						adapter.getList().get(i).total = result.get(i).total;

						adapter.getList().get(i).focus = 1;// 标识是自选股
					}
					// adapter.getList().clear();
					adapter.notifyDataSetChanged();
					isSinaUpdate = false;
				}
				break;
			}
		}
	};

	private Handler nethandler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			// searchCodeStrs = "sh601006,sh601001,sh600601,sh601106";
			switch (msg.what) {
			case 1: // success
				Object[] obj = (Object[]) msg.obj;
				searchCodeStrs = "";
				if ((Boolean) obj[0]) {
					dbOrNetList.clear();
					dbOrNetList = (List<StockInfoBean>) obj[2];
					for (int i = 0; i < dbOrNetList.size(); i++) {
						dbOrNetList.get(i).stockname = "未知股票";
						if (i == (dbOrNetList.size() - 1)) {
							searchCodeStrs += dbOrNetList.get(i).stockcode;
						} else {
							searchCodeStrs = searchCodeStrs
									+ dbOrNetList.get(i).stockcode + ",";
						}
					}
					adapter.getList().clear();
					adapter.add(dbOrNetList);
					adapter.notifyDataSetChanged();
					isSinaUpdate = true;
					isUpdate = true;
					readDataFromSina();
				} else {
					if (obj[1].toString().contains("未添加")) {
						adapter.getList().clear();
						adapter.notifyDataSetChanged();
					}
					if (!"nostockcode".equals(Utils.getpreference(mContext,
							"nodata"))) {
						Utils.savePreference(mContext, "nodata", "nostockcode");
						ToastUtils.showShort(mContext, obj[1] + "");
					}
					isUpdate = false;
				}
				break;
			case 0:// wrong
				LogUtil.i("myStock,wrong:" + msg.obj);
				isUpdate = false;
				// ToastUtils.showShort(mContext, msg.obj + "");
				break;
			}
		}
	};

}
