package com.inwhoop.ddcj.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout.LayoutParams;

import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ChannelList;
import com.inwhoop.ddcj.bean.EditQBChannellist;
import com.inwhoop.ddcj.bean.NewsBean;
import com.inwhoop.ddcj.bean.PsClassImg;
import com.inwhoop.ddcj.bean.SearchStock;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.ImageUtil;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.ZipUtil;

/**
 * package_name: com.inwhoop.ddcj.activity Created with IntelliJ IDEA User:
 * Administrator Date: 2014/12/20 Time: 15:06
 */
public class ReprintedInfoActivity extends BaseFragmentActivity implements
		MLocationLinsener {
	private NewsBean newsBean;
	private EditText msgEdit;
	private TextView name;

	private String username = "";

	private String userid = "";

	private String content = "";

	private ArrayList<EditQBChannellist> channellist = null;

	private PopupWindow popupWindow = null;

	private LayoutInflater inflater = null;

	private ArrayList<PsClassImg> imglist = new ArrayList<PsClassImg>();

	private ArrayList<SearchStock> addlist = new ArrayList<SearchStock>();

	private CheckBox checkBox = null;

	private ImageView addImageView, addggImageView;

	private TextView imgsizeTextView, ggsizeTextView;

	private LinearLayout addggLayout = null;

	private String sd1, sd;

	private String zipname = "";

	private String channelid = "";

	private String title = "";

	private EditText titleEditText = null;
	
	private List<CheckBox> boxs = new ArrayList<CheckBox>();

	@SuppressWarnings("unchecked")
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.reprinted_info_activity);
		username = getIntent().getStringExtra("name");
		userid = getIntent().getStringExtra("userid");
		content = getIntent().getStringExtra("content");
		channellist = (ArrayList<EditQBChannellist>) getIntent()
				.getSerializableExtra("channellist");
		inflater = LayoutInflater.from(mContext);
		initData();
		initPopupWindow();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.zhuanzai1);
		setRightLayout(R.string.send, true);
		title_right_layout.setOnClickListener(this);
		if (null != getIntent()) {
			newsBean = (NewsBean) getIntent().getSerializableExtra("newsBean");
		}
		msgEdit = (EditText) findViewById(R.id.sendmsg);
		titleEditText = (EditText) findViewById(R.id.title_eidt);
		name = (TextView) findViewById(R.id.text_time_name);
		if (null != newsBean) {
			msgEdit.setText(newsBean.content);
			name.setText(newsBean.createtime + " 摘自@" + newsBean.name);
		}
		checkBox = (CheckBox) findViewById(R.id.cg_box);
		checkBox.setChecked(true);
		checkBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton view, boolean flag) {
				checkBox.setChecked(true);
				showPopupWindow(view);
			}
		});
		addImageView = (ImageView) findViewById(R.id.qbimg);
		addImageView.setOnClickListener(this);
		imgsizeTextView = (TextView) findViewById(R.id.imgsize);
		addggImageView = (ImageView) findViewById(R.id.addggimg);
		ggsizeTextView = (TextView) findViewById(R.id.ggsize);
		addggLayout = (LinearLayout) findViewById(R.id.addgglayout);
		addggLayout.setOnClickListener(this);
	}

	@SuppressWarnings("deprecation")
	private void initPopupWindow() {
		View view = inflater.inflate(R.layout.select_channel_layout, null);
		popupWindow = new PopupWindow(view, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		ImageView cancel = (ImageView) view.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != popupWindow && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
		Button sureButton = (Button) view.findViewById(R.id.sure);
		sureButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != popupWindow && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
		LinearLayout itemlayout = (LinearLayout) view
				.findViewById(R.id.itemlayout);
		View itemView = null;
		for (int i = 0; i < channellist.size(); i++) {
			itemView = inflater.inflate(R.layout.select_channel_item_layout,
					null);
			CheckBox box = (CheckBox) itemView.findViewById(R.id.item_box);
			boxs.add(box);
			box.setTag(i);
			box.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton v, boolean flag) {
					int pos = (Integer) v.getTag();
					for (int j = 0; j < channellist.size(); j++) {
						channellist.get(j).isselect = false;
						boxs.get(j).setChecked(false);
					}
					channellist.get(pos).isselect = flag;
					boxs.get(pos).setChecked(flag);
				}
			});
			TextView nameTextView = (TextView) itemView.findViewById(R.id.name);
			nameTextView.setText(channellist.get(i).name);
			itemlayout.addView(itemView);
		}
	}

	private void showPopupWindow(View view) {
		popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		Intent intent = null;
		Bundle bundle = null;
		switch (v.getId()) {
		case R.id.qbimg:
			intent = new Intent(mContext, AddPictureActivity.class);
			bundle = new Bundle();
			bundle.putSerializable("imglist", imglist);
			intent.putExtras(bundle);
			startActivityForResult(intent, 1001);
			break;

		case R.id.addgglayout:
			intent = new Intent(mContext, SearchGeguActivity.class);
			bundle = new Bundle();
			bundle.putSerializable("stocklist", addlist);
			intent.putExtras(bundle);
			startActivityForResult(intent, 1002);
			break;

		case R.id.title_right_layout:
			String check = check();
			if ("".equals(check)) {
				startLocation(this);
				// setMyShare();

			} else {
				showToast(check);
			}
			break;

		default:
			break;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 1001) {
			imglist = (ArrayList<PsClassImg>) data
					.getSerializableExtra("imglist");
			if (imglist.size() == 0) {
				imgsizeTextView.setVisibility(View.INVISIBLE);
			} else {
				imgsizeTextView.setVisibility(View.VISIBLE);
				imgsizeTextView.setText("" + imglist.size());
			}
		} else if (resultCode == 1002) {
			addlist = (ArrayList<SearchStock>) data
					.getSerializableExtra("stocklist");
			if (addlist.size() > 0) {
				addggImageView.setVisibility(View.GONE);
				ggsizeTextView.setVisibility(View.VISIBLE);
				ggsizeTextView.setText("" + addlist.size());
			} else {
				addggImageView.setVisibility(View.VISIBLE);
				ggsizeTextView.setVisibility(View.GONE);
			}
		}
	}

	private String check() {
		content = msgEdit.getText().toString().trim();
		title = titleEditText.getText().toString().trim();
		if("".equals(title)){
			return "标题不能为空";
		}
		if (content.length() == 0) {
			return "情报内容不能为空";
		}
		if (addlist.size() == 0) {
			return "请添加关联个股";
		}
		if (!checkIsOnwhere()) {
			return "请选择发布的场所";
		}
		return "";
	}

	private boolean checkIsOnwhere() {
		boolean isf = true;
		for (int i = 0; i < channellist.size(); i++) {
			if (channellist.get(i).isselect) {
				isf = false;
			}
		}
		if (isf) {
			return false;
		}
		return true;
	}

	@Override
	public void mLocationSucess(String address) {
		sendZip(address);
	}

	@Override
	public void mLocationFaile(String errorInfo) {

	}

	private void sendZip(final String address) {
		showProgressDialog("正在上传，请稍后~~");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					if (imglist.size() > 0) {
						zipFile();
					}
					getTypeAndChannelid();
					Object[] obj = JsonUtils.editQibao(
							UserInfoUtil.getUserInfo(mContext).userid,title, sd1,
							"" + 2, content, address, getStocode(), zipname,
							channelid);
					msg.what = Configs.READ_SUCCESS;
					msg.obj = obj;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					showToast("转载成功");
					finish();
				} else {
					showToast("" + obj[1].toString());
				}
				break;

			case Configs.READ_FAIL:
				showToast("转载失败，请稍后再试");
				break;

			default:
				break;
			}
		};
	};

	private String getStocode() {
		String code = "";
		for (int i = 0; i < addlist.size(); i++) {
			if (i == (addlist.size() - 1)) {
				code = code + addlist.get(i).stockcode;
			} else {
				code = code + addlist.get(i).stockcode + ",";
			}
		}
		return code;
	}

	private void zipFile() throws Exception {
		zipname = "" + System.currentTimeMillis();
		sd = Environment.getExternalStorageDirectory() + "/ddcj/qbimg/"
				+ zipname;
		sd1 = Environment.getExternalStorageDirectory() + "/ddcj/qbimg/"
				+ System.currentTimeMillis() + ".zip";
		Bitmap map = null;
		String path = "";
		for (int j = 0; j < imglist.size(); j++) {
			path = imglist.get(j).imgpath;
			if (null != map)
				map.recycle();
			BitmapFactory.Options option = new BitmapFactory.Options();
			option.inJustDecodeBounds = true;
			map = BitmapFactory.decodeFile(path, option);
			int h = option.outHeight;
			int w = option.outWidth;
			if (h > 300 || w > 300) {
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inSampleSize = 2;
				map = BitmapFactory.decodeFile(path, o);
			} else {
				map = BitmapFactory.decodeFile(path);
			}
			ImageUtil.saveBitmapToFile(map, sd,
					path.substring(path.lastIndexOf("/") + 1, path.length()));
		}
		ZipUtil.ZipFolder(sd, sd1);
	}

	private void getTypeAndChannelid() {
		for (int i = 0; i < channellist.size(); i++) {
			if (channellist.get(i).isselect) {
				channelid = channelid + channellist.get(i).id + ",";
			}
		}
		channelid = channelid.substring(0, channelid.lastIndexOf(","));
	}

}