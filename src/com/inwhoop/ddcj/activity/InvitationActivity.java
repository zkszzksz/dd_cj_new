package com.inwhoop.ddcj.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;

import com.inwhoop.R;
import com.inwhoop.ddcj.fragment.InvitationFragment;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.PagerSlidingTabStrip;

/**
 * 邀请函
 * 
 * @Project: DDCJ
 * @Title: InvitationActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-11-26 下午6:36:42
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class InvitationActivity extends BaseFragmentActivity {

	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private MyPagerAdapter adapter;

	private LinearLayout pushLayout = null;

	private String[] titles = { "全部", "同城", "群组", "好友", "关注", "我的" };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.invaldate_list_layout);
		mContext = InvitationActivity.this;
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleStrText("邀请函");
		setLeftLayout(R.drawable.back_btn_selector, false);
		tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		pager = (ViewPager) findViewById(R.id.pager);
		final int pageMargin = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
						.getDisplayMetrics());
		pager.setPageMargin(pageMargin);
		pager.setCurrentItem(0);
		adapter = new MyPagerAdapter(getSupportFragmentManager());
		pager.setAdapter(adapter);
		pager.setOffscreenPageLimit(titles.length);
		tabs.setViewPager(pager);
		pushLayout = (LinearLayout) findViewById(R.id.fabulayout);
		pushLayout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				String ddid = UserInfoUtil.getUserInfo(mContext).ddid;
				if (TextUtils.isEmpty(ddid)) {
					Act.toAct(mContext, LoginActivity.class);
				} else {
					Intent intent = new Intent(mContext,
							ReleaseQiBaoActivity.class);
					startActivity(intent);
				}
			}
		});
	}

	public class MyPagerAdapter extends FragmentPagerAdapter {

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return titles[position];
		}

		@Override
		public int getCount() {
			return titles.length;
		}

		@Override
		public Fragment getItem(int position) {
			int tyPosition = 0;
			if (position >= 2) // 目前需要后台修改添加群组type，所以手动+1
				tyPosition = position - 1;

			return InvitationFragment.newInstance(tyPosition);
		}
	}
}
