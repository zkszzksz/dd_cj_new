package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import android.widget.Toast;

import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.update.ConfigInfo;
import com.inwhoop.ddcj.update.Updata;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.DialogShowStyle;
import com.inwhoop.ddcj.util.ExitAppUtils;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.Utils;

/**
 * 设置界面——其他
 *
 * @author ZOUXU
 */
public class SettingOtherActivity extends Activity implements OnClickListener {

    private Context ctx;
    private View updateLayout;// 更新组件layout
    private DialogShowStyle dialog;
    private PopupWindow checkPopupWindow = null;
    private Updata updata = new Updata();
    private LinearLayout pinfen;
    private UserBean userBean;
    private LinearLayout exitLinear;
    private TextView versionView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = this;
        setContentView(R.layout.setting_other_acvtivity);
        userBean = UserInfoUtil.getUserInfo(ctx);
        findViewById(R.id.title_left_tv).setOnClickListener(this);

        findViewById(R.id.SettingOtherActivity_check_update)
                .setOnClickListener(this);
        findViewById(R.id.SettingOtherActivity_contact_us).setOnClickListener(
                this);
        findViewById(R.id.SettingOtherActivity_deal).setOnClickListener(this);
        exitLinear = (LinearLayout) findViewById(R.id.SettingOtherActivity_exit);
        exitLinear.setOnClickListener(this);
        findViewById(R.id.SettingOtherActivity_grade).setOnClickListener(this);
        findViewById(R.id.SettingOtherActivity_help_feedback)
                .setOnClickListener(this);
        findViewById(R.id.SettingOtherActivity_version)
                .setOnClickListener(this);
        pinfen = (LinearLayout) findViewById(R.id.SettingOtherActivity_grade);
        versionView=(TextView) findViewById(R.id.version_name);
        pinfen.setOnClickListener(this);
        try {
			versionView.setText(Utils.getVersionName(ctx));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        initPopupWindow();
        if (userBean.ddid.equals("")) {
            exitLinear.setVisibility(View.GONE);
        } else {
            exitLinear.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()) {
            case R.id.title_left_tv:
                finish();
                break;
            case R.id.SettingOtherActivity_check_update:
                updateLayout = v;
                setUpdata();
                break;
            case R.id.SettingOtherActivity_contact_us:
                Act.toAct(ctx, ConnectUsActivity.class);
                break;
            case R.id.SettingOtherActivity_exit:
                new AlertDialog.Builder(SettingOtherActivity.this)
                        .setTitle("退出登录")
                        .setMessage("是否退出？")
                        .setPositiveButton("是",
                                new DialogInterface.OnClickListener() {

                                    @Override
                                    public void onClick(DialogInterface arg0,
                                                        int arg1) {
                                        loginout();
                                    }
                                }).setNegativeButton("否", null).show();
                break;
            case R.id.SettingOtherActivity_grade:
                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                break;
            case R.id.SettingOtherActivity_help_feedback:
                i = new Intent(this, OtherWebviewActivity.class);
                i.putExtra("pos", 0);
                startActivity(i);
                break;
            case R.id.SettingOtherActivity_deal:
                i = new Intent(this, OtherWebviewActivity.class);
                i.putExtra("pos", 1);
                startActivity(i);
                break;
            case R.id.SettingOtherActivity_version:
                i = new Intent(this, OtherWebviewActivity.class);
                i.putExtra("pos", 2);
                startActivity(i);
                break;
            default:
                break;
        }
    }

    private void loginout() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils.loginout(UserInfoUtil.getUserInfo(ctx).userid);
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = Configs.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_FAIL:
                    Toast.makeText(ctx, "退出失败", 0).show();
                    break;

                case Configs.READ_SUCCESS:
                    Object[] obj = (Object[]) msg.obj;
                    if ((Boolean) obj[0]) {
                        UserInfoUtil.clear(ctx);
                        try {
                            Act.toAct(ctx, LoginActivity.class);
                        } catch (Exception e) {
                        }
                        sucexithandler.sendEmptyMessageDelayed(0, 100);
                    } else {
                        Toast.makeText(ctx, "退出失败", 0).show();
                    }
                    break;

                default:
                    break;
            }
        }

        ;
    };

    private Handler sucexithandler = new Handler() {
        public void handleMessage(Message msg) {
            Act.toAct(ctx, StartActivity.class);
            finish();
        }

        ;
    };

    @SuppressWarnings("deprecation")
    private void initPopupWindow() {
        View view = LayoutInflater.from(ctx).inflate(
                R.layout.check_version_popupwindow_layout, null);
        checkPopupWindow = new PopupWindow(view, LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT, true);
        checkPopupWindow.setContentView(view);
        checkPopupWindow.setFocusable(true);
        checkPopupWindow.setOutsideTouchable(false);
        checkPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        view.setFocusable(true);
        view.setFocusableInTouchMode(true);
        view.findViewById(R.id.download_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        updata.down(ctx, Configs.UPDATA_SERVER);
                        checkPopupWindow.dismiss();
                    }
                });
        view.findViewById(R.id.cancel_btn).setOnClickListener(
                new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkPopupWindow.dismiss();
                    }
                });
    }

    private void showCheckPopupWindow(View v) {
        checkPopupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
    }

    /*--------------以下为更新----------------*/
    private void setUpdata() {
        if (dialog == null)
            dialog = new DialogShowStyle(ctx, "");
        dialog.dialogShow();
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message message = Message.obtain();
                try {
                    message.obj = updata.getServerVerCode();
                    updataHandler.sendMessage(message);
                } catch (Exception e) {
                    System.out.println("" + e.toString());
                }

            }
        }).start();
    }

    private Handler updataHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            dialog.dialogDismiss();
            if ((Boolean) msg.obj) {
                int vercode = ConfigInfo.getVerCode(ctx);
                if (updata.newVerCode > vercode) {
                    updata.findNewVerCode(ctx);
                    showCheckPopupWindow(updateLayout);
                } else
                    Toast.makeText(ctx, "暂无更新", 0).show();
            } else
                Toast.makeText(ctx, "暂无更新", 0).show();
        }
    };
}
