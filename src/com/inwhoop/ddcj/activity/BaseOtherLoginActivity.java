package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.widget.Toast;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.framework.utils.UIHandler;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.LogUtil;

import java.util.HashMap;

/**
 * @Describe: TODO 第三方登录父类 * * * ****** Created by ZK ********
 * @Date: 2014/11/11 9:42
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public abstract class BaseOtherLoginActivity extends BaseFragmentActivity
		implements PlatformActionListener, Handler.Callback {
	protected static final int MSG_USERID_FOUND = 1;
	protected static final int MSG_LOGIN = 2;
	protected static final int MSG_AUTH_CANCEL = 3;
	protected static final int MSG_AUTH_ERROR = 4;
	protected static final int MSG_AUTH_COMPLETE = 5;

	protected static final String TYPE_WEIXIN = "3"; // 用于标识
	protected static final String TYPE_QQ = "1";
	protected static final String TYPE_SINA = "2";

	@Override
	protected void onDestroy() {
		ShareSDK.stopSDK(this);
		super.onDestroy();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ShareSDK.initSDK(this);
	}

	// ------------------以下为第三方登录------------------
	protected void authorize(Platform plat) {
		if (plat.isValid()) {
			String userId = plat.getDb().getUserId();
			if (!TextUtils.isEmpty(userId)) {
                Message msg= new Message();
                msg.obj=plat;
                msg.what=MSG_USERID_FOUND;
                UIHandler.sendMessage(msg,this);
//				UIHandler.sendEmptyMessage(MSG_USERID_FOUND, this);
				login(plat.getName(), userId, null);
//				myLogin(plat);  //此获取到信息后，本地操作
				return;
			}
		}
		plat.setPlatformActionListener(this);
		// true不使用SSO授权，false使用SSO授权
		plat.SSOSetting(false);
		plat.showUser(null);
	}

	public void onError(Platform platform, int action, Throwable t) {
		if (action == Platform.ACTION_USER_INFOR) {
			UIHandler.sendEmptyMessage(MSG_AUTH_ERROR, this);
		}
		t.printStackTrace();
	}

	public void onCancel(Platform platform, int action) {
		if (action == Platform.ACTION_USER_INFOR) {
			UIHandler.sendEmptyMessage(MSG_AUTH_CANCEL, this);
		}
	}

	protected void login(String plat, String userId,
			HashMap<String, Object> userInfo) {
		Message msg = new Message();
		msg.what = MSG_LOGIN;
		msg.obj = plat;
		UIHandler.sendMessage(msg, this);
	}

	protected static final int BIND_OK = 101;

	// TODO 初始绑定接口
	public static void bindUser(final Handler handle, final UserBean bean) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.bindUser(bean);
					msg.what = BIND_OK;
				} catch (Exception e) {
					LogUtil.e("exception,bind" + e.toString());
					msg.what = Configs.READ_FAIL;
				}
				handle.sendMessage(msg);
			}
		}).start();
	}

	// TODO 登录后的绑定接口
	protected static void loginBindUser(final Handler handle,
			final UserBean bean) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.loginBindUser(bean);
					msg.what = BIND_OK;
				} catch (Exception e) {
					LogUtil.e("exception,bind" + e.toString());
					msg.what = Configs.READ_FAIL;
				}
				handle.sendMessage(msg);
			}
		}).start();
	}

	public boolean handleMessage(Message msg) {
		System.out.printf("otherlogin:base:" + msg.what + "@@@"
				+ System.currentTimeMillis());
		switch (msg.what) {
		case MSG_USERID_FOUND:
			Toast.makeText(this, R.string.userid_found, Toast.LENGTH_SHORT)
					.show();
			break;
		case MSG_LOGIN:
			// String text = getString(R.string.logining, msg.obj);
			// Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
			break;
		case MSG_AUTH_CANCEL:
			Toast.makeText(this, R.string.auth_cancel, Toast.LENGTH_SHORT)
					.show();
			break;
		case MSG_AUTH_ERROR:
			Toast.makeText(this, R.string.auth_error, Toast.LENGTH_SHORT)
					.show();
			break;
		case MSG_AUTH_COMPLETE:
			Toast.makeText(this, R.string.auth_complete, Toast.LENGTH_SHORT)
					.show();
			break;
		}
		return true;
	}
}
