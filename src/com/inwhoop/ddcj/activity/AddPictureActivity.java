package com.inwhoop.ddcj.activity;

import java.util.ArrayList;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.inwhoop.R;
import com.inwhoop.ddcj.bean.PsClassImg;
import com.inwhoop.ddcj.util.Utils;
import com.inwhoop.ddcj.view.MyLayout;

/**
 * 编辑情报
 * 
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: EditQiBaoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * @date 2014-11-12 下午6:58:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class AddPictureActivity extends BaseFragmentActivity implements
		OnClickListener {

	private LayoutInflater inflater = null;

	private MyLayout myLayout = null;

	private View addimgView = null;

	private ArrayList<PsClassImg> imglist = new ArrayList<PsClassImg>();

	private LinearLayout.LayoutParams parm = null;

	private String picpath = "";

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.add_picture_layout);
		inflater = LayoutInflater.from(mContext);
		imglist = (ArrayList<PsClassImg>) getIntent().getSerializableExtra(
				"imglist");
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleStrText("添加图片");
		setLeftLayout(R.drawable.back_btn_selector, false);
		setRightLayout(R.string.save, true);
		title_right_layout.setOnClickListener(this);
		title_right_tv
				.setTextColor(getResources().getColor(R.color.save_color));
		myLayout = (MyLayout) findViewById(R.id.layout);
		@SuppressWarnings("deprecation")
		int picWidth = (int) ((getWindowManager().getDefaultDisplay()
				.getWidth() - 2 * getResources().getDimension(
				R.dimen.k_line_space)) / 3);
		parm = new LinearLayout.LayoutParams(picWidth, picWidth);
		addimgView = inflater.inflate(R.layout.addimg_item_layout, null);
		ImageView img = (ImageView) addimgView.findViewById(R.id.img);
		img.setBackgroundResource(R.drawable.btn_addimg_selector);
		RelativeLayout layout = (RelativeLayout) addimgView
				.findViewById(R.id.imglayout);
		layout.setLayoutParams(parm);
		addimgView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (imglist.size() > 8) {
					showToast("亲，只能添加9张图片O(∩_∩)O~~");
					return;
				}
				showCameraDialog(handler, false, 0, 0, false);
			}
		});
		myLayout.addView(addimgView);
		setImglayout();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			picpath = msg.obj.toString();
			setImglayout();
		}
	};

	private void setImglayout() {
		if (!"".equals(picpath)) {
			PsClassImg img = new PsClassImg();
			img.imgpath = picpath;
			imglist.add(img);
			picpath = "";
		}
		myLayout.removeAllViews();
		myLayout.addView(addimgView);
		for (int i = 0; i < imglist.size(); i++) {
			View item = inflater.inflate(R.layout.addimg_item_layout, null);
			ImageView imgImageView = (ImageView) item.findViewById(R.id.img);
			imgImageView
					.setImageBitmap(Utils.compressImage(imglist.get(i).imgpath));
			RelativeLayout layout = (RelativeLayout) item
					.findViewById(R.id.imglayout);
			layout.setLayoutParams(parm);
			ImageView delImageView = (ImageView) item.findViewById(R.id.delete);
			delImageView.setVisibility(View.VISIBLE);
			delImageView.setTag(i);
			delImageView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					final int pos = (Integer) v.getTag();
					imglist.remove(pos);
					setImglayout();
				}
			});
			myLayout.addView(item);
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (null != dialog && dialog.isShowing()) {
				dialog.cancel();
				dialog = null;
			} else {
				finish();
			}
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.title_right_layout:
			Intent intent = new Intent();
			Bundle bundle = new Bundle();
			bundle.putSerializable("imglist", imglist);
			intent.putExtras(bundle);
			setResult(1001, intent);
			finish();
			break;

		default:
			break;
		}
	}

}
