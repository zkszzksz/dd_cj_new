package com.inwhoop.ddcj.activity;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.PhoneAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.PhoneInfo;
import com.inwhoop.ddcj.bean.SendPhone;
import com.inwhoop.ddcj.bean.SendPhoneData;
import com.inwhoop.ddcj.bean.UserRelation;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;
import android.widget.Toast;

/**
 * 本地通讯录
 * 
 * @Project: DDCJ
 * @Title: LocalPhoneActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-28 下午7:02:01
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class LocalPhoneActivity extends BaseFragmentActivity implements
		OnClickListener {

	private ListView mailListView = null;

	private List<PhoneInfo> list = new ArrayList<PhoneInfo>();// 本地通讯录数据

	private List<PhoneInfo> add_list = new ArrayList<PhoneInfo>();// 存储添加好友状态的数据

	private List<PhoneInfo> invita_list = new ArrayList<PhoneInfo>();// 存储邀请好友状态的数据

	private List<PhoneInfo> f_list = new ArrayList<PhoneInfo>();// 存储已是好友状态的数据

	private List<PhoneInfo> all_list = new ArrayList<PhoneInfo>();// 存储所有状态的数据

	private List<UserRelation> ulist = new ArrayList<UserRelation>();// 返回比对后的数据

	private SendPhoneData sPhoneData;

	private String uid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.phone_list);
		initView();
	}

	private void initView() {
		initData();
		mContext = this;
		sPhoneData = new SendPhoneData();
		setTitleResText(R.string.contanct);
		setLeftLayout(R.drawable.back_btn_selector, false);
		mailListView = (ListView) findViewById(R.id.maillist);
		showProgressDialog("正在获取通讯录...");
		setData();

	}

	private void setData() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				list = getPhoneContacts();
				try {
					uid = UserInfoUtil.getUserInfo(mContext).userid;
					Object[] data = JsonUtils.checkPhone(uid,
							getPhoneJson(sPhoneData));
					if ((Boolean) data[0]) {
						ulist = (List<UserRelation>) data[2];
						handler.sendEmptyMessage(Configs.READ_SUCCESS);
					} else {
						handler.sendEmptyMessage(Configs.READ_FAIL);
					}
				} catch (Exception e) {
					handler.sendEmptyMessage(Configs.READ_FAIL);
					e.printStackTrace();
				}

			}
		}).start();

	}

	private void setListData() {
		all_list.clear();
		f_list.clear();
		invita_list.clear();
		add_list.clear();
		for (int i = 0; i < list.size(); i++) {
			for (int j = 0; j < ulist.size(); j++) {
				if (list.get(i).phone.equals(ulist.get(j).phone.number)) {
					if (ulist.get(j).relative == 1) {
						list.get(i).isGf = ulist.get(j).relative;
						list.get(i).id = ulist.get(j).userid;
						f_list.add(list.get(i));
					} else if (ulist.get(j).relative == 0) {
						list.get(i).isGf = ulist.get(j).relative;
						list.get(i).id = ulist.get(j).userid;
						invita_list.add(list.get(i));
					} else if (ulist.get(j).relative == 2) {
						list.get(i).isGf = ulist.get(j).relative;
						list.get(i).id = ulist.get(j).userid;
						add_list.add(list.get(i));
					}

				}
			}
		}
		for (int i = 0; i < add_list.size(); i++) {
			all_list.add(add_list.get(i));
		}
		for (int i = 0; i < f_list.size(); i++) {
			all_list.add(f_list.get(i));
		}
		for (int i = 0; i < invita_list.size(); i++) {
			all_list.add(invita_list.get(i));
		}

		for (int i = 0; i < all_list.size(); i++) {
			String userid = all_list.get(i).id;
			if (!TextUtils.isEmpty(userid) && userid.equals(uid)) {
				all_list.remove(i);
			}

		}
		mailListView.setAdapter(new PhoneAdapter(mContext, all_list));
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				setListData();
				break;

			case Configs.READ_FAIL:
				Toast.makeText(LocalPhoneActivity.this, "获取通讯录失败",
						Toast.LENGTH_SHORT).show();
				break;
			}

		}
	};

	/**
	 * 获取本地通讯录
	 * 
	 * @Title: getPhoneContacts
	 * @Description: TODO
	 * @param @return
	 * @return List<MailInfo>
	 */
	private List<PhoneInfo> getPhoneContacts() {
		ContentResolver resolver = mContext.getContentResolver();
		ArrayList<PhoneInfo> list = new ArrayList<PhoneInfo>();
		ArrayList<SendPhone> sendPhones = new ArrayList<SendPhone>();
		String[] columns = new String[] { Phone.DISPLAY_NAME, Phone.NUMBER,
				Phone.PHOTO_ID, Phone.CONTACT_ID };
		Cursor cursor = resolver.query(Phone.CONTENT_URI, columns, null, null,
				null);
		while (cursor.moveToNext()) {
			PhoneInfo pc = new PhoneInfo();
			SendPhone sPhone = new SendPhone();
			String name = cursor.getString(cursor
					.getColumnIndex(Phone.DISPLAY_NAME));
			String number = cursor
					.getString(cursor.getColumnIndex(Phone.NUMBER))
					.replaceAll("\\s*", "").replace("+86", "");
			if (number.startsWith("17951")) {
				number = number.replace("17951", "");
			}
			if (TextUtils.isEmpty(number)) { // 判断号码是否为空
				continue;
			}
			pc.name = name;
			pc.phone = number;
			sPhone.number = number;
			list.add(pc);
			sendPhones.add(sPhone);
		}
		sPhoneData.phone = sendPhones;
		cursor.close();
		return list;
	}

	/**
	 * 生成上传通讯录的json
	 * 
	 * @param @param phone
	 * @param @return
	 * @return String
	 * @Title: getPhoneJson
	 * @Description: TODO
	 */
	private String getPhoneJson(SendPhoneData phone) {
		String phoneJson = "";
		Gson gson = new Gson();
		phoneJson = gson.toJson(phone);
		return phoneJson;
	}
}
