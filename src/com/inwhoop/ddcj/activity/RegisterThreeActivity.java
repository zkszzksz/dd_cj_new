package com.inwhoop.ddcj.activity;

import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.AreaAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.AreaListItem;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.db.DBManager;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.CircleImageview;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO 注册第三个界面，要实现头像、昵称、地区，以及同意协议而完成注册 * * * ****** Created by ZK
 *            ********
 * @Date: 2014/11/06 15:38
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class RegisterThreeActivity extends BaseFragmentActivity {
	private UserBean userBean;
	private CircleImageview headImg;
	private Bitmap headBmp;
	private TextView areaTV;
	private DBManager dbm;
	private SQLiteDatabase db;
	private AreaAdapter provinceAdapter;// 省份适配器
	private AreaAdapter cityAdapter;// 地区适配器
	private CheckBox isCheckBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userBean = (UserBean) getIntent().getSerializableExtra("bean");
		setContentView(R.layout.act_register_three);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.register_three);
		setRightLayout(R.string.finish, true);
		title_right_tv.setTextColor(getResources()
				.getColor(R.color.blue_028efd));
		title_right_layout.setOnClickListener(this);
		findViewById(R.id.head_layout).setOnClickListener(this);
		areaTV = (TextView) findViewById(R.id.arge_edit);
		areaTV.setOnClickListener(this);
		headImg = ((CircleImageview) findViewById(R.id.head_img));
		headImg.setBackground(getResources().getColor(R.color.gray_f2f2f2));
		isCheckBtn = (CheckBox) findViewById(R.id.cb_agree_use_info);
		findViewById(R.id.tv_use_info).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.title_right_layout:// 完成上传
			next();
			break;
		case R.id.head_layout:
			showCameraDialog(handler, true, 200, 200, true);
			break;
		case R.id.arge_edit: // 地区选择
			showPubWindow(true);
			break;
		case R.id.tv_use_info:
//			Act.toAct(mContext, UseHelpActivity.class);
            Bundle b=new Bundle();
            b.putInt("pos", 1);
            Act.toAct(mContext, OtherWebviewActivity.class);
			break;
		}
	}

	private void next() {
		userBean.name = ((EditText) findViewById(R.id.nick_edit)).getText()
				.toString().trim();
		userBean.location = areaTV.getText().toString().replaceAll(" ", "");
		userBean.udid = Utils.getPhoneUdid(mContext);

		String msg = check(userBean);
		if (!"".equals(msg)) {
			ToastUtils.showShort(mContext, msg);
		} else {
			showProgressDialog("请稍后...");
			progressDialog.setCancelable(false);
			new Thread(new Runnable() {

				@Override
				public void run() {
					Message msg = new Message();
					try {
						if (null != headBmp)
							userBean.img = Utils.imgToBase64(headBmp);
						msg.obj = JsonUtils.regist(userBean);
						msg.what = Configs.READ_SUCCESS;
					} catch (Exception e) {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);
				}
			}).start();
		}
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("注册失败，请稍后再试~~");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					if (LoginBindActivity.isThisBind != 0) {
						Act.toActClearTop(mContext, LoginBindActivity.class);
					} else {
                        UserInfoUtil.rememberUserInfo(mContext,userBean);
                        Act.toActClearTop(mContext, WelcomeActivity.class);
//						Act.toActClearTop(mContext, StartActivity.class,b);
//						Act.toActClearTop(mContext, LoginActivity.class);
					}
					showToast(obj[1].toString());
				} else {
					showToast(obj[1].toString());
				}
				break;

			case RESULT_CUT_IMG:
				headBmp = (Bitmap) msg.obj;
				headImg.setImageBitmap(headBmp);
				break;
			}
		}
	};

	private String check(UserBean user) {
		if (null == headBmp) {
			return "请上传头像";
		} else if ("".equals(user.name)) {
			return "请输入昵称";
		} else if (user.name.length() > 10) {
			return "请输入10个字以内的昵称";
		} else if ("".equals(user.location)) {
			return "请选择地区";
		} else if (!isCheckBtn.isChecked()) {
			return "请点击同意使用条款";
		}
		return "";
	}

	// TODO 弹窗
	private void showPubWindow(final boolean isProvince) {
		LayoutInflater inflater = getLayoutInflater();
		View winView = inflater.inflate(R.layout.dialog_choose_area, null);
		final Dialog winDialog = new Dialog(mContext,
				R.style.choose_area_Dialog);
		winDialog.setContentView(winView);
		winDialog.setCanceledOnTouchOutside(false);
		winDialog.show();

		View close = winView.findViewById(R.id.close_layout);
		close.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				winDialog.dismiss();
			}
		});

		Button okBtn = (Button) winView.findViewById(R.id.next_btn);
		if (isProvince) {

			initSpinner1((ListView) winView.findViewById(R.id._listview), "");
			okBtn.setText(R.string.next_step);
		} else {
			initSpinner1((ListView) winView.findViewById(R.id._listview),
					provinceAdapter.getItem(provinceAdapter.selectPosition)
							.getPcode());
			okBtn.setText(R.string.ok);
		}

		okBtn.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (isProvince) { // 在省份dialog里
					if (provinceAdapter.selectPosition == -1) {
						showToast("您并未选择任何省份");
					} else {
						showPubWindow(false);
						winDialog.dismiss();
					}
				} else { // 在地区dialog里
					if (cityAdapter.selectPosition == -1) {
						showToast("您并未选择任何城市");
					} else {
						areaTV.setText(provinceAdapter.getItem(
								provinceAdapter.selectPosition).getName()
								+ "  "
								+ cityAdapter.getItem(
										cityAdapter.selectPosition).getName());
						winDialog.dismiss();
					}
				}
			}
		});
	}

	public void initSpinner1(ListView listView, String pcode) {
		dbm = new DBManager(mContext);
		dbm.openDatabase();
		db = dbm.getDatabase();
		List<AreaListItem> list = new ArrayList<AreaListItem>();

		try {
			String sql;// = "select * from province";
			if ("".equals(pcode)) // 省份
				sql = "select * from province";
			// sql = "SELECT * FROM AREA WHERE ParentID =0";
			else
				// 地区
				// sql = "SELECT * FROM Area WHERE ParentID ='" + pcode + "'";
				sql = "select * from city where pcode='" + pcode + "'";

			Cursor cursor = db.rawQuery(sql, null);
			cursor.moveToFirst();
			while (!cursor.isLast()) {
				itemAdd(list, cursor);
				cursor.moveToNext();
			}
			itemAdd(list, cursor);

		} catch (Exception e) {
			System.out
					.println("============Exception==========" + e.toString());
		}
		dbm.closeDatabase();
		db.close();
		if ("".equals(pcode)) { // 省份
			provinceAdapter = new AreaAdapter(mContext, list);
			listView.setAdapter(provinceAdapter);
		} else {
			cityAdapter = new AreaAdapter(mContext, list);
			listView.setAdapter(cityAdapter);
		}

	}

	private void itemAdd(List<AreaListItem> list, Cursor cursor)
			throws UnsupportedEncodingException {
		String code = cursor.getString(cursor.getColumnIndex("code")); // houtai_city的这里是AreaID
		byte bytes[] = cursor.getBlob(2); // 注意用city.s3db这里是2 ，用houtai_city这里是1
		String name = new String(bytes, "gbk"); // houtai_city是 utf-8
		AreaListItem areaListItem = new AreaListItem();
		areaListItem.setName(name);
		areaListItem.setPcode(code);
		list.add(areaListItem);
	}
}
