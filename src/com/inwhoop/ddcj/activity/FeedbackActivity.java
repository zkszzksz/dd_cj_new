package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * Created by Administrator on 2015/1/23.
 */
public class FeedbackActivity extends BaseFragmentActivity {
    private EditText editText;
    private UserBean userBean;
    private Context context;
    private Object[] obj;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.feedback_activity);
        context = FeedbackActivity.this;
        initData();
        userBean = UserInfoUtil.getUserInfo(context);
        setLeftLayout(R.drawable.back_btn_selector, false);
        setTitleResText(R.string.fankui);
        setRightLayout(R.string.send, true);
        title_right_tv.setTextColor(getResources().getColor(R.color.blue));
        initView();
    }

    private void initView() {
        editText = (EditText) findViewById(R.id.feed_back_activity_edit);
        title_right_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (editText.getText().toString().trim().equals("")) {
                    Toast.makeText(context, "反馈信息不能为空", Toast.LENGTH_SHORT).show();
                } else {
                    setSendData();
                }
            }
        });

        title_left_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void setSendData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.feedBack(userBean.userid, editText.getText().toString().trim());
                    if ((Boolean)obj[0]) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Toast.makeText(context, "提交反馈成功", Toast.LENGTH_SHORT).show();
                    finish();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, "提交反馈失败", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}