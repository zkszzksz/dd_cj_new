package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.KeyBoard;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * Created by Administrator on 2014/10/23. 写评论
 */
public class WriteCommentActivity extends BaseFragmentActivity implements
		View.OnClickListener {
	private RelativeLayout back;
	private TextView backText;
	private TextView title;
	private RelativeLayout sendBtn;
	private TextView sendText;
	private EditText inputEdit;
	private Context context;

	private String content = "";

	private int id;
	private int tag;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.write_comment_activity);
		context = WriteCommentActivity.this;
		id = getIntent().getIntExtra("id", 0);
		tag = getIntent().getIntExtra("tag", 0);
		initView();
	}

	private void initView() {
		back = (RelativeLayout) findViewById(R.id.title_left_layout);
		back.setOnClickListener(this);
		back.setVisibility(View.VISIBLE);
		backText = (TextView) findViewById(R.id.title_left_tv);
		backText.setBackgroundResource(R.drawable.back_btn_selector);
		title = (TextView) findViewById(R.id.title_center_tv);
		title.setText(getResources().getString(R.string.write_comm));
		sendBtn = (RelativeLayout) findViewById(R.id.title_right_layout);
		sendBtn.setVisibility(View.VISIBLE);
		sendBtn.setOnClickListener(this);
		sendText = (TextView) findViewById(R.id.title_right_tv);
		sendText.setTextColor(getResources().getColor(R.color.k_line_tv_select));
		sendText.setText(getResources().getString(R.string.send));
		inputEdit = (EditText) findViewById(R.id.write_comment_activity_edit);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.title_left_layout:
			finish();
			break;
		case R.id.title_right_layout:
			content = inputEdit.getText().toString().trim();
			if (TextUtils.isEmpty(content)) {
				Toast.makeText(context,
						getResources().getString(R.string.no_com),
						Toast.LENGTH_SHORT).show();
			} else {
				submitComment();
			}
			break;
		}
	}

	private void submitComment() {
		showProgressDialog("正在提交评论...");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = null;
					String uid = UserInfoUtil.getUserInfo(context).userid;
					if (tag == Configs.QINGBAO_COMMENT_TAG) {
						obj = JsonUtils.sendCommentToQingbao(uid, id, content);
					} else if (tag == Configs.NEWS_COMMENT_TAG) {
						obj = JsonUtils.sendNewsCommnet(uid, id, content);
					}
					msg.obj = obj;
					msg.arg1 = id;
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("评论失败");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					showToast("评论成功");
					setResult(1001);
					finish();
				} else {
					showToast(obj[1].toString());
				}
				break;

			default:
				break;
			}

		};
	};

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }
}