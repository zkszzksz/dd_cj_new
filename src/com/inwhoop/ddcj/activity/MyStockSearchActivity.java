package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.*;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.SearchListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.SearchStock;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.db.HandleStocklistDB;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.SysPrintUtil;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.Utils;
import com.inwhoop.ddcj.view.KeybordPopupWindow;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * 自选股搜索界面
 * 
 * @author ligang@inwhoop.com
 * @version V1.0
 * @Project: DDCJ
 * @Title: MyStockSearchActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * @date 2014-10-22 下午6:53:25
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 */
public class MyStockSearchActivity extends BaseFragmentActivity implements
		MLocationLinsener {
	private EditText searchView;
	private MyHandler handler;
	private ScheduledExecutorService scheduledExecutor = Executors
			.newSingleThreadScheduledExecutor();
	// private String currentSearchTip;
	private InputMethodManager inputMethodManager;
	private TextView cancleBt = null;
	private View view = null;
	private List<SearchStock> searchStocks = new ArrayList<SearchStock>();
	private ListView listView;
	private Context context;
	private boolean searchTag = false; // 是否正在查询
	private String type;
	private SearchListAdapter adapter;
	private ProgressBar progressBar;
	private ArrayList<SearchStock> list = new ArrayList<SearchStock>();
	private HandleStocklistDB db = null;
	private UserBean userBean;
	private String address = "";

	private KeybordPopupWindow keybordPopupWindow = null;
	private int sw = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		view = LayoutInflater.from(this)
				.inflate(R.layout.my_stock_search, null);
		view.setBackgroundColor(0xbf000000);
		setContentView(view);
		context = MyStockSearchActivity.this;
		sw = getWindowManager().getDefaultDisplay().getWidth();
		keybordPopupWindow = new KeybordPopupWindow(mContext, sw);
		init();
	}

	private void init() {
		userBean = UserInfoUtil.getUserInfo(context);
		db = new HandleStocklistDB(context);
		inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		handler = new MyHandler();
		searchView = (EditText) findViewById(R.id.search_view);
		searchView.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (getWindow().getAttributes().softInputMode != WindowManager.LayoutParams.SOFT_INPUT_STATE_UNSPECIFIED) {
					searchView.setInputType(InputType.TYPE_NULL);
					keybordPopupWindow.setEdittext(searchView);
					keybordPopupWindow.showkeybordPopupWindow();
				}
				return false;
			}
		});
		progressBar = (ProgressBar) findViewById(R.id.my_stock_search_bar);
		progressBar.setVisibility(View.GONE);
		cancleBt = (TextView) findViewById(R.id.search_cancle);
		listView = (ListView) findViewById(R.id.search_list);
		searchView.addTextChangedListener(mTextWatcher);
		adapter = new SearchListAdapter(context, searchStocks);
		listView.setAdapter(adapter);
		cancleBt.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();
			}
		});
		startLocation(this);

		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int i, long l) {
				StockInfoBean bean = new StockInfoBean();
				if (null != adapter) {
					List<SearchStock> list1 = adapter.getAll();
					bean.stockcode = list.get(i).stockcode;
					bean.stockname = list1.get(i).stockname;
					bean.focus = Integer.valueOf(list1.get(i).focus);
				}
				Bundle bundle = new Bundle();
				bundle.putSerializable("bean", bean);
				Intent intent = new Intent(context, StockInfoActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
				finish();

			}
		});
		addhandler.sendEmptyMessageDelayed(0, 120);
	}

	private Handler addhandler = new Handler() {
		public void handleMessage(Message msg) {
			keybordPopupWindow.setEdittext(searchView);
			keybordPopupWindow.showkeybordPopupWindow();
		};
	};

	@Override
	public void mLocationSucess(String address) {
		// adapter = new SearchListAdapter(context, searchStocks, address);
		// listView.setAdapter(adapter);
		this.address = address;
	}

	@Override
	public void mLocationFaile(String errorInfo) {

	}

	private TextWatcher mTextWatcher = new TextWatcher() {
		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			// currentSearchTip = s.toString();
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			// TODO Auto-generated method stub
			list.clear();
		}

		@Override
		public void afterTextChanged(Editable s) {
			String inputText = searchView.getText().toString().trim();
			searchView.setSelection(inputText.length());

			if (inputText.length() > 0) {
				view.setBackgroundColor(0xffffffff);
			} else {
				view.setBackgroundColor(0x7f000000);
				adapter.getAll().clear();
				adapter.notifyDataSetChanged();
				return;
			}

			if (!searchTag) {
				searchTag = true;
				progressBar.setVisibility(View.VISIBLE);
				schedule(new SearchTipThread(inputText), 0);
			}
			// showSearchTip(inputText);
			// } else {
			// if (TextUtils.isEmpty(inputText)) {
			// adapter.add(list, address);
			// adapter.notifyDataSetChanged();
			// } else {
			// if (null != searchStocks && searchStocks.size() > 0) {
			// getDataShow(inputText);
			// }
			// }
			// }

			// if (TextUtils.isEmpty(searchView.getText().toString().trim())) {
			// searchTag = true;
			// }
		}
	};

	private void getDataShow(final String inputText) {
		scheduledExecutor.execute(new Runnable() {

			@Override
			public void run() {
				list = new ArrayList<SearchStock>();
				for (int i = 0; i < searchStocks.size(); i++) {
					if (type.equals("0")) {
						if (searchStocks.get(i).stockcode.contains(inputText)) {
							list.add(searchStocks.get(i));
						}
					} else if (type.equals("1")) {
						if (searchStocks.get(i).stockname.contains(inputText)) {
							list.add(searchStocks.get(i));
						}
					} else if (type.equals("2")) {
						if (searchStocks.get(i).aliasname.contains(inputText)) {
							list.add(searchStocks.get(i));
						}
					}
				}
				handler.sendEmptyMessage(Configs.READ_SUCCESS + 1);
				// scheduledExecutor.shutdown();
			}
		});

	}

	// public void showSearchTip(String newText) {
	// // excute after 500ms, and when excute, judge current search tip and
	// // newText
	// searchTag=true;
	// progressBar.setVisibility(View.VISIBLE);
	// schedule(new SearchTipThread(newText), 0);
	// }

	class SearchTipThread implements Runnable {

		String newText;

		public SearchTipThread(String newText) {
			this.newText = newText;
		}

		public void run() {
			// keep only one thread to load current search tip, u can get data
			// from network here
			if (Utils.isNumeric(newText)) {
				type = "0";
			} else if (Utils.isEnglish(newText)) {
				type = "2";
			} else {
				type = "1";
			}

			// if (newText.equals("6")) {
			// newText = "sh" + newText;
			// } else if (newText.equals("0") || newText.equals("3")
			// || newText.equals("4")) {
			// newText = "sz" + newText;
			// }
			try {
				SysPrintUtil.pt("用户的ID为", userBean.userid);
				if (!userBean.userid.equals("")) {
					searchStocks = JsonUtils.getSearchMyStock(userBean.userid,
							type, newText);
				} else {
					searchStocks = JsonUtils.getSearchMyStock("0", type,
							newText);
					List<StockInfoBean> list1 = db.getFriendlist();
					// SysPrintUtil.pt("解析数据异常sss", list1.get(0).focus+"");
					if (null != searchStocks && searchStocks.size() > 0){
						if (null != list1 && list1.size() > 0) {
							for (int i = 0; i < searchStocks.size(); i++) {
								for (int j = 0; j < list1.size(); j++) {
									if (searchStocks.get(i).stockcode.equals(list1
											.get(j).stockcode)) {
										searchStocks.get(i).focus = "1";
									}
								}
							}
						}
					}
				}
				if (null != searchStocks && searchStocks.size() > 0) {
					handler.sendEmptyMessage(Configs.READ_SUCCESS);
				} else {
					handler.sendEmptyMessage(Configs.READ_FAIL);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				SysPrintUtil.pt("解析数据异常", e.toString());
				handler.sendEmptyMessage(Configs.READ_ERROR);
			}
		}
	}

	public ScheduledFuture<?> schedule(Runnable command, long delayTimeMills) {
		return scheduledExecutor.schedule(command, delayTimeMills,
				TimeUnit.MILLISECONDS);
	}

	private class MyHandler extends Handler {

		@Override
		public void handleMessage(Message msg) {
			progressBar.setVisibility(View.GONE);
			searchTag = false;
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				getDataShow(searchView.getText().toString().trim());
				break;
			case Configs.READ_FAIL:
				Toast.makeText(context,
						getResources().getString(R.string.no_search_data),
						Toast.LENGTH_SHORT).show();
				break;
			case Configs.READ_ERROR:
				Toast.makeText(context,
						getResources().getString(R.string.data_error),
						Toast.LENGTH_SHORT).show();
				break;
			case Configs.READ_SUCCESS + 1:
				adapter.add(list, address);
				adapter.notifyDataSetChanged();

				break;
			}
		}
	}

	/**
	 * hide soft input
	 */
	private void hideSoftInput() {
		if (inputMethodManager != null) {
			View v = MyStockSearchActivity.this.getCurrentFocus();
			if (v == null) {
				return;
			}

			inputMethodManager.hideSoftInputFromWindow(v.getWindowToken(),
					InputMethodManager.HIDE_NOT_ALWAYS);
			searchView.clearFocus();
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (null != keybordPopupWindow.keybordPopupWindow
					&& keybordPopupWindow.keybordPopupWindow.isShowing()) {
				keybordPopupWindow.keybordPopupWindow.dismiss();
				keybordPopupWindow.keybordPopupWindow = null;
			} else {
				finish();
			}
		}
		return false;
	}

}
