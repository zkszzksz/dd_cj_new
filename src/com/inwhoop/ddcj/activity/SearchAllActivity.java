package com.inwhoop.ddcj.activity;

import com.inwhoop.ddcj.util.Act;
import net.tsz.afinal.FinalBitmap;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.util.SearchUtil;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * 大搜索界面
 * 
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class SearchAllActivity extends BaseFragmentActivity {
	private final int COUNT = 5;
	private LayoutInflater inflater = null;

	private LinearLayout contentLayout = null;

	private EditText searchEditText = null;
	private String searchstr = "";
	private SearchUtil searchUtil;
	private TextView rightView;
	private LinearLayout searchUser,searchGroup,searchChannel;

	private LinearLayout userLayout, groupLayout, channelLayout, chatLayout,
			userlistLayout, grouplistLayout, channellistLayout, chatlistLayout;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_all_layout);
		inflater = LayoutInflater.from(mContext);
		initData();

        String uid = UserInfoUtil.getUserInfo(mContext).userid;
         if (TextUtils.isEmpty(uid)){
             Act.toAct(mContext,LoginActivity.class);
             finish();
         }
    }

	public void initData() {
		searchUtil = new SearchUtil();
		contentLayout = (LinearLayout) findViewById(R.id.contentlayout);
		userLayout = (LinearLayout) findViewById(R.id.userlayout);
		groupLayout = (LinearLayout) findViewById(R.id.grouplayout);
		channelLayout = (LinearLayout) findViewById(R.id.channellayout);
		chatLayout = (LinearLayout) findViewById(R.id.chatlayout);
		userlistLayout = (LinearLayout) findViewById(R.id.userlistlayout);
		grouplistLayout = (LinearLayout) findViewById(R.id.grouplistlayout);
		channellistLayout = (LinearLayout) findViewById(R.id.channellistlayout);
		chatlistLayout = (LinearLayout) findViewById(R.id.chatlistlayout);
		rightView = (TextView) findViewById(R.id.search_cancle);
		searchUser = (LinearLayout) findViewById(R.id.more_search_user);
		searchUser.setOnClickListener(this);
		searchGroup = (LinearLayout) findViewById(R.id.more_search_group);
		searchGroup.setOnClickListener(this);
		searchChannel = (LinearLayout) findViewById(R.id.more_search_channel);
		searchChannel.setOnClickListener(this);
		rightView.setOnClickListener(this);
		searchEditText = (EditText) findViewById(R.id.search_view);
		searchEditText.addTextChangedListener(mTextWatcher);

	}

	private TextWatcher mTextWatcher = new TextWatcher() {

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {

		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {

		}

		@Override
		public void afterTextChanged(Editable s) {
			searchstr = searchEditText.getText().toString();
			if (TextUtils.isEmpty(searchstr)) {
				rightView.setText("取消");
			} else {
				rightView.setText("搜索");
			}

		}
	};

	private void search() {
		String uid = UserInfoUtil.getUserInfo(mContext).userid;
		if ("".equals(searchstr)) {
			showToast("请输入搜索内容");
			return;
		}
		showProgressDialog("正在查询...");
		searchUtil.startSearch(searchstr, uid, handler);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.search_cancle:
			String str = rightView.getText().toString();
			if (str.contains("取消")) {
				finish();
			} else {
				search();
			}
			break;
		case R.id.more_search_user:
			Intent intent = new Intent(mContext, SearchAllItemUserActivity.class);
			intent.putExtra("content", searchstr);
			startActivity(intent);
			break;
		case R.id.more_search_group:
			Intent intent1 = new Intent(mContext, SearchAllItemGroupActivity.class);
			intent1.putExtra("content", searchstr);
			startActivity(intent1);
			break;
		case R.id.more_search_channel:
			Intent intent2 = new Intent(mContext, SearchAllItemChannelActivity.class);
			intent2.putExtra("content", searchstr);
			startActivity(intent2);
			break;
		}
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Configs.SEARCH_USER_SUCESS:
				userLayout.setVisibility(View.VISIBLE);
				setUserlayout();
				break;
			case Configs.SEARCH_USER_FAIL:
				userLayout.setVisibility(View.GONE);
				break;
			case Configs.SEARCH_GROUP_SUCESS:
				groupLayout.setVisibility(View.VISIBLE);
				setGrouplayout();
				break;
			case Configs.SEARCH_GROUP_FAIL:
				groupLayout.setVisibility(View.GONE);
				break;
			case Configs.SEARCH_NEWS_SUCESS:
				channelLayout.setVisibility(View.VISIBLE);
				setChannellayout();
				break;
			case Configs.SEARCH_NEWS_FAIL:
				channelLayout.setVisibility(View.GONE);
				break;
			}
			contentLayout.setVisibility(View.VISIBLE);
			dismissProgressDialog();
		}
	};

	public void setUserlayout() {
		FinalBitmap fb = FinalBitmap.create(mContext);
		fb.configLoadfailImage(R.drawable.home_head);
		fb.configLoadingImage(R.drawable.home_head);
		View view = null;
		userlistLayout.removeAllViews();
		int num = 0;
		if (SearchUtil.userList.size() > COUNT) {
			searchUser.setVisibility(View.VISIBLE);
			num = 5;
		}else{
			num = SearchUtil.userList.size();
		}
		if(null==SearchUtil.userList||SearchUtil.userList.size() == 0){
			userLayout.setVisibility(View.GONE);
		}
		for (int i = 0; i < num; i++) {
			final int position = i;
			view = inflater.inflate(R.layout.seacher_user_list_item, null);
			View line = view.findViewById(R.id.line);
			ImageView img = (ImageView) view.findViewById(R.id.pheadimg);
			TextView name = (TextView) view.findViewById(R.id.name);
			if (i == SearchUtil.userList.size() - 1) {
				line.setVisibility(View.GONE);
			}
			fb.display(img, SearchUtil.userList.get(i).img);
			name.setText(SearchUtil.userList.get(i).name);
			userlistLayout.addView(view);
			view.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Intent intent = new Intent(mContext,
							PersonInfoActivity.class);
					intent.putExtra("userid",
							SearchUtil.userList.get(position).userid);
					startActivity(intent);
				}
			});
		}
	}

	public void setGrouplayout() {
		FinalBitmap fb = FinalBitmap.create(mContext);
		fb.configLoadfailImage(R.drawable.qingbao_loading_img);
		fb.configLoadingImage(R.drawable.qingbao_loading_img);
		View view = null;
		grouplistLayout.removeAllViews();
		int num = 0;
		if (SearchUtil.groupList.size() > COUNT) {
			searchGroup.setVisibility(View.VISIBLE);
			num = 5;
		}else{
			num = SearchUtil.groupList.size();
		}
		if(null==SearchUtil.groupList||SearchUtil.groupList.size() == 0){
			groupLayout.setVisibility(View.GONE);
		}
		for (int i = 0; i < num; i++) {
			final int position = i;
			view = inflater.inflate(R.layout.seacher_group_list_item, null);
			View line = view.findViewById(R.id.line);
			ImageView img = (ImageView) view.findViewById(R.id.pheadimg);
			TextView name = (TextView) view.findViewById(R.id.name);
			if (i == SearchUtil.groupList.size() - 1) {
				line.setVisibility(View.GONE);
			}
			fb.display(img, SearchUtil.groupList.get(i).img);
			name.setText(SearchUtil.groupList.get(i).name);
			grouplistLayout.addView(view);
			view.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Bundle bundle = new Bundle();
					bundle.putSerializable("CateChannel",
							SearchUtil.groupList.get(position));
					Intent intent = new Intent(mContext,
							GroupInfoActivity.class);
					intent.putExtras(bundle);
					startActivity(intent);

				}
			});
		}
	}

	public void setChannellayout() {
		FinalBitmap fb = FinalBitmap.create(mContext);
		fb.configLoadfailImage(R.drawable.qingbao_loading_img);
		fb.configLoadingImage(R.drawable.qingbao_loading_img);
		View view = null;
		channellistLayout.removeAllViews();
		int num = 0;
		if (SearchUtil.newsList.size() > COUNT) {
			searchChannel.setVisibility(View.VISIBLE);
			num = 5;
		}else{
			num = SearchUtil.newsList.size();
		}
		if(null==SearchUtil.newsList||SearchUtil.newsList.size() == 0){
			channelLayout.setVisibility(View.GONE);
		}
		for (int i = 0; i < num; i++) {
			final int position = i;
			view = inflater.inflate(R.layout.seacher_channel_list_item, null);
			View line = view.findViewById(R.id.line);
			ImageView img = (ImageView) view.findViewById(R.id.pheadimg);
			TextView name = (TextView) view.findViewById(R.id.name);
			TextView dec = (TextView) view.findViewById(R.id.introuce);
			if (i == SearchUtil.newsList.size() - 1) {
				line.setVisibility(View.GONE);
			}
			fb.display(img, SearchUtil.newsList.get(i).thumbnail);
			name.setText(SearchUtil.newsList.get(i).title);
			dec.setText(SearchUtil.newsList.get(i).des);
			channellistLayout.addView(view);
			view.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					try {
						Intent intent = new Intent(mContext,
								NewsDetailActivity.class);
						intent.putExtra("news_id",
								SearchUtil.newsList.get(position).id);
						intent.putExtra("channel_id",
								Integer.parseInt(SearchUtil.newsList
										.get(position).channelid.trim()));
						intent.putExtra("channel_name",
								SearchUtil.newsList.get(position).channelname);
						intent.putExtra("news",
								SearchUtil.newsList.get(position));
						startActivity(intent);
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			});
		}
	}

	@Override
	protected void onDestroy() {
		searchUtil.stop(true);
		super.onDestroy();
	}
}
