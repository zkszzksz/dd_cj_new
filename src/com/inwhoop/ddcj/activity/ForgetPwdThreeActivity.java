package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;

/**
 * @Describe: TODO 忘记密码第三个界面。输入密码提交 * * * ****** Created by ZK ********
 * @Date: 2014/11/10 9:49
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class ForgetPwdThreeActivity extends BaseFragmentActivity {
	private UserBean userBean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		userBean = (UserBean) getIntent().getSerializableExtra("bean");
		setContentView(R.layout.act_forget_pwd_three);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.forget_pwd_three);
		setRightLayout(R.string.refer, true);
		title_right_tv.setTextColor(getResources()
				.getColor(R.color.blue_028efd));
		title_right_layout.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.title_right_layout:
			next();
			break;
		}
	}

	private void next() {
		userBean.pwd = ((EditText) findViewById(R.id.new_pwd)).getText()
				.toString().trim();
		String msg = check(userBean);
		if (!"".equals(msg)) {
			showToast(msg);
		} else {
			showProgressDialog("请稍后...");
			new Thread(new Runnable() {

				@Override
				public void run() {
					Message msg = new Message();
					try {
						msg.obj = JsonUtils.userForgetPwd(userBean);
						msg.what = Configs.READ_SUCCESS;
					} catch (Exception e) {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);
				}
			}).start();
		}
	}

	private String check(UserBean user) {
		String surePwd = ((EditText) findViewById(R.id.sure_pwd_et)).getText()
				.toString();
		if ("".equals(user.pwd)) {
			return "请输入密码";
		} else if ("".equals(surePwd)) {
			return "请确认密码";
		} else if (!surePwd.equals(user.pwd)) {
			return "请检查俩次密码是否一致";
		}
		return "";
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("验证失败，请稍后再试~~");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					Act.toActClearTop(mContext, StartActivity.class);
					Act.toActClearTop(mContext, LoginActivity.class);
					showToast(obj[1].toString());
				} else {
					showToast(obj[1].toString());
				}
				break;
			}
		}
	};
}
