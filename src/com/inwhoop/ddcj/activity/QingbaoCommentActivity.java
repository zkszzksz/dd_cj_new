package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * 情报评论
 * 
 * @Project: DDCJ
 * @Title: QingbaoCommentActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-12-22 下午4:02:38
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class QingbaoCommentActivity extends BaseFragmentActivity implements
		View.OnClickListener {
	private RelativeLayout back;
	private TextView backText;
	private TextView title;
	private RelativeLayout sendBtn;
	private TextView sendText;
	private EditText inputEdit;
	private Context context;

	private String content = "";

	private int id;
	private int commentid;

	private String touserid = "";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.write_comment_activity);
		context = QingbaoCommentActivity.this;
		id = getIntent().getIntExtra("id", 0);
		touserid = getIntent().getStringExtra("touserid");
		commentid = getIntent().getIntExtra("commentid", 0);
		initView();
	}

	private void initView() {
		back = (RelativeLayout) findViewById(R.id.title_left_layout);
		back.setOnClickListener(this);
		back.setVisibility(View.VISIBLE);
		backText = (TextView) findViewById(R.id.title_left_tv);
		backText.setBackgroundResource(R.drawable.back_btn_selector);
		title = (TextView) findViewById(R.id.title_center_tv);
		title.setText(getResources().getString(R.string.write_comm));
		sendBtn = (RelativeLayout) findViewById(R.id.title_right_layout);
		sendBtn.setVisibility(View.VISIBLE);
		sendBtn.setOnClickListener(this);
		sendText = (TextView) findViewById(R.id.title_right_tv);
		sendText.setTextColor(getResources().getColor(R.color.k_line_tv_select));
		sendText.setText(getResources().getString(R.string.send));
		inputEdit = (EditText) findViewById(R.id.write_comment_activity_edit);
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.title_left_layout:
			finish();
			break;
		case R.id.title_right_layout:
			content = inputEdit.getText().toString().trim();
			if (TextUtils.isEmpty(content)) {
				Toast.makeText(context,
						getResources().getString(R.string.no_com),
						Toast.LENGTH_SHORT).show();
			} else {
				submitComment();
			}
			break;
		}
	}

	private void submitComment() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = null;
					String uid = UserInfoUtil.getUserInfo(context).userid;
					obj = JsonUtils.sendCommentAtQingbao(uid, id, content,
							touserid, commentid);
					msg.obj = obj;
					msg.arg1 = id;
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("获取评论列表失败");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					showToast("评论成功");
					setResult(1005);
					finish();
				} else {
					showToast(obj[1].toString());
				}
				break;

			default:
				break;
			}

		};
	};

}