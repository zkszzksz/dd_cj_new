package com.inwhoop.ddcj.activity;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;

import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.GroupChatlistAdapter;
import com.inwhoop.ddcj.bean.GroupChatinfo;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.db.HandleGroupchatRecordDB;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: FriendChatActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 群聊
 * @date 2014-6-14 上午10:47:58
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class GroupChatRecordActivity extends BaseFragmentActivity implements
        IXListViewListener, OnClickListener{
    private String groupid; // 聊天对象用户昵称

    private String groupname; // 聊天对象openfire的账号

    private XListView listView = null;

    private List<GroupChatinfo> chatList = new ArrayList<GroupChatinfo>();

    private GroupChatlistAdapter adapter = null; // 适配器

    private HandleGroupchatRecordDB db = null;

    private int page = 1; // 用于聊天记录分页
    
    private UserBean userinfo = null;
    
    private String id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                        | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.group_chatrecord_layout);
        mContext = GroupChatRecordActivity.this;
        userinfo = UserInfoUtil.getUserInfo(mContext);
        db = new HandleGroupchatRecordDB(mContext);
        if (null != getIntent()) {
            groupid = getIntent().getStringExtra("groupid");
            groupname = getIntent().getStringExtra("groupname");
            id = getIntent().getStringExtra("id");
        }
        initData();
    }

    @Override
    public void initData() {
        super.initData();
        setTitleStrText("" + groupname);
        setLeftLayout(R.drawable.back_btn_selector, false);
        listView = (XListView) findViewById(R.id.chatlist);
        listView.setXListViewListener(this);
        listView.setPullLoadEnable(false);
        initListviewData();
    }

    /**
     * 初始化listview
     *
     * @param
     * @return void
     * @Title: initListview
     * @Description: TODO
     */
    private void initListviewData() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                chatList = db.searchChatid(groupid, userinfo.tel, id, 0);
                handler.sendEmptyMessage(0);
            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            adapter = new GroupChatlistAdapter(mContext, chatList, listView);
            listView.setAdapter(adapter);
        };
    };

    @Override
    public void onRefresh() {
        try {
            chatList = db.searchChatid(groupid, userinfo.tel, id, page);
            page++;
            adapter.addChatinfo(chatList);
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
        }
        listView.stopRefresh();
    }

    @Override
    public void onLoadMore() {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_left_layout:
                finish();
                break;

            default:
                break;
        }
    }
}