package com.inwhoop.ddcj.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.*;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.CommentInfoAdapter;
import com.inwhoop.ddcj.adapter.MyStockListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.*;
import com.inwhoop.ddcj.impl.WebUrlLoadLinsener;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.CustomSlidingDrawer;
import com.inwhoop.ddcj.view.HVListView;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 资讯详情
 * 
 * @Project: DDCJ
 * @Title: NewsDetailActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-7 上午10:18:53
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class NewsDetailActivity extends BaseFragmentActivity implements
		OnClickListener, IXListViewListener, OnItemClickListener,
		WebUrlLoadLinsener, PlatformActionListener, OnScrollListener {
	public final static int READ_SINA_SUCCESS = 11; // 读取网络数据成功
	public final static int READ_SINA_FAIL = 12; // 读取网络为空
	public final static int READ_ORDER_SUCCESS = 13; // 读取网络数据成功
	public final static int READ_OORDER_FAIL = 14; // 读取网络为空
	public final static int READ_COMMNET_SUCCESS = 15; // 读取网络数据成功
	public final static int READ_COMMENT_FAIL = 16; // 读取网络为空
	public final static int READ_STOCK_SUCCESS = 17; // 读取网络数据成功
	public final static int READ_SHARE_SUCCESS = 18; // 读取网络数据成功
	public final static int READ_SHARE_FAIL = 19; // 读取网络为空
	public final static int READ_MY_SUCCESS = 20; // 读取网络数据成功
	public final static int READ_MY_FAIL = 21; // 读取网络为空
	public final static int COLLECT_SUCCESS = 22; // 读取网络数据成功
	public final static int COLLECT_FAIL = 23; // 读取网络为空
	private LinearLayout comment;
	private XListView mListView;
	private CommentInfoAdapter adapter;
	private NewsContentBean nContentBean;
	private Fouces fouces;
	private HVListView hListview;
	private List<CommentInfo> commentInfos = new ArrayList<CommentInfo>();
	private List<StockInfoBean> stockInfoBeans = new ArrayList<StockInfoBean>();
	private ChannelList channelList;
	private String news_id;
	private WebView webView;
	private RelativeLayout titleLayout;
	private MyStockListAdapter myStockListAdapter;
	private LinearLayout slidLayout;
	private LinearLayout shareLayout;
	private LinearLayout collectLayout;
	private LinearLayout topLayout;
	private CustomSlidingDrawer slidingDrawer;
	private TextView titleView;
	private TextView timeView;
	private TextView categoryView;
	private TextView orderButton;
	private TextView comNumView;
	private TextView shareNumView;
	private TextView tabCommentView;
	private TextView collectTextView;
	private ImageView collectImg;
	private WebView subjectView;
	private View hListLayout;
	private String channelId;
	private String stockCode;
	private String stockName;
	private LinearLayout.LayoutParams params;
	private boolean isOrder;
	private boolean isMyCollect;
	private boolean isLoadMore = false;
	private int index = 0;
	private String uid;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.news_detail);
		initData();
		getCommentListData();
	}

	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.news_detail);
		params = new LinearLayout.LayoutParams(
				LinearLayout.LayoutParams.FILL_PARENT,
				LinearLayout.LayoutParams.FILL_PARENT);
		nContentBean = new NewsContentBean();
		fouces = new Fouces();
		uid = UserInfoUtil.getUserInfo(mContext).userid;
		news_id = getIntent().getStringExtra("news_id");
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.news_detail_item, null);
		hListLayout = LayoutInflater.from(mContext).inflate(
				R.layout.news_slid_list, null);
		titleView = (TextView) view.findViewById(R.id.title);
		timeView = (TextView) view.findViewById(R.id.time);
		categoryView = (TextView) view.findViewById(R.id.category);
		orderButton = (TextView) view.findViewById(R.id.order);
		webView = (WebView) view.findViewById(R.id.webview);
		subjectView = (WebView) view.findViewById(R.id.subject);
		comNumView = (TextView) view.findViewById(R.id.comment_info_num);
		comment = (LinearLayout) findViewById(R.id.comment_bt);
		collectTextView = (TextView) findViewById(R.id.collection);
		collectImg = (ImageView) findViewById(R.id.collection_img);
		shareLayout = (LinearLayout) findViewById(R.id.share_layout);
		shareNumView = (TextView) findViewById(R.id.share_num);
		collectLayout = (LinearLayout) findViewById(R.id.collect_bt);
		topLayout = (LinearLayout) findViewById(R.id.top_layout);
		mListView = (XListView) findViewById(R.id.xListView);
		tabCommentView = (TextView) findViewById(R.id.comment_num);
		titleLayout = (RelativeLayout) findViewById(R.id.top);
		hListview = (HVListView) hListLayout.findViewById(R.id.listview);
		// 设置列头
		hListview.mListHead = (LinearLayout) hListLayout
				.findViewById(R.id.head);
		myStockListAdapter = new MyStockListAdapter(mContext, hListview);
		slidLayout = (LinearLayout) findViewById(R.id.sliding);
		// slidingDrawer = new CustomSlidingDrawer(this, mListView,
		// LayoutParams.FILL_PARENT, 1000, 800);
		adapter = new CommentInfoAdapter(mContext);
		mListView.addHeaderView(view);
		mListView.setPullLoadEnable(false);
		mListView.setPullRefreshEnable(false);
		mListView.setXListViewListener(this);
		mListView.setOnItemClickListener(this);
		mListView.setOnScrollListener(this);
		comment.setOnClickListener(this);
		orderButton.setOnClickListener(this);
		shareLayout.setOnClickListener(this);
		title_right_layout.setOnClickListener(this);
		collectLayout.setOnClickListener(this);
		WebUtils.setWebUrlLoadLinsener(this);
		mListView.setAdapter(adapter);
		// slidLayout.addView(slidingDrawer);
		myStockListAdapter.setTag(1);
		hListview.setAdapter(myStockListAdapter);
		// slidingDrawer.fillPanelContainer(hListLayout);
		params.bottomMargin = (int) getResources().getDimension(R.dimen.dp_60);
		mListView.setLayoutParams(params);
		getContent();
		setViewText();
	}

	private int getPullLvHeight(ListView pull, int count) {
		int totalHeight = 0;
		// ListAdapter adapter = pull.getAdapter();
		for (int i = 0; i < count; i++) { // listAdapter.getCount()返回数据项的数目
			View listItem = adapter.getView(i, null, pull);
			listItem.measure(0, 0); // 计算子项View 的宽高
			totalHeight += listItem.getMeasuredHeight(); // 统计所有子项的总高度
		}

		totalHeight = totalHeight
				+ (pull.getDividerHeight() * (pull.getCount() - 1));
		return totalHeight;
	}

	private void setCollectState(boolean isCollect) {
		if (isCollect) {
			collectImg.setBackgroundResource(R.drawable.ico_favor_pressed);
			collectTextView.setTextColor(getResources().getColor(
					R.color.text_color));
			collectTextView.setText(R.string.collectioned);
		} else {
			collectImg.setBackgroundResource(R.drawable.ico_m_favor);
			collectTextView
					.setTextColor(getResources().getColor(R.color.white));
			collectTextView.setText(R.string.collection);
		}

	}

	/**
	 * 设置标题栏等信息显示
	 * 
	 * @Title: setViewText
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void setViewText() {
		channelList = (ChannelList) getIntent().getSerializableExtra("news");
		channelId = getIntent().getStringExtra("channel_id");
		titleView.setText(channelList.title);
		timeView.setText(TimeUtils.yestodayDistanceToday(
				channelList.createtime,
				TimeUtils.getStrTime(System.currentTimeMillis())));
		categoryView.setText("[" + getIntent().getStringExtra("channel_name")
				+ "]");

	}

	/**
	 * 设置题材
	 * 
	 * @Title: setSubject
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void setSubject(List<Theme> themelist) {
		if (null == themelist || themelist.size() == 0) {
			return;
		}
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < themelist.size(); i++) {
			if (i < themelist.size() - 1) {
				sb.append(themelist.get(i).theme);
				sb.append("、");
			} else {
				sb.append(themelist.get(i).theme);
			}
		}
		WebSettings webSettings = subjectView.getSettings();
		webSettings.setTextSize(WebSettings.TextSize.SMALLER);
		WebUtils.LoadcontentWeb(mContext, subjectView, null,
				"题材概念：" + sb.toString());
	}

	/**
	 * 获取新闻详情内容
	 * 
	 * @Title: getContent
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void getContent() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				try {
					Object[] data = JsonUtils.getNewsContent(uid, channelId,
							news_id);
					if ((Boolean) data[0]) {
						nContentBean = (NewsContentBean) data[2];
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
					handler.sendMessage(msg);
				}

			}
		}).start();

	}

	private String getStockCoce(List<Stock> stocklist) {
		StringBuilder sb = new StringBuilder();
		// for (int i = 0; i < stocklist.size(); i++) {
		// if (i < stocklist.size() - 1) {
		// sb.append(Utils.getCodeFromLink(stocklist.get(i).stock));
		// sb.append(",");
		// } else {
		// sb.append(Utils.getCodeFromLink(stocklist.get(i).stock));
		// }
		// }
		for (int i = 0; i < stocklist.size(); i++) {
			if (i < stocklist.size() - 1) {
				sb.append(stocklist.get(i).stock);
				sb.append(",");
			} else {
				sb.append(stocklist.get(i).stock);
			}
		}
		return sb.toString();
	}

	/**
	 * 从新浪获取股票信息
	 * 
	 * @Title: getSinaStock
	 * @Description: TODO
	 * @param @param stockCode
	 * @return void
	 */
	private void getSinaStock(final String stockCode, final int r_what) {
		new Thread(new Runnable() {
			Message msg = new Message();

			@Override
			public void run() {
				try {
					String stock = "";
					// if (r_what == READ_SINA_SUCCESS) {
					// stock = Utils.getCodeFromLink(stockCode);
					// } else {
					// stock = stockCode;
					// }
					stockInfoBeans = JsonUtils.getStockInfoList(stockCode);
					msg.what = r_what;
					handler.sendMessage(msg);
				} catch (Exception e) {
					msg.what = READ_SINA_FAIL;
					handler.sendMessage(msg);
					e.printStackTrace();
				}

			}
		}).start();

	}

	/**
	 * 获取评论信息
	 * 
	 * @Title: setListData
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void getCommentListData() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				try {
					Object[] data = JsonUtils.getNewsCommentList(uid, news_id,
							Configs.COUNT, index);
					if ((Boolean) data[0]) {
						commentInfos = (List<CommentInfo>) data[2];
						msg.what = READ_COMMNET_SUCCESS;
					} else {
						msg.what = READ_COMMENT_FAIL;
					}
				} catch (Exception e) {
					msg.what = READ_COMMENT_FAIL;
					e.printStackTrace();
				}
				handler.sendMessage(msg);

			}
		}).start();

	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		UserBean userBean = UserInfoUtil.getUserInfo(mContext);
		switch (v.getId()) {
		case R.id.comment_bt:
			if (!TextUtils.isEmpty(userBean.ddid)) {
				Intent intent = new Intent(NewsDetailActivity.this,
						WriteCommentActivity.class);
				try {
					intent.putExtra("id", Integer.valueOf(news_id));
				} catch (Exception e) {
					// TODO: handle exception
				}
				intent.putExtra("tag", Configs.NEWS_COMMENT_TAG);
				startActivityForResult(intent, 1001);

			} else {
				Act.toAct(mContext, LoginActivity.class);
			}
			break;

		case R.id.order:
			if (!TextUtils.isEmpty(userBean.ddid)) {
				orderChannel(isOrder);
				isOrder = !isOrder;
			} else {
				Act.toAct(mContext, LoginActivity.class);
			}
			break;
		case R.id.share_layout:
            if (null != nContentBean && null != channelList) {
                String imgurl = channelList.img;
                if ("".equals(imgurl)) {
                    imgurl = UserInfoUtil.getUserInfo(mContext).img;
                }
                ShareUtil.showShare(mContext, channelList.title, imgurl,
                        nContentBean.sharecontent, this);
                // ShareUtil.showShare(mContext, nContentBean.content, imgurl,
                // nContentBean.sharecontent, this);
            } else {
                Toast.makeText(mContext, "分享数据错误", Toast.LENGTH_SHORT).show();
            }
            break;
		case R.id.collect_bt:
			if (!TextUtils.isEmpty(userBean.ddid)) {
				collect(isMyCollect);
			} else {
				Act.toAct(mContext, LoginActivity.class);
			}
			break;
		}
	}

	/**
	 * 收藏资讯
	 * 
	 * @Title: collect
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void collect(final boolean isCollect) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				try {
					Object[] data = null;
					if (!isCollect) {
						data = JsonUtils.collectMyNews(uid, news_id);
					} else {
						data = JsonUtils.cancelCollect(uid, news_id);
					}
					if ((Boolean) data[0]) {
						msg.what = COLLECT_SUCCESS;
					} else {
						msg.what = COLLECT_FAIL;
					}
					handler.sendMessage(msg);
				} catch (Exception e) {
					msg.what = COLLECT_FAIL;
					handler.sendMessage(msg);
				}

			}
		}).start();
	}

	/**
	 * 订阅
	 * 
	 * @Title: orderChannel
	 * @Description: TODO
	 * @param @param isOrder
	 * @return void
	 */
	private void orderChannel(final boolean isOrder) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				try {
					String uid = UserInfoUtil.getUserInfo(mContext).userid;
					Object[] data = JsonUtils.orderChannel(uid, channelId + "",
							isOrder);
					if ((Boolean) data[0]) {
						msg.what = READ_ORDER_SUCCESS;
					} else {
						msg.what = READ_OORDER_FAIL;
					}
				} catch (Exception e) {
					msg.what = READ_OORDER_FAIL;
					e.printStackTrace();
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	/**
	 * 分享数累计
	 * 
	 * @Title: shareCount
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void shareCount() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				try {
					Object[] data = JsonUtils.shareCount(channelList.id);
					if ((Boolean) data[0]) {
						msg.what = READ_SHARE_SUCCESS;
					} else {
						msg.what = READ_SHARE_FAIL;
					}
				} catch (Exception e) {
					msg.what = READ_SHARE_FAIL;
					e.printStackTrace();
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	/**
	 * 是否是自选股
	 * 
	 * @Title: ifFoucs
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void ifFoucs() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				try {
					String uid = UserInfoUtil.getUserInfo(mContext).userid;
					Object[] data = JsonUtils.getIfFoucs(uid, stockCode);
					if ((Boolean) data[0]) {
						fouces = (Fouces) data[2];
						msg.what = READ_MY_SUCCESS;
					} else {
						msg.what = READ_MY_FAIL;
					}
				} catch (Exception e) {
					msg.what = READ_MY_FAIL;
					e.printStackTrace();
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				String content = nContentBean.content;
				WebUtils.LoadcontentWeb(mContext, webView, null, content);
				getSinaStock(getStockCoce(nContentBean.stocklist),
						READ_SINA_SUCCESS);
				setSubject(nContentBean.themelist);
				if (!TextUtils.isEmpty(nContentBean.sharenum)) {
					shareNumView.setText(nContentBean.sharenum);
				} else {
					shareNumView.setText(0 + "");
				}

				if (nContentBean.collect == 1) {
					setCollectState(true);
					isMyCollect = true;
				} else {
					setCollectState(false);
					isMyCollect = false;
				}
				orderButton.setVisibility(View.VISIBLE);
				if (nContentBean.subscribe == 1) {
					orderButton.setText("取消订阅");
					isOrder = false;
				} else {
					orderButton.setText("+订阅");
					isOrder = true;
				}
				break;

			case Configs.READ_FAIL:
				break;
			case READ_SINA_SUCCESS:
				setSinaStock(stockInfoBeans);
				break;
			case READ_SINA_FAIL:
				break;
			case READ_ORDER_SUCCESS:
				if (!isOrder) {
					Toast.makeText(mContext, "订阅成功", Toast.LENGTH_SHORT).show();
					orderButton.setText("取消订阅");
				} else {
                    Toast.makeText(mContext, "取消成功", Toast.LENGTH_SHORT).show();
					orderButton.setText("+订阅");
                    finish();
				}
				break;
			case READ_OORDER_FAIL:
				break;
			case READ_COMMNET_SUCCESS:
				if (commentInfos.size() >= Configs.COUNT) {
					mListView.setPullLoadEnable(true);
				} else {
					mListView.setPullLoadEnable(false);
				}
				if (commentInfos.size() > 0) {
					if (null != commentInfos.get(0)) {
						comNumView.setText(commentInfos.get(0).count + "条评论");
						tabCommentView.setText(commentInfos.get(0).count);
					}
				}
				adapter.addChannelList(commentInfos, isLoadMore);
				adapter.notifyDataSetChanged();
				break;
			case READ_COMMENT_FAIL:
				break;
			case READ_STOCK_SUCCESS:
				break;
			case READ_SHARE_SUCCESS:
				if (!TextUtils.isEmpty(nContentBean.sharenum)) {
					int count = Integer.valueOf((nContentBean.sharenum).trim());
					shareNumView.setText((count + 1) + "");
				} else {
					shareNumView.setText(1 + "");
				}
				break;
			case READ_SHARE_FAIL:
				break;
			case READ_MY_SUCCESS:
				try {
					StockInfoBean stockInfoBean = new StockInfoBean();
					stockInfoBean.stockcode = stockCode;
					stockInfoBean.stockname = URLDecoder.decode(stockName,
							"utf-8");
					stockInfoBean.focus = fouces.foucs;
					Bundle bundle = new Bundle();
					bundle.putSerializable("bean", stockInfoBean);
					Act.toAct(mContext, StockInfoActivity.class, bundle);
				} catch (UnsupportedEncodingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			case READ_MY_FAIL:
				Toast.makeText(mContext, "数据错误", Toast.LENGTH_SHORT).show();
				break;
			case COLLECT_SUCCESS:
				if (!isMyCollect) {
					setCollectState(true);
					Toast.makeText(mContext, "已收藏", Toast.LENGTH_SHORT).show();
				} else {
					setCollectState(false);
					Toast.makeText(mContext, "取消收藏", Toast.LENGTH_SHORT).show();
				}
				isMyCollect = !isMyCollect;
				break;
			case COLLECT_FAIL:
				if (!isMyCollect) {
					setCollectState(true);
					Toast.makeText(mContext, "收藏失败", Toast.LENGTH_SHORT).show();
				} else {
					setCollectState(false);
					Toast.makeText(mContext, "取消失败", Toast.LENGTH_SHORT).show();
				}
				break;
			}
		}
	};

	/**
	 * 设置股票信息
	 * 
	 * @Title: setSinaStock
	 * @Description: TODO
	 * @param @param result
	 * @return void
	 */
	private void setSinaStock(List<StockInfoBean> result) {
		if (result.size() != 0) {
			for (int i = 0; i < result.size()
					&& i < nContentBean.stocklist.size(); i++) {
				// result.get(i).stockcode = Utils
				// .getCodeFromLink(nContentBean.stocklist.get(i).stock);
				result.get(i).stockcode = nContentBean.stocklist.get(i).stock;
			}
			myStockListAdapter.getList().clear();
			myStockListAdapter.add(result);
			if (null != result.get(0)) {
				slidLayout.setVisibility(View.VISIBLE);
			}
			int count = hListview.getAdapter().getCount();
			int h = (int) (getPullLvHeight(hListview, count) - getResources()
					.getDimension(R.dimen.dp_70));
			int hide_h = (int) (getPullLvHeight(hListview, count - 1));
			if (count > 1) {
				slidingDrawer = new CustomSlidingDrawer(this, mListView,
						LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT,
						(int) (hide_h - getResources().getDimension(
								R.dimen.dp_60)));
			} else {
				slidingDrawer = new CustomSlidingDrawer(this, mListView,
						LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT, 1);
			}
			// slidingDrawer = new CustomSlidingDrawer(this, mListView,
			// LayoutParams.FILL_PARENT, 200, 100);
			slidLayout.addView(slidingDrawer);
			slidingDrawer.fillPanelContainer(hListLayout);
			// params.bottomMargin = params.bottomMargin = (int) getResources()
			// .getDimension(R.dimen.dp_60) + 180;
			// mListView.setLayoutParams(params);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 1001) {
			onRefresh();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub

	}

	private void onLoad() {
		mListView.stopRefresh();
		mListView.stopLoadMore();
		mListView.setRefreshTime("刚刚");
	}

	@Override
	public void onRefresh() {
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				isLoadMore = false;
				index = 0;
				getCommentListData();
				onLoad();
			}
		}, 2000);

	}

	@Override
	public void onLoadMore() {
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				isLoadMore = true;
				try {
					index = Integer.valueOf(commentInfos.get(commentInfos
							.size() - 1).id);
				} catch (Exception e) {
				}
				getCommentListData();
				onLoad();
			}
		}, 2000);

	}

	@Override
	public void loadTheme(String theme) {

	}

	@Override
	public void loadStock(String stockCode, String stockName) {
		this.stockCode = stockCode;
		this.stockName = stockName;
		ifFoucs();
	}

	@Override
	public void onCancel(Platform arg0, int arg1) {

	}

	@Override
	public void onComplete(Platform arg0, int arg1, HashMap<String, Object> arg2) {
		shareCount();
	}

	@Override
	public void onError(Platform arg0, int arg1, Throwable arg2) {

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// if (scrollState == OnScrollListener.SCROLL_STATE_IDLE) {
		if (view.getLastVisiblePosition() == view.getCount() - 1) {
			slidLayout.setVisibility(View.GONE);
		} else {
			if (stockInfoBeans.size() > 0 && null != stockInfoBeans.get(0)) {
				slidLayout.setVisibility(View.VISIBLE);
			}
		}
		// }
		// else {
		// slidLayout.setVisibility(View.GONE);
		// }
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		// TODO Auto-generated method stub

	}

}
