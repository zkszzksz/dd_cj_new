package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformDb;
import cn.sharesdk.framework.utils.UIHandler;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qzone.QZone;
import cn.sharesdk.wechat.friends.Wechat;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.ToastUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.HashMap;

/**
 * 绑定账号
 * 
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class BindAccountActivity extends BaseOtherLoginActivity {
	private String whatLogin = "";

	private LayoutInflater inflater = null;

	private CheckBox qqBox, sinaBox, wechatBox;
	private UserBean userBean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.band_account_layout);
		userBean = UserInfoUtil.getUserInfo(mContext);
		inflater = LayoutInflater.from(mContext);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleStrText("绑定账号");
		setLeftLayout(R.drawable.back_btn_selector, false);
		qqBox = (CheckBox) findViewById(R.id.qq_box);
		sinaBox = (CheckBox) findViewById(R.id.sina_box);
		wechatBox = (CheckBox) findViewById(R.id.wechat_box);

		setIfChecked();

		qqBox.setOnClickListener(this);
		sinaBox.setOnClickListener(this);
		wechatBox.setOnClickListener(this);

	}

	private void setIfChecked() {
		if (!TextUtils.isEmpty(userBean.qqid)) {
			qqBox.setChecked(true);
		} else
			qqBox.setChecked(false);
		if (!TextUtils.isEmpty(userBean.sinaid)) {
			sinaBox.setChecked(true);
		} else
			sinaBox.setChecked(false);
		if (!TextUtils.isEmpty(userBean.wxid)) {
			wechatBox.setChecked(true);
		} else
			wechatBox.setChecked(false);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.qq_box:
			whatLogin = TYPE_QQ;
			if (!qqBox.isChecked())
				loginBend();
			else
				authorize(new QZone(this));
			break;
		case R.id.sina_box:
			whatLogin = TYPE_SINA;
			if (!sinaBox.isChecked())
				loginBend();
			else
				authorize(new SinaWeibo(this));
			break;
		case R.id.wechat_box:
			whatLogin = TYPE_WEIXIN;
			if (!wechatBox.isChecked())
				loginBend();
			else
				authorize(new Wechat(this));
			break;
		}
	}

	/*
	 * @Override //删除 public void onCheckedChanged(CompoundButton buttonView,
	 * boolean isChecked) { if (!isChecked) { switch (buttonView.getId()) { case
	 * R.id.qq_box: whatLogin = TYPE_QQ; break; case R.id.sina_box: whatLogin =
	 * TYPE_SINA; break; case R.id.wechat_box: whatLogin = TYPE_WEIXIN; break; }
	 * loginBend(); return; } switch (buttonView.getId()) { case R.id.qq_box:
	 * whatLogin = TYPE_QQ; authorize(new QZone(this)); break; case
	 * R.id.sina_box: whatLogin = TYPE_SINA; authorize(new SinaWeibo(this));
	 * break; case R.id.wechat_box: whatLogin = TYPE_WEIXIN; authorize(new
	 * Wechat(this)); break; } }
	 */

	private void loginBend() {
		userBean.type = whatLogin;
		userBean.openid = "";
		loginBindUser(bindHandler, userBean);
		showProgressDialog("");
	}

	@Override
	public void onComplete(Platform platform, int action,
			HashMap<String, Object> res) {
		PlatformDb xx = platform.getDb();

		userBean.type = whatLogin;
		userBean.openid = xx.getUserId();

		if (action == Platform.ACTION_USER_INFOR) {
			UIHandler.sendEmptyMessage(MSG_AUTH_COMPLETE, this);
			login(platform.getName(), platform.getDb().getUserId(), res);
		}
		System.out.println("第三方登录数据：" + res);
	}

	private Handler bindHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case BIND_OK: // 绑定
				Object[] xx = (Object[]) msg.obj;

				if ((Boolean) xx[0]) {
					if (null != xx[2]) {
						UserBean userInfoBean = (UserBean) xx[2];
						UserInfoUtil.rememberUserInfo(mContext, userInfoBean);
					}
					ToastUtils.showShort(mContext, "操作成功");

					// Act.toAct(mContext, MainActivity.class);
					finish();
				} else
					ToastUtils.showLong(mContext, xx[1] + "");
				break;
			case Configs.READ_FAIL:
				ToastUtils.showShort(mContext, "数据异常");
				break;
			}
		}
	};

	public boolean handleMessage(Message msg) {
		switch (msg.what) {
		case MSG_USERID_FOUND:
//			Toast.makeText(this, R.string.userid_found, Toast.LENGTH_SHORT)
//					.show();
            PlatformDb xx = ((Platform)msg.obj).getDb();

            userBean.type = whatLogin;
            userBean.openid = xx.getUserId();
            loginBindUser(bindHandler, userBean);
            break;
		case MSG_LOGIN:
			// String text = getString(R.string.logining, msg.obj);
			// Toast.makeText(this, text, Toast.LENGTH_SHORT).show();
			break;
		case MSG_AUTH_CANCEL:
			Toast.makeText(this, R.string.auth_cancel, Toast.LENGTH_SHORT)
					.show();
			setIfChecked();
			break;
		case MSG_AUTH_ERROR:
			Toast.makeText(this, R.string.auth_error, Toast.LENGTH_SHORT)
					.show();
			setIfChecked();
			break;
		case MSG_AUTH_COMPLETE:
			Toast.makeText(this, R.string.auth_complete, Toast.LENGTH_SHORT)
					.show();
			showProgressDialog("");
			loginBindUser(bindHandler, userBean);
			break;
		}
		return false;
	}
}
