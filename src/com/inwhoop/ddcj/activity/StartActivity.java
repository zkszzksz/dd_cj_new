package com.inwhoop.ddcj.activity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import com.inwhoop.R;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.ScreenSizeUtil;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;

/**
 * @Describe: TODO 开始界面 * * * ****** Created by ZK ********
 * @Date: 2014/10/20 15:43
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class StartActivity extends BaseFragmentActivity {

	private Intent intent = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_start);
		findViewById(R.id.go_in).setOnClickListener(this);
		findViewById(R.id.register).setOnClickListener(this);
		findViewById(R.id.login_in).setOnClickListener(this);
		findViewById(R.id.visitor).setOnClickListener(this);
		findViewById(R.id.test1).setOnClickListener(this);

		int sw = ScreenSizeUtil.getScreenSize(this).screenW;
		int topW = sw * 291 / 377; // 377:291
		findViewById(R.id.top_layout).setLayoutParams(
				new LinearLayout.LayoutParams(topW, topW * 13 / 48)); // 480*130
		findViewById(R.id.bottom_layout).setLayoutParams(
				new LinearLayout.LayoutParams(sw, sw * 273 / 378)); // 378*273

		// intent = new Intent(mContext, XmppService.class);
		// startService(intent);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// stopService(intent);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.go_in:
			break;
		case R.id.login_in:
			// UserBean bean = UserInfoUtil.getUserInfo(mContext);
			// if ("0".equals(bean.userid))
//			Bundle bundle = new Bundle();
//			bundle.putString("act", "startactivity");
//			Act.toAct(mContext, LoginActivity.class,bundle);
			Intent intent=new Intent(mContext, LoginActivity.class);
			intent.putExtra("act", "startactivity");
			startActivity(intent);
			// else
			// Act.toAct(mContext, MainActivity.class);
			break;
		case R.id.register:
			Act.toAct(mContext, RegisterOneActivity.class);
			break;
		case R.id.visitor:
			// Act.toAct(mContext,FriendChatActivity.class);
			test();
			UserInfoUtil.clear(mContext);
			Act.toAct(mContext, MainActivity.class);
			break;
		case R.id.test1:
			// showCameraDialog(handler, true, 200, 200, true);
			break;

		}
	}

	List<StockInfoBean> list = null;

	private void test() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					// StockInfoBean info = new StockInfoBean();
					// info.stockcode = "sh601003";
					// info.stockname = "大秦铁路";
					// info.position = "四川省成都市";
					// StockUtil.addStockInfoToDB(info, handler, mContext);
					//
					// handler.sendEmptyMessage(0);
					UserBean userBean = UserInfoUtil.getUserInfo(mContext);
					String userid = userBean.userid;
					String location = userBean.location;

					// Object[] obj = JsonUtils.addUserChannel(userid, "1");
					// //【4001】

					// JsonUtils.addSubscribe("1", "1"); //【ok】 用户已经关注了频道
					// JsonUtils.delSubscribe("1", "1");
					// //【ok】{"msg":"取消关注成功","code":200}
					// JsonUtils.getUsersChannelList(userid); //
					// 【ok】[{"id":"5","name":"11","des":"2","img":"1"},{"id":"2","name":"22","des":"2","img":"2"}]
					// JsonUtils.myFocusStock("1");
					// JsonUtils.getUsersChannelList(userid); //
					// 【ok】[{"id":"5","name":"11","des":"2","img":"1"},{"id":"2","name":"22","des":"2","img":"2"}]
					// JsonUtils.myFocusStock("1");
					// //【ok】{"code":200,"msg":[{"stockcode":"1","userid":"1","id":"1","aliasname":null}]}
					// JsonUtils.addFocusStock("1", "sh601006"); //【ok】
					// {"code":200,"msg":"success"}
					// JsonUtils.delFoucsStock("1", "sh601006");
					// //【ok】{"msg":"取消关注成功","code":200}
					// JsonUtils.whatRankingList("1");
					// //【ok】{"msg":[{"img":"1414842497.jpg","id":"6","cate":"群组排行榜","categid":"1","name":"fdsfdsfd"},{"img":"1414842497.jpg","id":"9","cate":"群组排行榜","categid":"1","name":"dddd"}],"code":200}
					// JsonUtils.newsCount("1"); // 【ok】{"code":200,"msg":"7"}
					// JsonUtils.searchNewsForLocation("成都", 1, 5);
					// //【ok】{"code":200,"msg":[{"id":"4","type":"2","content":"555","imgpath":"234234","userid":"2","createtime":null,"location":"\u6210\u90fd","count":"3"},{"id":"3","type":"2","content":"333","imgpath":"333","userid":"1","createtime":null,"location":"\u6210\u90fd","count":"3"}]}
					// JsonUtils.newsInfos("1", "sh601006", 1, 4);
					// //{"msg":"stockcode未查询到任何信息","code":400}
					// JsonUtils.searchMyComment("1", 1, 3);
					// //【ok】{"code":200,"msg":[{"id":"2","newsid":"2","channelid":"2","userid":"2","content":"2","createtime":null,"title":null,"imgpath":null,"touserid":"1","status":"0","count":"2"},{"id":"1","newsid":"1","channelid":"1","userid":"1","content":"1","createtime":null,"title":null,"imgpath":null,"touserid":"1","status":"0","count":"2"}]}
					// JsonUtils.subcateginfo("1", "424", 1, 1); //
					// 【ok】{"code":200,"msg":[{"id":"4","relateid":"423","subcategid":"424","type":"1","count":"3"}]}
					// JsonUtils.insertPersonNews(new NetInsertPersonNewsBean(
					// "1", 0, "test content", "323",
					// Utils.imgToBase64(resultBmp))); //【ok】
					// JsonUtils.dianZan(userid, "" + 2) ;
					// //【ok】{"msg":"点赞成功","code":200}
					String touseid = "MczxUp3hMpTdQazsMdDQ0NjQ2NTU5LDE3LDE0MTEyNzA5NTI0NgO0O0OO0O0O";
					// JsonUtils.genTie(new
					// NetInvitationBean(cha"centtest","test,title","",touseid,userid,"3","1"));//【ok】{"msg":"跟帖成功","code":200}
					// JsonUtils.sendTie(new
					// NetInvitationBean("contenttest","test,title","path",userid,"3","1"));
					// //【ok】 {"msg":"发帖成功","code":200}
					// JsonUtils.collectNews(userid,"2"); //【OK】你已经收藏过该资讯了
					// JsonUtils.forwardMsg(userid,"2");
					// //【OK】{"msg":"没有找到这条资讯","code":300}
					// JsonUtils.getNewsComment(userid, "2", 5, 1); //
					// 【ok】{"code":200,"msg":[{"id":"2","newsid":"2","channelid":"2","userid":"2","content":"2","createtime":null,"title":null,"imgpath":null,"touserid":"1","status":"0","type":null,"count":"1"}]}

					// JsonUtils.getNewsContent("16"); //【ok】，
					// JsonUtils.getUserFriendsList(userid); //【同下下下】
					JsonUtils.attentionUser(userid, touseid, "来自群"); // 【ok】{"msg":"关注成功","code":200}
																		// [您已经关注过了]
					// JsonUtils.cancelAttentio(userid,touseid); // 【ok】取消关注成功
					// JsonUtils.myConcern(userid,touseid); //
					// {"code":200,"msg":[{"frienduserid":"MczxIpxhMpzdIaxsMdzIxMzIxMywxLDE0MTEwNjIwMTQwOAO0O0OO0O0O","name":"1","img":"\/upload\/header\/1","location":"1","otherinfo":"1"},{"frienduserid":"MczxUp3hMpTdQazsMdDQ0NjQ2NTU5LDgsMTQxMTExMTUwMjU2","name":"\u6d4b\u8bd5111","img":"\/upload\/header\/","location":null,"otherinfo":null},{"frienduserid":"OcDxYpyhMpDdka4sMdDI2MDg2MDU5LDE3LDE0MTEyNzE0MDA0NgO0O0OO0O0O","name":"\u6d4b\u8bd5","img":"\/upload\/header\/","location":"\u5c71\u897f\u7701\u0000
					// \u664b\u57ce\u5e02","otherinfo":""},{"frienduserid":"MczxUp3hMpTdQazsMdDQ0NjQ2NTU5LDksMTQxMTEyMTM0MzAy","name":"ganyuchuan","img":"\/upload\/header\/","location":null,"otherinfo":""},{"frienduserid":"OcDxYpyhMpDdka4sMdDI2MDg2MDU5LDE3LDE0MTEyNzE0MDA0NgO0O0OO0O0O","name":"\u6d4b\u8bd5","img":"\/upload\/header\/","location":"\u5c71\u897f\u7701\u0000
					// \u664b\u57ce\u5e02","otherinfo":""}]}
					// JsonUtils.MyFance(userid); // 【ok】

					// JsonUtils.getCityFriend(userid,"成都"); //【ok】返回是user对象列表

					// JsonUtils.getNewsContent("16"); //【ok】，
					// JsonUtils.getUserFriendsList(userid); //【同下下下】
					// JsonUtils.attentionUser(userid, touseid,"来自群");
					// //【ok】{"msg":"关注成功","code":200} [您已经关注过了]
					// JsonUtils.cancelAttentio(userid,touseid); // 【ok】取消关注成功
					// JsonUtils.myConcern(userid,touseid); //
					// {"code":200,"msg":[{"frienduserid":"MczxIpxhMpzdIaxsMdzIxMzIxMywxLDE0MTEwNjIwMTQwOAO0O0OO0O0O","name":"1","img":"\/upload\/header\/1","location":"1","otherinfo":"1"},{"frienduserid":"MczxUp3hMpTdQazsMdDQ0NjQ2NTU5LDgsMTQxMTExMTUwMjU2","name":"\u6d4b\u8bd5111","img":"\/upload\/header\/","location":null,"otherinfo":null},{"frienduserid":"OcDxYpyhMpDdka4sMdDI2MDg2MDU5LDE3LDE0MTEyNzE0MDA0NgO0O0OO0O0O","name":"\u6d4b\u8bd5","img":"\/upload\/header\/","location":"\u5c71\u897f\u7701\u0000
					// \u664b\u57ce\u5e02","otherinfo":""},{"frienduserid":"MczxUp3hMpTdQazsMdDQ0NjQ2NTU5LDksMTQxMTEyMTM0MzAy","name":"ganyuchuan","img":"\/upload\/header\/","location":null,"otherinfo":""},{"frienduserid":"OcDxYpyhMpDdka4sMdDI2MDg2MDU5LDE3LDE0MTEyNzE0MDA0NgO0O0OO0O0O","name":"\u6d4b\u8bd5","img":"\/upload\/header\/","location":"\u5c71\u897f\u7701\u0000
					// \u664b\u57ce\u5e02","otherinfo":""}]}
					// JsonUtils.MyFance(userid); // 【ok】

					// JsonUtils.getCityFriend(userid,"成都"); //【ok】返回是user对象列表
					JsonUtils.getUserGroupList(userid);

				} catch (Exception e) {
					System.out.println("" + e.toString());
				}
			}
		}).start();
	}

	private Bitmap resultBmp;
	private String resultstr;
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case RESULT_CUT_IMG:
				resultBmp = (Bitmap) msg.obj;
				((ImageView) findViewById(R.id.test_img))
						.setImageBitmap(resultBmp);
				break;

			case RESULT_IMG:
				resultstr = msg.obj.toString();
				test1(resultstr);
				break;
			}
		}
	};

	private void test1(String path) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {

				} catch (Exception e) {
					System.out.println("" + e.toString());
				}
			}
		}).start();
	}

	public byte[] getByte(String fileName) {
		byte[] bt = null;
		FileInputStream stream = null;
		ByteArrayOutputStream out = null;
		try {
			stream = new FileInputStream(fileName);
			out = new ByteArrayOutputStream(1000);
			byte[] b = new byte[1000];
			int len = 0;
			while ((len = stream.read(b)) != -1) {
				out.write(b, 0, len);
			}
			stream.close();
			out.close();
			bt = out.toByteArray();
		} catch (Exception e) {
			e.printStackTrace();
			try {
				if (stream != null)
					stream.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			try {
				if (out != null)
					out.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		out = null;
		return bt;
	}

}
