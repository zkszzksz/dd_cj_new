package com.inwhoop.ddcj.activity;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout.LayoutParams;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.GroupInfomation;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.db.HandleGroupchatRecordDB;
import com.inwhoop.ddcj.db.HandleSetGrouptopDB;
import com.inwhoop.ddcj.util.ExitAppUtils;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class GroupSettingActivity extends BaseFragmentActivity {

    private LayoutInflater inflater = null;

    private CheckBox msgButton, stickButton;

    private RelativeLayout topLayout = null;

    private String groupid = "";

    private GroupInfomation info = null;

    private Button addfriend, searchRecButton,clearrecButton, deleteGroupButton;

    private TextView nameTextView, countTextView, introduceTextView;

    private boolean isAdmin = false;

    private HandleSetGrouptopDB db = null;

    private CheckBox topBox = null;
    private Context context;
    private UserBean userBean;
    private Object[] obj;

    private LinearLayout typelayout = null;
    private View lineView = null;

    private RadioButton meetButton, fredButton;

    private RadioGroup radioGroup = null;
    
    private HandleGroupchatRecordDB grdb = null;
    
    private boolean isclear = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_set_layout);
        context = GroupSettingActivity.this;
        inflater = LayoutInflater.from(mContext);
        groupid = getIntent().getStringExtra("groupid");
        info = (GroupInfomation) getIntent().getSerializableExtra("groupinfo");
        db = new HandleSetGrouptopDB(mContext);
        grdb = new HandleGroupchatRecordDB(mContext);
        initData();
    }

    @SuppressLint("CutPasteId")
    @SuppressWarnings("deprecation")
    @Override
    public void initData() {
        super.initData();
        userBean = UserInfoUtil.getUserInfo(context);
        setTitleStrText("群设置");
        setLeftLayout(R.drawable.back_btn_selector, false);
        msgButton = (CheckBox) findViewById(R.id.trouble_box);
        stickButton = (CheckBox) findViewById(R.id.trick_box);
        topLayout = (RelativeLayout) findViewById(R.id.toplayout);
        LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, getWindowManager()
                .getDefaultDisplay().getHeight() / 3 * 1);
        topLayout.setLayoutParams(parm);
        addfriend = (Button) findViewById(R.id.addqx_btn);
        addfriend.setOnClickListener(this);
        nameTextView = (TextView) findViewById(R.id.gname);
        countTextView = (TextView) findViewById(R.id.gcount);
        introduceTextView = (TextView) findViewById(R.id.groupintrouce);
        searchRecButton = (Button) findViewById(R.id.search_btn);
        searchRecButton.setOnClickListener(this);
        deleteGroupButton = (Button) findViewById(R.id.delgroup_btn);
        deleteGroupButton.setOnClickListener(this);
        clearrecButton = (Button) findViewById(R.id.clear_btn);
        clearrecButton.setOnClickListener(this);
        topBox = (CheckBox) findViewById(R.id.trick_box);
        if (db.isExist(groupid, UserInfoUtil.getUserInfo(mContext).tel)) {
            topBox.setChecked(true);
        } else {
            topBox.setChecked(false);
            typelayout = (LinearLayout) findViewById(R.id.grouptypelayout);
            meetButton = (RadioButton) findViewById(R.id.radioFemale);
            fredButton = (RadioButton) findViewById(R.id.radioMale);
            radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
            radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == meetButton.getId()) {
                        setType("yes");
                    } else {
                        setType("no");
                    }
                }
            });
            lineView = findViewById(R.id.groupline);
            if (db.isExist(groupid, UserInfoUtil.getUserInfo(mContext).tel)) {
                topBox.setChecked(true);
            } else {
                topBox.setChecked(false);
            }
            topBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton arg0, boolean flag) {
                    if (db.isExist(groupid, UserInfoUtil.getUserInfo(mContext).tel)) {
                        db.delete(groupid, UserInfoUtil.getUserInfo(mContext).tel);
                    }
                    if (flag) {
                        db.save(groupid, UserInfoUtil.getUserInfo(mContext).tel);
                    }
                }
            });
            readnet();
        }
    }

    private void readnet() {
        if (null != info) {
            setview();
            return;
        }
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    Object[] obj = JsonUtils.getVeryGroupInfo(groupid,
                            UserInfoUtil.getUserInfo(mContext).userid);
                    msg.obj = obj;
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = Configs.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @SuppressWarnings("unchecked")
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Object[] obj = (Object[]) msg.obj;
                    if ((Boolean) obj[0]) {
                        info = ((List<GroupInfomation>) obj[2]).get(0);
                        setview();
                    } else {
                        showToast("" + obj[1]);
                    }
                    break;

                case Configs.READ_FAIL:
                    break;

                default:
                    break;
            }
        }
    };

    private void setType(final String type) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    Object[] obj = JsonUtils.setGroupChatType(groupid, type);
                    msg.obj = obj;
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = Configs.READ_FAIL;
                }
                sethandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler sethandler = new Handler() {
        @SuppressWarnings("unchecked")
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Object[] obj = (Object[]) msg.obj;
                    if ((Boolean) obj[0]) {
                        showToast("设置成功");
                    } else {
                        showToast("" + obj[1]);
                    }
                    break;

                case Configs.READ_FAIL:
                    showToast("设置失败");
                    break;

                default:
                    break;
            }
        }
    };

    private void setview() {
        if (null == info) {
            return;
        }
        if (UserInfoUtil.getUserInfo(mContext).tel.equals(info.tel)) {
            addfriend.setVisibility(View.VISIBLE);
            isAdmin = true;
            deleteGroupButton.setText("删除并退出群");
            typelayout.setVisibility(View.VISIBLE);
            lineView.setVisibility(View.VISIBLE);
            if (info.ismeeting == 1) {
                meetButton.setChecked(true);
            } else {
                meetButton.setChecked(false);
            }
        } else {
            addfriend.setVisibility(View.GONE);
            isAdmin = false;
            deleteGroupButton.setText("退出群");
            typelayout.setVisibility(View.GONE);
            lineView.setVisibility(View.GONE);
        }
        nameTextView.setText(info.name);
        countTextView.setText(info.groupnum);
        introduceTextView.setText(info.des);

        deleteGroupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (UserInfoUtil.getUserInfo(mContext).userid.equals(info.adminid)) {
                    dialogShow("是否确认退出并删除该群?", true);
                } else {
                    dialogShow("是否确认退出该群?", false);
                }
            }
        });
    }

    private void dialogShow(String content, final boolean isAdmin) {
        new AlertDialog.Builder(context)
                .setTitle("提示")
                .setMessage(content)
                .setPositiveButton("是",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface arg0,
                                                int arg1) {
                                if (isAdmin) {
                                    delGroupData();
                                } else {
                                    quitGroupData();
                                }
                            }
                        }).setNegativeButton("否", null).show();
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.search_btn:
                Intent intent = new Intent(mContext, SearchChatRecordActivity.class);
                intent.putExtra("groupid", groupid);
                startActivity(intent);
                break;

            case R.id.clear_btn:
            	clearRec();
                break;
                
            case R.id.addqx_btn:
                Intent intent1 = new Intent(mContext, InvitationFriendActivity.class);
                intent1.putExtra("groupid", groupid);
                startActivity(intent1);
                break;
                
            case R.id.title_left_layout:
            	if(isclear){
    				setResult(1000);
    			}
                finish();
                break;

            default:
                break;
        }
    }
    
    private void clearRec(){
    	 new AlertDialog.Builder(context)
         .setTitle("提示")
         .setMessage("是否清空聊天记录？")
         .setPositiveButton("是",
                 new DialogInterface.OnClickListener() {

                     @Override
                     public void onClick(DialogInterface arg0,
                                         int arg1) {
                    	if(grdb.deleteGmember(userBean.tel,groupid)){
                    		showToast("操作成功");
                    		isclear = true;
                    	}else{
                    		showToast("操作失败");
                    	}
                     }
                 }).setNegativeButton("否", null).show();
    }

    private void quitGroupData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.quitGroup(userBean.userid, groupid);
                    if (((String) obj[0]).equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Toast.makeText(context, "你已成功退出该群", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, (String) obj[1], Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void delGroupData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.delGroup(userBean.userid, groupid);
                    if (((String) obj[0]).equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mnhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mnhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Toast.makeText(context, "你已成功删除该群", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, (String) obj[1], Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
    
    @Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if(isclear){
				setResult(1000);
			}
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

}
