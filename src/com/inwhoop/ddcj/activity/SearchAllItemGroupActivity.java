package com.inwhoop.ddcj.activity;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.SearchAllItemGrouplistAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.SearchUtil;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * @Project: DDCJ
 * @Title: SearchAllItemUserActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2015-1-30 下午3:50:33
 * @Copyright: 2015 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class SearchAllItemGroupActivity extends BaseFragmentActivity implements
		IXListViewListener {

	private XListView listView = null;

	private String content = "";

	private List<CateChannel> groupList  = new ArrayList<CateChannel>();
	
	private SearchAllItemGrouplistAdapter adapter = null;
	
	private int page = 1;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.search_all_item_user);
		content = getIntent().getStringExtra("content");
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleStrText("群组");
		setLeftLayout(R.drawable.back_btn_selector, false);
		listView = (XListView) findViewById(R.id.searchlistview);
		listView.setXListViewListener(this);
		listView.setPullLoadEnable(false);
		listView.setPullRefreshEnable(true);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
				Bundle bundle = new Bundle();
				bundle.putSerializable("CateChannel",
						adapter.getList().get(position-1));
				Intent intent = new Intent(mContext,
						GroupInfoActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});
		adapter = new SearchAllItemGrouplistAdapter(mContext);
		listView.setAdapter(adapter);
		showProgressDialog("正在加载数据，请稍后...");
		readData(0);
	}

	private void readData(final int id) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					if(id == 0){
						msg.obj = JsonUtils.searchAllGroup(content, UserInfoUtil.getUserInfo(mContext).userid, 10, 1);
					}else{
						msg.obj = JsonUtils.searchAllGroup(content, UserInfoUtil.getUserInfo(mContext).userid, 10, page);
					}
					msg.what = Configs.READ_SUCCESS;
					msg.arg1 = id;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			listView.stopLoadMore();
			listView.stopRefresh();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if((Boolean) obj[0]){
					if(msg.arg1 == 0){
						adapter.getList().clear();
						page = 1;
					}
					groupList = (List<CateChannel>) obj[2];
					if(null!=groupList&&groupList.size()>0&&null!=groupList.get(0)){
						if(groupList.size()<10){
							listView.setPullLoadEnable(false);
						}else{
							listView.setPullLoadEnable(true);
							page++;
						}
						adapter.addList(groupList);
						adapter.notifyDataSetChanged();
					}
				}else{
					showToast("网络错误，请稍后操作~~");
				}
				break;

			case Configs.READ_FAIL:
				showToast("网络错误，请稍后操作~~");
				break;
				
			default:
				break;
			}
		};
	};

	@Override
	public void onRefresh() {
		readData(0);
	}

	@Override
	public void onLoadMore() {
		readData(1);
	}

}
