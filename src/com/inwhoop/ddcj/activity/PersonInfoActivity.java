package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.OtherFriendInfo;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.CircleImageview;
import net.tsz.afinal.FinalBitmap;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class PersonInfoActivity extends BaseFragmentActivity {

    private LayoutInflater inflater = null;

    private LinearLayout groupLayout = null;

    private CircleImageview headImageview = null;

    private LinearLayout bottomLayout = null;
    private String userid = "";
    private UserBean userBean;
    private Context context;
    private OtherFriendInfo info;
    private TextView name;
    private TextView content;
    private TextView address;
    private TextView qmtv;
    private TextView relation;
    private TextView regtime;
    private TextView send_msg;
    private Object[] obj;
    private FinalBitmap fb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.person_info_layout);
        context = PersonInfoActivity.this;
        inflater = LayoutInflater.from(mContext);
        initData();
    }

    @Override
    public void initData() {
        super.initData();
        userBean = UserInfoUtil.getUserInfo(context);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.home_head);
        fb.configLoadfailImage(R.drawable.home_head);
        if (null != getIntent()) {
            userid = getIntent().getStringExtra("userid");
            if (null == userid) {
                userid = "";
            }
        }
        setTitleStrText("详细资料");
        setLeftLayout(R.drawable.back_btn_selector, false);
        headImageview = (CircleImageview) findViewById(R.id.pheadimg);
        name = (TextView) findViewById(R.id.name);
        content = (TextView) findViewById(R.id.content);
        address = (TextView) findViewById(R.id.address);
        qmtv = (TextView) findViewById(R.id.qmtv);
        relation = (TextView) findViewById(R.id.relation);
        regtime = (TextView) findViewById(R.id.regtime);
        send_msg = (TextView) findViewById(R.id.send_msg);
        headImageview.setBackground(getResources().getColor(
                R.color.person_info_head_bkg));
        groupLayout = (LinearLayout) findViewById(R.id.grouplistlayout);
        bottomLayout = (LinearLayout) findViewById(R.id.bottomlayout);
        bottomLayout.setOnClickListener(this);
        getOtherInfo();
    }

    private void getOtherInfo() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    info = JsonUtils
                            .getOtherFriendInfo(userBean.userid, userid);
                    if (null != info) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("个人信息页面异常" + e.toString());
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    setDataShow();
                    if (null != info.groupInfoList && info.groupInfoList.size() > 0) {
                        addGrouplayout();
                    }
                    break;
                case Configs.READ_FAIL:
                    break;
                case Configs.READ_ERROR:
                    break;
            }
        }
    };

    private void setDataShow() {
        if (info.img.startsWith("http://")) {
            fb.display(headImageview, info.img);
        } else {
            fb.display(headImageview, Configs.HOST + info.img);
        }
        name.setText(info.name);
        address.setText(info.location);
        qmtv.setText(info.otherinfo);
        relation.setText(info.relation);
        content.setText("用户ID：" + info.ddid);
        if (!"".equals(info.createtime)
                && info.createtime.lastIndexOf(" ") != -1) {
            regtime.setText("注册日期："
                    + info.createtime.substring(0,
                    info.createtime.lastIndexOf(" ")));
        }
        System.out.println("他人信息页面" + info.relation);
        if (info.relation.equals("陌生人")) {
            send_msg.setText("加关注");
        } else if (info.relation.equals("已关注")) {
            send_msg.setText("取消关注");
        }

        send_msg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (send_msg.getText().toString().trim().equals("加关注")) {
                    attention();
                } else if (send_msg.getText().toString().trim().equals("取消关注")) {
                    cancelAttention();
                }
            }
        });
    }


    private void attention() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.attentionUser(userBean.userid, userid,
                            "来自帐号查找");
                    if ((Boolean) obj[0]) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Configs.isCareChage=true;
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    send_msg.setText("取消关注");
                    Toast.makeText(context, "关注成功", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, "关注失败", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void cancelAttention() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.cancelAttentio(userBean.userid, userid);
                    if ((Boolean) obj[0]) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mcHandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mcHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Configs.isCareChage=true;
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    send_msg.setText("加关注");
                    Toast.makeText(context, "取消关注成功", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, "取消关注失败", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void addGrouplayout() {
        View view = null;
        for (int i = 0; i < info.groupInfoList.size(); i++) {
            view = inflater.inflate(R.layout.group_list_layout, null);
            View line = view.findViewById(R.id.line);
            if (i == 2) {
                line.setVisibility(View.GONE);
            }
            ImageView groupImg = (ImageView) view.findViewById(R.id.headimg);
            TextView gname = (TextView) view.findViewById(R.id.gname);
            fb.display(groupImg, Configs.HOST + info.groupInfoList.get(i).image);
            gname.setText(info.groupInfoList.get(i).name);
            groupLayout.addView(view);
            view.setTag(i);
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int pos = (Integer) view.getTag();
                    Intent intent = new Intent(mContext, GroupInfoActivity.class);
                    intent.putExtra("flag", 2);
                    intent.putExtra("groupid", info.groupInfoList.get(pos).groupid);
                    startActivity(intent);
                }
            });
        }
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        switch (v.getId()) {
            case R.id.bottomlayout:
                Bundle bundle = new Bundle();
                bundle.putString("touserid", userid);
                Act.toAct(mContext, HisStockListActivity.class, bundle);
                break;

            default:
                break;
        }
    }
}
