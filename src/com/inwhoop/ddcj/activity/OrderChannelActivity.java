package com.inwhoop.ddcj.activity;

import java.util.ArrayList;
import java.util.List;

import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.OrderChannelAdapter;
import com.inwhoop.ddcj.bean.OrderChannel;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.GridView;

/**
 * 订阅频道界面
 * 
 * @Project: DDCJ
 * @Title: OrderChannelActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-6 下午7:36:43
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class OrderChannelActivity extends BaseFragmentActivity {
	private OrderChannelAdapter adapter;
	private Context context;
	private GridView gridView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.order_channel);
		context = this;
		initData();
	}

	public void initData() {
		super.initData();
		setTitleResText(R.string.order_channel);
		setLeftLayout(R.drawable.back_btn_selector, false);
		gridView = (GridView) findViewById(R.id.gridview);
		List<OrderChannel> list = new ArrayList<OrderChannel>();

		for (int i = 0; i < 5; i++) {
			OrderChannel orderChannel = new OrderChannel();
			orderChannel.id = i + "";
			orderChannel.imgUrl = "";
			orderChannel.title = "沸点财经";
			list.add(orderChannel);
		}

		adapter = new OrderChannelAdapter(context, list);
		gridView.setAdapter(adapter);

		title_left_tv.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				finish();

			}
		});
	}

}
