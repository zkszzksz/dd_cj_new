package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import com.inwhoop.R;

/**
 * @Describe: TODO 使用帮助 * * * ****** Created by ZK ********
 * @Date: 2014/11/10 16:01
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class UseHelpActivity extends BaseFragmentActivity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_user_help);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.use_help);

	}
}
