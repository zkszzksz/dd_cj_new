package com.inwhoop.ddcj.activity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.framework.utils.UIHandler;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.CommentAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CommentBean;
import com.inwhoop.ddcj.bean.InvitationInfo;
import com.inwhoop.ddcj.bean.InvitationListInfo;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.CircleImageview;
import com.inwhoop.ddcj.view.MyLayout;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import net.tsz.afinal.FinalBitmap;

import java.util.HashMap;
import java.util.List;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: TODO 邀请函详情 内部会议，由邀请函的item跳转而来的详情
 * @Package com.inwhoop.ddcj.activity
 * @Date: 2014/11/27 14:41
 * @version: 1.0
 */
public class InvitationContentMeetingActivity extends BaseFragmentActivity
		implements IXListViewListener, PlatformActionListener, Handler.Callback {

	private final static int READINFO_SUCCESS = 101;
	private final static int READINFO_FAIL = 102;

	private final static int READCOMMENT_SUCCESS = 103;
	private final static int READCOMMENT_FAIL = 104;

	private final static int SEND_SUCCESS = 105;
	private final static int SEND_FAIL = 106;

	private CommentAdapter adapter;
	private PopupWindow checkPopupWindow;

	private InvitationListInfo info = null;

	private TextView bmTextView = null, commentcountTextView;

	private InvitationInfo invitationInfo = null;

	private MyLayout piclayout = null;

	private LayoutInflater inflater = null;

	private EditText commentEditText = null;

	private TextView sendTextView = null;

	private String comment = "";

	private FinalBitmap fb = null;
	private View headLayout;// 头部layout
	private XListView mXlistview;
    private UserBean userbean;

    @Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_invitation_content);
        userbean = UserInfoUtil.getUserInfo(mContext);

        inflater = LayoutInflater.from(mContext);
        fb = FinalBitmap.create(mContext);
		fb.configLoadingImage(R.drawable.qingbao_loading_img);
		fb.configLoadfailImage(R.drawable.qingbao_loading_img);
		ShareSDK.initSDK(mContext);
		info = (InvitationListInfo) getIntent().getSerializableExtra("info");
		initPopupWindow();
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleStrText("" + info.title);
		setRightLayout(R.drawable.btn_yaoqinghan_content_right_selector, false);
		title_right_layout.setOnClickListener(this);

		headLayout = inflater.inflate(R.layout.act_inside_meeting, null);

		mXlistview = (XListView) findViewById(R.id.listview);
		mXlistview.addHeaderView(headLayout);
		mXlistview.setPullLoadEnable(true);
		mXlistview.setPullRefreshEnable(false);
		mXlistview.setXListViewListener(this);
		adapter = new CommentAdapter(mContext);
		mXlistview.setAdapter(adapter);
		bmTextView = (TextView) headLayout.findViewById(R.id.top_head_sign_up);
		bmTextView.setOnClickListener(this);
		((TextView) headLayout.findViewById(R.id.top_title))
				.setText(info.title);
		((TextView) headLayout.findViewById(R.id.top_head_name))
				.setText(info.name);
		((TextView) headLayout.findViewById(R.id.top_head_location))
				.setText(info.location);
		((TextView) headLayout.findViewById(R.id.top_head_time))
				.setText(info.time);

		piclayout = (MyLayout) headLayout.findViewById(R.id.piclayout);

		commentEditText = (EditText) findViewById(R.id.edit_input_view_edit);
		sendTextView = (TextView) findViewById(R.id.edit_input_view_send);
		sendTextView.setOnClickListener(this);
		commentcountTextView = (TextView) findViewById(R.id.center_how_many_comment);

		readData();
		readCommentlist(-1);
	}

	private void readData() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = JsonUtils.getInvitationInfo(""+userbean.userid,info.id);
					msg.obj = obj;
					msg.what = READINFO_SUCCESS;
				} catch (Exception e) {
					msg.what = READINFO_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private void readCommentlist(final int id) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = JsonUtils.getInvitationCommentlist(info.id,
							id, 10);
					msg.obj = obj;
					msg.what = READCOMMENT_SUCCESS;
				} catch (Exception e) {
					msg.what = READCOMMENT_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private void setHeadImgsLayout() {
		LinearLayout addLayout = (LinearLayout) findViewById(R.id.add_head_img_layout);
		int count = 0;
		if (invitationInfo.user.size() <= 5) {
			count = invitationInfo.user.size();
		} else {
			count = 5;
		}
		((TextView) findViewById(R.id.center_how_many_login)).setText("已有"
				+ count + "人报名");
		if (count == 0) {
			addLayout.setVisibility(View.GONE);
			return;
		} else {
			addLayout.setVisibility(View.VISIBLE);
		}
		addLayout.removeAllViews();
		int imgW = ScreenSizeUtil.getScreenSize(mContext).screenW / 7 - 10;
		for (int i = 0; i < count; i++) {
			CircleImageview img = new CircleImageview(mContext);
			img.setBackgroundResource(R.drawable.home_head);
			img.setScaleType(ImageView.ScaleType.CENTER_CROP);
			fb.display(img, invitationInfo.user.get(i).img);
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					imgW, imgW);
			img.setTag(i);
			img.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					int pos = (Integer) v.getTag();
					Intent intent = new Intent(mContext, PersonInfoActivity.class);
					intent.putExtra("userid", invitationInfo.user.get(pos).userid);
					startActivity(intent);
				}
			});
			parm.setMargins(0, 0, 10, 0);
			img.setLayoutParams(parm);
			addLayout.addView(img);
		}
		if (count > 5) {
			ImageView img = new ImageView(mContext);
			img.setBackgroundResource(R.drawable.look_more_selector);
			img.setScaleType(ImageView.ScaleType.CENTER_CROP);
			LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
					imgW, imgW);
			parm.setMargins(0, 0, 10, 0);
			img.setLayoutParams(parm);
			addLayout.addView(img);
			img.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View arg0) {
					Bundle b = new Bundle();
					b.putSerializable("bean", invitationInfo);
					Act.toAct(mContext, InvitationListActivity.class, b);
				}
			});
		}
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.title_right_layout:
			checkPopupWindow.showAsDropDown(v, -160, 7);
			break;

		case R.id.top_head_sign_up:
			baoming();
			break;

		case R.id.edit_input_view_send:
			sendComment();
			break;

		}
	}

	private void sendComment() {
		comment = commentEditText.getText().toString().trim();
		if ("".equals(comment)) {
			showToast("评论内容不能为空~~");
			return;
		}
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = JsonUtils.sendInvitationComment(
							UserInfoUtil.getUserInfo(mContext).userid, info.id,
							comment);
					msg.obj = obj;
					msg.what = SEND_SUCCESS;
				} catch (Exception e) {
					msg.what = SEND_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	/**
	 * 删除活动，删除这个邀请函
	 */
	private void deleteInvitationAct() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.deleteInvitationAct(
							UserInfoUtil.getUserInfo(mContext).userid, ""
									+ info.id);
					msg.what = Configs.DELETE_SUCCESS;
				} catch (Exception e) {
					msg.obj = e.toString();
					msg.what = Configs.DELETE_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	@SuppressWarnings("deprecation")
	private void initPopupWindow() {
		View view = LayoutInflater.from(mContext).inflate(
				R.layout.invitation_popupwindow_layout, null);
		checkPopupWindow = new PopupWindow(view,
				RelativeLayout.LayoutParams.WRAP_CONTENT,
				RelativeLayout.LayoutParams.WRAP_CONTENT, true);
		checkPopupWindow.setContentView(view);
		checkPopupWindow.setFocusable(true);
		checkPopupWindow.setOutsideTouchable(true);
		checkPopupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		view.findViewById(R.id.wechat_share_layout).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Platform.ShareParams sp = new Platform.ShareParams();
						// sp.setTitle(getString(R.string.wechat_demo_title));
						sp.setText("分享这个text文字");
						sp.setShareType(Platform.SHARE_TEXT);

						Platform plat = null;
						plat = ShareSDK.getPlatform("Wechat");
						plat.setPlatformActionListener(InvitationContentMeetingActivity.this);
						plat.share(sp);

						checkPopupWindow.dismiss();
					}
				});
		view.findViewById(R.id.say_invitation_layout).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						Bundle b = new Bundle();
						b.putSerializable("bean", invitationInfo);
						Act.toAct(mContext, InvitationListActivity.class, b);
						checkPopupWindow.dismiss();
					}
				});
		view.findViewById(R.id.del_activity_layout).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						checkPopupWindow.dismiss();

						AlertDialog.Builder builder = new AlertDialog.Builder(
								mContext);
						builder.setMessage("确认删除此活动？");
						// builder.setTitle("提示");
						builder.setPositiveButton("确认",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										deleteInvitationAct();
									}
								});
						builder.setNegativeButton("取消",
								new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog,
											int which) {
										dialog.dismiss();
									}
								});
						builder.create().show();
					}
				});
	}

	@Override
	protected void onPause() {
		super.onPause();
		KeyBoard.HiddenInputPanel(mContext);
	}

	private void baoming() {
		showProgressDialog("正在提交您的申请信息，请稍后~~");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = JsonUtils.baomingToInvitation(
							UserInfoUtil.getUserInfo(mContext).userid, info.id);
					msg.obj = obj;
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					showToast("报名成功");
					readData();
				} else {
					showToast(obj[1].toString());
				}
				break;

			case Configs.READ_FAIL:
				showToast("申请失败，请稍后再试~~");
				break;

			case READINFO_FAIL:
				showToast("获取信息失败，请稍后再试~~");
				break;

			case READINFO_SUCCESS:
				Object[] obj1 = (Object[]) msg.obj;
				if ((Boolean) obj1[0]) {
					@SuppressWarnings("unchecked")
					List<InvitationInfo> list = (List<InvitationInfo>) obj1[2];
					if (null != list && list.size() > 0) {
						invitationInfo = list.get(0);
						if (null != invitationInfo) {
							((TextView) findViewById(R.id.top_head_context))
									.setText("" + invitationInfo.content);
							commentcountTextView.setText(""
									+ invitationInfo.commentcount + "条评论");
							setHeadImgsLayout();
							addPiclayout();
						}
					}
				}
				break;

			case READCOMMENT_FAIL:
				showToast("申请失败，请稍后再试~~");
				break;

			case READCOMMENT_SUCCESS:
				Object[] cobj = (Object[]) msg.obj;
				if ((Boolean) cobj[0]) {
					@SuppressWarnings("unchecked")
					List<CommentBean> cl = (List<CommentBean>) cobj[2];
					for (int i = 0; i < cl.size(); i++) {
						cl.get(i).createtime = TimeUtils.twoDateDistance(
								TimeRender.getStandardDate(Long.parseLong(cl
										.get(i).createtime + "000")),
								TimeRender.getStandardDate());
					}
					if (cl.size() < 10) {
						mXlistview.setPullLoadEnable(false);
					}
					adapter.add(cl);
					adapter.notifyDataSetChanged();
				}
				break;

			case SEND_FAIL:
				showToast("评论失败，请稍后再试~~");
				break;

			case SEND_SUCCESS:
				Object[] sobj = (Object[]) msg.obj;
				if ((Boolean) sobj[0]) {
					CommentBean bean = new CommentBean();
					bean.content = comment;
					bean.userid = userbean.userid;
					bean.name = userbean.name;
					bean.createtime = TimeUtils.twoDateDistance(
							TimeRender.getStandardDate(),
							TimeRender.getStandardDate());
					bean.img = userbean.img;
					adapter.add(bean);
					adapter.notifyDataSetChanged();
					commentEditText.setText("");
					invitationInfo.commentcount = invitationInfo.commentcount + 1;
					commentcountTextView.setText(""
							+ invitationInfo.commentcount + "条评论");
					showToast("评论成功");
				} else {
					showToast("" + sobj[1].toString());
				}
				break;

			case Configs.DELETE_SUCCESS:
				Object[] obj11 = (Object[]) msg.obj;
				if ((Boolean) obj11[0] && !"".equals(obj11[1])) {
					ToastUtils.showShort(mContext, "删除成功");
					Configs.isRe = true;
					finish();
				}else {
                    ToastUtils.showShort(mContext, "您无法删除此活动 "+obj11[1]);
                }
				break;
			case Configs.DELETE_FAIL:
				ToastUtils.showShort(mContext, "" + msg.obj);
				break;
			default:
				break;
			}
		}
	};

	private void addPiclayout() {
		View item = null;
		piclayout.removeAllViews();
		@SuppressWarnings("deprecation")
		int w = (int) (getWindowManager().getDefaultDisplay().getWidth() - getResources()
				.getDimension(R.dimen.invitation_pic_space));
		LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(w / 4,
				w / 4);
		for (int i = 0; i < invitationInfo.img.size(); i++) {
			item = inflater.inflate(R.layout.invitation_imgs_itemlayout, null);
			ImageView img = (ImageView) item.findViewById(R.id.itemimg);
			img.setScaleType(ImageView.ScaleType.CENTER_CROP);
			img.setTag(invitationInfo.img.get(i));
			img.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					Intent intent = new Intent(mContext, ShowWebImageActivity.class);
					intent.putExtra("image", v.getTag().toString());
					startActivity(intent);
				}
			});
			fb.display(img, invitationInfo.img.get(i));
			img.setLayoutParams(parm);
			piclayout.addView(item);
		}
	}

	@Override
	public void onRefresh() {
	}

	@Override
	public void onLoadMore() {
		readCommentlist(adapter.getlist().get(adapter.getlist().size() - 1).id);
	}

	@Override
	public void onComplete(Platform platform, int action,
			HashMap<String, Object> stringObjectHashMap) {
		System.out.println("URL地址为URL地址为URL地址为====onComplete");
		Message msg = new Message();
		msg.arg1 = 1;
		msg.arg2 = action;
		msg.obj = platform;
		UIHandler.sendMessage(msg, this);
	}

	@Override
	public void onError(Platform platform, int action, Throwable throwable) {
		System.out.println("URL地址为URL地址为URL地址为====onError");
		Message msg = new Message();
		msg.arg1 = 2;
		msg.arg2 = action;
		msg.obj = throwable;
		UIHandler.sendMessage(msg, this);

	}

	@Override
	public void onCancel(Platform platform, int action) {
		System.out.println("URL地址为URL地址为URL地址为====onCancel");
		Message msg = new Message();
		msg.arg1 = 3;
		msg.arg2 = action;
		msg.obj = platform;
		UIHandler.sendMessage(msg, this);
	}

	public boolean handleMessage(Message msg) {
		String text = actionToString(msg.arg2);
		switch (msg.arg1) {
		case 1: {
			// 成功
			Platform plat = (Platform) msg.obj;
			text = plat.getName() + " completed at " + text;
		}
			break;
		case 2: {
			// 失败
			if ("WechatClientNotExistException".equals(msg.obj.getClass()
					.getSimpleName())) {
				text = getString(R.string.wechat_client_inavailable);
			} else if ("WechatTimelineNotSupportedException".equals(msg.obj
					.getClass().getSimpleName())) {
				text = getString(R.string.wechat_client_inavailable);
			} else {
				text = getString(R.string.share_failed);
			}
		}
			break;
		case 3: {
			// 取消
			Platform plat = (Platform) msg.obj;
			text = plat.getName() + " canceled at " + text;
		}
			break;
		}

		Toast.makeText(mContext, text, Toast.LENGTH_LONG).show();
		return false;
	}

	/**
	 * 将action转换为String
	 */
	public static String actionToString(int action) {
		switch (action) {
		case Platform.ACTION_AUTHORIZING:
			return "ACTION_AUTHORIZING";
		case Platform.ACTION_GETTING_FRIEND_LIST:
			return "ACTION_GETTING_FRIEND_LIST";
		case Platform.ACTION_FOLLOWING_USER:
			return "ACTION_FOLLOWING_USER";
		case Platform.ACTION_SENDING_DIRECT_MESSAGE:
			return "ACTION_SENDING_DIRECT_MESSAGE";
		case Platform.ACTION_TIMELINE:
			return "ACTION_TIMELINE";
		case Platform.ACTION_USER_INFOR:
			return "ACTION_USER_INFOR";
		case Platform.ACTION_SHARE:
			return "ACTION_SHARE";
		default: {
			return "UNKNOWN";
		}
		}
	}

}
