package com.inwhoop.ddcj.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.DragAdapter;
import com.inwhoop.ddcj.view.DragGridView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Describe: TODO 订阅列表管理 * * * ****** Created by ZK ********
 * @Date: 2014/11/10 17:28
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
@SuppressLint("WrongViewCast")
public class SubscribepListActivity extends BaseFragmentActivity {
	private List<HashMap<String, Object>> dataSourceList = new ArrayList<HashMap<String, Object>>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_subscribe_list);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.dingyueguanli);
		DragGridView mDragGridView = (DragGridView) findViewById(R.id.gridview);
		for (int i = 0; i < 5; i++) {
			HashMap<String, Object> itemHashMap = new HashMap<String, Object>();
			itemHashMap.put("item_image", R.drawable.subscribe_grass);
			itemHashMap.put("item_text", "拖拽 " + Integer.toString(i));
			dataSourceList.add(itemHashMap);
		}

		final DragAdapter mDragAdapter = new DragAdapter(this, dataSourceList);

		mDragGridView.setAdapter(mDragAdapter);
	}
}
