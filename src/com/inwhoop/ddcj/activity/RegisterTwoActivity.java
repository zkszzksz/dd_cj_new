package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;

/**
 * @Describe: TODO 注册第二个界面，获取验证码后输入验证码 * * * ****** Created by ZK ********
 * @Date: 2014/11/06 15:00
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class RegisterTwoActivity extends BaseFragmentActivity {

	private UserBean userBean;
	private Button reTryBtn;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_register_two);
		userBean = (UserBean) getIntent().getSerializableExtra("bean");
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleResText(R.string.register_two);
		setLeftLayout(R.drawable.back_btn_selector, false);
		setRightLayout(R.string.next_step, true);
		title_right_tv.setTextColor(getResources()
				.getColor(R.color.blue_028efd));
		title_right_layout.setOnClickListener(this);

		findViewById(R.id.my_next_btn).setOnClickListener(this);
		reTryBtn = (Button) findViewById(R.id.re_send_btn);
		setLoadingBtn();
	}

	private void setLoadingBtn() {
		MyCount mc = new MyCount(60 * 1000, 1000);
		mc.start();
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.title_right_layout:
			next();
			break;
		case R.id.my_next_btn: // 自定义跳转下关页面，隐藏了
			// phone = phoneEditText.getText().toString().trim();
			Bundle bundle = new Bundle();
			bundle.putSerializable("bean", userBean);
			Act.toAct(mContext, RegisterThreeActivity.class, bundle);
			break;

		default:
			break;
		}
	}

	private void next() {
		userBean.yzm = ((EditText) findViewById(R.id.code_edit)).getText()
				.toString().trim();
		String msg = check(userBean);
		if (!"".equals(msg)) {
			showToast(msg);
		} else {
			showProgressDialog("请稍后...");
			new Thread(new Runnable() {

				@Override
				public void run() {
					Message msg = new Message();
					try {
						msg.obj = JsonUtils.checkyzm(userBean);
						msg.what = Configs.READ_SUCCESS;
					} catch (Exception e) {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);
				}
			}).start();
		}
	}

	private Handler sendMsgHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			setLoadingBtn(); // 第一次点击后再次loadding
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("数据异常，请稍后再试~~");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
				} else {
				}
				showToast(obj[1].toString());
				break;
			}
		}
	};
	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("验证失败，请稍后再试~~");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					Bundle bundle = new Bundle();
					bundle.putSerializable("bean", userBean); // 可能you第三方绑定跳转过来注册

					Act.toAct(mContext, RegisterThreeActivity.class, bundle);
					// showToast(obj[1].toString()); //被提出不能提示
				} else {
					showToast(obj[1].toString());
				}
				break;

			default:
				break;
			}
		}
	};

	private String check(UserBean user) {
		if ("".equals(user.yzm)) {
			return "请输入验证码";
		}
		return "";
	}

	/* 定义一个倒计时的内部类 */
	class MyCount extends CountDownTimer {
		public MyCount(long millisInFuture, long countDownInterval) {
			super(millisInFuture, countDownInterval);
			reTryBtn.setBackgroundResource(R.drawable.btn_login_pressed_40);
			reTryBtn.setTextColor(getResources().getColor(R.color.black_2e2e2e));
		}

		@Override
		public void onFinish() {
			reTryBtn.setBackgroundResource(R.drawable.blue_btn_selector);
			reTryBtn.setText(R.string.retry_send);
			reTryBtn.setTextColor(getResources().getColor(R.color.white));
			reTryBtn.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					showProgressDialog("请稍后...");
					RegisterOneActivity.registerCheckPhone(sendMsgHandler,
							userBean);
				}
			});
		}

		@Override
		public void onTick(long millisUntilFinished) {
			long sec = millisUntilFinished / 1000;

			reTryBtn.setText("重新发送倒计时(" + sec + ")秒");
		}
	}
}