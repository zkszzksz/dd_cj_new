package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.GroupItemListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.FansBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.List;

/**
 * Created by Administrator on 2015/1/13.
 */
public class GroupItemActivity extends BaseFragmentActivity {
    private ListView listView;
    private String groupid = "";
    private List<FansBean> list;
    private Context context;
    private GroupItemListAdapter adapter;
    private UserBean userBean;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_item_activity);
        context = GroupItemActivity.this;
        initData();
        setLeftLayout(R.drawable.back_btn_selector, false);
        setTitleResText(R.string.group_item);
        initView();
    }

    private void initView() {
        userBean = UserInfoUtil.getUserInfo(context);
        if (null != getIntent()) {
            groupid = getIntent().getStringExtra("groupid");
            if (null == groupid) {
                groupid = "";
            }
        }
        listView = (ListView) findViewById(R.id.group_item_activity_list);
        adapter = new GroupItemListAdapter(context);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            	if(userBean.ddid.equals("")){
            		startActivity(new Intent(context, LoginActivity.class));
            	}else{
                Intent intent = new Intent(context, PersonInfoActivity.class);
                intent.putExtra("userid", list.get(i).id);
                startActivity(intent);
            	}
            }
        });
        fansData();
    }

    private void fansData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    list = JsonUtils.getFansData(groupid, userBean.userid);
                    if (null != list && list.size() > 0) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.println("群成员数据异常" + e.toString());
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    adapter.addList(list);
                    adapter.notifyDataSetChanged();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, "暂无群成员", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };
}