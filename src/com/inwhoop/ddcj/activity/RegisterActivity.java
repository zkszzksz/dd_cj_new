package com.inwhoop.ddcj.activity;

import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.inwhoop.ddcj.util.ToastUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * @Project: DDCJ
 * @Title: RegisterActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO 注册 目前无用
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-20 下午6:32:05
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class RegisterActivity extends BaseFragmentActivity {

	private EditText nickEditText, passEditText, surepassEditText,
			codeEditText;

	private Button registButton = null;

	private String nick = "", pass = "", surepass = "", code = "";

	private String phone = "";
	private UserBean otherUser;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.regist_layout);
		otherUser = (UserBean) getIntent().getSerializableExtra("bean");
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleResText(R.string.input_phone);
		setLeftLayout(R.drawable.back_btn_selector, false);
		phone = getIntent().getStringExtra("phone");

		nickEditText = (EditText) findViewById(R.id.nick_edit);
		nickEditText.setText("调通");
		passEditText = (EditText) findViewById(R.id.setpass_edit);
		passEditText.setText("123456");
		surepassEditText = (EditText) findViewById(R.id.surepass_edit);
		surepassEditText.setText("123456");
		codeEditText = (EditText) findViewById(R.id.code_edit);
		codeEditText.setText("123");
		registButton = (Button) findViewById(R.id.regist_btn);
		registButton.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.regist_btn:
			regist();
			break;

		default:
			break;
		}
	}

	private void regist() {
		nick = nickEditText.getText().toString().trim();
		pass = passEditText.getText().toString().trim();
		surepass = surepassEditText.getText().toString().trim();
		code = codeEditText.getText().toString().trim();
		String msg = check();
		if (!"".equals(msg)) {
			showToast(msg);
		} else {
			showProgressDialog("正在注册...");
			new Thread(new Runnable() {

				@Override
				public void run() {
					Message msg = new Message();
					try {
						String comeType = getIntent().getStringExtra("type");
						String comeOpenId = getIntent()
								.getStringExtra("openid");
						if (null != comeType) // 用于可能第三方绑定跳转过来注册
							comeType = "";
						if (null != comeOpenId)
							comeOpenId = "";
						UserBean bb = new UserBean();
						bb.tel = phone;
						bb.name = nick;
						bb.pwd = pass;
						bb.yzm = code;
						if (null != otherUser) { // 用于可能第三方绑定跳转过来注册
							bb.openid = otherUser.openid;
							bb.type = otherUser.type;
						}
						Object[] obj = JsonUtils.regist(bb);
						msg.obj = obj;
						msg.what = Configs.READ_SUCCESS;
					} catch (Exception e) {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);
				}
			}).start();

		}
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("注册失败，请稍后再试~~");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					showToast("注册成功");
					UserBean user = new UserBean();
					if (null != obj[2])
						user = (UserBean) obj[2];
					user.pwd = pass;
					UserInfoUtil.rememberUserInfo(mContext, user);
					ToastUtils.showShort(mContext, "登陆成功");

					Act.toActClearTop(mContext, MainActivity.class);
					finish();
				} else {
					showToast(obj[1].toString());
				}
				break;

			default:
				break;
			}
		};
	};

	private String check() {
		if ("".equals(nick)) {
			return "昵称不能为空";
		} else if ("".equals(pass)) {
			return "密码不能为空";
		} else if ("".equals(surepass)) {
			return "确认密码不能为空";
		} else if ("".equals(code)) {
			return "验证码不能为空";
		} else if (pass.length() < 6) {
			return "密码必须大于6个字节";
		} else if (!pass.equals(surepass)) {
			return "前后密码必须一致";
		}
		return "";
	}

}
