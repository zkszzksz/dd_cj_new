package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * @Describe: TODO 反馈建议 * * * ****** Created by ZK ********
 * @Date: 2014/11/10 15:28
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class WriteSuggestActivity extends BaseFragmentActivity {
	private UserBean user;

	@Override
	protected void onPause() {
		super.onPause();
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.showSoftInput(((EditText) findViewById(R.id.content)),
				InputMethodManager.SHOW_FORCED);
		imm.hideSoftInputFromWindow(
				((EditText) findViewById(R.id.content)).getWindowToken(), 0); // 强制隐藏键盘
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_write_suggest);
		user = UserInfoUtil.getUserInfo(mContext);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.tousujianyi);
		setRightLayout(R.string.send, true);
		title_right_tv.setTextColor(getResources()
				.getColor(R.color.blue_028efd));
		title_right_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				String content = ((EditText) findViewById(R.id.content))
						.getText().toString().trim();
				if ("".equals(content))
					showToast("您并未输入任何内容");
				else
					netSuggest(user.userid, content);
			}
		});
	}

	private void netSuggest(final String uid, final String content) {
		showProgressDialog("");
		progressDialog.setCancelable(false);
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Thread.sleep(500);
					msg.obj = JsonUtils.netSuggest(uid, content); // 暂缺
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("上传失败，请稍后再试~~");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					finish();
					showToast(obj[1].toString());
				} else {
					showToast(obj[1].toString());
				}
				break;
			}
		}
	};
}
