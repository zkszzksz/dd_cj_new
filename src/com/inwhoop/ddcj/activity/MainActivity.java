package com.inwhoop.ddcj.activity;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.JpushBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.bean.UserMessage;
import com.inwhoop.ddcj.db.PushMsgDb;
import com.inwhoop.ddcj.fragment.*;
import com.inwhoop.ddcj.impl.ClickTabCallBack;
import com.inwhoop.ddcj.service.XmppService;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.xmpp.FaceConversionUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * create by ZK TODO 主页，包含底部tab
 */
public class MainActivity extends BaseFragmentActivity implements
        ClickTabCallBack {
    private Fragment[] newFrags = new Fragment[]{
            new Tab0MyStockCertificateFragment(), new Tab1FindFragment(),
            new ChannelFragment(), new Tab3ChatRecordlistFragment(),
            new Tab4ContactsFragment()};
    private int[] texts = {R.string.zixuangu, R.string.faxian, R.string.zixun,
            R.string.xiaozhitiao, R.string.lianxiren};
    private Context context;
    private long firstClick;
    private Intent intent;
    private MessageListUtil messageListUtil;
    private boolean msgTag1 = false;
    private boolean msgTag2 = false;
    private PushMsgDb db;
    private UserBean userBean;
    private ImageView redArrow1;
    private ImageView redArrow2;
    private ReceiveBroadCast broadCast;
    private int position = 0;
    private String tag;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_main);
        context = MainActivity.this;
        Configs.widthPixels = getWindowManager().getDefaultDisplay().getWidth();
        Configs.heightPixels = getWindowManager().getDefaultDisplay()
                .getHeight();
        initData();
    }

    @Override
    public void initData() {
        if (null != getIntent()) {
            tag = getIntent().getStringExtra("tag");
        }
        getPosition(this);
        messageListUtil = MessageListUtil.getInstance(mContext);
        db = new PushMsgDb(mContext);
        userBean = UserInfoUtil.getUserInfo(context);

        broadCast = new ReceiveBroadCast();
        IntentFilter filter = new IntentFilter();
        filter.addAction(Configs.RED_SHOW);
        filter.addAction(Configs.RED_HIDE);
        filter.addAction(Configs.PUSH_ACTION1);
        filter.addAction(Configs.PUSH_ACTION6);
        filter.addAction(Configs.COME_MESSAGE2);
        mContext.registerReceiver(broadCast, filter);

        Drawable[] draws = new Drawable[]{
                getResources().getDrawable(R.drawable.zixuangu_img_checked),
                getResources().getDrawable(R.drawable.faxian_img_checked),
                getResources().getDrawable(R.drawable.zixun_img_checked),
                getResources().getDrawable(R.drawable.xiaozhitiao_img_checked),
                getResources().getDrawable(R.drawable.lianxiren_img_checked),};
        //第二个参数默认下标2
        super.initTabData(2, 2, false, newFrags, 1, draws, texts,
                R.style.top_tab_btn);
        btns[2].setChecked(true);
        if (null != tag && tag.equals("push")) {
            loadFragment(3, newFrags);
            btns[3].setChecked(true);
        }
        groupLayout.setBackgroundColor(getResources().getColor(
                R.color.black_title_3a3a3a));
        btns[2].setVisibility(View.VISIBLE);
        btns[3].setVisibility(View.VISIBLE);
        btns[4].setVisibility(View.VISIBLE);
        redArrow1 = (ImageView) findViewById(R.id.red_arrow1);
        redArrow2 = (ImageView) findViewById(R.id.red_arrow2);
        redArrow1.setVisibility(View.GONE);
        redArrow2.setVisibility(View.GONE);

        boolean isRun = isServiceRunning(context, XmppService.class.getName());
        // if (!isRun) {
        intent = new Intent(context, XmppService.class);
        startService(intent);
        // }
        new Thread(new Runnable() {
            @Override
            public void run() {
                FaceConversionUtil.getInstace().getFileText(getApplication());
            }
        }).start();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setMsgArrowShow();
    }

    /**
     * 获取底部Tab点击的位置
     *
     * @param pos
     */
    @Override
    public void setClickPos(int pos) {
        position = pos;
        if (position == 1) {
            db.updateQbiaoData("1", "1", userBean.userid);
            redArrow1.setVisibility(View.GONE);
        }
    }

    private void setMsgArrowShow() {
        msgTag1 = false;
        msgTag2 = false;
        List<UserMessage> lists = messageListUtil.getUserMsglist();
        if (null != lists && lists.size() > 0) {
            for (int i = 0; i < lists.size(); i++) {
                if (lists.get(i).number > 0) {
                    msgTag1 = true;
                    break;
                }
            }
        }

        List<JpushBean> beans = db.getQbData(userBean.userid);
        if (null != beans && beans.size() > 0) {
            List<JpushBean> bean1 = new ArrayList<JpushBean>();
            for (int i = 0; i < beans.size(); i++) {
                if (beans.get(i).isread.equals("0")) {
                    bean1.add(beans.get(i));
                }
            }
            if (null != bean1 && bean1.size() > 0) {
                msgTag2 = true;
            }
            bean1 = null;
        }
        if (msgTag1 || msgTag2) {
            redArrow2.setVisibility(View.VISIBLE);
        } else {
            redArrow2.setVisibility(View.GONE);
        }
        readQBiaoData();
    }

    private void readQBiaoData() {
        List<JpushBean> list = db.getQbData("1", userBean.userid);
        if (null != list && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).isread.equals("0")) {
                    if (position != 1) {
                        redArrow1.setVisibility(View.VISIBLE);
                    } else {
                        db.updateQbiaoData("1", "1", userBean.userid);
                        redArrow1.setVisibility(View.GONE);
                    }
                    break;
                }
            }
        }
    }

    /**
     * 判断服务是否还在运行
     *
     * @param mContext  上下文对象
     * @param className 服务的名称
     * @return
     */
    public boolean isServiceRunning(Context mContext, String className) {

        boolean isRunning = false;
        ActivityManager activityManager = (ActivityManager) mContext
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningServiceInfo> serviceList = activityManager
                .getRunningServices(30);

        if (!(serviceList.size() > 0)) {
            return false;
        }

        for (int i = 0; i < serviceList.size(); i++) {
            if (serviceList.get(i).service.getClassName().equals(className) == true) {
                isRunning = true;
                break;
            }
        }
        return isRunning;
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
    if (keyCode == KeyEvent.KEYCODE_BACK) {
			long secondClick = System.currentTimeMillis();
			if (secondClick - firstClick > 5000) {
				Toast.makeText(MainActivity.this,
						getResources().getString(R.string.exit),
						Toast.LENGTH_SHORT).show();
				firstClick = secondClick;
				return true;
			} else {
				finish();
				ExitAppUtils.getInstance().exit();
				handler.sendEmptyMessageDelayed(0, 1000);
				return true;
			}
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private Handler handler = new Handler(){
		public void handleMessage(android.os.Message msg) {
			System.exit(0);
		};
	};
	
	@Override
	protected void onPause() {
		super.onPause();
	}
	
	@Override
	protected void onStop() {
		super.onStop();
	}

    @Override
    protected void onDestroy() {
        try {
            Utils.savePreference(context, "nodata", "");
            stopService(intent);
            if (null != broadCast) {
                unregisterReceiver(broadCast);
            }
        } catch (Exception e) {
        }
        super.onDestroy();
    }

    private class ReceiveBroadCast extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            // 得到广播中得到的数据，并显示出来
            if (null != intent) {
                if (intent.getAction().equals(Configs.RED_HIDE)) {
                    redArrow1.setVisibility(View.GONE);
                } else if (intent.getAction().equals(Configs.PUSH_ACTION6)
                        || intent.getAction().equals(Configs.COME_MESSAGE2)) {
                    setMsgArrowShow();
                } else if (intent.getAction().equals(Configs.PUSH_ACTION1)) {
                    readQBiaoData();
                }
            }
        }

    }

}
