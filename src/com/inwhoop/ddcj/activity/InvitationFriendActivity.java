package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.InvitationFriendListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ContactsInfo;
import com.inwhoop.ddcj.bean.InviteJoinBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * 邀请好友页面
 * Created by Administrator on 2015/1/14.
 */
public class InvitationFriendActivity extends BaseFragmentActivity {
    private ListView listView;
    private UserBean userBean;
    private Context context;
    private Object[] objs;
    private InvitationFriendListAdapter adapter;
    private String groupId;
    private Object[] obj;
    private List<InviteJoinBean> list;
    private RelativeLayout right_layout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invitation_friend_activity);
        context = InvitationFriendActivity.this;
        initData();
        setLeftLayout(R.drawable.back_btn_selector, false);
        setTitleResText(R.string.yaoq);
        setRightLayout(R.string.sure, true);
        initView();
    }

    private void initView() {
        userBean = UserInfoUtil.getUserInfo(context);
        if (null != getIntent()) {
            groupId = getIntent().getStringExtra("groupid");
        }
        listView = (ListView) findViewById(R.id.invitation_friend_activity_list);
        adapter = new InvitationFriendListAdapter(context);
        listView.setAdapter(adapter);
        getFriendData();

        right_layout = (RelativeLayout) findViewById(R.id.title_right_layout);
        right_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                List<InviteJoinBean> list1 = new ArrayList<InviteJoinBean>();
                if (null != adapter.getList() && adapter.getList().size() > 0) {
                    for (int i = 0; i < adapter.getList().size(); i++) {
                        if (adapter.getList().get(i).checkTag) {
                            list1.add(adapter.getList().get(i));
                        }
                    }
                    invitationFriend(list1);
                } else {
                    Toast.makeText(context, "请选择要邀请的好友", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void getFriendData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    list = JsonUtils.getInviteJoinData(
                            userBean.userid, groupId);
                    if (null != list && list.size() > 0) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    System.out.println("邀请好友异常111" + e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    adapter.addList(list);
                    adapter.notifyDataSetChanged();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, "暂无好友数据", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "您的好友已全部加入该群", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void invitationFriend(List<InviteJoinBean> list1) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        List<JsonBean> list = new ArrayList<JsonBean>();
        JsonBean bean = null;
        if (null != list1 && list1.size() > 0)
            for (int i = 0; i < list1.size(); i++) {
                bean = new JsonBean();
                bean.userid = list1.get(i).frienduserid;
                bean.groupid = groupId;
                list.add(bean);
            }
        final String jsonStr = gson.toJson(list);
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.inviteJoinGroup(jsonStr);
                    if (((String) obj[0]).equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    System.out.println("邀请好友异常" + e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Toast.makeText(context, "邀请成功，等待好友确认", Toast.LENGTH_SHORT).show();
                    finish();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, (String) obj[1], Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private class JsonBean {
        protected String userid = "";
        protected String groupid = "";
    }
}