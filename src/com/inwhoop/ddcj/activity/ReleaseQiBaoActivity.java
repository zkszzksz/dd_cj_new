package com.inwhoop.ddcj.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.*;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.GroupBean;
import com.inwhoop.ddcj.bean.PsClassImg;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.BitMapInsertEdit;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * 编译情报
 */
public class ReleaseQiBaoActivity extends BaseFragmentActivity implements
		OnClickListener, MLocationLinsener {

	private LayoutInflater inflater;
	private ImageView addImgBtn;
	private BitMapInsertEdit bitMapEdit;
	private ArrayList<PsClassImg> imglist = new ArrayList<PsClassImg>();
	private TextView imgsizeTextView;

	private String title = "", address = "", pushtime = "", introduce = "";

	private EditText titleEditText, addressEditText;

	private BitMapInsertEdit contentEditText;

	private TextView timeTextView = null;

	private DatePickerDialog datedialog = null;
	private TimePickerDialog timeDialog = null;

	private Calendar calendar = null;

	private String sd1, sd;

	private String zipname = "";

	private String sdate = "";
	private CheckBox checkGroupBox;// 选择群组checkbox
	private PopupWindow popupWindow;
	private UserBean userBean;
	private List<CateChannel> groupList = new ArrayList<CateChannel>();// 获取到的当前用户的群组列表

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.invitation_release_activity);
		inflater = LayoutInflater.from(mContext);
		userBean = UserInfoUtil.getUserInfo(mContext);
		initData();

		readGroupList();
	}

	@SuppressLint("CutPasteId")
	@Override
	public void initData() {
		super.initData();
		findViewById(R.id.title_left_layout).setOnClickListener(this);
		findViewById(R.id.title_right_layout).setOnClickListener(this);

		addImgBtn = (ImageView) findViewById(R.id.qbimg);
		addImgBtn.setOnClickListener(this);
		addImgBtn.setOnClickListener(this);
		bitMapEdit = (BitMapInsertEdit) findViewById(R.id.et_content);
		imgsizeTextView = (TextView) findViewById(R.id.imgsize);
		titleEditText = (EditText) findViewById(R.id.title_edit);
		addressEditText = (EditText) findViewById(R.id.address_edit);
		contentEditText = (BitMapInsertEdit) findViewById(R.id.et_content);

		timeTextView = (TextView) findViewById(R.id.time_tv);
		timeTextView.setOnClickListener(this);
		pushtime = TimeRender.getStandardDateWithoutSec();
		timeTextView.setText(pushtime);

		calendar = Calendar.getInstance();
		datedialog = new DatePickerDialog(mContext, dateListener,
				calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
				calendar.get(Calendar.DAY_OF_MONTH));
		timeDialog = new TimePickerDialog(mContext, timeListener,
				calendar.get(Calendar.HOUR_OF_DAY),
				calendar.get(Calendar.MINUTE), true);

		checkGroupBox = (CheckBox) findViewById(R.id.check_group_box);
		checkGroupBox
				.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

					@Override
					public void onCheckedChanged(CompoundButton view,
							boolean flag) {
						if (groupList.size() == 0) {
							ToastUtils.showShort(mContext, "您还为加入任何群组，无法添加关联");
							return;
						}
						if (flag) {
							popupWindow.showAtLocation(view, Gravity.CENTER, 0,
									0);
						}
					}
				});
	}

	@Override
	public void onClick(View v) {
		Intent intent = null;
		Bundle bundle = null;
		switch (v.getId()) {
		case R.id.qbimg:
			intent = new Intent(mContext, AddPictureActivity.class);
			bundle = new Bundle();
			bundle.putSerializable("imglist", imglist);
			intent.putExtras(bundle);
			startActivityForResult(intent, 1001);
			break;

		case R.id.title_left_layout:
			finish();
			break;

		case R.id.title_right_layout:
			startLocation(this);
			break;

		case R.id.time_tv:
			datedialog.show();
			break;

		default:
			break;
		}
	}

	private void pushQingbao(final String addr) throws ParseException {
		String check = check();
		if (!"".equals(check)) {
			showToast(check);
			return;
		}
		showProgressDialog("正在发布情报~~");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = null;
					if (imglist.size() == 0) {
						obj = JsonUtils.pushInvitation(userBean.userid,
								introduce, title,
								TimeRender.getLongtime(pushtime), address,
								addr, "", "");
					} else {
						zipFile();
						obj = JsonUtils.pushInvitation(userBean.userid,
								introduce, title,
								TimeRender.getLongtime(pushtime), address,
								addr, zipname, sd1);
					}
					msg.what = Configs.READ_SUCCESS;
					msg.obj = obj;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				sendHandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler sendHandler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					showToast("发布成功");
					Configs.isRe = true;
					finish();
				} else {
					showToast("" + obj[1].toString());
				}
				break;

			case Configs.READ_FAIL:
				showToast("发布失败，请稍后再试~~");
				break;

			default:
				break;
			}
		};
	};

	private String check() throws ParseException {
		title = titleEditText.getText().toString().trim();
		address = addressEditText.getText().toString().trim();
		introduce = contentEditText.getText().toString().trim();
		if ("".equals(title)) {
			return "主题不能为空~~";
		} else if ("".equals(address)) {
			return "地址不能为空~~";
		} else if ("".equals(introduce)) {
			return "内容不能为空~~";
		}
		if (introduce.length() > 255) {
			return "亲，情报内容长度不能超过255个字节~~";
		}
		if (!TimeRender.compareTwotime(pushtime,
				TimeRender.getStandardDateWithoutSec())) {
			return "您设置的邀请函时间已经过期了";
		}
		return "";
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 1001) {
			imglist = (ArrayList<PsClassImg>) data
					.getSerializableExtra("imglist");
			if (imglist.size() == 0) {
				imgsizeTextView.setVisibility(View.INVISIBLE);
			} else {
				imgsizeTextView.setVisibility(View.VISIBLE);
				imgsizeTextView.setText("" + imglist.size());
			}
		}
	}

	DatePickerDialog.OnDateSetListener dateListener = new DatePickerDialog.OnDateSetListener() {
		@Override
		public void onDateSet(DatePicker datePicker, int year, int month,
				int dayOfMonth) {
			month = month + 1;
			String date = "";
			if (month > 9) {
				if (dayOfMonth > 9) {
					date = year + "-" + month + "-" + dayOfMonth;
				} else {
					date = year + "-" + month + "-0" + dayOfMonth;
				}
			} else {
				if (dayOfMonth > 9) {
					date = year + "-0" + month + "-" + dayOfMonth;
				} else {
					date = year + "-0" + month + "-0" + dayOfMonth;
				}
			}
			sdate = date;
			timeDialog.show();
		}
	};

	TimePickerDialog.OnTimeSetListener timeListener = new TimePickerDialog.OnTimeSetListener() {

		@Override
		public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
			String time = "";
			if (hourOfDay < 10) {
				if (minute < 10) {
					time = "0" + hourOfDay + ":" + "0" + minute;
				} else {
					time = "0" + hourOfDay + ":" + minute;
				}
			} else {
				if (minute < 10) {
					time = hourOfDay + ":" + "0" + minute;
				} else {
					time = hourOfDay + ":" + minute;
				}
			}
			pushtime = sdate + "\t\t\t" + time;
			timeTextView.setText("" + pushtime);
		}
	};

	private void zipFile() throws Exception {
		zipname = "" + System.currentTimeMillis();
		sd = Environment.getExternalStorageDirectory() + "/ddcj/qbimg/"
				+ zipname;
		sd1 = Environment.getExternalStorageDirectory() + "/ddcj/qbimg/"
				+ System.currentTimeMillis() + ".zip";
		Bitmap map = null;
		String path = "";
		for (int j = 0; j < imglist.size(); j++) {
			path = imglist.get(j).imgpath;
			// if (null != map)
			// map.recycle();
			// BitmapFactory.Options option = new BitmapFactory.Options();
			// option.inJustDecodeBounds = true;
			// map = BitmapFactory.decodeFile(path, option);
			// int h = option.outHeight;
			// int w = option.outWidth;
			// if (h > 300 || w > 300) {
			// BitmapFactory.Options o = new BitmapFactory.Options();
			// o.inSampleSize = 2;
			// map = BitmapFactory.decodeFile(path, o);
			// } else {
			// map = BitmapFactory.decodeFile(path);
			// }
			map = Utils.compressImage(path);
			ImageUtil.saveBitmapToFile(map, sd,
					path.substring(path.lastIndexOf("/") + 1, path.length()));
		}
		ZipUtil.ZipFolder(sd, sd1);
	}

	@Override
	public void mLocationSucess(String address) {
		try {
			pushQingbao(address);
		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void mLocationFaile(String errorInfo) {
		showToast("" + errorInfo);
	}

	@Override
	protected void onPause() {
		super.onPause();
		KeyBoard.HiddenInputPanel(mContext);
	}

	/**
	 * 读取群组列表
	 */
	private void readGroupList() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.getUserGroupList(userBean.userid);
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				groupHandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler groupHandler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				groupList = (List<CateChannel>) msg.obj;
				if (groupList.size() == 1 && groupList.get(0) == null)// 如果是报异常的有size的list，清空
					groupList.clear();
				if (groupList.size() == 0) {
					checkGroupBox
							.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

								@Override
								public void onCheckedChanged(
										CompoundButton arg0, boolean arg1) {
									checkGroupBox.setChecked(true);
								}
							});
				} else {
					initPopupWindow();
				}
				break;

			case Configs.READ_FAIL:
				showToast("获取群组列表失败");
				break;
			}
		};
	};

	private void initPopupWindow() {
		View view = inflater.inflate(R.layout.select_channel_layout, null);
		popupWindow = new PopupWindow(view,
				ViewGroup.LayoutParams.MATCH_PARENT,
				ViewGroup.LayoutParams.MATCH_PARENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		ImageView cancel = (ImageView) view.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != popupWindow && popupWindow.isShowing()) {
					if (groupList.size() == 0 || !isGroupItemChecked())
						checkGroupBox.setChecked(false);
					popupWindow.dismiss();
				}
			}
		});
		Button sureButton = (Button) view.findViewById(R.id.sure);
		sureButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != popupWindow && popupWindow.isShowing()) {
					if (groupList.size() == 0 || !isGroupItemChecked())
						checkGroupBox.setChecked(false);

					popupWindow.dismiss();
				}
			}
		});
		LinearLayout itemlayout = (LinearLayout) view
				.findViewById(R.id.itemlayout);
		View itemView = null;
		for (int i = 0; i < groupList.size(); i++) {
			itemView = inflater.inflate(R.layout.select_channel_item_layout,
					null);
			CheckBox box = (CheckBox) itemView.findViewById(R.id.item_box);
			box.setTag(i);
			box.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton v, boolean flag) {
					int pos = (Integer) v.getTag();
					groupList.get(pos).isselect = flag;
				}
			});
			TextView nameTextView = (TextView) itemView.findViewById(R.id.name);
			nameTextView.setText(groupList.get(i).name);
			itemlayout.addView(itemView);
		}
	}

	/**
	 * 判断pupwindow里的列表是否一项被选中
	 * 
	 * @return
	 */
	private boolean isGroupItemChecked() {
		for (int i = 0; i < groupList.size(); i++) {
			if (groupList.get(i).isselect)
				return true;
		}
		return false;
	}

}
