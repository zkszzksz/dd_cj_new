package com.inwhoop.ddcj.activity;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.ChatlistAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.FriendChatinfo;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.db.HandleFriendchatNoReadRecordDB;
import com.inwhoop.ddcj.db.HandleFriendchatRecordDB;
import com.inwhoop.ddcj.db.HandleFriendchatlistRecordDB;
import com.inwhoop.ddcj.util.KeyBoard;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.Utils;
import com.inwhoop.ddcj.view.FaceRelativeLayout;
import com.inwhoop.ddcj.view.TouchSayButton;
import com.inwhoop.ddcj.view.TouchSayButton.OnRecordListener;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;
import com.inwhoop.ddcj.xmpp.SingleChatPacketExtension;
import com.inwhoop.ddcj.xmpp.XmppConnectionUtil;
import com.pocketdigi.utils.FLameUtils;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: FriendChatActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 单聊
 * @date 2014-6-14 上午10:47:58
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class FriendChatActivity extends BaseFragmentActivity implements
        IXListViewListener, OnClickListener, OnRecordListener {

    private String usernick = "d123"; // 聊天对象用户昵称

    private String username = "18302820052"; // 聊天对象openfire的账号

    private String userphoto;// 聊天对象的头像

    private XListView listView = null;

    private Button sendButton = null; // 发送按钮

    private TouchSayButton audioButton = null; // 按住说话按钮

    private ImageView recordImageView = null; // 录音按钮

    private EditText msgEditText = null; // 输入框

    private boolean isRecording = false;

    private ImageView carmerImageView = null; // 调用照相机

    private List<FriendChatinfo> chatList = new ArrayList<FriendChatinfo>();

    private String msgcontent = ""; // 消息内容

    private ChatlistAdapter adapter = null; // 适配器

    private HandleFriendchatRecordDB db = null;

    private HandleFriendchatNoReadRecordDB ndb = null;

    private HandleFriendchatlistRecordDB fdb = null;

    private int page = 1; // 用于聊天记录分页

    private FaceRelativeLayout faceRelativeLayout = null;

    private UserBean userinfo = null;

    private ChatManager cm = null;

    private Chat newchat = null;// chat聊天对象
    private String headImg = "";
    private RelativeLayout back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                        | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.friend_chat_layout);
        mContext = FriendChatActivity.this;
        userinfo = UserInfoUtil.getUserInfo(mContext);
        db = new HandleFriendchatRecordDB(mContext);
        ndb = new HandleFriendchatNoReadRecordDB(mContext);
        fdb = new HandleFriendchatlistRecordDB(mContext);
        try {
            cm = XmppConnectionUtil.getInstance().getConnection()
                    .getChatManager();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // username = getIntent().getStringExtra("username");
        // usernick = getIntent().getStringExtra("usernick");
        // userphoto = getIntent().getStringExtra("userphoto");
        initData();
    }

    @Override
    public void initData() {
        if (null != getIntent() && null != getIntent().getExtras()) {
            Bundle bundle = getIntent().getExtras();
            usernick = bundle.getString("usernick");
            username = bundle.getString("username");
        }
        headImg = userinfo.img;
        Configs.friendid = username;
        System.out.println("聊天的对象为====================" + usernick + "===="
                + username + "===" + headImg);
        super.initData();
        setTitleStrText("" + usernick);
        setLeftLayout(R.drawable.back_btn_selector, false);
        back = (RelativeLayout) findViewById(R.id.title_left_layout);
        back.setOnClickListener(this);
        listView = (XListView) findViewById(R.id.chatlist);
        listView.setXListViewListener(this);
        listView.setPullLoadEnable(false);
        sendButton = (Button) findViewById(R.id.sendbtn);
        sendButton.setOnClickListener(this);
        recordImageView = (ImageView) findViewById(R.id.record_img);
        recordImageView.setOnClickListener(this);
        recordImageView.setVisibility(View.VISIBLE);
        audioButton = (TouchSayButton) findViewById(R.id.audiobtn);
        audioButton.setOnRecordListener(this);
        carmerImageView = (ImageView) findViewById(R.id.carmer_bg);
        carmerImageView.setOnClickListener(this);
        faceRelativeLayout = (FaceRelativeLayout) findViewById(R.id.faceRelativeLayout);
        listView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                faceRelativeLayout.hide();
                return false;
            }
        });
        IntentFilter filter = new IntentFilter();// 创建IntentFilter对象
        filter.addAction(Configs.COME_MESSAGE);
        registerReceiver(mRecever, filter);
        msgEditText = (EditText) findViewById(R.id.sendmsg);
        // 监听输入框的内容
        msgEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2,
                                      int arg3) {
                if (s.length() > 0) {
                    sendButton.setVisibility(View.VISIBLE);
                    recordImageView.setVisibility(View.GONE);
                    audioButton.setVisibility(View.GONE);
                    msgEditText.setVisibility(View.VISIBLE);
                } else {
                    sendButton.setVisibility(View.GONE);
                    recordImageView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
        initListviewData();
    }

    /**
     * 初始化listview
     *
     * @param
     * @return void
     * @Title: initListview
     * @Description: TODO
     */
    private void initListviewData() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                chatList = db.getUserBygroupid(username,
                        UserInfoUtil.getUserInfo(mContext).tel, page);
                List<FriendChatinfo> nlist = ndb.getUserBygroupid(username,
                        mContext);
                for (int i = 0; i < nlist.size(); i++) {
                    chatList.add(nlist.get(i));
                    db.saveGmember(nlist.get(i));
                }
                if (null != nlist && nlist.size() > 0) {
                    ndb.deleteGmember(
                            UserInfoUtil.getUserInfo(mContext).tel, username);
                }
                handler.sendEmptyMessage(0);
            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            page++;
            adapter = new ChatlistAdapter(mContext, chatList, listView);
            listView.setAdapter(adapter);
            listView.setSelection(listView.getCount() - 1);
        }
    };

    @Override
    public void onRefresh() {
        try {
            chatList = db.getUserBygroupid(username,
                    UserInfoUtil.getUserInfo(mContext).tel, page);
            page++;
            adapter.addChatinfo(chatList);
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
        }
        listView.stopRefresh();
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendbtn:
                sendMsgcontext();
                msgEditText.setText("");
                break;

            case R.id.record_img:
                if (isRecording) {
                    isRecording = false;
                    msgEditText.setVisibility(View.VISIBLE);
                    audioButton.setVisibility(View.GONE);
                    recordImageView.setBackgroundResource(R.drawable.btn_vol);
                } else {
                    isRecording = true;
                    msgEditText.setVisibility(View.GONE);
                    audioButton.setVisibility(View.VISIBLE);
                    recordImageView.setBackgroundResource(R.drawable.btn_keybord);
                }
                faceRelativeLayout.hide();
                break;

            case R.id.carmer_bg:
                showCameraDialog(imgpathHandler, false, 0, 0, false);
                break;
            case R.id.title_left_layout:
                finish();
                break;
            default:
                break;
        }
    }

    /**
     * 调用系统照相机
     */
    @SuppressLint("HandlerLeak")
    private Handler imgpathHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (RESULT_IMG == msg.what) {
                msgcontent = Utils.imgToBase64(Utils.compressImage(msg.obj
                        .toString()));
                sendMsg("", userinfo.name, 2, msgcontent, 0, headImg);
            }
        }

        ;
    };

    /**
     * 录音之后进行的处理
     */
    @Override
    public void onRecord(String filepath, long time) {
        if ("".equals(filepath)) {
            return;
        }
        if (time < 1) {
            showToast("亲，您录音时间太短了~~");
            return;
        }
        UserBean userinfo = UserInfoUtil.getUserInfo(mContext);
        try {
            String path = "/ddcj/audio/" + System.currentTimeMillis() + ".mp3";
            FLameUtils lameUtils = new FLameUtils(1, 16000, 96);
            lameUtils.raw2mp3(Environment.getExternalStorageDirectory()
                    + filepath, Environment.getExternalStorageDirectory()
                    + path);
            sendMsg(path,
                    userinfo.name,
                    3,
                    Utils.encodeBase64File(Environment
                            .getExternalStorageDirectory() + path), (int) time,
                    headImg);
        } catch (Exception e) {
            System.out.println("======filepath========" + e.toString());
            showToast("断线了%>_<%~~");
            return;
        }
    }

    /**
     * 发送文字信息
     *
     * @param
     * @return void
     * @Title: sendMsgcontext
     * @Description: TODO
     */
    private void sendMsgcontext() {
        msgcontent = msgEditText.getText().toString().trim();
        if ("".equals(msgcontent)) {
            showToast("消息内容不能为空");
        } else {
            sendMsg("", userinfo.name, 1, msgcontent, 0, headImg);
        }
    }

    @Override
    protected void onDestroy() {
        Configs.friendid = "-1";
        unregisterReceiver(mRecever);
        super.onDestroy();
    }

    private void sendMsg(final String audiopath, final String usernick,
                         final int msgtype, final String msgcontent, final int audiolength,
                         final String headpath) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    SingleChatPacketExtension pack = new SingleChatPacketExtension();
                    pack.setAudiopath(audiopath);
                    pack.setUsernick(usernick);
                    pack.setMsgtype(msgtype);
                    pack.setContent(msgcontent);
                    pack.setAudiolength(audiolength);
                    pack.setSendtime(TimeRender.getStandardDate());
                    pack.setHeadpath(headpath);
                    pack.setUserid(userinfo.tel);
                    getChatManager(username).sendMessage(pack.toXML());
                    msg.obj = new FriendChatinfo(msgcontent, userinfo.tel, userinfo.tel,
                            userinfo.username, username, userinfo.img,
                            TimeRender.getStandardDate(), true, msgtype,
                            audiopath, audiolength);
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = Configs.READ_FAIL;
                }
                chatHandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler chatHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_FAIL:
                    showToast("断线了~~");
                    break;

                case Configs.READ_SUCCESS:
                    FriendChatinfo chatinfo = (FriendChatinfo) msg.obj;
                    db.saveGmember(chatinfo);
//				if (!fdb.isExist(username, 1, TimeRender.getStandardDate(),
//						userinfo.tel)) {
//					fdb.saveGmember(username, 1, TimeRender.getStandardDate(),
//							userinfo.tel, userphoto, usernick);
//				} else {
//					fdb.UpdateChatagetTime(username, 1, userinfo.tel,
//							TimeRender.getStandardDate(), userphoto, usernick);
//				}
                    if (fdb.isExist(username, 1, TimeRender.getStandardDate(),
                            userinfo.tel)) {
                        fdb.deleteGmember(username, "" + UserInfoUtil.getUserInfo(mContext).tel);
                    }
                    fdb.saveGmember(username, 1, TimeRender.getStandardDate(),
                            userinfo.tel, userphoto, usernick);
                    adapter.addChatinfo(chatinfo);
                    adapter.notifyDataSetChanged();
                    listView.setSelection(listView.getCount() - 1);
                    break;

                default:
                    break;
            }
        }

        ;
    };

    public BroadcastReceiver mRecever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Configs.COME_MESSAGE)) {
                FriendChatinfo chatinfo = (FriendChatinfo) intent
                        .getSerializableExtra("info");
                System.out.println("用户的头像" + chatinfo.userheadpath);
                adapter.addChatinfo(chatinfo);
                adapter.notifyDataSetChanged();
                listView.setSelection(listView.getCount() - 1);
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
    }

    public Chat getChatManager(String username) {
        if (null == cm) {
            cm = XmppConnectionUtil.getInstance().getConnection()
                    .getChatManager();
        }
        if (null == newchat) {
            newchat = cm.createChat(username
                    + "@"
                    + XmppConnectionUtil.getInstance().getConnection()
                    .getServiceName(), null);
        }
        return newchat;
    }

}
