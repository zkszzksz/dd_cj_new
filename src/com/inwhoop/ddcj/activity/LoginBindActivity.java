package com.inwhoop.ddcj.activity;

import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import cn.sharesdk.framework.Platform;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.ToastUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.Utils;

import net.tsz.afinal.FinalBitmap;


import java.util.HashMap;

/**
 * @author ZK
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @Title: ${file_name} 登录时绑定第三方
 * @Package ${package_name}
 * @Date: 2014/11/18 19:06
 * @version: 1.0
 */
public class LoginBindActivity extends BaseOtherLoginActivity {
	public static int isThisBind = 0;
	private UserBean userBean;
	private FinalBitmap fb;
	private EditText userET, pwdET;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_login_bind);
		userBean = (UserBean) getIntent().getSerializableExtra("bean");
		fb = FinalBitmap.create(mContext);
		fb.configLoadingImage(R.drawable.home_head);
		fb.configLoadfailImage(R.drawable.home_head);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		fb.display(findViewById(R.id.head_img), userBean.img);

		String whatStr;
		if (userBean.type.equals(TYPE_QQ)) {
			whatStr = "QQ登录";
		} else if (userBean.type.equals(TYPE_SINA)) {
			whatStr = "新浪登录";
		} else if (userBean.type.equals(TYPE_WEIXIN)) {
			whatStr = "微信登录";
		} else {
			whatStr = "";
		}
		setTitleStrText(whatStr);

		userET = (EditText) findViewById(R.id.email_et);
		pwdET = (EditText) findViewById(R.id.pwd_et);
		((TextView) findViewById(R.id.tv_what_login)).setText(whatStr
				+ "成功\n请绑定您的大大财经帐号");
		findViewById(R.id.bind_btn).setOnClickListener(this);
		findViewById(R.id.tv_register_right_away).setOnClickListener(this);
	}

	@Override
	public void onComplete(Platform platform, int i,
			HashMap<String, Object> stringObjectHashMap) {
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.bind_btn:
			login();
			break;
		case R.id.tv_register_right_away:
			isThisBind = 1;
			Act.toAct(mContext, RegisterOneActivity.class);
			break;
		}
	}

	// TODO 判断输入信息以及登录
	private void login() {
		String user = userET.getText().toString();
		String pwd = pwdET.getText().toString();
		if ("".equals(user)) {
			ToastUtils.showShort(mContext, R.string.please_put_username);
			return;
		} else if ("".equals(pwd)) {
			ToastUtils.showShort(mContext, R.string.please_put_pwd);
			return;
		}
		userBean.tel = user;
		userBean.pwd = pwd;
		userBean.udid = Utils.getPhoneUdid(mContext);

		showProgressDialog("");
		bindUser(handle, userBean);
	}

	private Handler handle = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case BIND_OK: // 绑定
			case Configs.READ_SUCCESS: // 普通登录
				Object[] xx = (Object[]) msg.obj;

				if ((Boolean) xx[0]) {
					String pwd = userBean.pwd;
					if (null != xx[2])
						userBean = (UserBean) xx[2];
					// userInfoBean.pwd = pwd; //目前直接用加密后返回的密码，用来登录openfire
					UserInfoUtil.rememberUserInfo(mContext, userBean);
					ToastUtils.showShort(mContext, "绑定成功");

					Act.toActClearTop(mContext, StartActivity.class);
					Act.toAct(mContext, MainActivity.class);
					finish();
				} else
					ToastUtils.showLong(mContext, xx[1] + "");
				break;
			case Configs.READ_FAIL:
				ToastUtils.showShort(mContext, "数据异常");
				break;
			}
		}
	};
}
