package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.LinearLayout;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import com.inwhoop.R;
import com.inwhoop.ddcj.view.ZoomableImageView;

/**
 * 显示html页面的图片
 * 
 * @Project: GuoTeng
 * @Title: ShowWebImageActivity.java
 * @Package com.inwhoop.guoteng.activty
 * @Description: TODO
 * 
 * @author ylligang118@126.com
 * @date 2014-4-4 下午4:55:33
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class ShowWebImageActivity extends Activity {
	private String imagePath = null;
	private ZoomableImageView imageView = null;
	private Drawable drawable = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.show_webimage);
		this.imagePath = getIntent().getStringExtra("image");
		imageView = (ZoomableImageView) findViewById(R.id.show_webimage_imageview);

		new Thread(new Runnable() {

			@Override
			public void run() {

				try {
					drawable = loadImageFromUrl(imagePath);
					handler.sendEmptyMessage(1);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		}).start();

	}

	private Drawable loadImageFromUrl(String url) throws IOException {
		URL m = new URL(url);
		InputStream i = (InputStream) m.getContent();
		Drawable d = Drawable.createFromStream(i, "src");
		return d;
	}

	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (null != drawable) {
				LinearLayout.LayoutParams params = null;
				BitmapDrawable bd = (BitmapDrawable) drawable;
				Bitmap bm = bd.getBitmap();
				imageView.setImageBitmap(bm);
			}
		};
	};

	public void goBack(View view) {
		ShowWebImageActivity.this.finish();
	}
}
