package com.inwhoop.ddcj.activity;

import android.annotation.SuppressLint;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.update.ConfigInfo;
import com.inwhoop.ddcj.update.Updata;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.Utils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class SettingHomeActivity extends BaseFragmentActivity {

	private LayoutInflater inflater = null;

	private LinearLayout contentLayout = null;

	private String[] itemStr = { "我的", "账号(用户ID：1849)", "功能", "其他" };

	private int[] itemImg = { R.drawable.ico_my, R.drawable.ico_account,
			R.drawable.ico_function, R.drawable.ico_othr };

	private String[] mystrs, accountstrs, functionstrs, otherstrs;

	private List<String[]> strlist = new ArrayList<String[]>();

	private LinearLayout accountLayout = null; // 有多少第三方登录的帐号 的layout

	private TextView cashTextView, versionTextView;

	private PopupWindow checkPopupWindow = null;
	private UserBean userBean;

	private Updata updata = new Updata();
	private View updateLayout;// 更新组件layout

	@Override
	protected void onResume() {
		super.onResume();
		userBean = UserInfoUtil.getUserInfo(mContext);
		setOtherBindIcon();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.set_homepage_layout);
		userBean = UserInfoUtil.getUserInfo(mContext);
		inflater = LayoutInflater.from(mContext);
		mystrs = getResources().getStringArray(R.array.set_tab_one);
		accountstrs = getResources().getStringArray(R.array.set_tab_two);
		functionstrs = getResources().getStringArray(R.array.set_tab_three);
		otherstrs = getResources().getStringArray(R.array.set_tab_four);
		strlist.add(mystrs);
		strlist.add(accountstrs);
		strlist.add(functionstrs);
		strlist.add(otherstrs);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleStrText("设置");
		setLeftLayout(R.drawable.back_btn_selector, false);
		contentLayout = (LinearLayout) findViewById(R.id.contentlayout);
		setContentlayout();
		initPopupWindow();
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.bottomlayout:
			break;

		default:
			break;
		}
	}

	private void setContentlayout() {
		View view = null;
		for (int i = 0; i < itemStr.length; i++) {
			view = inflater.inflate(R.layout.set_item_layout, null);
			ImageView img = (ImageView) view.findViewById(R.id.item_img);
			img.setBackgroundResource(itemImg[i]);
			TextView tv = (TextView) view.findViewById(R.id.item_tv);
			tv.setText(itemStr[i]);
			LinearLayout itemlayout = (LinearLayout) view
					.findViewById(R.id.itemlayout);
			setItemlayout(i, itemlayout);
			contentLayout.addView(view);
		}
	}

	@SuppressLint("CutPasteId")
	private void setItemlayout(final int pos, LinearLayout itemlayout) {
		View itemView = null;
		for (int i = 0; i < strlist.get(pos).length; i++) {
			itemView = inflater.inflate(R.layout.set_item_item_layout, null);
			TextView tv = (TextView) itemView.findViewById(R.id.sitem_tv);
			tv.setText(strlist.get(pos)[i]);
			View line = itemView.findViewById(R.id.line);
			if (i == (strlist.get(pos).length - 1)) {
				line.setVisibility(View.GONE);
			}
			if (pos == 1 && i == 0) {
				accountLayout = (LinearLayout) itemView
						.findViewById(R.id.imglayout);
				setOtherBindIcon();
			}
			if (pos == 2 && i == 2) {
				cashTextView = (TextView) itemView
						.findViewById(R.id.itemcontent_tv);
				cashTextView.setText("516MB");
			}
			if (pos == 3 && i == 3) {
				versionTextView = (TextView) itemView
						.findViewById(R.id.itemcontent_tv);
				try {
					versionTextView.setText("当前版本"
							+ Utils.getVersionName(mContext));
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			itemView.setTag(i);
			itemView.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					int p = (Integer) v.getTag();
					gotoAct(pos, p, v);
				}
			});
			itemlayout.addView(itemView);
		}
	}

	// 设置是否有第三方登录的图标
	private void setOtherBindIcon() {
		accountLayout.removeAllViews();
		if (!TextUtils.isEmpty(userBean.qqid)) {
			View acView = inflater.inflate(R.layout.account_item_img, null);
			ImageView aimg = (ImageView) acView.findViewById(R.id.accountimg);
			aimg.setBackgroundResource(R.drawable.login_ico_qq);
			accountLayout.addView(acView);
		}
		if (!TextUtils.isEmpty(userBean.sinaid)) {
			View acView = inflater.inflate(R.layout.account_item_img, null);
			ImageView aimg = (ImageView) acView.findViewById(R.id.accountimg);
			aimg.setBackgroundResource(R.drawable.login_ico_wb);
			accountLayout.addView(acView);
		}
		if (!TextUtils.isEmpty(userBean.wxid)) {
			View acView = inflater.inflate(R.layout.account_item_img, null);
			ImageView aimg = (ImageView) acView.findViewById(R.id.accountimg);
			aimg.setBackgroundResource(R.drawable.login_ico_wechat);
			accountLayout.addView(acView);
		}
	}

	private void gotoAct(int m, int n, View v) {
		switch (m) {
		case 0:
			if (userBean.userid.equals("0")) {
				showToast("请先登录");
				return;
			}
			if (n == 0) { // 申请建群资格
				Act.toAct(mContext, ApplySetGroupActivity.class);
			} else if (n == 1) { // 我的群组
				Act.toAct(mContext, MyGroupListActivity.class);
			} else { // 外面叫【订阅列表】，里面叫【订阅管理】
				Act.toAct(mContext, SubscribepListActivity.class);
			}
			break;
		case 1:
			if (userBean.userid.equals("0")) {
				showToast("请先登录");
				return;
			}
			if (n == 0) { // 绑定帐号
				Act.toAct(mContext, BindAccountActivity.class);
			} else if (n == 1) { // 我的资料
				Act.toAct(mContext, MyInfoActivity.class);
			} else { // 修改密码
				Act.toAct(mContext, ChangeUserPwdActivity.class);
			}
			break;
		case 2:
			if (n == 0) { // 隐身模式
				Act.toAct(mContext, SettingStealthActivity.class);
			} else if (n == 1) { // 消息提醒
				Act.toAct(mContext, SettingWhatGetMessageActivity.class);
			} else { // 清理缓存

			}
			break;
		case 3:
			if (n == 0) { // 使用帮助
				Act.toAct(mContext, UseHelpActivity.class);
			} else if (n == 1) { // 反馈建议
				Act.toAct(mContext, WriteSuggestActivity.class);
			} else if (n == 2) { // 联系我们
				Act.toAct(mContext, ConnectUsActivity.class);
			} else if (n == 3) { // 检测更新
				updateLayout = v;
				setUpdata();
			} else if (n == 4) {

			}
			break;

		default:
			break;
		}
	}

	@SuppressWarnings("deprecation")
	private void initPopupWindow() {
		View view = inflater.inflate(R.layout.check_version_popupwindow_layout,
				null);
		checkPopupWindow = new PopupWindow(view, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, true);
		checkPopupWindow.setContentView(view);
		checkPopupWindow.setFocusable(true);
		checkPopupWindow.setOutsideTouchable(false);
		checkPopupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		view.findViewById(R.id.download_btn).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						updata.down(mContext, Configs.UPDATA_SERVER);
						checkPopupWindow.dismiss();
					}
				});
		view.findViewById(R.id.cancel_btn).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						checkPopupWindow.dismiss();
					}
				});
	}

	private void showCheckPopupWindow(View v) {
		checkPopupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
	}

	/*--------------以下为更新----------------*/
	private void setUpdata() {
		showProgressDialog("");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message message = Message.obtain();
				try {
					message.obj = updata.getServerVerCode();
					updataHandler.sendMessage(message);
				} catch (Exception e) {
					System.out.println("" + e.toString());
				}

			}
		}).start();
	}

	private Handler updataHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			if ((Boolean) msg.obj) {
				int vercode = ConfigInfo.getVerCode(mContext);
				if (updata.newVerCode > vercode) {
					updata.findNewVerCode(mContext);
					showCheckPopupWindow(updateLayout);
				} else
					showToast("暂无更新");
			} else
				showToast("暂无更新");
		}
	};
}
