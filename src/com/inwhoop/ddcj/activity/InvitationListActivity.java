package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.InvitationListActivityAdapter;
import com.inwhoop.ddcj.adapter.InvitationListActivityAdapter.AttentionUserOrCancleListener;
import com.inwhoop.ddcj.bean.InvitationInfo;
import com.inwhoop.ddcj.bean.InviterListContactBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 邀请人列表 ：发现模块-邀请函
 *
 * @author ZOUXU
 */
public class InvitationListActivity extends Activity implements
        IXListViewListener, OnClickListener, AttentionUserOrCancleListener {

    private XListView lv_inviter = null;
    private List<InviterListContactBean> mData = null;
    private InvitationListActivityAdapter friends_adapter;
    private Context context;
    private TextView tv_totalNum;
    private int totalNum = 0;
    /**
     * 标题栏右侧人数统计
     */
    private static final String str_totalNum = "共有%1$d人报名";

    private UserBean userBean;
    private DialogShowStyle dialogShowStyle;
    private InvitationInfo invInfoBean;  //传过来的邀请函对象

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        this.context = this;
        invInfoBean = (InvitationInfo) getIntent().getSerializableExtra("bean");
        setContentView(R.layout.invitation_list_activity);
        initComponet(LayoutInflater.from(context));
    }

    private void initComponet(LayoutInflater inflater) {
        mData = new ArrayList<InviterListContactBean>();
        lv_inviter = (XListView) findViewById(R.id.lv_group);
        lv_inviter.setPullLoadEnable(false);
        lv_inviter.setXListViewListener(this);

        tv_totalNum = (TextView) findViewById(R.id.tv_totalNum);
        tv_totalNum.setText(String.format(str_totalNum, mData.size()));
        findViewById(R.id.title_left_layout).setOnClickListener(this);

        new GetNetDataTask().execute();
    }

    /**
     * 调用接口获取网络数据
     */
    private void getData() {

        Object[] objs;
        try {
            objs = JsonUtils.getInvitationListAboutInviter(userBean.userid, "" + invInfoBean.id);
            System.out.println(objs[0] + "---------------" + objs[1]);
            mData.clear();
            List<InviterListContactBean> data_temp = (List<InviterListContactBean>) objs[2];
            for (InviterListContactBean info : data_temp) {
                mData.add(info);
            }
            totalNum = mData.size();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean attention(String userid, String touserid) {
        // TODO Auto-generated method stub
        Object[] objs = null;
        try {
            if (userid.equals(touserid)) return false;//如果是自己id
            objs = JsonUtils.attentionUser(userid, touserid, "来自邀请函");
            System.out.println(objs[0] + "---------------" + objs[1]);
        } catch (Exception e) {
            LogUtil.e("exception:查看邀请，关注：" + e.toString());
            return false;
        }
        return (Boolean) objs[0];
    }

    @Override
    public boolean cancelAttention(String userid, String touserid) {
        Object[] objs = null;
        try {
            objs = JsonUtils.cancelAttentio(userid, touserid);
            System.out.println(objs[0] + "---------------" + objs[1]);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return (Boolean) objs[0];
    }

    @Override
    public void onRefresh() {
        new GetNetDataTask().execute();
    }

    @Override
    public void onLoadMore() {

    }

    /**
     * 与网络相关的异步操作：提供标识符区分操作（执行异步任务前修改标识符{@link actionFlag}）
     *
     * @author ZOUXU
     */
    class GetNetDataTask extends AsyncTask<Void, Integer, Integer> {
        private static final int NONET = 0;
        private static final int SLOWNET = 2;

        @Override
        protected void onPreExecute() {

            int netType = new NetWorkUtils(context).getNetType();
            switch (netType) {
                case NONET:
                    Toast.makeText(context, "请检查网络连接", Toast.LENGTH_LONG).show();
                    this.cancel(true);
                    break;
                case SLOWNET:
                    dialogShowStyle = new DialogShowStyle(context,
                            "当前为非WIFI网络，正在连接服务器");
                    dialogShowStyle.dialogShow();
                    break;
            }

            if (userBean == null) {
                userBean = UserInfoUtil.getUserInfo(context);
            }
        }

        @Override
        protected Integer doInBackground(Void... params) {
            try {
                getData();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return 0;
        }

        @Override
        protected void onPostExecute(Integer result) {
            if (dialogShowStyle != null) {
                dialogShowStyle.dialogDismiss();
                dialogShowStyle = null;
            }
            tv_totalNum.setText(String.format(str_totalNum, totalNum));
            if (lv_inviter.getAdapter() == null) {

                friends_adapter = new InvitationListActivityAdapter(context,
                        mData);
                lv_inviter.setAdapter(friends_adapter);
                // 注册接口回调
                friends_adapter.setAttentionListener(
                        InvitationListActivity.this, userBean.userid);
            } else {
                friends_adapter.notifyDataSetChanged();
                ToastUtils.showShort(context, "数据已经刷新");
            }
            tv_totalNum.setText(String.format(str_totalNum, totalNum));
            lv_inviter.stopRefresh();
        }
    }

    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub
        switch (v.getId()) {
            case R.id.title_left_layout:
                finish();
                break;
            default:
                break;
        }
    }

}
