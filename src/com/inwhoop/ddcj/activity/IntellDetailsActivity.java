package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.ChBgGridAdapter;
import com.inwhoop.ddcj.adapter.CommentListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.*;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.CircleImageview;
import com.inwhoop.ddcj.view.GridForListView;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.xmpp.FaceConversionUtil;
import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


/**
 * Created by ding on 2014/10/23. 情报详情页面
 */
public class IntellDetailsActivity extends BaseFragmentActivity implements
        XListView.IXListViewListener, OnClickListener, PlatformActionListener {
    private XListView xListView;
    private List<QingbaoComment> list = new ArrayList<QingbaoComment>();
    private Context context;
    private LayoutInflater mInflater;
    private ScreenSize screenSize;
    private GridForListView gridView;
    private LinearLayout commentBtn;
    private RelativeLayout back;
    private TextView backText;
    private TextView title;

    private CommentListAdapter adapter = null;
    private NewsBean newsBean = null;

    private TextView commentcount = null;

    private PopupWindow popupWindow;

    private int newsid = -1;

    private FinalBitmap fb = null;

    @SuppressWarnings("static-access")
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.intell_details_activity);
        context = IntellDetailsActivity.this;
        newsBean = (NewsBean) getIntent().getSerializableExtra("info");
        newsid = getIntent().getIntExtra("newsid", -1);
        System.out.println("接收到的情报详情ID为"+newsid);
        fb = FinalBitmap.create(context);
        fb.configLoadingImage(R.drawable.qingbao_loading_img);
        fb.configLoadfailImage(R.drawable.qingbao_loading_img);
        initData();
    }

    @Override
    public void initData() {
        super.initData();
        setTitleStrText("情报详情");
        setLeftLayout(R.drawable.back_btn_selector, false);
        setRightLayout(R.drawable.btn_yaoqinghan_content_right_selector, false);
        title_right_layout.setOnClickListener(this);
        xListView = (XListView) findViewById(R.id.intell_details_activity_list);
        commentBtn = (LinearLayout) findViewById(R.id.intell_details_activity_comment);
        commentBtn.setOnClickListener(this);
        xListView.setPullLoadEnable(false);
        xListView.setPullRefreshEnable(false);
        xListView.setXListViewListener(this);
        adapter = new CommentListAdapter(context, list);
        xListView.setAdapter(adapter);
        initPopupWindow();
        if (null != newsBean) {
            addheadview();
        } else {
            readInfo();
        }
        read(-1);
        showProgressDialog("正在加载数据...");
        findViewById(R.id.share_layout).setOnClickListener(this);
    }

    private void readInfo() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    Object[] obj = JsonUtils.getQingbaoInfo(newsid);
                    msg.obj = obj;
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = Configs.READ_FAIL;
                }
                infoHandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler infoHandler = new Handler() {
        @SuppressWarnings("unchecked")
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Object[] obj = (Object[]) msg.obj;
                    if ((Boolean) obj[0]) {
                        newsBean = ((List<NewsBean>) obj[2]).get(0);
                        if (null != newsBean) {
                            newsBean.id = newsid;
                            addheadview();
                        }
                    } else {
                        showToast(obj[1].toString());
                    }
                    break;
                case Configs.READ_FAIL:
                    showToast("加载数据失败，请稍后再试");
                    break;

                default:
                    break;
            }
        }

        ;
    };

    private void addheadview() {
        mInflater = LayoutInflater.from(this);
        View view = mInflater.inflate(R.layout.intell_head_item, null);
        xListView.addHeaderView(view);
        CircleImageview head = (CircleImageview) view
                .findViewById(R.id.headimg);
        head.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent(mContext, PersonInfoActivity.class);
                intent.putExtra("userid", newsBean.userid);
                startActivity(intent);
            }
        });
        if (newsBean.userimg.startsWith("http://")) {
            fb.display(head, newsBean.userimg);
        } else {
            fb.display(head, Configs.HOST + newsBean.userimg);
        }
        TextView username = (TextView) view.findViewById(R.id.username);
        username.setText("" + newsBean.name);
        TextView pushtimeTextView = (TextView) view.findViewById(R.id.ptime);
        pushtimeTextView.setText(""
                + TimeUtils.twoDateDistance(newsBean.createtime,
                TimeUtils.getStrTime(System.currentTimeMillis())));
        TextView content = (TextView) view.findViewById(R.id.content);
        SpannableString spannableString = FaceConversionUtil.getInstace()
                .getExpressionString(context, newsBean.content);
        content.setText(spannableString);
        commentcount = (TextView) view.findViewById(R.id.comentcount);
        commentcount.setText("" + newsBean.commentcount);
        screenSize = ScreenSizeUtil.getScreenSize(context);
        gridView = (GridForListView) findViewById(R.id.intell_details_activity_grid);
        final List<ChangeThemeBean> list = new ArrayList<ChangeThemeBean>();
        ChangeThemeBean bean = null;
        for (int i = 0; i < newsBean.imgpath.size(); i++) {
            bean = new ChangeThemeBean();
            bean.imgs = newsBean.imgpath.get(i);
            bean.tag = false;
            list.add(bean);
        }
        final ChBgGridAdapter adapter1 = new ChBgGridAdapter(context, list,
                screenSize.screenW);
        gridView.setAdapter(adapter1);
        gridView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int position, long arg3) {
                Intent intent = new Intent(context, ShowWebImageActivity.class);
                if (list.get(position).imgs.startsWith("http://")) {
                    intent.putExtra("image", list.get(position).imgs);
                } else {
                    intent.putExtra("image", Configs.HOST
                            + list.get(position).imgs);
                }
                startActivity(intent);
            }
        });
    }

    /**
     * 转载框初始化
     *
     * @param
     * @return void
     * @Title: initPopupWindow
     * @Description: TODO
     */
    private void initPopupWindow() {
        View popupWindow_view = getLayoutInflater().inflate(
                R.layout.pup_window_zhuanzai, null, false);
        popupWindow = new PopupWindow(popupWindow_view,
                LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true);
        popupWindow.setAnimationStyle(R.style.PopupAnimation);
        popupWindow.setFocusable(true); // 设置PopupWindow可获得焦点
        popupWindow.setTouchable(true); // 设置PopupWindow可触摸
        popupWindow.setOutsideTouchable(true); // 设置非PopupWindow区域可触摸
        popupWindow.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        popupWindow_view.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                readChannellist();
            }
        });
    }

    private void readChannellist() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    msg.obj = JsonUtils
                            .getChannellistByuserid(UserInfoUtil
                                    .getUserInfo(mContext).userid);
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = Configs.READ_FAIL;
                }
                channelHandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler channelHandler = new Handler() {
        @SuppressWarnings("unchecked")
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Object[] obj = (Object[]) msg.obj;
                    if ((Boolean) obj[0]) {
                        ArrayList<EditQBChannellist> channellist = (ArrayList<EditQBChannellist>) obj[2];
                        if (channellist.size() == 0) {
                            showToast("您没有转载权限");
                        } else {
                            Intent intent = new Intent(mContext,
                                    ReprintedInfoActivity.class);
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("channellist", channellist);
                            bundle.putSerializable("newsBean", newsBean);
                            intent.putExtras(bundle);
                            startActivity(intent);
                        }
                    } else {
                        showToast("您没有转载权限");
                    }
                    break;

                case Configs.READ_FAIL:
                    showToast("您没有转载权限");
                    break;

                default:
                    break;
            }
        }

        ;
    };

    private void showPopup(View view) {
        int[] location = new int[2];
        view.getLocationInWindow(location);
        popupWindow.showAtLocation(view, Gravity.NO_GRAVITY,
                location[0] + view.getWidth(), location[1] + view.getHeight());

    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        String ddid = UserInfoUtil.getUserInfo(mContext).ddid;
        switch (v.getId()) {
            case R.id.intell_details_activity_comment:
                if (TextUtils.isEmpty(ddid)) {
                    Act.toAct(mContext, LoginActivity.class);
                    return;
                }
                Intent intent = new Intent(context, WriteCommentActivity.class);
                intent.putExtra("id", newsid);
                intent.putExtra("tag", Configs.QINGBAO_COMMENT_TAG);
                startActivityForResult(intent, 1001);
                break;
            case R.id.title_right_layout:
                showPopup(v);
                break;
            case R.id.share_layout:
                if (TextUtils.isEmpty(ddid)) {
                    Act.toAct(mContext, LoginActivity.class);
                    return;
                }

                String imgurl = "";
                if (null != newsBean) {
                    imgurl = newsBean.img;
                }
                if ("".equals(imgurl))
                    imgurl = UserInfoUtil.getUserInfo(mContext).img;
                ShareUtil.showShare(mContext, newsBean.content, imgurl, "http://"
                        + newsBean.sharecontent, this);
                break;
        }
    }

    private void read(final int id) {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    Object[] obj = JsonUtils.getQingbaoCommentList(newsid, id,
                            10);
                    msg.obj = obj;
                    msg.arg1 = id;
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = Configs.READ_FAIL;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @SuppressWarnings("unchecked")
        public void handleMessage(android.os.Message msg) {
            xListView.stopLoadMore();
            xListView.stopRefresh();
            switch (msg.what) {
                case Configs.READ_FAIL:
                    xListView.setPullLoadEnable(false);
                    showToast("获取评论列表失败");
                    break;

                case Configs.READ_SUCCESS:
                    Object[] obj = (Object[]) msg.obj;
                    if ((Boolean) obj[0]) {
                        List<QingbaoComment> nblist = (List<QingbaoComment>) obj[2];
                        if (nblist.size() < 10) {
                            xListView.setPullLoadEnable(false);
                        }else
                            xListView.setPullLoadEnable(true);
                        if (msg.arg1 == -1) {
                            adapter.getAll().clear();
                        }
                        adapter.addlist(nblist);
                        adapter.notifyDataSetChanged();
                    } else {
                        xListView.setPullLoadEnable(false);
                        showToast(obj[1].toString());
                    }
                    break;

                default:
                    break;
            }

        }

        ;
    };

    @Override
    public void onRefresh() {
        xListView.stopRefresh();
    }

    @Override
    public void onLoadMore() {
        read(adapter.getAll().get(adapter.getAll().size() - 1).id);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1001) {
            newsBean.commentcount = newsBean.commentcount + 1;
            commentcount.setText("" + newsBean.commentcount);
            read(-1);
        }
    }

    @Override
    public void onCancel(Platform arg0, int arg1) {
    }

    @Override
    public void onComplete(Platform arg0, int arg1, HashMap<String, Object> arg2) {
    }

    @Override
    public void onError(Platform arg0, int arg1, Throwable arg2) {
    }
}