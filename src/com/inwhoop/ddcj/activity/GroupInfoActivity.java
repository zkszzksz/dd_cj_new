package com.inwhoop.ddcj.activity;

import android.content.Context;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.*;
import android.widget.LinearLayout.LayoutParams;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class GroupInfoActivity extends BaseFragmentActivity implements View.OnClickListener {

    private LayoutInflater inflater = null;

    private RelativeLayout topLayout = null;
    private TextView groupId;
    private TextView createTime;
    private TextView count;
    private TextView host;
    private TextView fensi;
    private TextView qinbao;
    private Button addGroupBtn;
    private UserBean userBean;
    private Context context;
    private RelativeLayout back;
    private Object[] obj;
    private Button gzQz;
    private Button lookFans;
    private Button dyBtn;
    private boolean tag = false;

    private String groupid;

    private int flag = 0;
    private CateChannel cateChannel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.group_info_layout);
        inflater = LayoutInflater.from(mContext);
        context = GroupInfoActivity.this;

        setGroupInfo();
        initData();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void initData() {
        super.initData();
        userBean = UserInfoUtil.getUserInfo(this);
        setTitleStrText("群信息");
        setLeftLayout(R.drawable.back_btn_selector, false);
        back = (RelativeLayout) findViewById(R.id.title_left_layout);
        back.setOnClickListener(this);
        if (null != getIntent()) {
            flag = getIntent().getIntExtra("flag", 0);
            if (flag == 2) {
                groupid = getIntent().getStringExtra("groupid");
            } else {
                cateChannel = (CateChannel) getIntent().getSerializableExtra("CateChannel");
                groupid = cateChannel.id;
            }
        }
        topLayout = (RelativeLayout) findViewById(R.id.toplayout);
        groupId = (TextView) findViewById(R.id.gid);
        createTime = (TextView) findViewById(R.id.g_createtime);
        count = (TextView) findViewById(R.id.g_count);
        host = (TextView) findViewById(R.id.g_host);
        fensi = (TextView) findViewById(R.id.g_fensi);
        qinbao = (TextView) findViewById(R.id.g_qingbao);
        gzQz = (Button) findViewById(R.id.add_host_btn);
        gzQz.setOnClickListener(this);

        lookFans = (Button) findViewById(R.id.look_btn);
        lookFans.setOnClickListener(this);
        addGroupBtn = (Button) findViewById(R.id.addqx_btn);
        dyBtn = (Button) findViewById(R.id.subscribe_btn);

        addGroupBtn.setOnClickListener(this);
        dyBtn.setOnClickListener(this);

        LinearLayout.LayoutParams parm = new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT, getWindowManager()
                .getDefaultDisplay().getHeight() / 3);
        topLayout.setLayoutParams(parm);
        getGroupInfo();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addqx_btn:
                if (userBean.ddid.equals("")) {
                    Act.toAct(mContext, LoginActivity.class);
                } else {
                    if (!addGroupBtn.getText().toString().trim().equals("已加入")) {
                        addGroup();
                    } else {
                        Toast.makeText(context, "您已加入该群", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
            case R.id.title_left_layout:
                finish();
                break;
            case R.id.add_host_btn:
                if (userBean.ddid.equals("")) {
                    Act.toAct(mContext, LoginActivity.class);
                } else {
                    if (null != cateChannel) {
                        if (gzQz.getText().toString().trim().equals("+群组")) {
                            attention();
                        } else {
                            cancelAttention();
                        }
                    }
                }
                break;
            case R.id.look_btn:
                Intent intent = new Intent(context, GroupItemActivity.class);
                intent.putExtra("groupid", cateChannel.id);
                startActivity(intent);
                break;
            case R.id.subscribe_btn:
                if (userBean.ddid.equals("")) {
                    Act.toAct(mContext, LoginActivity.class);
                } else {
                    dyData(tag);
                }
                break;
        }
    }

    private void addGroup() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.applayJoinGroup(userBean.userid, cateChannel.id);
                    if (obj[0].equals("200")) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                handler.sendMessage(msg);
            }
        }).start();
    }

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Toast.makeText(context, "已提交加群申请，请等待确认", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, (String) obj[1], Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "提交失败，请稍后再试", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void attention() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.attentionUser(userBean.userid, cateChannel.adminid,
                            "来自帐号查找");
                    if ((Boolean) obj[0]) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    System.out.println("家群组===" + e.toString());
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mHandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    gzQz.setText("取消关注");
                    gzQz.setEnabled(false);
                    Toast.makeText(context, "关注成功", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, (String) obj[1], Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void dyData(final boolean isOrder) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.orderChannel(userBean.userid, cateChannel.channelid, isOrder);
                    if ((Boolean) obj[0]) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mnhandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mnhandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    dyBtn.setText("已订阅");
                    Toast.makeText(context, "订阅成功", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, (String) obj[1], Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void cancelAttention() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    obj = JsonUtils.cancelAttentio(userBean.userid, cateChannel.adminid);
                    if ((Boolean) obj[0]) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                }
                mcHandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler mcHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    gzQz.setText("+群组");
                    Toast.makeText(context, "取消关注成功", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_FAIL:
                    Toast.makeText(context, "取消关注失败", Toast.LENGTH_SHORT).show();
                    break;
                case Configs.READ_ERROR:
                    Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    private void getGroupInfo() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    cateChannel = JsonUtils.getGroupInfo(groupid, userBean.userid);
                    if (null != cateChannel) {
                        msg.what = Configs.READ_SUCCESS;
                    } else {
                        msg.what = Configs.READ_FAIL;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    msg.what = Configs.READ_ERROR;
                    System.out.println("解析异常====111===" + e.toString());
                }
                chandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler chandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    setGroupInfo();
                    break;
                case Configs.READ_FAIL:
                    break;
                case Configs.READ_ERROR:
                    break;
            }
        }
    };

    private void setGroupInfo() {
        if (cateChannel.isgroup.equals("yes")) {
            addGroupBtn.setText("已加入");
        } else {
            addGroupBtn.setText("+加入群组");
        }
        if (cateChannel.ischannel.equals("yes")) {
            dyBtn.setText("已订阅");
            tag = false;
        } else {
            dyBtn.setText("+订阅");
            tag = true;
        }

        if (null != cateChannel) {
            groupId.setText(cateChannel.id);
            //createTime.setText(cateChannel.c);
            count.setText(cateChannel.groupnum);
            host.setText(cateChannel.groupmanager);
            fensi.setText(cateChannel.fanscount);
            qinbao.setText(cateChannel.newscount);
            if (cateChannel.adminattention.equals("yes")) {
                gzQz.setText("取消关注");
            }
        }
    }
}
