package com.inwhoop.ddcj.activity;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.WindowManager;
import android.widget.*;

import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.GroupChatlistAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.GroupChatTypeBean;
import com.inwhoop.ddcj.bean.GroupChatinfo;
import com.inwhoop.ddcj.bean.GroupInfomation;
import com.inwhoop.ddcj.bean.Groups;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.db.HandleFriendchatlistRecordDB;
import com.inwhoop.ddcj.db.HandleGroupchatNoReadRecordDB;
import com.inwhoop.ddcj.db.HandleGroupchatRecordDB;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.FaceRelativeLayout;
import com.inwhoop.ddcj.view.TouchSayButton;
import com.inwhoop.ddcj.view.TouchSayButton.OnRecordListener;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;
import com.inwhoop.ddcj.xmpp.ClientGroupServer;
import com.inwhoop.ddcj.xmpp.GroupChatPacketExtension;
import com.pocketdigi.utils.FLameUtils;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: FriendChatActivity.java
 * @Package com.inwhoop.gameproduct.acitivity
 * @Description: TODO 群聊
 * @date 2014-6-14 上午10:47:58
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class GroupChatActivity extends BaseFragmentActivity implements
        IXListViewListener, OnClickListener, OnRecordListener {
    private String groupid; // 聊天对象用户昵称

    private String groupname; // 聊天对象openfire的账号

    private String groupphoto; // 聊天对象头像

    private XListView listView = null;

    private Button sendButton = null; // 发送按钮

    private TouchSayButton audioButton = null; // 按住说话按钮

    private ImageView recordImageView = null; // 录音按钮

    private EditText msgEditText = null; // 输入框

    private boolean isRecording = false;

    private ImageView carmerImageView = null; // 调用照相机

    private List<GroupChatinfo> chatList = new ArrayList<GroupChatinfo>();

    private String msgcontent = ""; // 消息内容

    private GroupChatlistAdapter adapter = null; // 适配器

    private HandleGroupchatRecordDB db = null;

    private HandleGroupchatNoReadRecordDB ndb = null;

    private HandleFriendchatlistRecordDB fdb = null;

    private int page = 1; // 用于聊天记录分页

    private FaceRelativeLayout faceRelativeLayout = null;
    private int isCreate;// 标识从创建成功跳转而来，是11

    private UserBean userinfo = null;
    private CateChannel cateChannel;
    private List<CateChannel> listGroup;
    private ArrayList<Groups> list1;

    private ScheduledExecutorService scheduledExecutorService;

    private boolean isFred = true;

    private boolean isGetType = true;

    private GroupInfomation info = null;
    private boolean isBottom=false;

    @SuppressWarnings("unchecked")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(
                WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE
                        | WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setContentView(R.layout.friend_chat_layout);
        mContext = GroupChatActivity.this;
        userinfo = UserInfoUtil.getUserInfo(mContext);
        db = new HandleGroupchatRecordDB(mContext);
        ndb = new HandleGroupchatNoReadRecordDB(mContext);
        fdb = new HandleFriendchatlistRecordDB(mContext);
        if (null != getIntent()) {
            groupid = getIntent().getStringExtra("groupid");
            groupname = getIntent().getStringExtra("groupname");
            groupphoto = getIntent().getStringExtra("groupphoto");
            isCreate = getIntent().getIntExtra("isCreate", 0);
            Configs.groupid = groupid;
            listGroup = (List<CateChannel>) getIntent().getSerializableExtra(
                    "listobj");
            Groups groups = null;
            list1 = new ArrayList<Groups>();
            for (int i = 0; i < listGroup.size(); i++) {
                groups = new Groups();
                groups.groupid = listGroup.get(i).id;
                groups.groupname = listGroup.get(i).name;
                list1.add(groups);
            }
            System.out.println("群聊的房间号为" + groupid + "==" + groupname + "=="
                    + groupphoto);
        }
        initData();
    }

    @Override
    public void initData() {
        super.initData();
        setTitleStrText("" + groupname);
        setLeftLayout(R.drawable.back_btn_selector, false);
        listView = (XListView) findViewById(R.id.chatlist);
        listView.setXListViewListener(this);
        listView.setPullLoadEnable(false);
        sendButton = (Button) findViewById(R.id.sendbtn);
        sendButton.setOnClickListener(this);
        recordImageView = (ImageView) findViewById(R.id.record_img);
        recordImageView.setOnClickListener(this);
        recordImageView.setVisibility(View.VISIBLE);
        audioButton = (TouchSayButton) findViewById(R.id.audiobtn);
        audioButton.setOnRecordListener(this);
        carmerImageView = (ImageView) findViewById(R.id.carmer_bg);
        carmerImageView.setOnClickListener(this);
        faceRelativeLayout = (FaceRelativeLayout) findViewById(R.id.faceRelativeLayout);
        listView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(View arg0, MotionEvent arg1) {
                faceRelativeLayout.hide();
                return false;
            }
        });
        IntentFilter filter = new IntentFilter();// 创建IntentFilter对象
        filter.addAction(Configs.COME_MESSAGE);
        registerReceiver(mRecever, filter);
        msgEditText = (EditText) findViewById(R.id.sendmsg);
        // 监听输入框的内容
        msgEditText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int arg1, int arg2,
                                      int arg3) {
                if (s.length() > 0) {
                    sendButton.setVisibility(View.VISIBLE);
                    recordImageView.setVisibility(View.GONE);
                    audioButton.setVisibility(View.GONE);
                    msgEditText.setVisibility(View.VISIBLE);
                } else {
                    sendButton.setVisibility(View.GONE);
                    recordImageView.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int arg1, int arg2,
                                          int arg3) {
            }

            @Override
            public void afterTextChanged(Editable arg0) {

            }
        });
        initListviewData();

        RelativeLayout rela = (RelativeLayout) findViewById(R.id.title_right_layout);
        rela.setVisibility(View.VISIBLE);
        TextView textView = (TextView) findViewById(R.id.title_right_tv);
        textView.setBackgroundResource(R.drawable.group_img);
        rela.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, GroupSettingActivity.class);
                Bundle bundle = new Bundle();
                bundle.putSerializable("groupinfo", info);
                bundle.putString("groupid", groupid);
                intent.putExtras(bundle);
                startActivityForResult(intent, 1000);
            }
        });
      /*  msgEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {

                }
            }
        });*/
        msgEditText.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("LISTVIEW的滚动状态" + isBottom);
                if (!isBottom) {
                    listView.setSelection(listView.getCount() - 1);
                }
            }
        });
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            public void onScrollStateChanged(AbsListView view, int scrollState) {
                switch (scrollState) {
                    // 当不滚动时
                    case AbsListView.OnScrollListener.SCROLL_STATE_IDLE:
                        // 判断滚动到底部
                        if (listView.getLastVisiblePosition() == (listView.getCount() - 1)) {
                            isBottom=true;
                        }
                        // 判断滚动到顶部

                        if (listView.getFirstVisiblePosition() == 0) {
                        }

                        if (listView.getFirstVisiblePosition()>0&&listView.getFirstVisiblePosition()<(listView.getCount() - 1)){
                            isBottom=false;
                        }

                        break;
                }
            }

            @Override
            public void onScroll(AbsListView absListView, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
            }
        });
    }

    private void readnet() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    Object[] obj = JsonUtils.getVeryGroupInfo(groupid,
                            UserInfoUtil.getUserInfo(mContext).userid);
                    msg.obj = obj;
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = Configs.READ_FAIL;
                }
                groupinfohandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler groupinfohandler = new Handler() {
        @SuppressWarnings("unchecked")
        public void handleMessage(Message msg) {
            dismissProgressDialog();
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    Object[] obj = (Object[]) msg.obj;
                    if ((Boolean) obj[0]) {
                        info = ((List<GroupInfomation>) obj[2]).get(0);
                    } else {
                        showToast("" + obj[1]);
                    }
                    break;

                case Configs.READ_FAIL:
                    break;

                default:
                    break;
            }
        }
    };

    private void gotoMainActivity() {
        if (isCreate == 11) {
            Act.toActClearTop(mContext, MainActivity.class);
        }
        finish();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            gotoMainActivity();
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * 初始化listview
     *
     * @param
     * @return void
     * @Title: initListview
     * @Description: TODO
     */
    private void initListviewData() {
        new Thread(new Runnable() {

            @Override
            public void run() {
                chatList = db.getUserBygroupid(groupid,
                        UserInfoUtil.getUserInfo(mContext).tel, page);
                List<GroupChatinfo> nlist = ndb.getUserBygroupid(groupid,
                        mContext);
                for (int i = 0; i < nlist.size(); i++) {
                    chatList.add(nlist.get(i));
                    db.saveGmember(nlist.get(i));
                }
                if (null != nlist && nlist.size() > 0) {
                    ndb.deleteGmember(groupid,
                            UserInfoUtil.getUserInfo(mContext).tel);
                }
                handler.sendEmptyMessage(0);
            }
        }).start();
    }

    @SuppressLint("HandlerLeak")
    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            page++;
            adapter = new GroupChatlistAdapter(mContext, chatList, listView);
            listView.setAdapter(adapter);
            listView.setSelection(listView.getCount() - 1);
        }
    };

    @Override
    public void onRefresh() {
        try {
            chatList = db.getUserBygroupid(groupid,
                    UserInfoUtil.getUserInfo(mContext).tel, page);
            page++;
            adapter.addChatinfo(chatList);
            adapter.notifyDataSetChanged();
        } catch (Exception e) {
        }
        listView.stopRefresh();
    }

    @Override
    public void onLoadMore() {

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sendbtn:
                sendMsgcontext();
                break;

            case R.id.record_img:
                if (isRecording) {
                    isRecording = false;
                    msgEditText.setVisibility(View.VISIBLE);
                    audioButton.setVisibility(View.GONE);
                    recordImageView.setBackgroundResource(R.drawable.btn_vol);
                } else {
                    isRecording = true;
                    msgEditText.setVisibility(View.GONE);
                    audioButton.setVisibility(View.VISIBLE);
                    recordImageView.setBackgroundResource(R.drawable.btn_keybord);
                }
                faceRelativeLayout.hide();
                break;

            case R.id.carmer_bg:
                showCameraDialog(imgpathHandler, false, 0, 0, false);
                break;

            case R.id.title_left_layout:
                finish();
                break;

            default:
                break;
        }
    }

    /**
     * 调用系统照相机
     */
    @SuppressLint("HandlerLeak")
    private Handler imgpathHandler = new Handler() {
        public void handleMessage(android.os.Message msg) {
            if (RESULT_IMG == msg.what) {
                msgcontent = Utils.imgToBase64(Utils.compressImage(msg.obj
                        .toString()));
                sendGroupchatmessage("", 2, msgcontent, 0);
            }
        }
    };

    /**
     * 录音之后进行的处理
     */
    @Override
    public void onRecord(String filepath, long time) {
        if ("".equals(filepath)) {
            return;
        }
        if (time < 1) {
            showToast("亲，您录音时间太短了~~");
            return;
        }
        try {
            String path = "/ddcj/audio/" + System.currentTimeMillis() + ".mp3";
            FLameUtils lameUtils = new FLameUtils(1, 16000, 96);
            lameUtils.raw2mp3(Environment.getExternalStorageDirectory()
                    + filepath, Environment.getExternalStorageDirectory()
                    + path);
            sendGroupchatmessage(
                    path,
                    3,
                    Utils.encodeBase64File(Environment
                            .getExternalStorageDirectory() + path), (int) time);
        } catch (Exception e) {
            System.out.println("======filepath========" + e.toString());
            showToast("断线了%>_<%~~");
            return;
        }
    }

    /**
     * 发送文字信息
     *
     * @param
     * @return void
     * @Title: sendMsgcontext
     * @Description: TODO
     */
    private void sendMsgcontext() {
        msgcontent = msgEditText.getText().toString().trim();
        if ("".equals(msgcontent)) {
            showToast("消息内容不能为空");
        } else {
            sendGroupchatmessage("", 1, msgcontent, 0);
            msgEditText.setText("");
        }
    }

    @Override
    protected void onDestroy() {
        Configs.groupid = "-1";
        unregisterReceiver(mRecever);
        super.onDestroy();
    }

    private void sendGroupchatmessage(final String audiopath,
                                      final int msgtype, final String msgcontent, final int audiolength) {
        if (!isFred && null != info && !UserInfoUtil.getUserInfo(mContext).tel.equals(info.tel)) {
            showToast("现在处于会议模式，只有群主能发言");
            return;
        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                Message msg = new Message();
                try {
                    GroupChatPacketExtension pack = new GroupChatPacketExtension();
                    pack.setAudiopath(audiopath);
                    pack.setUsernick(userinfo.name);
                    pack.setGroupid(groupid);
                    pack.setGroupname(groupname);
                    pack.setMsgtype(msgtype);
                    pack.setContent(msgcontent);
                    pack.setAudiolength(audiolength);
                    pack.setSendtime(TimeRender.getStandardDate());
                    pack.setHeadpath(userinfo.img);
                    pack.setIsLoop(2);
                    pack.setGroupphoto(groupphoto);
                    pack.setChatid(userinfo.tel);
                    pack.setUserid(userinfo.tel);
                    ClientGroupServer.getInstance(mContext)
                            .getChatMu(list1, groupid)
                            .sendMessage(pack.toXML());
                    // getChatManager(username).sendMessage(pack.toXML());
                    msg.obj = new GroupChatinfo(msgcontent, "" + userinfo.tel,
                            userinfo.tel, userinfo.name, groupid, groupname,
                            userinfo.img, TimeRender.getStandardDate(), true,
                            msgtype, audiopath, audiolength, 2, groupphoto, ""
                            + userinfo.chatid);
                    sendMsgData();
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
                    msg.what = Configs.READ_FAIL;
                }
                chatHandler.sendMessage(msg);
            }
        }).start();
    }

    private Handler chatHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_FAIL:
                    showToast("断线了~~");
                    break;

                case Configs.READ_SUCCESS:
                    GroupChatinfo chatinfo = (GroupChatinfo) msg.obj;
                    db.saveGmember(chatinfo);
                    // if (!fdb.isExist(groupid, 2, TimeRender.getStandardDate(),
                    // userinfo.tel)) {
                    // fdb.saveGmember(groupid, 2, TimeRender.getStandardDate(),
                    // userinfo.tel, groupphoto, groupname);
                    // } else {
                    // fdb.UpdateChatagetTime(groupid, 2, userinfo.tel,
                    // TimeRender.getStandardDate(), groupphoto, groupname);
                    // }
                    if (fdb.isExist(groupid, 2, TimeRender.getStandardDate(),
                            userinfo.tel)) {
                        fdb.deleteGmember(groupid,
                                "" + UserInfoUtil.getUserInfo(mContext).tel);
                    }
                    fdb.saveGmember(groupid, 2, TimeRender.getStandardDate(),
                            userinfo.tel, groupphoto, groupname);
                    adapter.addChatinfo(chatinfo);
                    adapter.notifyDataSetChanged();
                    listView.setSelection(listView.getCount() - 1);
                    break;

                default:
                    break;
            }
        }

        ;
    };

    public BroadcastReceiver mRecever = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Configs.COME_MESSAGE)) {
                GroupChatinfo chatinfo = (GroupChatinfo) intent
                        .getSerializableExtra("info");
                adapter.addChatinfo(chatinfo);
                adapter.notifyDataSetChanged();
                listView.setSelection(listView.getCount() - 1);
            }
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        KeyBoard.HiddenInputPanel(mContext);
        isGetType = false;
        if (null != scheduledExecutorService)
            scheduledExecutorService.shutdown();
    }

    @Override
    protected void onResume() {
        super.onResume();
        readnet();
        isGetType = true;
        scheduledExecutorService = null;
        scheduledExecutorService = Executors.newScheduledThreadPool(1);
        scheduledExecutorService.execute(new Runnable() {
            @Override
            public void run() {
                while (isGetType) {
                    Message msg = new Message();
                    try {
                        Object[] obj = JsonUtils.getGroupChatType(groupid);
                        msg.obj = obj;
                        msg.what = Configs.READ_SUCCESS;
                    } catch (Exception e) {
                        msg.what = Configs.READ_FAIL;
                    }
                    getHandler.sendMessage(msg);
                    try {
                        Thread.sleep(2000);
                    } catch (Exception e) {
                    }
                }
            }
        });
    }

    private Handler getHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_SUCCESS:
                    try {
                        Object[] obj = (Object[]) msg.obj;
                        if ((Boolean) obj[0]) {
                            @SuppressWarnings("unchecked")
                            GroupChatTypeBean type = ((List<GroupChatTypeBean>) obj[2])
                                    .get(0);
                            if (type.ismeetingtype.equals("yes")) {
                                isFred = false;
                            } else {
                                isFred = true;
                            }
                        }
                    } catch (Exception e) {
                    }
                    break;

                default:
                    break;
            }
        }

        ;
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
    	super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == 1000) {
            initListviewData();
        }
    };

    private void sendMsgData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                SyncHttp http = new SyncHttp();
                try {
                    http.httpGet(Configs.HOST + "/index.php/Group/ChatPush", "?groupid=" + groupid);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                http = null;
            }
        }).start();
    }

}