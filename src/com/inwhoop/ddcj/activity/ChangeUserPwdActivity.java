package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.EditText;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.KeyBoard;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * @Describe: TODO 修改密码 x * * * ****** Created by ZK ********
 * @Date: 2014/11/10 16:13
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class ChangeUserPwdActivity extends BaseFragmentActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_change_user_pwd);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.change_pwd);
		setRightLayout(R.string.refer, true);
		title_right_tv.setTextColor(getResources()
				.getColor(R.color.blue_028efd));
		title_right_layout.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				next();
			}
		});
	}

	private void next() {

		final UserBean userBean = UserInfoUtil.getUserInfo(mContext);
		userBean.pwd = ((EditText) findViewById(R.id.old_pwd)).getText()
				.toString().trim();
		final String newPwd = ((EditText) findViewById(R.id.new_pwd)).getText()
				.toString();
		String msg = check(userBean, newPwd);
		if (!"".equals(msg)) {
			showToast(msg);
		} else {
			showProgressDialog("请稍后...");
			new Thread(new Runnable() {

				@Override
				public void run() {
					Message msg = new Message();
					try {
						msg.obj = JsonUtils.userUpdatePwd(userBean, newPwd); // 未传tel，返回码500
						msg.what = Configs.READ_SUCCESS;
					} catch (Exception e) {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);
				}
			}).start();
		}
	}

	private String check(UserBean user, String newPwd) {
		String surePwd = ((EditText) findViewById(R.id.sure_pwd_et)).getText()
				.toString();
		if ("".equals(user.pwd)) {
			return "请输入原密码";
		} else if (!newPwd.equals(surePwd) || newPwd.equals("")) {
			return "请确认新密码";
		}
		return "";
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("验证失败，请稍后再试~~");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					Act.toActClearTop(mContext, StartActivity.class);
					Act.toActClearTop(mContext, LoginActivity.class);
				}
                showToast("" + obj[1].toString());
                break;
			}
		}
	};

	@Override
	protected void onPause() {
		super.onPause();
		KeyBoard.HiddenInputPanel(mContext);
	}
}
