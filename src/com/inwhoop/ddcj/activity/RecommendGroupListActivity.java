package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.GroupSettingAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;

import java.util.List;

/**
 * @Describe: TODO 优选 群组列表 * * * ****** Created by ZK ********
 * @Date: 2014/11/11 11:48
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class RecommendGroupListActivity extends BaseFragmentActivity implements
		XListView.IXListViewListener {

	private GroupSettingAdapter adapter;

	private XListView listView = null;
	private String id = "";
	private String name = "";
	private String type = "";
	private List<CateChannel> list;
	private Context context;
	private UserBean userBean;
	private int page = 1;
	private boolean isLoad = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.recommend_group_list);
		context = RecommendGroupListActivity.this;
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		if (null != getIntent() && null != getIntent().getExtras()) {
			id = getIntent().getExtras().getString("id");
			name = getIntent().getExtras().getString("name");
			type = getIntent().getExtras().getString("type");
			// System.out.println("类型为====="+type);
		}
		userBean = UserInfoUtil.getUserInfo(context);
		setLeftLayout(R.drawable.back_btn_selector, false);
		setRightLayout(R.drawable.search_btn_selector, false);
		title_right_layout.setOnClickListener(this);
		setTitleStrText(name);
		listView = (XListView) findViewById(R.id.rec_group_list);
		listView.setPullLoadEnable(true);
		listView.setXListViewListener(this);

		adapter = new GroupSettingAdapter(mContext);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Bundle bundle = new Bundle();
				bundle.putSerializable("CateChannel",
						adapter.getAll().get(arg2 - 1));
				Intent intent = new Intent(mContext, GroupInfoActivity.class);
				intent.putExtras(bundle);
				startActivity(intent);
			}
		});

	}

	@Override
	protected void onResume() {
		super.onResume();
		getData();
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.title_right_layout: // 搜索
			Act.toAct(mContext, SearchAllActivity.class);
			break;

		default:
			break;
		}
	}

	@Override
	public void onRefresh() {
		isLoad = false;
		page = 1;
		getData();
	}

	@Override
	public void onLoadMore() {
		isLoad = true;
		getData();
		page++;
	}

	private void getData() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getGetCateChannel(type, id,
							userBean.userid, Configs.COUNT + "", page + "");
					if (null != list && list.size() > 0) {
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
				} catch (Exception e) {
					e.printStackTrace();
					msg.what = Configs.READ_ERROR;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			listView.stopLoadMore();
			listView.stopRefresh();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				if (list.size() < Configs.COUNT) {
					listView.setPullLoadEnable(false);
				} else {
					listView.setPullLoadEnable(true);
				}
				adapter.add(list, isLoad);
				adapter.notifyDataSetChanged();
				break;
			case Configs.READ_FAIL:
				Toast.makeText(context, "暂无数据", Toast.LENGTH_SHORT).show();
				break;
			case Configs.READ_ERROR:
				Toast.makeText(context, "暂无数据", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

}
