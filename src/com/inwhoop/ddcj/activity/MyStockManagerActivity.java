package com.inwhoop.ddcj.activity;

import android.app.ListActivity;
import android.app.PendingIntent;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.*;

import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.MyStockManagerAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.impl.DetelStockLinsener;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.StockUtil;
import com.inwhoop.ddcj.view.CheckableLinearLayout;
import com.inwhoop.ddcj.view.DragSortListView;

import java.util.ArrayList;
import java.util.List;

/**
 * 自选股管理界面
 * 
 * @Project: DDCJ
 * @Title: MyStockManagerActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-10-27 下午6:18:12
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class MyStockManagerActivity extends ListActivity implements
		View.OnClickListener, DetelStockLinsener {
	private Context mContext;
	private MyStockManagerAdapter adapter;
	private List<StockInfoBean> tempList = new ArrayList<StockInfoBean>();// 临时存储原始的数据
	private List<StockInfoBean> list = new ArrayList<StockInfoBean>(); // 传递过来的我的自选股列表,也是排序后的数据
	private List<StockInfoBean> deleteList = new ArrayList<StockInfoBean>();
	private RelativeLayout back, serarch;
	private TextView backText, searchText;
	private TextView title;
	private CheckBox checkedTextView;
	private DragSortListView dragSortListView;
	private LinearLayout deleteLayout;
	private Button deleteBt = null;
	private List<Boolean> isChecked = new ArrayList<Boolean>();

	private DragSortListView.DropListener onDrop = new DragSortListView.DropListener() {
		@Override
		public void drop(int from, int to) {
			if (from != to) {
				StockInfoBean item = adapter.getItem(from);
				adapter.remove(item);
				adapter.insert(item, to);
				dragSortListView.moveCheckState(from, to);
				boolean isbool = isChecked.get(from);
				isChecked.remove(from);
				isChecked.add(to, isbool);

			}
		}
	};

	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.checkable_main);
		mContext = this;
		back = (RelativeLayout) findViewById(R.id.title_left_layout);
		back.setOnClickListener(this);
		serarch = (RelativeLayout) findViewById(R.id.title_right_layout);
		serarch.setVisibility(View.VISIBLE);
		serarch.setOnClickListener(this);
		back.setVisibility(View.VISIBLE);
		backText = (TextView) findViewById(R.id.title_left_tv);
		backText.setBackgroundResource(R.drawable.back_btn_selector);
		searchText = (TextView) findViewById(R.id.title_right_tv);
		searchText.setBackgroundResource(R.drawable.search_btn_selector);
		title = (TextView) findViewById(R.id.title_center_tv);
		title.setText(getResources().getString(R.string.manger));
		deleteLayout = (LinearLayout) findViewById(R.id.delete);
		deleteBt = (Button) findViewById(R.id.delete_bt);
		list = (ArrayList<StockInfoBean>) getIntent().getExtras().get("list");
		tempList = new ArrayList<StockInfoBean>(list);
		adapter = new MyStockManagerAdapter(this, R.layout.list_item_checkable,
				R.id.title, list);
		adapter.setData(list);
		adapter.setDetelStockLinsener(this);
		isChecked = adapter.getisCheckedList();
		setListAdapter(adapter);
		dragSortListView = getListView();
		dragSortListView.setDropListener(onDrop);
		deleteBt.setOnClickListener(this);
	}

	@Override
	public DragSortListView getListView() {
		return (DragSortListView) super.getListView();
	}

	// @Override
	// protected void onListItemClick(ListView l, View v, int position, long id)
	// {
	// checkedTextView = (CheckBox) v.findViewById(R.id.chechbox);
	// if (checkedTextView.isChecked()) {
	// deleteList.add(list.get(position));
	// isChecked.remove(position);
	// isChecked.add(position, true);
	// } else {
	// isChecked.remove(position);
	// isChecked.add(position, false);
	// for (int i = 0; i < deleteList.size(); i++) {
	// if (deleteList.get(i).stockcode
	// .equals(list.get(position).stockcode)) {
	// deleteList.remove(deleteList.get(i));
	// // for循环是先根据中间的值判断是否为true，然后再执行后面的i++
	// i--;
	// }
	// }
	// }
	// if (deleteList.size() > 0) {
	// deleteLayout.setVisibility(View.VISIBLE);
	// } else {
	// deleteLayout.setVisibility(View.GONE);
	// }
	// super.onListItemClick(l, v, position, id);
	// }

	/**
	 * 比较list是否相同
	 * 
	 * @Title: compareList
	 * @Description: TODO
	 * @param @return
	 * @return boolean
	 */
	private boolean compareList() {
		if (list.size() == tempList.size()) {
			for (int i = 0; i < list.size(); i++) {
				if (!list.get(i).stockname.equals(tempList.get(i).stockname)) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public void onClick(View view) {
		switch (view.getId()) {
		case R.id.title_left_layout:
			if (!compareList()) {
				StockUtil.saveList(list, handler, mContext);
			}
			finish();

			break;
		case R.id.delete_bt:
			StockUtil.deleteStocklist(deleteList, handler, mContext);
			break;

		case R.id.title_right_layout:
			Act.toAct(mContext, MyStockSearchActivity.class);
			break;
		}
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			Object[] objects = (Object[]) msg.obj;
			int what = (Integer) objects[1];
			if (what == Configs.DELETE_SUCCESS) {
				for (int i = 0; i < isChecked.size(); i++) {

					if (isChecked.get(i)) {
						dragSortListView.removeCheckState(i);
					}
					if (i < deleteList.size()) {
						StockInfoBean stockInfoBean = (StockInfoBean) deleteList
								.get(i);
						list.remove(stockInfoBean);
					}
				}
				adapter.initChecked(list);
				adapter.setData(list);
				adapter.notifyDataSetChanged();
				deleteList.clear();
				Toast.makeText(getApplicationContext(), R.string.delete_sucess,
						Toast.LENGTH_SHORT).show();
			} else if (what == Configs.DELETE_FAIL) {
				Toast.makeText(getApplicationContext(), R.string.delete_fail,
						Toast.LENGTH_SHORT).show();
			}
		}
	};

	@Override
	public void detelStock(List<StockInfoBean> deleteList) {
		this.deleteList = deleteList;
		if (deleteList.size() > 0) {
			deleteLayout.setVisibility(View.VISIBLE);
		} else {
			deleteLayout.setVisibility(View.GONE);
		}

	}

}
