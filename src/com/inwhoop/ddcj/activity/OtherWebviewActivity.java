package com.inwhoop.ddcj.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;

import com.inwhoop.R;

/**
 * 设置-其他-各项介绍页面 Webview
 *
 * @author ZOUXU
 */
public class OtherWebviewActivity extends BaseFragmentActivity {
    private WebView wv_showPage;
    private Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_other_webview_layout);
        context = OtherWebviewActivity.this;
        // setContentView(R.layout.connect_us_layout);
        initData();
        title_left_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void initData() {
        super.initData();
        wv_showPage = (WebView) findViewById(R.id.wv_content);
        int pos = getIntent().getIntExtra("pos", -1);
        switch (pos) {
            case 0:
                setTitleStrText("使用帮助");
                setRightLayout(R.string.fankui, true);
                //wv_showPage.loadUrl("http://www.cnblogs.com/frank-zouxu/");
                wv_showPage.loadUrl("file:///android_asset/xieyi.html");
                title_right_layout.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(context, FeedbackActivity.class));
                    }
                });

                break;
            case 1:
                setTitleStrText("用户协议");
                wv_showPage.loadUrl("file:///android_asset/xieyi.htm");
                break;
            case 2:
                setTitleStrText("版本介绍");
                wv_showPage.loadUrl("file:///android_asset/xieyi.htm");
                break;

            case 3:
                setTitleStrText("联系我们");
                break;
            case 4:
                setTitleStrText("风险提示");
                wv_showPage.loadUrl("file:///android_asset/tishi.html");
                break;
        }

        setLeftLayout(R.drawable.back_btn_selector, false);
    }

}
