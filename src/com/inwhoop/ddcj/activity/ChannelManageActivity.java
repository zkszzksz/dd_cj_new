package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationSet;
import android.view.animation.TranslateAnimation;
import android.widget.*;
import android.widget.AdapterView.OnItemClickListener;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.ChannelDragAdapter;
import com.inwhoop.ddcj.adapter.ChannelOtherAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ChannelInfo;
import com.inwhoop.ddcj.bean.ChannelItem;
import com.inwhoop.ddcj.bean.ChannelManage;
import com.inwhoop.ddcj.bean.UploadChannellist;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.ChannelDragGrid;
import com.inwhoop.ddcj.view.ChannelOtherGrid;

import java.util.ArrayList;
import java.util.List;

/**
 * 频道管理
 * 
 * @Author RA
 * @Blog http://blog.csdn.net/vipzjyno1
 */
public class ChannelManageActivity extends Activity implements
		OnItemClickListener {

	private final String SP_KEY = "LOCAL";// 匿名用户
	private final String SP_VALUE = "1";// 匿名用户value
	/**
	 * 用户栏目的GRIDVIEW
	 */
	private ChannelDragGrid userGridView;
	/**
	 * 其它栏目的GRIDVIEW
	 */
	private ChannelOtherGrid otherGridView;
	/**
	 * 用户栏目对应的适配器，可以拖动
	 */
	ChannelDragAdapter userAdapter;
	/**
	 * 其它栏目对应的适配器
	 */
	ChannelOtherAdapter otherAdapter;
	/**
	 * 其它栏目列表
	 */
	ArrayList<ChannelItem> otherChannelList = new ArrayList<ChannelItem>();
	/**
	 * 用户栏目列表
	 */
	ArrayList<ChannelItem> userChannelList = new ArrayList<ChannelItem>();

	/**
	 * 用户栏目列表临时存储
	 */
	private ArrayList<ChannelItem> userChannelTempList = new ArrayList<ChannelItem>();
	/**
	 * 是否在移动，由于这边是动画结束后才进行的数据更替，设置这个限制为了避免操作太频繁造成的数据错乱。
	 */
	boolean isMove = false;

	// 功能定制-修改
	private Context context;
	private DialogShowStyle dialogShowStyle;
	private Button showedNum;
	// private ArrayList<ChannelItem> netAllChannelList = new
	// ArrayList<ChannelItem>();
	public final static int SUCCESS = 0x1001;
	public final static int FAIL = 0x1002;
	public final static int ERROR = 0x1003;

	public static final int NONET = 0;
	public static final int SLOWNET = 2;
	public static final int WIFI = 1;
	private List<ChannelInfo> titleList;
	private UserBean userBean;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.subscribe_activity_2);
		initView();
		initData();
		setDataShow();
	}

	/**
	 * 初始化数据
	 */
	private void initData() {
		userBean = UserInfoUtil.getUserInfo(context);
		if (null == userBean.ddid || userBean.ddid.equals("")) {
			new getNetDataTask().execute();

		} else {
			loadData();
		}

	}

	/**
	 * 初始化布局
	 */
	private void initView() {
		userGridView = (ChannelDragGrid) findViewById(R.id.userGridView);
		otherGridView = (ChannelOtherGrid) findViewById(R.id.otherGridView);
		showedNum = (Button) findViewById(R.id.bt_showAllChannel);
		showedNum.setText("显示全部频道");
		showedNum.setTag(0);
		findViewById(R.id.bt_showAllChannel).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						if (Integer.parseInt(v.getTag().toString()) == 0) {
							showedNum.setTag(1);
							((Button) v).setText("折叠部分频道");
							otherAdapter.isShowAllChannel = true;
							otherAdapter.notifyDataSetChanged();
						} else if (Integer.parseInt(v.getTag().toString()) == 1) {
							showedNum.setTag(0);
							((Button) v).setText("显示全部频道");
							otherAdapter.isShowAllChannel = false;
							otherAdapter.notifyDataSetChanged();
						}
					}
				});

		findViewById(R.id.title_left_layout).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						// saveChannel();
						if (!compareList()) {
							userBean = UserInfoUtil.getUserInfo(context);
							if (null == userBean.ddid
									|| userBean.ddid.equals("")) {
								saveChannel();
							} else {
								saveChannellist();
							}
							setResult(100);
						}
						ChannelManageActivity.this.finish();
					}
				});
	}

	/**
	 * 比较list是否相同
	 * 
	 * @Title: compareList
	 * @Description: TODO
	 * @param @return
	 * @return boolean
	 */
	private boolean compareList() {
		if (userChannelList.size() == userChannelTempList.size()) {
			for (int i = 0; i < userChannelTempList.size(); i++) {
				if (!userChannelList.get(i).name.equals(userChannelTempList
						.get(i).name)) {
					return false;
				}
			}
		} else {
			return false;
		}
		return true;
	}

	/**
	 * GRIDVIEW对应的ITEM点击监听接口
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, final View view,
			final int position, final long id) {
		// 如果点击的时候，之前动画还没结束，那么就让点击事件无效
		if (isMove) {
			return;
		}
		switch (parent.getId()) {
		case R.id.userGridView:
			// position为 0，1 的不可以进行任何操作
			if (position != 0) {
				final ImageView moveImageView = getView(view);
				if (moveImageView != null) {
					final TextView newTextView = (TextView) view
							.findViewById(R.id.text_item);
					final int[] startLocation = new int[2];
					newTextView.getLocationInWindow(startLocation);
					final ChannelItem channel = ((ChannelDragAdapter) parent
							.getAdapter()).getItem(position);// 获取点击的频道内容
					otherAdapter.setVisible(false);
					// 添加到最后一个
					otherAdapter.addItem(channel);
					// WEB API 接口调用
					if (!userBean.ddid.equals("")) {
						new Thread(new Runnable() {
							@Override
							public void run() {
								// TODO Auto-generated method stub
								try {
									final Object[] res = JsonUtils
											.delUserChannel(userBean.userid,
													newTextView.getTag()
															.toString());
								} catch (Exception e) {
									e.printStackTrace();
								}
							}
						}).start();
					}
					new Handler().postDelayed(new Runnable() {
						public void run() {
							try {
								int[] endLocation = new int[2];
								// 获取终点的坐标
								otherGridView.getChildAt(
										otherGridView.getLastVisiblePosition())
										.getLocationInWindow(endLocation);
								MoveAnim(moveImageView, startLocation,
										endLocation, channel, userGridView);
								userAdapter.setRemove(position);
							} catch (Exception localException) {
							}
						}
					}, 50L);
				}
			}
			break;
		case R.id.otherGridView:
			final ImageView moveImageView = getView(view);
			if (moveImageView != null) {
				final TextView newTextView = (TextView) view
						.findViewById(R.id.text_item);
				final int[] startLocation = new int[2];
				newTextView.getLocationInWindow(startLocation);
				final ChannelItem channel = ((ChannelOtherAdapter) parent
						.getAdapter()).getItem(position);
				userAdapter.setVisible(false);
				// 添加到最后一个
				userAdapter.addItem(channel);

				// WEB API 接口调用
				if (!userBean.ddid.equals("")) {
					new Thread(new Runnable() {
						@Override
						public void run() {
							// TODO Auto-generated method stub
							try {
								final Object[] res = JsonUtils.addUserChannel(
										userBean.userid, newTextView.getTag()
												.toString());
								/*
								 * runOnUiThread(new Runnable() { public void
								 * run() { ToastUtils.showLong(
								 * getBaseContext(), "json==获取到的服务器的结果码数据为:" +
								 * res[0].toString() + "---" +
								 * res[1].toString()); } });
								 */
							} catch (Exception e) {
								e.printStackTrace();
							}
						}
					}).start();
				}
				new Handler().postDelayed(new Runnable() {
					public void run() {
						try {
							int[] endLocation = new int[2];
							// 获取终点的坐标
							userGridView.getChildAt(
									userGridView.getLastVisiblePosition())
									.getLocationInWindow(endLocation);
							MoveAnim(moveImageView, startLocation, endLocation,
									channel, otherGridView);
							otherAdapter.setRemove(position);
						} catch (Exception localException) {
						}
					}
				}, 50L);
			}
			break;
		default:
			break;
		}
	}

	/**
	 * 点击ITEM移动动画
	 * 
	 * @param moveView
	 * @param startLocation
	 * @param endLocation
	 * @param moveChannel
	 * @param clickGridView
	 */
	private void MoveAnim(View moveView, int[] startLocation,
			int[] endLocation, final ChannelItem moveChannel,
			final GridView clickGridView) {
		int[] initLocation = new int[2];
		// 获取传递过来的VIEW的坐标
		moveView.getLocationInWindow(initLocation);
		// 得到要移动的VIEW,并放入对应的容器中
		final ViewGroup moveViewGroup = getMoveViewGroup();
		final View mMoveView = getMoveView(moveViewGroup, moveView,
				initLocation);
		// 创建移动动画
		TranslateAnimation moveAnimation = new TranslateAnimation(
				startLocation[0], endLocation[0], startLocation[1],
				endLocation[1]);
		moveAnimation.setDuration(300L);// 动画时间
		// 动画配置
		AnimationSet moveAnimationSet = new AnimationSet(true);
		moveAnimationSet.setFillAfter(false);// 动画效果执行完毕后，View对象不保留在终止的位置
		moveAnimationSet.addAnimation(moveAnimation);
		mMoveView.startAnimation(moveAnimationSet);
		moveAnimationSet.setAnimationListener(new AnimationListener() {

			@Override
			public void onAnimationStart(Animation animation) {
				isMove = true;
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				moveViewGroup.removeView(mMoveView);
				// instanceof 方法判断2边实例是不是一样，判断点击的是DragGrid还是OtherGridView
				if (clickGridView instanceof ChannelDragGrid) {
					otherAdapter.setVisible(true);
					otherAdapter.notifyDataSetChanged();
					userAdapter.remove();
				} else {
					userAdapter.setVisible(true);
					userAdapter.notifyDataSetChanged();
					otherAdapter.remove();
				}
				isMove = false;
			}
		});
	}

	/**
	 * 获取移动的VIEW，放入对应ViewGroup布局容器
	 * 
	 * @param viewGroup
	 * @param view
	 * @param initLocation
	 * @return
	 */
	private View getMoveView(ViewGroup viewGroup, View view, int[] initLocation) {
		int x = initLocation[0];
		int y = initLocation[1];
		viewGroup.addView(view);
		LinearLayout.LayoutParams mLayoutParams = new LinearLayout.LayoutParams(
				LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		mLayoutParams.leftMargin = x;
		mLayoutParams.topMargin = y;
		view.setLayoutParams(mLayoutParams);
		return view;
	}

	/**
	 * 创建移动的ITEM对应的ViewGroup布局容器
	 */
	private ViewGroup getMoveViewGroup() {
		ViewGroup moveViewGroup = (ViewGroup) getWindow().getDecorView();
		LinearLayout moveLinearLayout = new LinearLayout(this);
		moveLinearLayout.setLayoutParams(new LinearLayout.LayoutParams(
				LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		moveViewGroup.addView(moveLinearLayout);
		return moveLinearLayout;
	}

	/**
	 * 获取点击的Item的对应View，
	 * 
	 * @param view
	 * @return
	 */
	private ImageView getView(View view) {
		view.destroyDrawingCache();
		view.setDrawingCacheEnabled(true);
		Bitmap cache = Bitmap.createBitmap(view.getDrawingCache());
		view.setDrawingCacheEnabled(false);
		ImageView iv = new ImageView(this);
		iv.setImageBitmap(cache);
		return iv;
	}

	/**
	 * 退出时候保存选择后数据库的设置
	 */
	private void saveChannel() {
		ChannelManage.getManage(Configs.getApp().getSQLHelper())
				.deleteAllChannel();
		ChannelManage.getManage(Configs.getApp().getSQLHelper())
				.saveUserChannel(userAdapter.getChannnelLst());
		ChannelManage.getManage(Configs.getApp().getSQLHelper())
				.saveOtherChannel(otherAdapter.getChannnelLst());
	}

	private void saveChannellist() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					JsonUtils.uploadChannellist(
							UserInfoUtil.getUserInfo(context).userid,
							getChannellistStr());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	private String getChannellistStr() {
		String json = "";
		List<ChannelItem> list = userChannelList;
		List<UploadChannellist> clist = new ArrayList<UploadChannellist>();
		UploadChannellist info = null;
		for (int i = 1; i < list.size(); i++) {
			info = new UploadChannellist();
			info.channelid = list.get(i).id;
			info.position = i;
			clist.add(info);
		}
		GsonBuilder gsonBuilder = new GsonBuilder();
		Gson gson = gsonBuilder.create();
		json = gson.toJson(clist);
		return json;
	}

	private void loadData() {
		new Thread(new Runnable() {

			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				try {
					String uid = UserInfoUtil.getUserInfo(context).userid;
					Object[] data = JsonUtils.getUsersChannelList(uid);
					if ((Boolean) data[0]) {
						titleList = (List<ChannelInfo>) data[2];
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				ChannelItem item = null;
				userChannelList.clear();
				ChannelItem bean = new ChannelItem();
				bean.name = "推荐";
				bean.id = 0;
				userChannelList.add(0, bean);
				userChannelTempList.add(0, bean);
				otherChannelList.clear();
				if (null != titleList && titleList.size() > 0) {
					for (int i = 0; i < titleList.size(); i++) {
						if (null != titleList.get(i)) {
							for (int j = 0; j < titleList.get(i).foucslist
									.size(); j++) {
								item = new ChannelItem();
								item.id = titleList.get(i).foucslist.get(j).id;
								item.name = titleList.get(i).foucslist.get(j).name;
								item.orderId = j;
								item.selected = 1;
								userChannelList.add(item);
								userChannelTempList.add(item);
							}
							for (int j = 0; j < titleList.get(i).alllist.size(); j++) {
								item = new ChannelItem();
								item.id = titleList.get(i).alllist.get(j).id;
								item.name = titleList.get(i).alllist.get(j).name;
								item.orderId = j;
								item.selected = 0;
								otherChannelList.add(item);
							}
						}
					}
				}
				setDataShow();
				break;

			case Configs.READ_FAIL:
				break;
			}

		}
	};

	class getNetDataTask extends AsyncTask<Void, Integer, Integer> {

		@Override
		protected void onPreExecute() {

			int netType = new NetWorkUtils(ChannelManageActivity.this)
					.getNetType();
			switch (netType) {
			case NONET:
				Toast.makeText(ChannelManageActivity.this, "请检查网络连接",
						Toast.LENGTH_LONG).show();
				this.cancel(true);
				break;
			case SLOWNET:
				dialogShowStyle = new DialogShowStyle(
						ChannelManageActivity.this, "当前为非WIFI网络，正在获取频道信息");
				dialogShowStyle.dialogShow();
				break;
			}

			userBean = UserInfoUtil.getUserInfo(context);
			userChannelList.clear();
			userChannelTempList.clear();
			otherChannelList.clear();
			ChannelItem bean = new ChannelItem();
			bean.name = "推荐";
			bean.id = 0;
			userChannelList.add(0, bean);
			userChannelTempList.add(0, bean);
			userChannelList = ((ArrayList<ChannelItem>) ChannelManage
					.getManage(Configs.getApp().getSQLHelper())
					.getUserChannel());
			userChannelTempList = ((ArrayList<ChannelItem>) ChannelManage
					.getManage(Configs.getApp().getSQLHelper())
					.getUserChannel());
			otherChannelList = ((ArrayList<ChannelItem>) ChannelManage
					.getManage(Configs.getApp().getSQLHelper())
					.getOtherChannel());
		}

		@Override
		protected Integer doInBackground(Void... params) {
			if (Utils.getpreference(context, SP_KEY).equals(SP_VALUE)) {
				return 0;
			}
			// 添加频道项:第一个是推荐，默认值
			// if (userChannelList.size() == 1 && otherChannelList.size() == 0)
			// {
			// new Thread(new Runnable() {
			// @Override
			// public void run() {
			// TODO Auto-generated method stub
			try {
				// otherChannelList = JsonUtils.getAllChannels(0 + "");

				// 此路不通
				// ChannelManage.defaultOtherChannels =
				// otherChannelList=JsonUtils.getAllChannels(0 + "");

				// 此法可行？？？
				// ArrayList<ChannelItem> list = new ArrayList<ChannelItem>();
				Object[] data = JsonUtils.getUsersChannelList("0");
				ChannelInfo channelInfo = new ChannelInfo();
				int leng = 0;
				if ((Boolean) data[0]) {
					channelInfo = ((List<ChannelInfo>) data[2]).get(0);
					leng = channelInfo.alllist.size()
							+ channelInfo.foucslist.size();
					for (int i = 0; i < channelInfo.alllist.size(); i++) {
						ChannelItem item = new ChannelItem();
						item.id = channelInfo.alllist.get(i).id;
						item.name = channelInfo.alllist.get(i).name;
						item.selected = channelInfo.alllist.get(i).foucs;
						item.orderId = i + 1 + 1;// 默认第一个为：推荐序号为1
						// list.add(item);
						otherChannelList.add(item);
					}
					for (int i = 0; i < channelInfo.foucslist.size(); i++) {
						ChannelItem item = new ChannelItem();
						item.id = channelInfo.foucslist.get(i).id;
						item.name = channelInfo.foucslist.get(i).name;
						item.selected = channelInfo.foucslist.get(i).foucs;
						item.orderId = i + 1 + 1;// 默认第一个为：推荐序号为1
						// list.add(item);
						userChannelList.add(item);
					}
				}
				// final int lg = list.size();
				/*
				 * runOnUiThread(new Runnable() { public void run() {
				 * ToastUtils.showLong(getBaseContext(),
				 * "json==获取到的服务器的结果数据num为:" + lg); } });
				 */

				// 服务器有更新
				// if (userChannelList.size() + otherChannelList.size() < leng)
				// {
				ChannelManage.defaultOtherChannels.addAll(otherChannelList);
				// list.removeAll(otherChannelList);
				// otherChannelList.clear();
				// int length = list.size();
				// for (int i = 0; i < length; i++) {
				// for (int j = 0; j < userChannelList.size(); j++) {
				// if (!list.get(i).name
				// .equals(userChannelList.get(j).name)) {
				// System.out.println("saveOtherChannel= "
				// + list.get(i).name + " = "
				// + userChannelList.get(j).name);
				// ChannelManage.defaultOtherChannels.add(list
				// .get(i));
				// }
				// }
				//
				// }

				ChannelManage.getManage(Configs.getApp().getSQLHelper())
						.deleteAllChannel();
				ChannelManage.getManage(Configs.getApp().getSQLHelper())
						.saveUserChannel(userChannelList);
				ChannelManage.getManage(Configs.getApp().getSQLHelper())
						.saveOtherChannel(ChannelManage.defaultOtherChannels);
				// 考虑可以不从数据库获取值，如果不出错
				// otherChannelList = ((ArrayList<ChannelItem>) ChannelManage
				// .getManage(Configs.getApp().getSQLHelper())
				// .getOtherChannel());
				// } else if (userChannelList.size() + otherChannelList.size() >
				// leng) {
				// // 服务器数据减少
				// }

			} catch (Exception e) {
				e.printStackTrace();
			}
			// }
			// }).start();
			// }

			// 测试baseAdapter的显示一定数量条目的功能
			// otherChannelList.add(new ChannelItem(7, "军事", 7, 0));
			// otherChannelList.add(new ChannelItem(8, "财经", 8, 0));
			// otherChannelList.add(new ChannelItem(9, "汽车", 9, 0));
			// otherChannelList.add(new ChannelItem(10, "房产", 10, 0));
			// otherChannelList.add(new ChannelItem(11, "社会",11, 0));

			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (dialogShowStyle != null) {
				dialogShowStyle.dialogDismiss();
				dialogShowStyle = null;
			}
			Utils.savePreference(context, SP_KEY, SP_VALUE);

			setDataShow();
		}

	}

	private void setDataShow() {
		userAdapter = new ChannelDragAdapter(context, userChannelList);
		userGridView.setAdapter(userAdapter);
		otherAdapter = new ChannelOtherAdapter(context, otherChannelList);
		otherGridView.setAdapter(otherAdapter);
		// 设置GRIDVIEW的ITEM的点击监听
		otherGridView.setOnItemClickListener(this);
		userGridView.setOnItemClickListener(this);
	}

	@Override
	public void onBackPressed() {
		// saveChannel();
		super.onBackPressed();
	}
}
