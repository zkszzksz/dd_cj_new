package com.inwhoop.ddcj.activity;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.LinearLayout.LayoutParams;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.framework.utils.UIHandler;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.wechat.moments.WechatMoments;
import com.inwhoop.R;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.EditQBChannellist;
import com.inwhoop.ddcj.bean.NewsBean;
import com.inwhoop.ddcj.bean.PsClassImg;
import com.inwhoop.ddcj.bean.SearchStock;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.FaceRelativeLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * 编辑情报
 * 
 * @Project: DDCJ
 * @Title: EditQiBaoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-11-12 下午6:58:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class EditQiBaoActivity extends BaseFragmentActivity implements
		OnClickListener, MLocationLinsener, Handler.Callback {

	private LayoutInflater inflater = null;

	private LinearLayout zxLayout = null;

	private CheckBox zxBox, otherBox;

	private FaceRelativeLayout faceRelativeLayout = null;

	private EditText contentEditText = null;

	private ArrayList<PsClassImg> imglist = new ArrayList<PsClassImg>();

	private ArrayList<SearchStock> addlist = new ArrayList<SearchStock>();

	// private LinearLayout.LayoutParams parm = null;

	private String picpath = "";

	private ImageView addImageView, addggImageView;

	private TextView imgsizeTextView, ggsizeTextView;

	private LinearLayout addggLayout = null;

	private String sd1, sd;

	private String zipname = "";

	private String content = "";

	private PopupWindow popupWindow = null;

	private List<EditQBChannellist> channellist = new ArrayList<EditQBChannellist>();
	private NewsBean newsBean;
	private CheckBox sinaCheckBox, wbCheckBox;

	private LinearLayout pdLayout = null;

	private int type = 1;

	private String channelid = "";
	
	private String title = "";
	
	private EditText titleEditText = null;
	
	private List<CheckBox> boxs = new ArrayList<CheckBox>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.edit_qingbao_layout);
		ShareSDK.initSDK(mContext);
		inflater = LayoutInflater.from(mContext);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleStrText("编辑情报");
		setLeftLayout(R.drawable.back_btn_selector, false);
		setRightLayout(R.string.send, true);
		title_right_layout.setOnClickListener(this);
		zxLayout = (LinearLayout) findViewById(R.id.zxlayout);
		zxBox = (CheckBox) findViewById(R.id.zx_box);
		pdLayout = (LinearLayout) findViewById(R.id.pdlayout);
		otherBox = (CheckBox) findViewById(R.id.cg_box);
		faceRelativeLayout = (FaceRelativeLayout) findViewById(R.id.faceRelativeLayout);
		contentEditText = (EditText) findViewById(R.id.sendmsg);
		titleEditText = (EditText) findViewById(R.id.title_eidt);
		faceRelativeLayout.setEtsendmessage(contentEditText);
		addImageView = (ImageView) findViewById(R.id.qbimg);
		addImageView.setOnClickListener(this);
		imgsizeTextView = (TextView) findViewById(R.id.imgsize);
		addggImageView = (ImageView) findViewById(R.id.addggimg);
		ggsizeTextView = (TextView) findViewById(R.id.ggsize);
		addggLayout = (LinearLayout) findViewById(R.id.addgglayout);
		addggLayout.setOnClickListener(this);
		otherBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton view, boolean flag) {
				if (flag) {
					showPopupWindow(view);
				}
			}
		});
		readChannellist();

		sinaCheckBox = (CheckBox) findViewById(R.id.sinaimg);
		wbCheckBox = (CheckBox) findViewById(R.id.wbimg);
	}

	@SuppressWarnings("static-access")
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			if (faceRelativeLayout.view.getVisibility() == View.VISIBLE) {
				faceRelativeLayout.view.setVisibility(View.GONE);
			} else {
				finish();
			}
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		Intent intent = null;
		Bundle bundle = null;
		switch (v.getId()) {
		case R.id.qbimg:
			intent = new Intent(mContext, AddPictureActivity.class);
			bundle = new Bundle();
			bundle.putSerializable("imglist", imglist);
			intent.putExtras(bundle);
			startActivityForResult(intent, 1001);
			break;

		case R.id.addgglayout:
			intent = new Intent(mContext, SearchGeguActivity.class);
			bundle = new Bundle();
			bundle.putSerializable("stocklist", addlist);
			intent.putExtras(bundle);
			startActivityForResult(intent, 1002);
			break;

		case R.id.title_right_layout:
			String check = check();
			if ("".equals(check)) {
				startLocation(this);
				// setMyShare(); //测试
			} else {
				showToast(check);
			}
			break;
		default:
			break;
		}
	}

	private String check() {
		content = contentEditText.getText().toString().trim();
		title = titleEditText.getText().toString().trim();
		if("".equals(title)){
			return "标题不能为空";
		}
		if (content.length() == 0) {
			return "情报内容不能为空";
		}
		if (addlist.size() == 0) {
			return "请添加关联个股";
		}
		if (!checkIsOnwhere()) {
			return "请选择发布的场所";
		}
		if (content.length() > 255) {
			return "亲，情报内容长度不能超过255个字节~~";
		}
		return "";
	}

	private boolean checkIsOnwhere() {
		if (!zxBox.isChecked() && !otherBox.isChecked()) {
			// boolean isf = true;
			// for (int i = 0; i < channellist.size(); i++) {
			// if(channellist.get(i).isselect){
			// isf = false;
			// }
			// }
			// if(isf){
			return false;
			// }
		}
		if (!zxBox.isChecked() && otherBox.isChecked()) {
			boolean isf = true;
			for (int i = 0; i < channellist.size(); i++) {
				if (channellist.get(i).isselect) {
					isf = false;
				}
			}
			if (isf) {
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 1001) {
			imglist = (ArrayList<PsClassImg>) data
					.getSerializableExtra("imglist");
			if (imglist.size() == 0) {
				imgsizeTextView.setVisibility(View.INVISIBLE);
			} else {
				imgsizeTextView.setVisibility(View.VISIBLE);
				imgsizeTextView.setText("" + imglist.size());
			}
		} else if (resultCode == 1002) {
			addlist = (ArrayList<SearchStock>) data
					.getSerializableExtra("stocklist");
			if (addlist.size() > 0) {
				addggImageView.setVisibility(View.GONE);
				ggsizeTextView.setVisibility(View.VISIBLE);
				ggsizeTextView.setText("" + addlist.size());
			} else {
				addggImageView.setVisibility(View.VISIBLE);
				ggsizeTextView.setVisibility(View.GONE);
			}
		}
	}

	private void sendZip(final String address) {
		showProgressDialogFalse("正在上传，请稍后~~");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					if (imglist.size() > 0) {
						zipFile();
					}
					getTypeAndChannelid();
					Object[] obj = JsonUtils.editQibao(
							UserInfoUtil.getUserInfo(mContext).userid,title, sd1, ""
									+ type, content, address, getStocode(),
							zipname, channelid);
					msg.what = Configs.READ_SUCCESS;
					msg.obj = obj;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private String getStocode() {
		String code = "";
		for (int i = 0; i < addlist.size(); i++) {
			if (i == (addlist.size() - 1)) {
				code = code + addlist.get(i).stockcode;
			} else {
				code = code + addlist.get(i).stockcode + ",";
			}
		}
		return code;
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					setMyShare();

					showToast("上传成功");
					setResult(1000);
					finish();
				} else {
					showToast("" + obj[1].toString());
				}
				break;

			case Configs.READ_FAIL:
				showToast("情报编辑失败，请稍后再试");
				break;

			default:
				break;
			}
		};
	};

	private void zipFile() throws Exception {
		zipname = "" + System.currentTimeMillis();
		sd = Environment.getExternalStorageDirectory() + "/ddcj/qbimg/"
				+ zipname;
		sd1 = Environment.getExternalStorageDirectory() + "/ddcj/qbimg/"
				+ zipname + ".zip";
		Bitmap map = null;
		String path = "";
		for (int j = 0; j < imglist.size(); j++) {
			path = imglist.get(j).imgpath;
			// if (null != map)
			// map.recycle();
			// BitmapFactory.Options option = new BitmapFactory.Options();
			// option.inJustDecodeBounds = true;
			// map = BitmapFactory.decodeFile(path, option);
			// int h = option.outHeight;
			// int w = option.outWidth;
			// if (h > 100 || w > 100) {
			// BitmapFactory.Options o = new BitmapFactory.Options();
			// o.inSampleSize = 5;
			// map = BitmapFactory.decodeFile(path, o);
			// } else {
			// map = BitmapFactory.decodeFile(path);
			// }
			map = Utils.compressImage(path);
			ImageUtil.saveBitmapToFile(map, sd,
					path.substring(path.lastIndexOf("/") + 1, path.length()));
		}
		ZipUtil.ZipFolder(sd, sd1);
	}

	@Override
	public void mLocationSucess(String address) {
		sendZip(address);
	}

	@Override
	public void mLocationFaile(String errorInfo) {
		showToast("" + errorInfo);
	}

	private void initPopupWindow() {
		View view = inflater.inflate(R.layout.select_channel_layout, null);
		popupWindow = new PopupWindow(view, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		ImageView cancel = (ImageView) view.findViewById(R.id.cancel);
		cancel.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != popupWindow && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
		Button sureButton = (Button) view.findViewById(R.id.sure);
		sureButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != popupWindow && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
			}
		});
		LinearLayout itemlayout = (LinearLayout) view
				.findViewById(R.id.itemlayout);
		View itemView = null;
		for (int i = 0; i < channellist.size(); i++) {
			itemView = inflater.inflate(R.layout.select_channel_item_layout,
					null);
			CheckBox box = (CheckBox) itemView.findViewById(R.id.item_box);
			boxs.add(box);
			box.setTag(i);
			box.setOnCheckedChangeListener(new OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton v, boolean flag) {
					int pos = (Integer) v.getTag();
					for (int j = 0; j < channellist.size(); j++) {
						channellist.get(j).isselect = false;
						boxs.get(j).setChecked(false);
					}
					channellist.get(pos).isselect = flag;
					boxs.get(pos).setChecked(flag);
				}
			});
			TextView nameTextView = (TextView) itemView.findViewById(R.id.name);
			nameTextView.setText(channellist.get(i).name);
			itemlayout.addView(itemView);
		}
	}

	private void showPopupWindow(View view) {
		popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);
	}

	private void readChannellist() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = JsonUtils
							.getChannellistByuserid(UserInfoUtil
									.getUserInfo(mContext).userid);
					msg.obj = obj;
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				channelHandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler channelHandler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					channellist = (List<EditQBChannellist>) obj[2];
					if (channellist.size() == 0) {
						pdLayout.setVisibility(View.GONE);
						zxBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {

							@Override
							public void onCheckedChanged(CompoundButton arg0,
									boolean arg1) {
								zxBox.setChecked(true);
							}
						});
					} else {
						pdLayout.setVisibility(View.VISIBLE);
						initPopupWindow();
					}
				} else {
					showToast("" + obj[1].toString());
				}
				break;

			case Configs.READ_FAIL:

				break;

			default:
				break;
			}
		};
	};

	private void getTypeAndChannelid() {
		if (zxBox.isChecked() && !otherBox.isChecked()) {
			type = 1;
			channelid = "";
		} else if (!zxBox.isChecked() && otherBox.isChecked()) {
			type = 2;
			for (int i = 0; i < channellist.size(); i++) {
				if (channellist.get(i).isselect) {
					channelid = channelid + channellist.get(i).id + ",";
				}
			}
			channelid = channelid.substring(0, channelid.lastIndexOf(","));
		} else {
			for (int i = 0; i < channellist.size(); i++) {
				if (channellist.get(i).isselect) {
					channelid = channelid + channellist.get(i).id + ",";
				}
			}
			if (!"".equals(channelid)) {
				channelid = channelid.substring(0, channelid.lastIndexOf(","));
				type = 3;
			} else {
				type = 1;
				channelid = "";
			}
		}
	}

	private void setMyShare() {
		if (sinaCheckBox.isChecked()) {
			/**
			 * 新浪微博的无界面分享
			 **/
			SinaWeibo.ShareParams sp = new SinaWeibo.ShareParams();
			sp.text = "分享的内容";
			Platform pf = ShareSDK.getPlatform(EditQiBaoActivity.this,
					SinaWeibo.NAME);
			if (pf.isValid()) {
				pf.removeAccount();
			}
			pf.setPlatformActionListener(paListener);
			pf.share(sp);
		}
		if (wbCheckBox.isChecked()) { // 获取资源失败，仅支持分享照片至朋友圈
			WechatMoments.ShareParams wcsp = new WechatMoments.ShareParams();
			String imgpath = "http://222.137.116.71:20139/BMT/wap/icon.png";
			// wcsp.imageUrl="http://222.137.116.71:20139/BMT/wap/icon.png";
			wcsp.imageData = BitmapFactory.decodeResource(getResources(),
					R.drawable.ic_launcher);
			// wcsp.shareType=Platform.SHARE_TEXT;
			wcsp.text = "分享的内容";
			// wcsp.title="分享的title";
			// wcsp.url="url";
			// wcsp.set
			Platform wcpf = ShareSDK.getPlatform(EditQiBaoActivity.this,
					WechatMoments.NAME);
			if (wcpf.isValid()) {
				wcpf.removeAccount();
			}
			wcpf.setPlatformActionListener(paListener);
			wcpf.share(wcsp);

		}
	}

	PlatformActionListener paListener = new PlatformActionListener() {
		@Override
		public void onComplete(Platform platform, int action,
				HashMap<String, Object> hashMap) {
			// Toast.makeText(mContext,"分享成功",2000).show();
			// 成功监听,handle the successful msg
			Message msg = new Message();
			msg.what = MSG_ACTION_CCALLBACK;
			msg.arg1 = 1;
			msg.arg2 = action;
			msg.obj = platform;
			UIHandler.sendMessage(msg, EditQiBaoActivity.this);
		}

		@Override
		public void onError(Platform platform, int action, Throwable throwable) {
			// 打印错误信息,print the error msg
			throwable.printStackTrace();
			// 错误监听,handle the error msg
			Message msg = new Message();
			msg.what = MSG_ACTION_CCALLBACK;
			msg.arg1 = 2;
			msg.arg2 = action;
			msg.obj = throwable;
			UIHandler.sendMessage(msg, EditQiBaoActivity.this);
		}

		@Override
		public void onCancel(Platform platform, int action) {
			// 取消监听,handle the cancel msg
			Message msg = new Message();
			msg.what = MSG_ACTION_CCALLBACK;
			msg.arg1 = 3;
			msg.arg2 = action;
			msg.obj = platform;
			UIHandler.sendMessage(msg, EditQiBaoActivity.this);
		}
	};

	private static final int MSG_TOAST = 1;
	private static final int MSG_ACTION_CCALLBACK = 2;
	private static final int MSG_CANCEL_NOTIFY = 3;

	@Override
	public boolean handleMessage(Message msg) {
		switch (msg.what) {
		case MSG_TOAST: {
			String text = String.valueOf(msg.obj);
			Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
		}
			break;
		case MSG_ACTION_CCALLBACK: {
			switch (msg.arg1) {
			case 1: { // 成功提示, successful notification
				showNotification(2000, getString(R.string.share_completed));
			}
				break;
			case 2: { // 失败提示, fail notification
				String expName = msg.obj.getClass().getSimpleName();
				if ("WechatClientNotExistException".equals(expName)
						|| "WechatTimelineNotSupportedException"
								.equals(expName)) {
					showNotification(2000,
							getString(R.string.wechat_client_inavailable));
				} else if ("GooglePlusClientNotExistException".equals(expName)) {
					showNotification(2000,
							getString(R.string.google_plus_client_inavailable));
				} else if ("QQClientNotExistException".equals(expName)) {
					showNotification(2000,
							getString(R.string.qq_client_inavailable));
				} else {
					showNotification(2000, getString(R.string.share_failed));
				}
			}
				break;
			case 3: { // 取消提示, cancel notification
				showNotification(2000, getString(R.string.share_canceled));
			}
				break;
			}
		}
			break;
		case MSG_CANCEL_NOTIFY: {
			NotificationManager nm = (NotificationManager) msg.obj;
			if (nm != null) {
				nm.cancel(msg.arg1);
			}
		}
			break;
		}
		return false;
	}

	// 在状态栏提示分享操作,the notification on the status bar
	private void showNotification(long cancelTime, String text) {
		try {
			Context app = getApplicationContext();
			NotificationManager nm = (NotificationManager) app
					.getSystemService(Context.NOTIFICATION_SERVICE);
			final int id = Integer.MAX_VALUE / 13 + 1;
			nm.cancel(id);

			long when = System.currentTimeMillis();
			Notification notification = new Notification(
					R.drawable.ic_launcher, text, when);
			PendingIntent pi = PendingIntent.getActivity(app, 0, new Intent(),
					0);
			notification.setLatestEventInfo(app, "sharesdk test", text, pi);
			notification.flags = Notification.FLAG_AUTO_CANCEL;
			nm.notify(id, notification);

			if (cancelTime > 0) {
				Message msg = new Message();
				msg.what = MSG_CANCEL_NOTIFY;
				msg.obj = nm;
				msg.arg1 = id;
				UIHandler.sendMessageDelayed(msg, cancelTime, this);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		KeyBoard.HiddenInputPanel(mContext);
	}
}
