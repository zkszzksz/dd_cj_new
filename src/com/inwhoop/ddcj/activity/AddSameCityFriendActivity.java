package com.inwhoop.ddcj.activity;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import com.inwhoop.R;
import com.inwhoop.ddcj.adapter.AddSameCityAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.impl.AddFriendLinsener;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.ToastUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;

import java.util.List;

/**
 * @author 张凯
 * @Copyright: nd.com (c) 2014 All rights reserved.
 * @ClassName: ${TYPE_NAME}
 * @Description: 添加同城好友_添加页面子页面 ,可能bug：获取到了已经关注的人
 * @Package com.inwhoop.ddcj.activity
 * @Date: 2014/12/18 12:00
 * @version: 1.0
 */
public class AddSameCityFriendActivity extends BaseFragmentActivity implements
		MLocationLinsener, XListView.IXListViewListener, AddFriendLinsener {

	private static final int ADD_FRIEND_SUCCESS = 101;

	private XListView xListview;
	private UserBean userBean;
	private AddSameCityAdapter adapter;
	private String address = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_add_same_city_friend);
		userBean = UserInfoUtil.getUserInfo(mContext);
		startLocation(this);

		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setLeftLayout(R.drawable.back_btn_selector, false);
		setTitleResText(R.string.leidajiahaoyou);

		xListview = (XListView) findViewById(R.id.xlistview);
		xListview.setPullRefreshEnable(false);
		xListview.setPullLoadEnable(false);
		xListview.setXListViewListener(this);
		adapter = new AddSameCityAdapter(mContext);
		xListview.setAdapter(adapter);
		adapter.setFollowLinsener(this);
	}

	@Override
	public void mLocationSucess(String address) {
		this.address = address;
		initNetCityFriend(this.address);
	}

	@Override
	public void mLocationFaile(String errorInfo) {
		ToastUtils.showShort(mContext, "当前地址获取失败");
	}

	private void initNetCityFriend(final String address) {
		showProgressDialog("");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.getCityFriend(userBean.userid, address);
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.obj = e.toString();
					msg.what = Configs.READ_FAIL;
				} finally {
					handler.sendMessage(msg);
				}
			}
		}).start();
	}

	private void netAddFriend(final String touseid, final int pos) {
		showProgressDialog("");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					msg.obj = JsonUtils.attentionUser(userBean.userid, touseid,
							"来自添加同城好友");
					msg.what = ADD_FRIEND_SUCCESS;
				} catch (Exception e) {
					msg.obj = e.toString();
					msg.what = Configs.READ_FAIL;
				} finally {
					handler.sendMessage(msg);
				}
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
            Configs.isCareChage=true;
			dismissProgressDialog();
			xListview.stopLoadMore();
			xListview.stopRefresh();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj11 = (Object[]) msg.obj;
				if ((Boolean) obj11[0]) {
					List<UserBean> list = (List<UserBean>) obj11[2];
					adapter.add(list);
				}
				break;
			case ADD_FRIEND_SUCCESS:
				Object[] obj22 = (Object[]) msg.obj;
				if ((Boolean) obj22[0]) {
					adapter.getlist().clear();
					initNetCityFriend(address);
				}
				ToastUtils.showShort(mContext, "" + obj22[1]);
				break;
			case Configs.READ_FAIL:
				ToastUtils.showShort(mContext, "数据异常：" + msg.obj);
				break;
			}
		}
	};

	@Override
	public void onRefresh() {
		xListview.stopRefresh();
	}

	@Override
	public void onLoadMore() {
		xListview.stopLoadMore();
	}

	@Override
	public void addFriend(UserBean bean, int position) {
		netAddFriend(bean.userid,position);
	}
}
