package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import com.inwhoop.R;
import com.inwhoop.ddcj.util.Act;

/**
 * 设置界面——我的
 * 
 * @author ZOUXU
 */
public class SettingMyActivity extends Activity implements OnClickListener {

	private Context ctx;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ctx = this;
		setContentView(R.layout.setting_my_acvtivity);

		findViewById(R.id.title_left_tv).setOnClickListener(this);

		findViewById(R.id.SettingMyActivity_apply_new).setOnClickListener(this);
		findViewById(R.id.SettingMyActivity_manage).setOnClickListener(this);
		findViewById(R.id.SettingMyActivity_my_group).setOnClickListener(this);
		findViewById(R.id.my_shoucang).setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_left_tv:
			finish();
			break;
		case R.id.SettingMyActivity_apply_new:
			Act.toAct(ctx, ApplySetGroupActivity.class);
			break;
		case R.id.SettingMyActivity_manage:
			Act.toAct(ctx, ChannelManageActivity.class);
			break;
		case R.id.SettingMyActivity_my_group:
			Act.toAct(ctx, MyGroupListActivity.class);
			break;
		case R.id.my_shoucang:
			Act.toAct(ctx, MyCollectionActivity.class);
			break;
		}
	}
}
