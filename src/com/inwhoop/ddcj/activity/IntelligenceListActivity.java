package com.inwhoop.ddcj.activity;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TextView;

import cn.sharesdk.framework.p;

import com.inwhoop.R;
import com.inwhoop.ddcj.fragment.CommentMeFragment;
import com.inwhoop.ddcj.fragment.QingbaoFragment;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.UserInfoUtil;

/**
 * 
 * 情报列表
 * 
 * @Project: DDCJ
 * @Title: StockInfoActivity.java
 * @Package com.inwhoop.ddcj.activity
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-22 下午5:34:29
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class IntelligenceListActivity extends BaseFragmentActivity implements
		OnClickListener {

	private TextView scTextView, gfTextView, zxTextView, cmTextView;
	private List<TextView> textViews = new ArrayList<TextView>();

	private View scline, gfline, zxline, cmline;
	private List<View> lineList = new ArrayList<View>();

	private QingbaoFragment fragment1, fragment2, fragment3;

	private CommentMeFragment fragment4;
	private FragmentTransaction mFt;

	private int nowposition = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.samecity_qb_type_layout);
		initData();
	}

	@Override
	public void initData() {
		super.initData();
		setTitleStrText("同城情报");
		setLeftLayout(R.drawable.back_btn_selector, false);
		setRightLayout(R.drawable.btn_edit_qb_selector, false);
		title_right_layout.setOnClickListener(this);
		scTextView = (TextView) findViewById(R.id.sc_qb);
		scTextView.setOnClickListener(this);
		gfTextView = (TextView) findViewById(R.id.gf_qb);
		gfTextView.setOnClickListener(this);
		zxTextView = (TextView) findViewById(R.id.zx_qb);
		zxTextView.setOnClickListener(this);
		cmTextView = (TextView) findViewById(R.id.cm_qb);
		cmTextView.setOnClickListener(this);
		textViews.add(scTextView);
		textViews.add(gfTextView);
		textViews.add(zxTextView);
		textViews.add(cmTextView);
		scline = findViewById(R.id.sc_line);
		gfline = findViewById(R.id.gf_line);
		zxline = findViewById(R.id.zx_line);
		cmline = findViewById(R.id.cm_line);
		lineList.add(scline);
		lineList.add(gfline);
		lineList.add(zxline);
		lineList.add(cmline);
		setSelect(0);
	}

	@Override
	public void onClick(View v) {
		super.onClick(v);
		switch (v.getId()) {
		case R.id.sc_qb:
			setSelect(0);
			break;
		case R.id.gf_qb:
			setSelect(1);
			break;
		case R.id.zx_qb:
			setSelect(2);
			break;
		case R.id.cm_qb:
			setSelect(3);
			break;
		case R.id.title_right_layout:
			String ddid = UserInfoUtil.getUserInfo(mContext).ddid;
			if (TextUtils.isEmpty(ddid)) {
				Act.toAct(mContext, LoginActivity.class);
			} else {
				Intent intent = new Intent(mContext, EditQiBaoActivity.class);
				startActivityForResult(intent, 1000);
			}
			break;

		default:
			break;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == 1000) {
			switch (nowposition) {
			case 0:
				fragment1.swithread();
				break;
			case 1:
				fragment2.swithread();
				break;
			case 2:
				fragment3.swithread();
				break;

			default:
				break;
			}
		} else if (resultCode == 1005) {
			fragment4.read(-1);
		}
	}

	private void setSelect(int position) {
		if (nowposition == position) {
			return;
		}
		nowposition = position;
		for (int i = 0; i < lineList.size(); i++) {
			lineList.get(i).setVisibility(View.GONE);
			textViews.get(i).setTextColor(
					getResources().getColor(R.color.white));
		}
		lineList.get(position).setVisibility(View.VISIBLE);
		textViews.get(position).setTextColor(
				getResources().getColor(R.color.qb_title_color));
		mFt = getSupportFragmentManager().beginTransaction();
		if (null != fragment1) {
			mFt.hide(fragment1);
		}
		if (null != fragment2) {
			mFt.hide(fragment2);
		}
		if (null != fragment3) {
			mFt.hide(fragment3);
		}
		if (null != fragment4) {
			mFt.hide(fragment4);
		}
		switch (position) {
		case 0:
			setTitleStrText("同城情报");
			if (null == fragment1) {
				fragment1 = new QingbaoFragment(0);
				mFt.add(R.id.qingbaoframelayout, fragment1);
			} else {
				mFt.show(fragment1);
			}
			break;
		case 1:
			setTitleStrText("好友情报");
			if (null == fragment2) {
				fragment2 = new QingbaoFragment(1);
				mFt.add(R.id.qingbaoframelayout, fragment2);
			} else {
				mFt.show(fragment2);
			}
			break;
		case 2:
			setTitleStrText("自选情报");
			if (null == fragment3) {
				fragment3 = new QingbaoFragment(2);
				mFt.add(R.id.qingbaoframelayout, fragment3);
			} else {
				mFt.show(fragment3);
			}
			break;
		case 3:
			setTitleStrText("评论我的");
			if (null == fragment4) {
				fragment4 = new CommentMeFragment();
				mFt.add(R.id.qingbaoframelayout, fragment4);
			} else {
				mFt.show(fragment4);
			}
			break;

		default:
			break;
		}
		mFt.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		mFt.commit();
	}

}
