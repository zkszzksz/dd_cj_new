package com.inwhoop.ddcj.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.*;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import com.baidu.location.*;
import com.baidu.location.LocationClientOption.LocationMode;
import com.inwhoop.R;
import com.inwhoop.ddcj.impl.ClickTabCallBack;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.ExitAppUtils;
import com.inwhoop.ddcj.util.ToastUtils;
import com.inwhoop.ddcj.util.Utils;
import com.inwhoop.ddcj.view.ScoreDialog;

/**
 * @Describe: TODO * * * ****** Created by ZK ********
 * @Date: 2014/10/20 15:43
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class BaseFragmentActivity extends FragmentActivity implements
        View.OnClickListener {
    protected Context mContext;
    protected View title_left_layout, title_right_layout;
    protected TextView title_left_tv, title_center_tv, title_right_tv;
    protected ProgressDialog progressDialog = null;
    public GeofenceClient mGeofenceClient;
    public MyLocationListener mMyLocationListener;
    public Vibrator mVibrator;
    private LocationClient mLocationClient;
    private LocationMode tempMode = LocationMode.Hight_Accuracy;
    private String tempcoor = "gcj02";
    private MLocationLinsener mLocationLinsener;
    public int pos=0;
    private Toast myToast;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mContext = this;
        ExitAppUtils.getInstance().addActivity(this);
        // CrashHandler.create(this); //崩溃退出异常。取消
    }

    /**
     * 初始化地图数据
     *
     * @param
     * @return void
     * @Title: initMap
     * @Description: TODO
     */
    private void initMap() {
        mLocationClient = new LocationClient(this.getApplicationContext());
        mMyLocationListener = new MyLocationListener();
        mLocationClient.registerLocationListener(mMyLocationListener);
        mGeofenceClient = new GeofenceClient(getApplicationContext());
        mVibrator = (Vibrator) getApplicationContext().getSystemService(
                Service.VIBRATOR_SERVICE);
        InitLocation();
    }

    public void initData() {
        title_left_layout = findViewById(R.id.title_left_layout);
        title_right_layout = findViewById(R.id.title_right_layout);

        title_left_tv = (TextView) findViewById(R.id.title_left_tv);
        title_center_tv = (TextView) findViewById(R.id.title_center_tv);
        title_right_tv = (TextView) findViewById(R.id.title_right_tv);
    }

    protected RadioButton[] btns;
    protected Fragment[] nullFfags = new Fragment[]{null, null, null, null,
            null, null};
    protected View[] tabLines;// 线
    protected RadioGroup groupLayout;

    /**
     * @param howManyItem 多少个tabitem，目前1~6
     * @param index       默认初始显示的下标0~5
     * @param hasLine     是否有下标线
     * @param newFrags    新的fragment组
     * @param whereDraw   图片在哪个位置left0，top1，right2，bottom3，整个并且没有文字4
     * @param draws
     * @param texts
     * @param styleRes
     */
    public void initTabData(int howManyItem, int index, boolean hasLine,
                            final Fragment[] newFrags, int whereDraw, final Drawable[] draws,
                            int[] texts, int styleRes) {
        btns = new RadioButton[]{(RadioButton) findViewById(R.id.tab0),
                (RadioButton) findViewById(R.id.tab1),
                (RadioButton) findViewById(R.id.tab2),
                (RadioButton) findViewById(R.id.tab3),
                (RadioButton) findViewById(R.id.tab4),
                (RadioButton) findViewById(R.id.tab5),};
        tabLines = new View[]{findViewById(R.id.tab0_line),
                findViewById(R.id.tab1_line), findViewById(R.id.tab2_line),
                findViewById(R.id.tab3_line), findViewById(R.id.tab4_line),
                findViewById(R.id.tab5_line)};
        for (int i = 0; i < btns.length && i < newFrags.length; i++) {
            if (i < howManyItem) {
                tabLines[i].setVisibility(View.VISIBLE);
                btns[i].setVisibility(View.VISIBLE);
            } else {
                tabLines[i].setVisibility(View.GONE);
                btns[i].setVisibility(View.GONE);
            }

            tabLines[howManyItem - 1].setVisibility(View.INVISIBLE);
            btns[i].setText(texts[i]);
            btns[i].setTextAppearance(mContext, styleRes);

            switch (whereDraw) {
                case 0: // left
                    btns[i].setCompoundDrawablesWithIntrinsicBounds(draws[i], null,
                            null, null);
                    break;
                case 1: // top
                    btns[i].setCompoundDrawablesWithIntrinsicBounds(null, draws[i],
                            null, null);
                    break;
                case 2: // right
                    btns[i].setCompoundDrawablesWithIntrinsicBounds(null, null,
                            draws[i], null);
                    break;
                case 3: // bottom
                    btns[i].setCompoundDrawablesWithIntrinsicBounds(null, null,
                            null, draws[i]);
                    break;
                case 4:
                    btns[i].setBackgroundDrawable(draws[i]);
                    break;
            }
        }
        if (hasLine)
            findViewById(R.id.lines_layout).setVisibility(View.VISIBLE);
        loadFragment(index, newFrags);
        groupLayout = ((RadioGroup) findViewById(R.id.radio_group));

        groupLayout
                .setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(RadioGroup group, int checkedId) {
                        for (int i = 0; i < btns.length && i < newFrags.length; i++) {
                            if (checkedId == btns[i].getId()) {
                                clickTabCallBack.setClickPos(i);
                                loadFragment(i, newFrags);
                                tabLines[i].setVisibility(View.VISIBLE);
                            } else
                                tabLines[i].setVisibility(View.INVISIBLE);
                        }
                    }
                });
    }

    private ClickTabCallBack clickTabCallBack;
    public void getPosition(ClickTabCallBack clickTabCallBack){
        this.clickTabCallBack=clickTabCallBack;
    }

    public void loadFragment(int isCheckStatus, Fragment[] newFrags) {
        // FragmentTransaction ft = getFragmentManager().beginTransaction();
        // //此为fragment
        android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager()
                .beginTransaction(); // 此为act
//        String ddid = UserInfoUtil.getUserInfo(mContext).ddid;
//    	if(isCheckStatus!=0&&isCheckStatus!=2){
//    		if(TextUtils.isEmpty(ddid)){
//    			Act.toAct(mContext, StartActivity.class);
//    			return;
//    		}
//    	}
        for (int i = 0; i < btns.length && i < newFrags.length; i++) {
            if (null != nullFfags[i]) {
                ft.hide(nullFfags[i]);
            }
            if (i == isCheckStatus) {
                // setTitleStrText(btns[i].getText().toString());
                if (null == nullFfags[isCheckStatus]) {
                    nullFfags[isCheckStatus] = newFrags[isCheckStatus];
                    ft.add(R.id.fragment_frame, nullFfags[isCheckStatus], "");
                } else
                    ft.show(nullFfags[isCheckStatus]);
            }
        }
        ft.commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.title_left_layout:
                finish();
                break;
        }
    }

    /**
     * TODO 设置左按钮,目前设定了固定正方形大小，放文字会有问题，若有文字到时候再说
     *
     * @param resId  资源id。文字的或者图片的
     * @param isText 判断是文字还是图片
     */
    protected void setLeftLayout(int resId, boolean isText) {
        title_left_layout.setVisibility(View.VISIBLE);
        title_left_layout.setOnClickListener(this);
        if (isText)
            title_left_tv.setText(resId);
        else
            title_left_tv.setBackgroundResource(resId);
    }

    /**
     * TODO 设置右按钮
     *
     * @param resId  资源id。文字的或者图片的
     * @param isText 判断是文字还是图片
     */
    protected void setRightLayout(int resId, boolean isText) {
        title_right_layout.setVisibility(View.VISIBLE);
        if (isText)
            title_right_tv.setText(resId);
        else
            title_right_tv.setBackgroundResource(resId);
    }

    /**
     * TODO 设置中间
     *
     * @param resId 资源id。文字
     */
    protected void setTitleResText(int resId) {
        title_center_tv.setVisibility(View.VISIBLE);
        title_center_tv.setText(resId);
    }

    /**
     * 显示toast
     *
     * @param @param msg
     * @return void
     * @Title: showToast
     * @Description: TODO
     */
    protected void showToast(String msg) {
        if (null==myToast)
            myToast=Toast.makeText(mContext, msg, Toast.LENGTH_SHORT);
        myToast.setText(msg);
        myToast.show();
    }

    /**
     * 取消显示toast，可在某个Activity的destoy方法设置
     */
    protected void cancelToast(){
       if (null!=myToast)
           myToast.cancel();
    }
    /**
     * TODO 设置中间
     *
     * @param str 资源id。文字
     */
    protected void setTitleStrText(String str) {
        title_center_tv.setVisibility(View.VISIBLE);
        title_center_tv.setText(str);
    }

    /**
     * 显示进度框
     *
     * @param @param msg
     * @return void
     * @Title: showProgressDialog
     * @Description: TODO
     */
    protected void showProgressDialog(String msg) {
        if (null == progressDialog) {
            progressDialog = new ProgressDialog(mContext);
        }
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(true);
        progressDialog.show();
    }

    /**
     * 显示进度框
     *
     * @param @param msg
     * @return void
     * @Title: showProgressDialog
     * @Description: TODO
     */
    protected void showProgressDialogFalse(String msg) {
        if (null == progressDialog) {
            progressDialog = new ProgressDialog(mContext);
        }
        progressDialog.setMessage(msg);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    /**
     * 关闭进度框
     *
     * @param
     * @return void
     * @Title: dismissProgressDialog
     * @Description: TODO
     */
    protected void dismissProgressDialog() {
        if (null != progressDialog && progressDialog.isShowing()) {
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    /**
     * 开始定位
     *
     * @param
     * @return void
     * @Title: startLocation
     * @Description: TODO
     */
    public void startLocation(MLocationLinsener mLocationLinsener) {
        initMap();
        this.mLocationLinsener = mLocationLinsener;
        mLocationClient.start();

    }

    // /**
    // * 停止定位
    // *
    // * @Title: stopLocation
    // * @Description: TODO
    // * @param
    // * @return void
    // */
    // public void stopLocation() {
    // if (null != mLocationClient) {
    // mLocationClient.stop();
    // }
    // }

    private void InitLocation() {
        LocationClientOption option = new LocationClientOption();
        option.setLocationMode(tempMode);// 设置定位模式
        option.setCoorType(tempcoor);// 返回的定位结果是百度经纬度，默认值gcj02
        int span = 1000;
        option.setScanSpan(span);// 设置发起定位请求的间隔时间为1000ms
        option.setIsNeedAddress(true);
        mLocationClient.setLocOption(option);
    }

    /**
     * 实现实位回调监听
     */
    private class MyLocationListener implements BDLocationListener {

        @Override
        public void onReceiveLocation(BDLocation location) {
            if (null != location) {
                String mAddress = location.getCity();
                if (!TextUtils.isEmpty(mAddress)) {
                    mLocationLinsener.mLocationSucess(mAddress);
                } else {
                    mLocationLinsener.mLocationFaile(getResources().getString(
                            R.string.error_location));
                }
            } else {
                mLocationLinsener.mLocationFaile(getResources().getString(
                        R.string.error_location));
            }

            if (null != mLocationClient) {
                mLocationClient.stop();
            }
        }
    }

    @Override
    protected void onStop() {
        // if (null != mLocationClient) {
        // mLocationClient.stop();
        // }
        super.onStop();
    }

    public static final int CAMERA = 1102;// 手机拍摄
    public static final int GALLEY = 1101;// 手机相册
    public static final int PHOTO_REQUEST_CUT = 1104;// 裁剪之后

    protected static final int RESULT_IMG = 1108;// 返回不裁剪
    protected static final int RESULT_CUT_IMG = 1109;// 返回裁剪

    private Handler handler;
    private boolean isCut = false;
    private int cutW = 200, cutH = 200;
    private Uri cameraUri;
    private boolean isScale = true;
    public ScoreDialog dialog = null;

    /**
     * TODO 以下为调用系统相机代码，目前bug：传true切图，照相的话问题。 TODO
     * 返回值为图片路径，ImageView.setImageBitmap(BitmapFactory.decodeFile(""+msg.obj));
     * TODO 如果需要压缩： Utils.compressImage(""+msg.obj);方法即可
     *
     * @param handler 需要hanlder接收数据:RESULT_IMG/RESULT_CUT_IMG，
     * @param isCut   是否裁剪
     * @param cutW    isCut为真才有效，裁剪宽度
     * @param cutH    裁剪高度
     * @param isScale 是否固定裁剪的比例,true表固定比例，传参cutW和cutH才有效
     */
    protected void showCameraDialog(Handler handler, final boolean isCut,
                                    int cutW, int cutH, boolean isScale) {
        this.handler = handler;
        this.isCut = isCut;
        this.cutW = cutW;
        this.cutH = cutH;
        this.isScale = isScale;
        String sdState = Environment.getExternalStorageState();
        if (!sdState.equals(Environment.MEDIA_MOUNTED)) { // 检测sd是否可用
            ToastUtils.showShort(mContext, "sd卡不存在");
            return;
        }
        dialog = new ScoreDialog(mContext, R.layout.select_camera_dialog,
                R.style.dialog_more_style);
        dialog.setParamsBotton();
        dialog.show();
        dialog.findViewById(R.id.select_camera_dialog_camera)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent camera = new Intent(
                                MediaStore.ACTION_IMAGE_CAPTURE);
                        cameraUri = Utils.createImagePathUri(mContext);
                        camera.putExtra(MediaStore.EXTRA_OUTPUT, cameraUri);
                        startActivityForResult(camera, CAMERA);
                    }
                });
        dialog.findViewById(R.id.select_camera_dialog_album)
                .setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        Intent picture = new Intent(
                                Intent.ACTION_PICK,
                                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(picture, GALLEY);
                    }
                });
        dialog.findViewById(R.id.select_camera_dialog_back).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

        Window dialogWindow = dialog.getWindow();
        dialogWindow.setWindowAnimations(R.style.dialog_more_style);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String picpath = "";
        Message msg = new Message();

        if (requestCode == CAMERA && resultCode == Activity.RESULT_OK
                && null == data) {
            if (isCut) {
                startPhotoZoom(cameraUri);
            } else {
                msg.what = RESULT_IMG;
                msg.obj = Utils.uriToPath(mContext, cameraUri);
                handler.sendMessage(msg);
            }
        } else if (requestCode == GALLEY && resultCode == Activity.RESULT_OK
                && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumns = {MediaStore.Images.Media.DATA};
            Cursor c = mContext.getContentResolver().query(selectedImage,
                    filePathColumns, null, null, null);
            c.moveToFirst();
            int columnIndex = c.getColumnIndex(filePathColumns[0]);
            String picturePath = c.getString(columnIndex);
            picpath = picturePath;
            c.close();

            if (isCut) {
                // startPhotoZoom(Utils.pathToUri(mContext,picpath));
                startPhotoZoom(selectedImage);
            } else {
                msg.what = RESULT_IMG;
                msg.obj = picpath;
                handler.sendMessage(msg);
            }
        } else if (requestCode == PHOTO_REQUEST_CUT
                && resultCode == Activity.RESULT_OK && null != data) {
            Bitmap bmp = data.getExtras().getParcelable("data");
            msg.what = RESULT_CUT_IMG;
            msg.obj = bmp;
            handler.sendMessage(msg);
        }
    }

    private void startPhotoZoom(Uri uri) {
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");
        // crop为true是设置在开启的intent中设置显示的view可以剪裁
        intent.putExtra("crop", "true");
        if (isScale) { // 目前bug，拍照不能自由比例
            // aspectX aspectY 是宽高的比例
            intent.putExtra("aspectX", cutW);
            intent.putExtra("aspectY", cutH);
        }
        // outputX,outputY 是剪裁图片的宽高,不能设置过大
        intent.putExtra("outputX", cutW);
        intent.putExtra("outputY", cutH);

        intent.putExtra("return-data", true);
        startActivityForResult(intent, PHOTO_REQUEST_CUT);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ExitAppUtils.getInstance().delActivity(this);
    }


}
