package com.inwhoop.ddcj.util;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import android.os.Handler;
import android.os.Message;

import com.ant.liao.GifView;

/**
 * @Project: DDCJ
 * @Title: GifViewUtil.java
 * @Package com.inwhoop.ddcj.util
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-23 上午10:32:06
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class GifViewUtil {

	public static void setGifViewNetUrl(final GifView gifView,
			final String strurl) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					Message msg = new Message();
					URL url = new URL(strurl);
					HttpURLConnection connection = (HttpURLConnection) url
							.openConnection();
					connection.setConnectTimeout(10000);
					connection.setReadTimeout(10000);
					connection.setRequestMethod("GET");
					if (connection.getResponseCode() == 200) {
						InputStream is = connection.getInputStream();
						msg.obj = new Object[] { gifView, is };
						handler.sendMessage(msg);
					}
				} catch (Exception e) {
				}
			}
		}).start();
	}

	private static Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			Object[] obj = (Object[]) msg.obj;
			GifView gifView = (GifView) obj[0];
			InputStream is = (InputStream) obj[1];
			gifView.setGifImage(is);
		};
	};

	public static String getKLineUrl(int position, String code) {
		if (code.substring(0, 1).equals("6")) {
			code = "sh" + code;
		} else {
			code = "sz" + code;
		}
		String url = "";
		switch (position) {
		case 0:
			url = "http://image.sinajs.cn/newchart/min/n/" + code;
			break;
		case 1:
			url = "http://image.sinajs.cn/newchart/daily/n/" + code;
			break;
		case 2:
			url = "http://image.sinajs.cn/newchart/weekly/n/" + code;
			break;
		case 3:
			url = "http://image.sinajs.cn/newchart/monthly/n/" + code;
			break;

		default:
			break;
		}
		return url;
	}

}
