package com.inwhoop.ddcj.util;

import com.inwhoop.R;

import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.onekeyshare.OnekeyShare;

import android.content.Context;
import android.text.TextUtils;

/**
 * 分享工具类
 * 
 * @Project: DDCJ
 * @Title: ShareUtil.java
 * @Package com.inwhoop.ddcj.util
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-12-15 下午1:53:05
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class ShareUtil {
	// 使用快捷分享完成分享（请务必仔细阅读位于SDK解压目录下Docs文件夹中OnekeyShare类的JavaDoc）
	public static void showShare(Context context, String Content,
			String imgpath, String url, PlatformActionListener callback) {
		ShareSDK.initSDK(context);
		final OnekeyShare oks = new OnekeyShare();
		// 分享时 Notification 的图标和文字
		oks.setNotification(R.drawable.ic_launcher,
				context.getString(R.string.app_name));
		// address 是接收人地址，仅在信息和邮件使用
//		oks.setAddress("");
		// title 标题，印象笔记、邮箱、信息、微信、人人网和 QQ 空间使用
		oks.setTitle(Content);
		// titleUrl 是标题的网络链接，仅在人人网和 QQ 空间使用
		oks.setTitleUrl(url);
		// text 是分享文本，所有平台都需要这个字段
		oks.setText(Content + "\r\n" + url);

		// imagePath 是图片的本地路径，Linked- In 以外的平台都支持此参数
		if (!TextUtils.isEmpty(imgpath)) {
			oks.setImagePath(imgpath);
			// imageUrl 是图片的网络路径，新浪微博、人人网、QQ 空间、
			// 微信、易信、Linked-In 支持此字段
			oks.setImageUrl(imgpath);
		}
		// 微信、易信中使用，表示视屏地址或网页地址
		oks.setUrl(url);
		// comment 是我对这条分享的评论，仅在人人网和 QQ 空间使用
		oks.setComment(context.getString(R.string.share));
		// site 是分享此内容的网站名称，仅在 QQ 空间使用
		oks.setSite(context.getString(R.string.app_name));
		// siteUrl 是分享此内容的网站地址，仅在 QQ 空间使用
		// oks.setSiteUrl("http://www.inwhoop.com");
//		 oks.setVenueName("Share SDK");
//		 oks.setVenueDescription("This is a beautiful place!");
		// oks.setLatitude(23.056081f);
		// oks.setLongitude(113.385708f);
		// 是否直接分享（true 则直接分享）
		oks.setSilent(false);
		oks.setCallback(callback);
		// 去除注释，可令编辑页面显示为Dialog模式
//		oks.setDialogMode();

		oks.show(context);
	}

}
