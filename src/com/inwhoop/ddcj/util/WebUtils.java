package com.inwhoop.ddcj.util;

import java.io.UnsupportedEncodingException;

import com.inwhoop.ddcj.impl.WebUrlLoadLinsener;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.RenderPriority;
import android.widget.TextView;

/**
 * web工具类
 * 
 * @Project: DDCJ
 * @Title: WebUtils.java
 * @Package com.inwhoop.ddcj.util
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-27 下午7:22:11
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class WebUtils {

	private static WebUrlLoadLinsener mCallback;

	public static void setWebUrlLoadLinsener(WebUrlLoadLinsener callback) {
		mCallback = callback;
	}

	/**
	 * 设置WebView
	 * 
	 * @Title: setWebView
	 * @Description: TODO
	 * @param @param webView
	 * @param @param textView
	 * @return void
	 */
	private static void setWebView(final Context context,
			final WebView webView, TextView textView) {
		webView.setVisibility(View.VISIBLE);
		if (null != textView) {
			textView.setVisibility(View.GONE);
		}
		WebSettings webSettings = webView.getSettings();
		webSettings.setBlockNetworkImage(true);
		webSettings.setRenderPriority(RenderPriority.HIGH);
		webSettings.setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		webView.setWebViewClient(new WebViewClient() {
			@Override
			public void onPageFinished(WebView view, String url) {

				view.getSettings().setBlockNetworkImage(false);

				super.onPageFinished(view, url);

			}

			@Override
			public boolean shouldOverrideUrlLoading(WebView view, String url) {
				// 当有新连接时，使用当前的 WebView
				// view.loadUrl(url);
				String str[] = url.split(":");
				if (str.length < 2) {
					return true;
				}
				if (str[0].startsWith("theme")) {
					mCallback.loadTheme(str[1]);
				} else if (url.startsWith("stockcode")) {
					if (str.length > 2) {
						mCallback.loadStock(str[1], str[2]);
					}
				}
				return true;
			}
		});

	}

	/**
	 * 加载WebView
	 * 
	 * @Title: LoadWeb
	 * @Description: TODO
	 * @param @param webView 加载内容的WebView
	 * @param @param textView 提示消息的textView
	 * @param @param url 需要加载的url
	 * @return void
	 */
	public static void LoadcontentWeb(Context context, WebView webView,
			TextView textView, String content) {
		if (null == content) {
			return;
		}
		setWebView(context, webView, textView);
		try {
			webView.loadDataWithBaseURL(
					"fake://not/needed",
					"<html><head><meta http-equiv='content-type' content='text/html;charset=utf-8'><style type=\"text/css\">img{ width:95%}</style><STYLE TYPE=\"text/css\"> BODY { margin:0; padding: 5px 3px 5px 5px; background-color:#ffffff;} </STYLE><BODY TOPMARGIN=0 rightMargin=0 MARGINWIDTH=0 MARGINHEIGHT=0></head><body>"
							+ new String(content.getBytes("utf-8"))
							+ "</body></html>", "text/html", "utf-8", "");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
	}
}
