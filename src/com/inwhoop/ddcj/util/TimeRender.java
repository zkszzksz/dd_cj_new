package com.inwhoop.ddcj.util;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @Describe: TODO * * * ****** Created by ZK ********
 * @Date: 2014/04/28 18:34
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
@SuppressLint("SimpleDateFormat")
public class TimeRender {

	private static SimpleDateFormat formatBuilder;

	public static String getPointDate() {
		formatBuilder = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		return formatBuilder.format(new Date());
	}

	/**
	 * 
	 * @return 斜线格式
	 */
	public static String getSlantTime() {
		formatBuilder = new SimpleDateFormat("yyyy/M/d H:mm:ss");
		return formatBuilder.format(new Date());
	}

	public static String getNoDayTime() {
		formatBuilder = new SimpleDateFormat("HH:mm:ss");
		return formatBuilder.format(new Date());
	}

	public static String getStandardDate() {
		formatBuilder = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return formatBuilder.format(new Date());
	}

	public static String getStandardDate(long time) {
		formatBuilder = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return formatBuilder.format(new Date(time));
	}

	public static String getStandardDateWithoutSec() {
		formatBuilder = new SimpleDateFormat("yyyy-MM-dd\t\t\tHH:mm");
		return formatBuilder.format(new Date());
	}

	public static long getLongtime(String time) throws ParseException {
		formatBuilder = new SimpleDateFormat("yyyy-MM-dd\t\t\tHH:mm");
		Date date = formatBuilder.parse(time);
		Long longtime = date.getTime() / 1000;
		return longtime;
	}

	public static boolean compareTwotime(String time, String nowtime)
			throws ParseException {
		formatBuilder = new SimpleDateFormat("yyyy-MM-dd\t\t\tHH:mm");
		Date date = formatBuilder.parse(time);
		Date nowdate = formatBuilder.parse(nowtime);
		if (date.getTime() <= nowdate.getTime()) {
			return false;
		}
		return true;
	}

	public static String getStandardDateBylongtime(long time) {
		formatBuilder = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
		return formatBuilder.format(time);
	}

	public static String getStandardH(long time) {
		formatBuilder = new SimpleDateFormat("HH:mm");
		return formatBuilder.format(time);
	}

	public static String getStandardDateriqiBylongtime(long time) {
		formatBuilder = new SimpleDateFormat("yyyy-MM-dd");
		return formatBuilder.format(time);
	}

	public static String[] getTimeStrs(String time) throws ParseException {
		String[] str = new String[3];
		formatBuilder = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date date = formatBuilder.parse(time);
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		str[0] = getWeekByInt(calendar.get(Calendar.DAY_OF_WEEK) - 1);
		SimpleDateFormat sd1 = new SimpleDateFormat("HH:mm");
		str[1] = sd1.format(date);
		SimpleDateFormat sd2 = new SimpleDateFormat("yyyy-MM-dd");
		str[2] = sd2.format(date);
		return str;
	}

	public static String getWeekByInt(int i) {
		String str = "";
		switch (i) {
		case 0:
			str = "周日";
			break;
		case 1:
			str = "周一";
			break;

		case 2:
			str = "周二";
			break;

		case 3:
			str = "周三";
			break;

		case 4:
			str = "周四";
			break;

		case 5:
			str = "周五";
			break;

		case 6:
			str = "周六";
			break;

		default:
			break;
		}
		return str;
	}

}
