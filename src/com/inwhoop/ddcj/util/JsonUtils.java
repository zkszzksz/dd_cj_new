package com.inwhoop.ddcj.util;

import android.net.Uri;
import android.text.TextUtils;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.inwhoop.ddcj.app.Configs.*;

/**
 * @Describe: TODO 网络读取工具类 * * * ****** Created by ZK ********
 * @Date: 2014/10/20 15:58
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */

public class JsonUtils {

    public static SyncHttp http = new SyncHttp();
    public static GsonBuilder gsonBuilder = new GsonBuilder();
    public static Gson gson = gsonBuilder.create();

    /**
     * //TODO 通用post请求
     *
     * @param getStr     请求接口名 ,域名已有
     * @param paramPairs 你存的参数对象
     * @param clazz      需要解析得到的对象，可传null
     * @param <T>        对象必须继承BaseBean
     * @return ｛成功否，提示信息，List<T>}，所以如果是单独一个对象，需要get(0)
     * @throws Exception
     */
    private static <T extends BaseBean> Object[] myGetNetPost(String getStr,
                                                              List<Parameter> paramPairs, Class<T> clazz) throws Exception {

        @SuppressWarnings("static-access")
        String result = http.httpPost(HOST + getStr, paramPairs);
        System.out.println("result=== " + result);
        return getObjects(clazz, result);
    }

    private static <T extends BaseBean> Object[] getNetStr(String getUrl,
                                                           String getStr, Class<T> clazz) throws Exception {
        String result = http.httpGet(HOST + getUrl, "?" + getStr);
        System.out.println("结果数据为" + result);
        return getObjects(clazz, result);
    }

    /**
     * 通用解析
     *
     * @param @param  clazz
     * @param @param  result
     * @param @return
     * @param @throws JSONException
     * @return Object[]
     * @Title: getObjects
     * @Description: TODO
     */
    public static <T extends BaseBean> Object[] getObjects(Class<T> clazz,
                                                           String result) throws JSONException {
        if ("".equals(result)) {
            return new Object[]{true, "", null};
        }
        JSONObject obj = new JSONObject(result);
        int resultcode = obj.getInt("code") / 100;
        if (resultcode == 40) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = "";
        List<T> list = new ArrayList<T>();
        try {
            if (obj.has("msg")) {
                if (obj.get("msg").getClass() == JSONArray.class) {
                    GsonBuilder gsonBuilder = new GsonBuilder();
                    Gson gson = gsonBuilder.create();
                    JSONArray array = obj.getJSONArray("msg");
                    for (int i = 0; i < array.length(); i++) {
                        T bean = gson.fromJson(array.getJSONObject(i)
                                .toString(), clazz);
                        if (!"".equals(bean.img))
                            if (!bean.img.startsWith("http://")) {
                                bean.img = HOST + bean.img;
                            }

                        list.add(bean);
                    }
                } else {
                    msg = Uri.decode(obj.getString("msg"));
                }
            }
        } catch (Exception e) {
            if (list.size() == 0)
                list.add(null);// 避免取值没有size，如果本身是数组，就没必要添加了，返回空数据数组
        }
        if (resultcode == 2) {
            return new Object[]{true, msg, list};
        } else {
            return new Object[]{false, msg, list};
        }
    }

    // TODO 用来遍历对象属性和属性值,添加到 List<Parameter> 但是父对象里的东西就遍历不了了！
    public static List<Parameter> readClassAttr(BaseBean tb) throws Exception {
        Field[] fields = tb.getClass().getDeclaredFields();
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        for (Field field : fields) {
            field.setAccessible(true);
            String name = field.getName();
            String va = field.get(tb).toString();
            if (!TextUtils.isEmpty(va))
                paramPairs.add(new Parameter(name, va));
        }
        return paramPairs;
    }

    // TODO 登录
    public static Object[] loginUser(UserBean bean) throws Exception {
        Object[] obs = myGetNetPost(USER_LOGIN, readClassAttr(bean),
                UserBean.class);
        List list = (List) obs[2];
        if (null == list) {
            list = new ArrayList();
        }
        if (list.size() == 0) {
            list.add(new UserBean());
        }
        return new Object[]{obs[0], obs[1], ((List) obs[2]).get(0)};
    }

    /**
     * 注册
     *
     * @param bean
     * @return Object[]
     * @throws Exception
     * @Title: regist
     * @Description: TODO
     */
    public static Object[] regist(UserBean bean) throws Exception {
        List<Parameter> ppr = readClassAttr(bean);
        ppr.add(new Parameter("img", bean.img));
        Object[] obs = myGetNetPost(USER_REGISTER, ppr, null);
        return new Object[]{obs[0], obs[1]};
    }

    /**
     * 注册时第一步，验证并发送短信
     *
     * @param @param  phone
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: phoneVerfiy
     * @Description: TODO
     */
    public static Object[] phoneVerfiy(UserBean user) throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("tel", user.tel));
        paramPairs.add(new Parameter("email", user.email));
        Object[] obs = myGetNetPost(USER_GETCODE, paramPairs, null);
        return new Object[]{obs[0], obs[1]};
    }

    /**
     * TODO 注册时第2步/忘记时第2步，请求验证码是否正确
     */
    public static Object[] checkyzm(UserBean user) throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("tel", user.tel));
        paramPairs.add(new Parameter("yzm", user.yzm));
        Object[] obs = myGetNetPost(USER_CHECK_YZM, paramPairs, null);
        return new Object[]{obs[0], obs[1]};
    }

    /**
     * 找回密码时，短信验证：
     *
     * @param @param  phone
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: phoneVerfiy
     * @Description: TODO
     */
    public static Object[] fogetPhoneVerfiy(String phone) throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("tel", phone));
        Object[] obs = myGetNetPost(USER_FORGET_GETCODE, paramPairs, null);
        return new Object[]{obs[0], obs[1]};
    }

    /**
     * 修改密码，目前用于找回密码
     *
     * @param bean
     * @return
     * @throws Exception
     */
    public static Object[] userForgetPwd(UserBean bean) throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("tel", bean.tel));
        // paramPairs.add(new Parameter("yzm", verfic));
        paramPairs.add(new Parameter("newpwd", bean.pwd));
        Object[] obs = myGetNetPost(USER_GETPWD, paramPairs, null);
        return new Object[]{obs[0], obs[1]};
    }

    /**
     * 修改密码，设置界面
     *
     * @param bean
     * @return
     * @throws Exception
     */
    public static Object[] userUpdatePwd(UserBean bean, String newPwd)
            throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("userid", bean.userid));
        paramPairs.add(new Parameter("pwd", bean.pwd));
        paramPairs.add(new Parameter("newpwd", newPwd));
        Object[] obs = myGetNetPost(USER_UPDATE_PWD, paramPairs, null);
        return new Object[]{obs[0], obs[1]};
    }

    /**
     * 获取股票详情
     *
     * @param @param  stockCode
     * @param @param  stockcode
     * @param @return
     * @param @throws Exception
     * @return StockInfoBean
     * @Title: getStockInfo
     * @Description: TODO
     */
    public static StockInfoBean getStockInfo(String stockCode) throws Exception {
        StockInfoBean info = null;
        if (stockCode.substring(0, 1).equals("6")) {
            stockCode = "sh" + stockCode;
        } else {
            stockCode = "sz" + stockCode;
        }
        String result = http.httpGet(GET_STOCKINFO + stockCode, "");
        if (!"".equals(result) && result.length() > 20) {
            info = new StockInfoBean();
            String real = result.substring(result.indexOf("\"") + 1,
                    result.lastIndexOf("\""));
            String[] stocks = real.split(",");
            info.stockname = stocks[0];
            DecimalFormat df1 = new DecimalFormat("#.00");
            info.nowDayOP = Double.parseDouble(df1.format(Double
                    .parseDouble(stocks[1])));
            info.yestprice = Double.parseDouble(df1.format(Double
                    .parseDouble(stocks[2])));
            info.nowPrice = Double.parseDouble(df1.format(Double
                    .parseDouble(stocks[3])));
            info.highPrice = Double.parseDouble(df1.format(Double
                    .parseDouble(stocks[4])));
            info.lowPrice = Double.parseDouble(df1.format(Double
                    .parseDouble(stocks[5])));
            info.addPrice = Double.parseDouble(df1
                    .format((info.nowPrice - info.yestprice)));
            info.swing = Double
                    .parseDouble(df1
                            .format(((info.highPrice - info.lowPrice) / info.yestprice) * 100));
            // DecimalFormat df = new DecimalFormat("#.00");
            info.addRate = Double.parseDouble(df1.format(info.addPrice
                    / info.yestprice * 100));
            info.volume = Double.parseDouble(df1.format(Double
                    .parseDouble(stocks[8]) / 1000000));
            info.total = Double.valueOf(stocks[9]) / 10000;
            DecimalFormat df = new DecimalFormat("#.00");
            info.total = Double.valueOf(df.format(info.total));
        }
        return info;
    }

    /**
     * 获取股票详情列表
     *
     * @param @param  stockCode 批量需要以逗号分开
     * @param @return
     * @param @throws Exception
     * @return StockInfoBean
     * @Title: getStockInfo
     * @Description: TODO
     */
    public static List<StockInfoBean> getStockInfoList(String stockCode)
            throws Exception {
        @SuppressWarnings("static-access")
        String[] str = stockCode.split(",");
        for (int i = 0; i < str.length; i++) {
            if (str[i].substring(0, 1).equals("6")) {
                str[i] = "sh" + str[i];
            } else {
                str[i] = "sz" + str[i];
            }
        }
        String real = "";
        for (int i = 0; i < str.length; i++) {
            if (i == (str.length - 1)) {
                real = real + str[i];
            } else {
                real = real + str[i] + ",";
            }
        }
        String result = http.httpGet(GET_STOCKINFO + real, "");
        return getStockInfoBeans(result);
    }

    public static List<StockInfoBean> getStockInfoBeans(String result)
            throws Exception {
        List<StockInfoBean> list = new ArrayList<StockInfoBean>();
        StockInfoBean info;
        if (!"".equals(result) && result.length() > 20) {
            result = result.substring(0, result.lastIndexOf("\n"));
            String[] strs = result.split(";");
            for (int i = 0; i < strs.length; i++) {
                info = new StockInfoBean();
                String real = strs[i].substring(strs[i].indexOf("\"") + 1,
                        strs[i].lastIndexOf("\""));
                if (!"".equals(real)) {
                    String[] stocks = real.split(",");
                    info.stockname = Uri.decode(stocks[0]);
                    DecimalFormat df1 = new DecimalFormat("#.00");
                    info.nowDayOP = Double.parseDouble(df1.format(Double
                            .parseDouble(stocks[1])));
                    info.yestprice = Double.parseDouble(df1.format(Double
                            .parseDouble(stocks[2])));
                    info.nowPrice = Double.parseDouble(df1.format(Double
                            .parseDouble(stocks[3])));
                    info.highPrice = Double.parseDouble(df1.format(Double
                            .parseDouble(stocks[4])));
                    info.lowPrice = Double.parseDouble(df1.format(Double
                            .parseDouble(stocks[5])));
                    info.addPrice = Double.parseDouble(df1.format(info.nowPrice
                            - info.yestprice));
                    info.swing = Double
                            .parseDouble(df1
                                    .format(((info.highPrice - info.lowPrice) / info.yestprice) * 100));
                    // DecimalFormat df = new DecimalFormat("#.0000");
                    info.addRate = Double.parseDouble(df1.format(info.addPrice
                            / info.yestprice * 100));
                    info.volume = Double.parseDouble(df1.format(Double
                            .parseDouble(stocks[8]) / 1000000));
                    info.total = Double.valueOf(stocks[9]) / 10000;
                    DecimalFormat df = new DecimalFormat("#.00");
                    info.total = Double.valueOf(df.format(info.total));
                    list.add(info);
                } else {
                    info.stockname = "未知股票";
                    list.add(info);
                }
            }
        }
        return list;
    }

    // TODO 获取服务器上我的自选股
    public static Object[] getNetMyStock(String uid, String touserid)
            throws Exception {
        Object[] obs = null;
        if ("".equals(touserid)) {
            obs = getNetStr(GET_MY_STOCK_LIST, "uid=" + uid,
                    StockInfoBean.class);
        } else {
            obs = getNetStr(GET_MY_STOCK_LIST, "uid=" + uid + "&touid="
                    + touserid, StockInfoBean.class);
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 搜索数据
     *
     * @param @param  uid
     * @param @param  type
     * @param @param  code
     * @param @return
     * @param @throws Exception
     * @return List<SearchStock>
     * @Title: getSearchMyStock
     * @Description: TODO
     */
    public static List<SearchStock> getSearchMyStock(String uid, String type,
                                                     String code) throws Exception {
        List<SearchStock> list = new ArrayList<SearchStock>();
        String flag = "/anonymous/serach";
        // if (uid.equals("0")) {
        // flag = "";
        /*
         * } else { flag = Configs.SEARCH_STOCK; }
		 */
        String result = http.httpGet(HOST + flag, "?uid=" + uid + "&type="
                + type + "&code=" + code);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        JSONArray jsonArray = jsonObject.getJSONArray("msg");
        for (int i = 0; i < jsonArray.length(); i++) {
            SearchStock searchStock = new SearchStock();
            JSONObject temp = jsonArray.getJSONObject(i);
            searchStock.stockcode = temp.getString("stockcode");
            searchStock.stockname = temp.getString("stockname");
            searchStock.aliasname = temp.getString("aliasname");
            searchStock.count = temp.getString("count");
            searchStock.focus = temp.getString("foucs");
            list.add(searchStock);
        }
        return list;
		/*
		 * Object[] objs = getNetStr(Configs.GET_MY_STOCK_LIST, "uid=" + uid +
		 * "&type=" + type + "&code=" + code, SearchStock.class); return
		 * (List<SearchStock>) objs[2];
		 */
    }

    /**
     * 发现-邀请函-邀请人列表界面
     *
     * @param userid
     * @return
     * @throws Exception
     * @author ZOUXU
     */
    public static Object[] getInvitationListAboutInviter(String userid,
                                                         String ivtitationId) throws Exception {
        String result = http.httpGet(HOST + INVITATIONUSERLIST, "?uid="
                + userid + "&id=" + ivtitationId);
        JSONObject obj = new JSONObject(result);
        int resultcode = obj.getInt("code") / 100;
        int resultcode1 = obj.getInt("code");
        if (resultcode1 == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = "";
        List<InviterListContactBean> list = new ArrayList<InviterListContactBean>();
        try {
            msg = Uri.decode(obj.getString("msg"));
            if (obj.has("msg")) {
                JSONArray array = obj.getJSONArray("msg");
                int length = array.length();
                for (int i = 0; i < length; i++) {
                    JSONObject j = array.getJSONObject(i);
                    InviterListContactBean bean = new InviterListContactBean();
                    bean.userId = j.getString("userid");
                    bean.imgUrl = j.getString("img");
                    bean.name = j.getString("name");
                    bean.isFriend = j.getInt("friend");
                    bean.maybeknow = j.getInt("mybekonw");
                    bean.location = j.getString("location");
                    // 判断服务器位置数据是否为NULL
                    if (bean.location.equalsIgnoreCase("null"))
                        bean.location = "";
                    bean.tel = j.getString("tel");
                    bean.attentionState = j.getInt("attention");
                    list.add(bean);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (resultcode == 2) {
            return new Object[]{true, msg, list};
        } else {
            return new Object[]{false, msg, list};
        }
    }

    /**
     * 添加自选股
     *
     * @param @param  uid
     * @param @param  code
     * @param @param  stockname
     * @param @param  aliasname
     * @param @param  location
     * @param @throws Exception
     * @return Object[]
     * @Title: addStock
     * @Description: TODO
     */
    public static Object[] addStock(String uid, String code, String stockname,
                                    String aliasname, String location, TextView textView,
                                    ImageView imageView, String type, int position) throws Exception {
        Object[] obs = getNetStr(ADD_STOCK, "uid=" + uid + "&stockcode=" + code
                + "&stockname=" + stockname + "&aliasname=" + aliasname
                + "&location=" + location, BaseBean.class);
        return new Object[]{obs[0], obs[1], textView, imageView, type,
                position};
    }

    /**
     * 取消自选股
     *
     * @param @param  uid
     * @param @param  code
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: cancelStock
     * @Description: TODO
     */
    public static Object[] cancelStock(String uid, String code,
                                       TextView textView, ImageView imageView, String type, int position)
            throws Exception {
        Object[] obs = getNetStr(CANCEL_STOCK, "uid=" + uid + "&stockcode="
                + code, BaseBean.class);
        return new Object[]{obs[0], obs[1], textView, imageView, type,
                position};
    }

    /**
     * 获取用户频道列表
     *
     * @param @param  uid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getUsersChannelList
     * @Description: TODO
     */
    public static Object[] getUsersChannelList(String uid) throws Exception {
        Object[] obs = getNetStr(GET_CHANNEL_LIST, "uid=" + uid,
                ChannelInfo.class);// GET_CHANNEL_LIST 错误
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // /**
    // * 获取所有频道
    // *
    // * @param uid
    // * @return
    // * @throws Exception
    // */
    // public static ArrayList<ChannelItem> getAllChannels(String uid)
    // throws Exception {
    // String result = http.httpGet(HOST + GET_CHANNEL_LIST, "?" + "uid="
    // + uid);
    // JSONObject obj = new JSONObject(result);
    // int resultcode = obj.getInt("code") / 100;
    // int resultcode1 = obj.getInt("code");
    // if (resultcode1 == 4001) {
    // Configs.getApp().loginout();
    // return null;
    // }
    // ArrayList<ChannelItem> itmes = new ArrayList<ChannelItem>();
    // try {
    // if (obj.has("msg")) {
    // JSONArray jArray = obj.getJSONArray("msg");
    // if (jArray.length() > 0) {
    // JSONObject jObject = (JSONObject) jArray.get(0);
    // JSONArray array = jObject.getJSONArray("alllist");
    // for (int i = 0; i < array.length(); i++) {
    // JSONObject jsonObject = (JSONObject) array.get(i);
    // ChannelItem item = new ChannelItem();
    // item.setId(Integer.valueOf(jsonObject.getString("id")));
    // item.setName(jsonObject.getString("name"));
    // item.setSelected(jsonObject.getInt("foucs"));
    // // 默认第一个为：推荐序号为1
    // item.setOrderId(i + 1 + 1);
    // itmes.add(item);
    // }
    // }
    // }
    // } catch (Exception e) {
    // return null;
    // }
    // if (resultcode == 2) {
    // return itmes;
    // } else {
    // return null;
    // }
    // }

    /**
     * 添加用户频道列表,用户关注频道接口（可批量添加）
     *
     * @param @param  uid
     * @param @param  channelid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: addUserChannel
     * @Description: TODO
     */
    public static Object[] addUserChannel(String uid, String channelid)
            throws Exception {
        Object[] obs = getNetStr(ADD_USER_CHANNEL, "uid=" + uid + "&channelid="
                + channelid, null);
        return new Object[]{obs[0], obs[1]};
    }

    /**
     * 删除用户频道列表
     *
     * @param @param  uid
     * @param @param  channelid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: delUserChannel
     * @Description: TODO
     */
    public static Object[] delUserChannel(String uid, String channelid)
            throws Exception {
        Object[] obs = getNetStr(CANCLE_USER_CHANNEL, "uid=" + uid
                + "&channelid=" + channelid, null);
        return new Object[]{obs[0], obs[1]};
    }

    /**
     * <<<<<<< HEAD ======= 更新设置界面-隐身模式设置
     *
     * @param uid
     * @param state
     * @return 大小为2的数组
     * @throws Exception
     */
    public static Object[] setStealthMode(String uid, int state)
            throws Exception {
        Object[] obs = getNetStr(SETTING_UPDATE_STEALTH_MODE, "userid=" + uid
                + "&state=" + state, null);
        return new Object[]{obs[0], obs[1]};
    }

    /*
     * >>>>>>> fd174d9cf9c8bc1b03e56be8ad034de4660e7ee8 上传排序数据接口
     *
     * @Title: loadOrderList
     *
     * @Description: TODO
     *
     * @param @param uid
     *
     * @param @param listdata
     *
     * @param @throws Exception
     *
     * @return boolean
     */
    public static boolean loadOrderList(String uid, String listdata)
            throws Exception {
        Response response = null;
        String result = http.httpGet(HOST + ORDER_LIST, "?uid=" + uid
                + "&listdata=" + listdata);
        response = gson.fromJson(result, Response.class);
        if (response.code == 200) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 批量删除
     *
     * @param @param  uid
     * @param @param  listdata
     * @param @return
     * @param @throws Exception
     * @return boolean
     * @Title: deleteList
     * @Description: TODO
     */
    public static boolean deleteList(String uid, String listdata)
            throws Exception {
        Response response = null;
        String result = http.httpGet(HOST + DELETE_LIST, "?uid=" + uid
                + "&listdata=" + listdata);
        response = gson.fromJson(result, Response.class);
        if (response.code == 200) {
            return true;
        } else {
            return false;
        }
    }

    // TODO 登录界面已有账户绑定 第三方
    // 需要 type，tel，openid，pwd，udid
    public static Object[] bindUser(UserBean bean) throws Exception {
        Object[] obs = myGetNetPost(BIND_USER, readClassAttr(bean),
                UserBean.class);
        return new Object[]{obs[0], obs[1], ((List) obs[2]).get(0)};
    }

    // TODO 登录后已有账户绑定/取消绑定 第三方
    // 需要 type，tel，openid 。若取消绑定，传空字符串
    public static Object[] loginBindUser(UserBean bean) throws Exception {
        Object[] obs = myGetNetPost(LOGIN_BIND_USER, readClassAttr(bean),
                UserBean.class);
        return new Object[]{obs[0], obs[1], ((List) obs[2]).get(0)};
    }

    // public static void loadOrderList(String uid, String listdata)
    // throws Exception {
    // String result = http.httpGet(Configs.HOST + Configs.ORDER_LIST, "?uid="
    // + uid + "&listdata=" + listdata);
    // System.out.println(result);
    // }

    /**
     * list转json
     *
     * @param <T>
     * @param @param  listData
     * @param @return
     * @return String
     * @Title: listToJson
     * @Description: TODO
     */
    public static <T> String listToJson(List<T> listData) {
        String result = "";
        if (null != listData) {
            result = gson.toJson(listData);
        }
        return result;
    }

    /*
     * 用户通过传入用户的Id，获取当前用户关注的所有频道下的资讯Id，需要分页返回 uid 用户ID page 页数 num 每一页的条数
     */
    public static List<ChannleIdsBean> getChannelChildIdsList(String uid,
                                                              String page, String num) throws Exception {
        List<ChannleIdsBean> list = new ArrayList<ChannleIdsBean>();
        String result = http.httpGet(HOST + CHANNEL_INFO_ID, "?uid=" + uid
                + "&id=" + page + "&num=" + num);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        JSONArray jsonArray = jsonObject.getJSONArray("msg");
        for (int i = 0; i < jsonArray.length(); i++) {
            ChannleIdsBean channleIdsBean = new ChannleIdsBean();
            JSONObject temp = jsonArray.getJSONObject(i);
            channleIdsBean.id = temp.getString("id");
            channleIdsBean.channelid = temp.getString("channelid");
            channleIdsBean.userid = temp.getString("userid");
            channleIdsBean.channelname = temp.getString("channelname");
            channleIdsBean.count = temp.getString("count");
            list.add(channleIdsBean);
        }
        return list;
    }

    // TODO 获取当前用户关注的所有频道下的资讯Id(同上)
    public static Object[] getChannelChildIdsList(String uid, int page, int num)
            throws Exception {
        Object[] obs = getNetStr(GET_USER_CHANNEL, "uid=" + uid + "&id=" + page
                + "&num=" + num, ChannleIdsBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 查看用户追踪股票
    public static Object[] myFocusStock(String uid) throws Exception {
        Object[] obs = getNetStr(MY_FOCUS_STOCK, "uid=" + uid,
                StockInfoBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 添加用户追踪股票
    public static Object[] addFocusStock(String uid, String stockcode)
            throws Exception {
        Object[] obs = getNetStr(ADD_FOCUS_STOCK, "uid=" + uid + "&stockcode="
                + stockcode, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 删除用户追踪股票
    public static Object[] delFoucsStock(String uid, String stockcode)
            throws Exception {
        Object[] obs = getNetStr(DEL_FOUCS_STOCK, "uid=" + uid + "&stockcode="
                + stockcode, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 排行榜分类查询
    public static Object[] whatRankingList(String categid) throws Exception {
        Object[] obs = getNetStr(WHAT_RANKING_LIST, "categid=" + categid,
                GroupBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 新闻总条数
    public static Object[] newsCount(String uid) throws Exception {
        Object[] obs = getNetStr(NEWS_COUNT, "uid=" + uid, BaseBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 根据地理位置查询新闻,loaction=成都，此接口不传用户id，写0或者不传
    public static Object[] searchNewsForLocation(String userid,
                                                 String location, int id, int num, int type) throws Exception {
        Object[] obs = null;
        if (id == -1) {
            obs = getNetStr(SEARCH_NEWS_FOR_LOCATION, "uid=" + userid
                            + "&city=" + location + "&num=" + num + "&type=" + type,
                    NewsBean.class);
        } else {
            obs = getNetStr(SEARCH_NEWS_FOR_LOCATION, "uid=" + userid
                    + "&city=" + location + "&id=" + id + "&num=" + num
                    + "&type=" + type, NewsBean.class);
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    public static Object[] searchNews(String userid, int id, int num, int type)
            throws Exception {
        Object[] obs = null;
        if (id == -1) {
            obs = getNetStr(SEARCH_NEWS_FOR_LOCATION, "uid=" + userid + "&num="
                    + num + "&type=" + type, NewsBean.class);
        } else {
            obs = getNetStr(SEARCH_NEWS_FOR_LOCATION, "uid=" + userid + "&id="
                    + id + "&num=" + num + "&type=" + type, NewsBean.class);
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 根据stockcode查询新闻
    public static Object[] newsInfos(String uid, String stockcode, int page,
                                     int num) throws Exception {
        Object[] obs = getNetStr(NEWS_INFO, "uid=" + uid + "&stockcode="
                + stockcode + "&id=" + page + "&num=" + num, NewsBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 根据uid查询对我的评论
    public static Object[] searchMyComment(String uid, int page, int num)
            throws Exception {
        Object[] obs = getNetStr(SEARCH_MY_COMMENT, "uid=" + uid + "&id="
                + page + "&num=" + num, CommentBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 通过 subcategid 去查询 relateid .只返回relateid和type
    // 比如 优选列表,点单独一项进去后的列表的查询
    public static Object[] subcateginfo(String uid, String subcategid,
                                        int page, int num) throws Exception {
        Object[] obs = getNetStr(SUBCATEG_INFO, "uid=" + uid + "&subcategid="
                        + subcategid + "&id=" + page + "&num=" + num,
                SubcategBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 往personnews 插入新闻数据
    public static Object[] insertPersonNews(NetInsertPersonNewsBean bean)
            throws Exception {
        Object[] obs = myGetNetPost(INSERT_PERSON_NEWS, readClassAttr(bean),
                null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 修改个人信息接口
    public static Object[] updateUser(UserBean userBean) throws Exception {
        List<Parameter> ppr = readClassAttr(userBean);
        ppr.add(new Parameter("img", userBean.img));
        Object[] obs = myGetNetPost(UPDATE_USER, ppr, UserBean.class);
        return new Object[]{obs[0], obs[1], ((List) obs[2]).get(0)};
    }

    // TODO 点赞接口
    public static Object[] dianZan(String userid, String newsid)
            throws Exception {
        Object[] obs = getNetStr(DIAN_ZAN, "userid=" + userid + "&newsid="
                + newsid, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 用户跟帖接口
    public static Object[] genTie(NetInvitationBean bean) throws Exception {
        Object[] obs = myGetNetPost(THREAD_MSG, readClassAttr(bean), null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 用户发帖接口
    public static Object[] sendTie(NetInvitationBean bean) throws Exception {
        Object[] obs = myGetNetPost(SEND_MSG, readClassAttr(bean), null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 收藏资讯接口
    public static Object[] collectNews(String userid, String newsid)
            throws Exception {
        Object[] obs = getNetStr(COLLECT_NEWS, "userid=" + userid + "&newsid="
                + newsid, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    // TODO 信息转发接口
    public static Object[] forwardMsg(String userid, String newsid)
            throws Exception {
        Object[] obs = getNetStr(FORWARD_MSG, "userid=" + userid + "&newsid="
                + newsid, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 获取评论列表 ,得到某天资讯/新闻 的评论列表
     *
     * @param userid
     * @param channelid 频道id
     * @param num
     * @param page
     * @return
     * @throws Exception
     */
    public static Object[] getNewsComment(String userid, String channelid,
                                          int num, int page) throws Exception {
        Object[] obs = getNetStr(COMMENT_LIST, "userid=" + userid
                        + "&channelid=" + channelid + "&num=" + num + "&id=" + page,
                CommentBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 获取资讯列表
     *
     * @param userid
     * @param channelid
     * @param num
     * @param page
     * @return
     * @throws Exception
     */
    public static Object[] getNewsList(int position, String userid,
                                       String channelid, int num, int page) throws Exception {
        Object[] obs = null;
        if (position == 0) {
            obs = getNetStr(Configs.TOP_NEWS, "uid=" + userid + "&num=" + num
                    + "&id=" + page, ChannelList.class);
        } else {
            obs = getNetStr(Configs.NEWS_LIST, "uid=" + userid + "&channelid="
                            + channelid + "&num=" + num + "&id=" + page,
                    ChannelList.class);
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 获取具体的新闻/资讯详情
     *
     * @param uid
     * @param channelid
     * @param newsid
     * @return
     * @throws Exception
     */
    public static Object[] getNewsContent(String uid, String channelid,
                                          String newsid) throws Exception {
        Object[] obs = getNetStr(NEWS_CONTENT, "uid=" + uid + "&channelid="
                + channelid + "&newsid=" + newsid, NewsContentBean.class);
        return new Object[]{obs[0], obs[1], ((List) obs[2]).get(0)};
    }

    public static Object[] getUserFriendsList(String userid) throws Exception {
        Object[] obs = getNetStr(USER_FRIENDS_LIST, "userid=" + userid,
                BaseBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 我的关注接口
     *
     * @param userid
     * @return
     * @throws Exception
     */
    public static List<ContactsInfo> myConcern(String userid, int num, int page)
            throws Exception {
        String result = http.httpGet(HOST + MY_CONCERN, "?" + "userid="
                + userid + "&num=" + num + "&page=" + page);
        System.out.println("我关注的===" + HOST + MY_CONCERN + "?" + "userid="
                + userid + "&num=" + num + "&page=" + page);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        List<ContactsInfo> list = new ArrayList<ContactsInfo>();
        if (null != msg && !msg.equals("")) {
            JSONArray jsonArray = jsonObject.getJSONArray("msg");
            for (int i = 0; i < jsonArray.length(); i++) {
                ContactsInfo contactsInfo = new ContactsInfo();
                JSONObject temp = jsonArray.getJSONObject(i);
                contactsInfo.userid = temp.getString("userid");
                contactsInfo.name = temp.getString("name");
                contactsInfo.otherinfo = temp.getString("otherinfo");
                contactsInfo.img = temp.getString("img");
                contactsInfo.location = temp.getString("location");
                contactsInfo.state = temp.getString("state");
                contactsInfo.count = temp.getString("count");
                list.add(contactsInfo);
            }
        }
        return list;
    }

    /**
     * 关注我的接口
     *
     * @param userid
     * @return
     * @throws Exception
     */
    public static List<ContactsInfo> MyFance(String userid, int num, int page)
            throws Exception {
        String result = http.httpGet(HOST + MY_FANCE, "?" + "userid=" + userid
                + "&num=" + num + "&page=" + page);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        List<ContactsInfo> list = new ArrayList<ContactsInfo>();
        if (null != msg && !msg.equals("")) {
            JSONArray jsonArray = jsonObject.getJSONArray("msg");
            for (int i = 0; i < jsonArray.length(); i++) {
                ContactsInfo contactsInfo = new ContactsInfo();
                JSONObject temp = jsonArray.getJSONObject(i);
                contactsInfo.userid = temp.getString("userid");
                contactsInfo.name = temp.getString("name");
                contactsInfo.otherinfo = temp.getString("otherinfo");
                contactsInfo.img = temp.getString("img");
                contactsInfo.location = temp.getString("location");
                contactsInfo.tel = temp.getString("tel");
                contactsInfo.state = temp.getString("state");
                contactsInfo.count = temp.getString("count");
                list.add(contactsInfo);
            }
        }
        return list;
    }

    /**
     * 编辑情报
     *
     * @param userid
     * @param
     * @return
     * @throws Exception
     */
    public static Object[] editQibao(String userid, String title,
                                     String zipfile, String type, String content, String location,
                                     String stockcode, String zipname, String channelid)
            throws Exception {
        List<Parameter> params = new ArrayList<Parameter>();
        params.add(new Parameter("uid", "" + userid));
        params.add(new Parameter("title", title));
        params.add(new Parameter("type", type));
        params.add(new Parameter("content", content));
        params.add(new Parameter("location", location));
        params.add(new Parameter("stockcode", stockcode));
        params.add(new Parameter("zipname", zipname));
        if (!"".equals(channelid)) {
            params.add(new Parameter("channelid", channelid));
        }
        String result = new SyncHttp().postfile(HOST + EDIT_QIBAO, zipfile,
                params);
        Object[] obj = getObjects(null, result);
        return new Object[]{obj[0], obj[1], obj[2]};
    }

    /**
     * 联系人好友列表、财迷、推荐（3个界面公用）接口
     *
     * @param userid
     * @return
     * @throws Exception
     */
    public static Object[] getUserFriendsList(String reqUrl, String userid)
            throws Exception {
        // Object[] obs = getNetStr(Configs.USER_FRIENDS_LIST, "userid=" +
        // userid,
        // FriendBean.class);
        String result = http.httpGet(HOST + reqUrl, "?userid=" + userid);
        JSONObject obj = new JSONObject(result);
        int resultcode = obj.getInt("code") / 100;
        int resultcode1 = obj.getInt("code");
        if (resultcode1 == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = "";
        List<ContactsInfo> list = new ArrayList<ContactsInfo>();
        try {
            msg = Uri.decode(obj.getString("msg"));
            if (obj.has("msg")) {
                JSONArray array = obj.getJSONArray("msg");
                int length = array.length();
                for (int i = 0; i < length; i++) {
                    JSONObject j = array.getJSONObject(i);
                    ContactsInfo bean = new ContactsInfo();
                    bean.userid = j.getString("userid");
                    bean.img = j.getString("img");
                    bean.name = j.getString("name");
                    bean.tel = j.getString("tel");
                    bean.otherinfo = j.getString("otherinfo");
                    bean.location = j.getString("location");
                    // 用户状态
                    bean.state = j.getString("state");
                    if (TextUtils.isDigitsOnly(bean.state)) {
                        // 几天前或者几周前
                        bean.state = TimeUtils.twoDateDistanceToday(bean.state,
                                new Date().getTime() + "");
                    }
                    // 用户状态
                    // bean.state = j.getString("state");
                    // if(TextUtils.isDigitsOnly(bean.state)){
                    // //几天前或者几周前
                    // bean.state=TimeUtils.twoDateDistanceToday(bean.state, new
                    // Date().getTime()+"");
                    // }
                    list.add(bean);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            if (list.size() == 0)
                list.add(null);// 避免取值没有size，如果本身是数组，就没必要添加了，返回空数据数组
        }
        if (resultcode == 2) {
            return new Object[]{true, msg, list};
        } else {
            return new Object[]{false, msg, list};
        }
    }

    /**
     * 关注好友接口
     *
     * @param userid
     * @param touserid
     * @param source   需要传的以什么方式关注这个人的字符串。如："来自群"、
     * @return
     * @throws Exception
     */
    public static Object[] attentionUser(String userid, String touserid,
                                         String source) throws Exception {
        System.out.println("家群组关注" + Configs.HOST + ATTENTION_USER + "userid="
                + userid + "&touserid=" + touserid + "&source=" + source);
        Object[] obs = getNetStr(ATTENTION_USER, "userid=" + userid
                + "&touserid=" + touserid + "&source=" + source, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 取消关注好友接口
     *
     * @param userid
     * @param touserid
     * @return
     * @throws Exception
     */
    public static Object[] cancelAttentio(String userid, String touserid)
            throws Exception {
        Object[] obs = getNetStr(CANCEL_ATTENTION, "userid=" + userid
                + "&touserid=" + touserid, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 获取资讯评论列表
     *
     * @param @param userid
     * @param @param channelid
     * @param @param num
     * @param @param page
     * @return Object[]
     * @Title: getNewsCommentList
     * @Description: TODO
     */
    public static Object[] getNewsCommentList(String userid, String newsid,
                                              int num, int page) throws Exception {
        Object[] obs = getNetStr(NEWS_COMMENT, "uid=" + userid + "&newsid="
                + newsid + "&num=" + num + "&id=" + page, CommentInfo.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 反馈建议
     *
     * @param uid
     * @param content 上传的内容
     * @return
     */
    public static Object[] netSuggest(String uid, String content)
            throws Exception {
        Object[] obs = getNetStr(SET_SUGGEST, "uid=" + uid + "&content="
                + content, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    public static Object[] getInvitationlist(String userid, int type, int num,
                                             int id, boolean isf, String city) throws Exception {
        Object[] obs = null;
        if (isf) {
            if ("".equals(city)) {
                obs = getNetStr(INVATATION_LIST, "uid=" + userid + "&num="
                        + num + "&type=" + type, InvitationListInfo.class);
            } else {
                obs = getNetStr(INVATATION_LIST, "uid=" + userid + "&num="
                                + num + "&type=" + type + "&city=" + city,
                        InvitationListInfo.class);
            }
        } else {
            if ("".equals(city)) {
                obs = getNetStr(INVATATION_LIST, "uid=" + userid + "&num="
                                + num + "&type=" + type + "&id=" + id,
                        InvitationListInfo.class);
            } else {
                obs = getNetStr(INVATATION_LIST,
                        "uid=" + userid + "&num=" + num + "&type=" + type
                                + "&id=" + id + "&city=" + city,
                        InvitationListInfo.class);
            }
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 邀请函报名
     *
     * @param @param  userid 用户id
     * @param @param  id 邀请函id
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: baomingToInvitation
     * @Description: TODO
     */
    public static Object[] baomingToInvitation(String userid, int id)
            throws Exception {
        Object[] obs = getNetStr(BAOMING_INVITATION, "uid=" + userid + "&id="
                + id, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 用户订阅频道内容或取消频道订阅
     *
     * @param @param userid
     * @param @param channelid
     * @param @param isOrder 订阅还是取消
     * @Title: orderChannel
     * @Description: TODO
     */
    public static Object[] orderChannel(String userid, String channelid,
                                        boolean isOrder) throws Exception {
        Object[] obs = null;
        String url = "";
        if (isOrder) {
            url = ORDER_CHANNEL;
        } else {
            url = CANCLE_ORDER_CHANNEL;
        }
        obs = getNetStr(url, "uid=" + userid + "&channelid=" + channelid,
                InvitationListInfo.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 邀请函详情
     *
     * @param uid 用户id
     * @param id  邀请函的id
     * @Title: getInvitationInfo
     * @Description: TODO
     */
    public static Object[] getInvitationInfo(String uid, int id)
            throws Exception {
        SyncHttp http = new SyncHttp();
        String result = http.httpGet(HOST + INVITATIONINFO, "?" + "id=" + id
                + "&uid=" + uid);
        JSONObject obj = new JSONObject(result);
        int resultcode = obj.getInt("code") / 100;
        int resultcode1 = obj.getInt("code");
        if (resultcode1 == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = "";
        List<InvitationInfo> list = new ArrayList<InvitationInfo>();
        try {
            msg = Uri.decode(obj.getString("msg"));
            if (obj.has("msg")) {
                GsonBuilder gsonBuilder = new GsonBuilder();
                Gson gson = gsonBuilder.create();
                JSONArray array = obj.getJSONArray("msg");
                InvitationInfo bean = null;
                JSONObject joj = null;
                for (int i = 0; i < array.length(); i++) {
                    joj = array.getJSONObject(i);
                    bean = new InvitationInfo();
                    bean.id = id;
                    bean.title = joj.getString("title");
                    bean.content = joj.getString("content");
                    bean.location = joj.getString("location");
                    bean.begintime = joj.getString("begintime");
                    bean.commentcount = joj.getInt("commentcount");
                    bean.ifinvitation = joj.getInt("ifinvitation");
                    bean.createuserid = joj.getString("createuserid");
                    JSONArray ar = joj.getJSONArray("img");
                    for (int j = 0; j < ar.length(); j++) {
                        bean.img.add(HOST + "/" + ar.getString(j).toString());
                    }
                    JSONArray uja = joj.getJSONArray("user");
                    for (int j = 0; j < uja.length(); j++) {
                        InvitationUserlist userBean = gson.fromJson(uja
                                        .getJSONObject(j).toString(),
                                InvitationUserlist.class);
                        if (!"".equals(userBean.img))
                            userBean.img = HOST + "/" + userBean.img;
                        bean.user.add(userBean);
                    }
                    list.add(bean);
                }
            }
        } catch (Exception e) {
            if (list.size() == 0)
                list.add(null);// 避免取值没有size，如果本身是数组，就没必要添加了，返回空数据数组
        }
        if (resultcode == 2) {
            return new Object[]{true, msg, list};
        } else {
            return new Object[]{false, msg, list};
        }
    }

    /**
     * 邀请函发送评论
     *
     * @param @param  userid 用户uid
     * @param @param  id 邀请函id
     * @param @param  content 邀请函内容
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: sendInvitationComment
     * @Description: TODO
     */
    public static Object[] sendInvitationComment(String userid, int id,
                                                 String content) throws Exception {
        Object[] obs = getNetStr(INVITATION_SENDCOMMENT, "uid=" + userid
                + "&id=" + id + "&content=" + content, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * @param id 搜索的用户或群组的ID
     * @return
     * @throws Exception
     */

    public static FriendMsgBean searchFriend(String id)
            throws Exception {
        String result = http.httpGet(HOST + SEARCH_FRIEND, "?" + "id=" + id);
        System.out.println("sssss=== " + result);
        FriendMsgBean bean=new FriendMsgBean();
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        List list = null;
        if (null != msg && !msg.equals("") && msg.contains("[") && !msg.equals("[]")) {
            // JSONArray jsonArray = jsonObject.getJSONArray("msg");
            JSONArray jsonArray = new JSONArray(msg);
            list = new ArrayList<SearchFriendBean>();
            for (int i = 0; i < jsonArray.length(); i++) {
                SearchFriendBean searchFriendBean = new SearchFriendBean();
                JSONObject temp = jsonArray.getJSONObject(i);
                if (temp.has("userid")) { //用户对象
                    searchFriendBean.userid = temp.getString("userid");
                    searchFriendBean.name = temp.getString("name");
                    searchFriendBean.img = temp.getString("img");
                    searchFriendBean.otherinfo = temp.getString("otherinfo");
                    list.add(searchFriendBean);
                } else {//群组对象
                    CateChannel gibean = gson.fromJson(jsonArray.getJSONObject(i)
                            .toString(), CateChannel.class);
                    if (!"".equals(gibean.img))
                        if (!gibean.img.startsWith("http://")) {
                            gibean.img = HOST + gibean.img;
                        }
                    bean.groupList.add(gibean);
                }
            }
        } else {
            bean.msg=msg;
        }
        bean.list=list;
        return bean;
    }

    /**
     * 发布邀请函
     *
     * @param @param  userid
     * @param @param  content
     * @param @param  title
     * @param @param  begintime
     * @param @param  location
     * @param @param  city
     * @param @param  zipname
     * @param @param  filepath
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: pushInvitation
     * @Description: TODO
     */
    public static Object[] pushInvitation(String userid, String content,
                                          String title, long begintime, String location, String city,
                                          String zipname, String filepath) throws Exception {
        List<Parameter> params = new ArrayList<Parameter>();
        params.add(new Parameter("uid", userid));
        params.add(new Parameter("content", content));
        params.add(new Parameter("title", title));
        params.add(new Parameter("begintime", "" + begintime));
        params.add(new Parameter("location", location));
        params.add(new Parameter("city", city));
        if (!"".equals(zipname)) {
            params.add(new Parameter("zipname", zipname));
        }
        String result = new SyncHttp().postfile(HOST + PUSH_INVITATION,
                filepath, params);
        Object[] obj = getObjects(null, result);
        return new Object[]{obj[0], obj[1], obj[2]};
    }

    /**
     * s 查询同城的用户
     *
     * @param userid
     * @param location 地址中文，如：成都
     * @return
     * @throws Exception
     */
    public static Object[] getCityFriend(String userid, String location)
            throws Exception {
        Object[] obj = getNetStr(Configs.CITY_FRIEND, "userid=" + userid
                + "&location=" + location, UserBean.class);
        return new Object[]{obj[0], obj[1], obj[2]};
    }

    /**
     * 情报评论列表
     *
     * @param @param  newsid
     * @param @param  id
     * @param @param  num
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getQingbaoCommentList
     * @Description: TODO
     */
    public static Object[] getQingbaoCommentList(int newsid, int id, int num)
            throws Exception {
        Object[] obs = null;
        if (-1 == id) {
            obs = getNetStr(GET_QINGBAOCOMMENTLIST, "newsid=" + newsid
                    + "&num=" + num, QingbaoComment.class);
        } else {
            obs = getNetStr(GET_QINGBAOCOMMENTLIST, "newsid=" + newsid + "&id="
                    + id + "&num=" + num, QingbaoComment.class);
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    public static FriendChatinfo getJpushData(String jsonString)
            throws Exception {
        FriendChatinfo bean = new FriendChatinfo();
        JSONObject jsonObject = new JSONObject(jsonString);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("message");
        JSONObject jsonObject1 = new JSONObject(msg);
        String extras = jsonObject1.getString("extras");
        JSONObject jsonObject2 = new JSONObject(extras);
        bean.userid = jsonObject2.getString("userid");
        bean.usernick = jsonObject2.getString("usernick");
        bean.content = jsonObject2.getString("content");
        bean.userheadpath = jsonObject2.getString("headImg");
        bean.msgtype = jsonObject2.getInt("msgType");
        bean.time = jsonObject2.getString("time");
        bean.isMymsg = false;
        return bean;
    }

    /**
     * 查询其他人的信息，里面可查出群组列表
     *
     * @param userid   用户的ID
     * @param touserid 所要查询的用户的ID
     * @return
     * @throws Exception
     */
    public static OtherFriendInfo getOtherFriendInfo(String userid,
                                                     String touserid) throws Exception {
        OtherFriendInfo friendInfo = new OtherFriendInfo();
        String result = http.httpGet(HOST + FINDUSER, "?" + "userid=" + userid
                + "&touserid=" + touserid);
        System.out.println("sssss=== " + HOST + FINDUSER + "?" + "userid="
                + userid + "&touserid=" + touserid + "====" + result);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        JSONObject jsonObject1 = new JSONObject(msg);
        friendInfo.userid = jsonObject1.getString("userid");
        friendInfo.name = jsonObject1.getString("name");
        friendInfo.location = jsonObject1.getString("location");
        friendInfo.otherinfo = jsonObject1.getString("otherinfo");
        friendInfo.ddid = jsonObject1.getString("ddid");
        friendInfo.img = jsonObject1.getString("img");
        friendInfo.createtime = jsonObject1.getString("createtime");
        friendInfo.tel = jsonObject1.getString("tel");
        friendInfo.relation = jsonObject1.getString("relation");
        String group = jsonObject1.getString("group");

        if (null != group && !group.equals("") && !group.equals("[]")) {
            JSONArray jsonArray = new JSONArray(group);
            friendInfo.groupInfoList = new ArrayList<GroupInfo>();
            for (int i = 0; i < jsonArray.length(); i++) {
                GroupInfo groupInfo = new GroupInfo();
                JSONObject temp = jsonArray.getJSONObject(i);
                groupInfo.groupid = temp.getString("groupid");
                groupInfo.name = temp.getString("name");
                groupInfo.des = temp.getString("des");
                groupInfo.image = temp.getString("groupimg");
                friendInfo.groupInfoList.add(groupInfo);
            }
        }
        return friendInfo;
    }

    public static AllCateBean getAllCateData() throws Exception {
        AllCateBean allCateBean = new AllCateBean();
        String result = http.httpGet(HOST + GETALLCATE, "");
        System.out.println("sssss=== " + HOST + FINDUSER + "====" + result);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        JSONObject jsonObject1 = new JSONObject(msg);
        String cateForGroup = jsonObject1.getString("cateForGroup");
        String cateForMsg = jsonObject1.getString("cateForMsg");

        if (null != cateForGroup && !cateForGroup.equals("")
                && !cateForGroup.equals("[]")) {
            JSONArray jsonArray = new JSONArray(cateForGroup);
            allCateBean.groupsList = new ArrayList<CateForGroup>();
            for (int i = 0; i < jsonArray.length(); i++) {
                CateForGroup group = new CateForGroup();
                JSONObject temp = jsonArray.getJSONObject(i);
                group.cateforgroupid = temp.getString("cateforgroupid");
                group.catename = temp.getString("catename");
                group.createtime = temp.getString("createtime");
                group.cateimg = temp.getString("cateimg");
                allCateBean.groupsList.add(group);
            }
        }

        if (null != cateForMsg && !cateForMsg.equals("")
                && !cateForMsg.equals("[]")) {
            JSONArray jsonArray = new JSONArray(cateForMsg);
            allCateBean.msgList = new ArrayList<CateForMsg>();
            for (int i = 0; i < jsonArray.length(); i++) {
                CateForMsg msgs = new CateForMsg();
                JSONObject temp = jsonArray.getJSONObject(i);
                msgs.cateformsgid = temp.getString("cateformsgid");
                msgs.catename = temp.getString("catename");
                msgs.createtime = temp.getString("createtime");
                msgs.cateimg = temp.getString("cateimg");
                allCateBean.msgList.add(msgs);
            }
        }
        return allCateBean;
    }

    /**
     * 获取邀请函评论列表
     *
     * @param @param  invitationid 邀请函id
     * @param @param  id
     * @param @param  num
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getInvitationCommentlist
     * @Description: TODO
     */
    public static Object[] getInvitationCommentlist(int invitationid, int id,
                                                    int num) throws Exception {
        Object[] obs = null;
        if (id == -1) {
            obs = getNetStr(INVITATION_COMMENTLIST, "invitationid="
                    + invitationid + "&num=" + num, CommentBean.class);
        } else {
            obs = getNetStr(INVITATION_COMMENTLIST, "invitationid="
                            + invitationid + "&num=" + num + "&id=" + id,
                    CommentBean.class);
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 发表情报评论
     *
     * @param @param  userid
     * @param @param  id
     * @param @param  content
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: sendCommentAtQingbao
     * @Description: TODO
     */
    public static Object[] sendCommentAtQingbao(String uid, int id,
                                                String content, String touserid, int commentid) throws Exception {
        Object[] obs = getNetStr(SEND_QINGBAOCOMMENT, "uid=" + uid + "&id="
                + id + "&content=" + content + "&touserid=" + touserid
                + "&commentid=" + commentid, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * @param @param  uid
     * @param @param  id
     * @param @param  content
     * @param @param  touserid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: sendCommentToQingbao
     * @Description: TODO
     */
    public static Object[] sendCommentToQingbao(String uid, int id,
                                                String content) throws Exception {
        Object[] obs = getNetStr(SEND_COMMENTTOQINGBAO, "uid=" + uid + "&id="
                + id + "&content=" + content, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    public static List<CateChannel> getGetCateChannel(String type, String id,
                                                      String userId, String num, String page) throws Exception {
        CateChannel cateChannel = null;
        List<CateChannel> list = null;
        String result = "";
        if (type.equals("0")) {
            result = http.httpGet(HOST + GETCATECHANNEL, "?"
                    + "cateforgroupid=" + id + "&userid=" + userId + "&page="
                    + page + "&num=" + num);
        } else {
            result = http.httpGet(HOST + GETCATECHANNEL, "?" + "cateformsgid="
                    + id + "&userid=" + userId + "&page=" + page + "&num="
                    + num);
        }
        System.out.println("sssss=== " + HOST + GETCATECHANNEL + "?"
                + "cateformsgid=" + id + "&userid=" + userId + "&page=" + page
                + "&num=" + num + "====" + result);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");

        if (null != msg && !msg.equals("") && !msg.equals("[]")) {
            JSONArray jsonArray = new JSONArray(msg);
            list = new ArrayList<CateChannel>();
            for (int i = 0; i < jsonArray.length(); i++) {
                cateChannel = new CateChannel();
                JSONObject temp = jsonArray.getJSONObject(i);
                cateChannel.id = temp.getString("id");
                cateChannel.name = temp.getString("name");
                cateChannel.des = temp.getString("des");
                cateChannel.img = temp.getString("img");
                cateChannel.groupmanager = temp.getString("groupmanager");
                cateChannel.newscount = temp.getString("newscount");
                cateChannel.fanscount = temp.getString("fanscount");
                cateChannel.groupnum = temp.getString("groupnum");
                cateChannel.adminid = temp.getString("adminid");
                // cateChannel.adminattention =
                // temp.getString("adminattention");
                // cateChannel.isgroup = temp.getString("isgroup");
                // cateChannel.ischannel = temp.getString("ischannel");
                cateChannel.channelid = temp.getString("channelid");
                list.add(cateChannel);
            }
        }
        return list;
    }

    /**
     * 资讯分享计数
     *
     * @param @param  newsid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: shareCount
     * @Description: TODO
     */
    public static Object[] shareCount(String newsid) throws Exception {
        Object[] obs = getNetStr(Configs.SHARE, "newsid=" + newsid, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 用户是否关注了某只股票
     *
     * @param @param  uid
     * @param @param  stockCode
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getIfFoucs
     * @Description: TODO
     */
    public static Object[] getIfFoucs(String uid, String stockCode)
            throws Exception {
        Object[] obs = getNetStr(Configs.IF_FOUCE, "uid=" + uid + "&stockcode="
                + stockCode, Fouces.class);
        return new Object[]{obs[0], obs[1],
                ((ArrayList<Fouces>) obs[2]).get(0)};
    }

    /**
     * 上传本地通讯录返回结果
     *
     * @param @param  uid
     * @param @param  str
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: checkPhone
     * @Description: TODO
     */
    public static Object[] checkPhone(String uid, String str) throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("uid", uid));
        paramPairs.add(new Parameter("str", str));
        Object[] obs = myGetNetPost(Configs.TEL, paramPairs, UserRelation.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 个股关联情报
     *
     * @param @param  stockcode
     * @param @param  id
     * @param @param  num
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getGeguguanlianqingbao
     * @Description: TODO
     */
    public static Object[] getGeguguanlianqingbao(String stockcode, int id,
                                                  int num) throws Exception {
        Object[] obs = null;
        if (id == -1) {
            obs = getNetStr(STOCKCODECONNECT_QINGBAO, "stockcode=" + stockcode
                    + "&num=" + num, NewInfoBean.class);
        } else {
            obs = getNetStr(STOCKCODECONNECT_QINGBAO, "stockcode=" + stockcode
                    + "&num=" + num + "&id=" + id, NewInfoBean.class);
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 同时他们也在关注
     *
     * @param @param  stockcode
     * @param @param  id
     * @param @param  num
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getFollowlist
     * @Description: TODO
     */
    public static Object[] getFollowlist(String uid, String stockcode, int id,
                                         int num, int type) throws Exception {
        Object[] obs = null;
        if (id == -1) {
            obs = getNetStr(STOCK_FOLLOW, "stockcode=" + stockcode + "&num="
                            + num + "&uid=" + uid + "&type=" + type,
                    StockUserInfo.class);
        } else {
            obs = getNetStr(STOCK_FOLLOW, "stockcode=" + stockcode + "&num="
                            + num + "&id=" + id + "&uid=" + uid + "&type=" + type,
                    StockUserInfo.class);
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    public static JpushBean getPushData(String msgContent) throws Exception {
        JpushBean bean = new JpushBean();
        JSONObject jsonObject = new JSONObject(msgContent);
        bean.sender = jsonObject.getString("sender");
        bean.id = jsonObject.getString("id");
        bean.content = jsonObject.getString("content");
        bean.time = jsonObject.getString("time");
        bean.headImg = jsonObject.getString("headImg");
        bean.type = jsonObject.getInt("type");
        bean.name = jsonObject.getString("name");
        return bean;
    }

    /**
     * 删除邀请函
     *
     * @param uid          用户id
     * @param invitationId 要删除的邀请函id
     * @return
     * @throws Exception
     */
    public static Object[] deleteInvitationAct(String uid, String invitationId)
            throws Exception {
        Object[] obs = getNetStr(DELETE_INVITATION, "uid=" + uid + "&id="
                + invitationId, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 获取情报详情
     *
     * @param @param  newsid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getQingbaoInfo
     * @Description: TODO
     */
    public static Object[] getQingbaoInfo(int newsid) throws Exception {
        Object[] obs = getNetStr(Configs.QINGBAO_INFO, "newsid=" + newsid,
                NewsBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 用户评论新闻插入接口
     *
     * @param @param  uid
     * @param @param  id
     * @param @param  content
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: sendNewsCommnet
     * @Description: TODO
     */
    public static Object[] sendNewsCommnet(String uid, int id, String content)
            throws Exception {
        Object[] obs = getNetStr(NEWS_SENDCOMMENT, "uid=" + uid + "&newsid="
                + id + "&content=" + content, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 获取频道专栏列表
     *
     * @param @param  uid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getChannellistByuserid
     * @Description: TODO
     */
    public static Object[] getChannellistByuserid(String uid) throws Exception {
        Object[] obs = getNetStr(GET_CHANNELLIST, "uid=" + uid,
                EditQBChannellist.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 收藏资讯
     *
     * @param @param  uid
     * @param @param  news_id
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: collectMyNews
     * @Description: TODO
     */
    public static Object[] collectMyNews(String uid, String news_id)
            throws Exception {
        Object[] obs = getNetStr(NEWS_COLLECT, "uid=" + uid + "&newsid="
                + news_id, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 删除邀请函
     *
     * @param @return
     * @param @throws Exception
     * @return String
     * @Title: getCountnews
     * @Description: TODO
     */
    public static String getCountnews() throws Exception {
        String result = http.httpGet(HOST + COUNTNEWS, "");
        JSONObject jsonObject = new JSONObject(result);
        return jsonObject.getString("msg");
    }

    /**
     * 取消用户收藏
     *
     * @param @param  uid
     * @param @param  news_id
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: cancelCollect
     * @Description: TODO
     */
    public static Object[] cancelCollect(String uid, String news_id)
            throws Exception {
        Object[] obs = getNetStr(CANCEL_COLLECT, "uid=" + uid + "&newsid="
                + news_id, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 评论我的
     *
     * @param @param  uid
     * @param @param  num
     * @param @param  id
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getCommentMelist
     * @Description: TODO
     */
    public static Object[] getCommentMelist(String uid, int num, int id)
            throws Exception {
        Object[] obs = null;
        if (id == -1) {
            obs = getNetStr(GET_COMMENTLIST, "uid=" + uid + "&num=" + num,
                    CommentMeList.class);
        } else {
            obs = getNetStr(GET_COMMENTLIST, "uid=" + uid + "&num=" + num
                    + "&id=" + id, CommentMeList.class);
        }
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 删除评论
     *
     * @param @param  uid
     * @param @param  id
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: deleteComment
     * @Description: TODO
     */
    public static Object[] deleteComment(String uid, int id) throws Exception {
        Object[] obs = getNetStr(DELETE_COMMENT, "uid=" + uid + "&id=" + id,
                null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 得到用户的群组
     *
     * @param userid 用户id
     * @return
     * @throws Exception
     */
    public static List<CateChannel> getUserGroupList(String userid)
            throws Exception {
        CateChannel cateChannel = null;
        String result = http.httpGet(HOST + USER_GROUP_LIST, "?userid="
                + userid);
        System.out.println("sssss=== " + HOST + GETGROUPINFO + "====" + result);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        List<CateChannel> list =new ArrayList<CateChannel>();
        if (null != msg && !msg.equals("") && !msg.equals("[]")) {
            JSONArray jsonArray = new JSONArray(msg);
            for (int i = 0; i < jsonArray.length(); i++) {
                cateChannel = new CateChannel();
                JSONObject temp = jsonArray.getJSONObject(i);
                cateChannel.id = temp.getString("id");
                cateChannel.name = temp.getString("name");
                cateChannel.des = temp.getString("des");
                cateChannel.img = temp.getString("img");
                cateChannel.groupmanager = temp.getString("groupmanager");
                cateChannel.newscount = temp.getString("newscount");
                cateChannel.fanscount = temp.getString("fanscount");
                cateChannel.groupnum = temp.getString("groupnum");
                cateChannel.adminid = temp.getString("adminid");
                list.add(cateChannel);
            }
        }
        return list;
    }

    /**
     * 创建频道接口
     *
     * @param uid        用户id
     * @param name       频道名称
     * @param des        频道描述
     * @param img        频道图片
     * @param cateformsg 按频道消息来源分类id
     * @return object[0] 结果码 object[1] 结果信息
     * @throws Exception
     */
    public static Object[] createGroupData(String uid, String name, String des,
                                           String img, String cateformsg) throws Exception {
        List<Parameter> list = new ArrayList<Parameter>();
        Parameter parameter = null;
        String[] str1 = {"userid", "name", "des", "img", "cateformsg"};
        String[] str2 = {uid, name, des, img, cateformsg};
        for (int i = 0; i < 5; i++) {
            parameter = new Parameter();
            parameter.setName(str1[i]);
            parameter.setValue(str2[i]);
            list.add(parameter);
        }
        String result = http.httpPost(HOST + CREATECHANNEL, list);
        System.out
                .println("sssss=== " + HOST + CREATECHANNEL + "====" + result);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        return new Object[]{jsonObject.getString("code"),
                jsonObject.getString("msg")};
    }

    /**
     * 申请加群接口
     *
     * @param uid     用户的ID
     * @param groupid 所加群的群组ID
     * @return object[0] 结果码 object[1] 结果信息
     * @throws Exception
     */
    public static Object[] applayJoinGroup(String uid, String groupid)
            throws Exception {
        String result = http.httpGet(HOST + APPLAYJOINGROUP, "?userid=" + uid
                + "&groupid=" + groupid);
        System.out.println("sssss=== " + HOST + APPLAYJOINGROUP + "===="
                + result);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        return new Object[]{resultcode,
                jsonObject.getString("msg")};
    }

    /**
     * 处理加群申请接口
     *
     * @param type    是否同意 0 同意 1 不同意
     * @param uid     用户的ID
     * @param groupid 所加群的群组ID
     * @return object[0] 结果码 object[1] 结果信息
     * @throws Exception
     */
    public static Object[] handleJoinGroup(String type, String uid,
                                           String groupid) throws Exception {
        String result = http.httpGet(HOST + HANDLEJOINGROUP, "?type=" + type
                + "&userid=" + uid + "&groupid=" + groupid);
        // System.out.println("sssss=== " + HOST + HANDLEJOINGROUP + "===="
        // + result);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        return new Object[]{jsonObject.getString("code"),
                jsonObject.getString("msg")};
    }

    /**
     * 获取群组信息
     *
     * @param groupid 群组ID
     * @return
     * @throws Exception
     */

    public static CateChannel getGroupInfo(String groupid, String uerid)
            throws Exception {
        CateChannel cateChannel = null;
        String result = http.httpGet(HOST + GETGROUPINFO, "?gid=" + groupid
                + "&userid=" + uerid);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        List<CateChannel> list = null;
        if (null != msg && !msg.equals("") && !msg.equals("[]")) {
            JSONArray jsonArray = new JSONArray(msg);
            list = new ArrayList<CateChannel>();
            for (int i = 0; i < jsonArray.length(); i++) {
                cateChannel = new CateChannel();
                JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                cateChannel.name = jsonObject1.getString("name");
                cateChannel.des = jsonObject1.getString("des");
                cateChannel.img = jsonObject1.getString("img");
                cateChannel.adminid = jsonObject1.getString("adminid");
                cateChannel.groupmanager = jsonObject1
                        .getString("groupmanager");
                cateChannel.adminattention = jsonObject1
                        .getString("adminattention");
                cateChannel.newscount = jsonObject1.getString("newscount");
                cateChannel.fanscount = jsonObject1.getString("fanscount");
                cateChannel.isgroup = jsonObject1.getString("isgroup");
                cateChannel.groupnum = jsonObject1.getString("groupnum");
                cateChannel.id = jsonObject1.getString("id");
                cateChannel.channelid = jsonObject1.getString("channelid");
                cateChannel.ischannel = jsonObject1.getString("ischannel");
                list.add(cateChannel);
            }
            return list.get(0);
        }
        return null;
    }

    /**
     * 邀请好友加入群
     *
     * @param jsonStr [{“userid”:“1”,“groupid”:“1”},{ “userid”:“1”,“groupid”:“1”},{
     *                “userid”:“1”,“groupid”:“1”}]
     * @return object[0] 结果码 object[1] 结果信息
     * @throws Exception
     */
    public static Object[] inviteJoinGroup(String jsonStr) throws Exception {
        System.out.println("邀请加群" + HOST + INVITEJOINGROUP + "?msg=" + jsonStr);
        Parameter parameter = new Parameter();
        List<Parameter> list = new ArrayList<Parameter>();
        parameter.setName("msg");
        parameter.setValue(jsonStr);
        list.add(parameter);
        String result = http.httpPost(HOST + INVITEJOINGROUP, list);
        JSONObject jsonObject = new JSONObject(result);
        return new Object[]{jsonObject.getString("code"),
                jsonObject.getString("msg")};
    }

    /**
     * 退出群
     *
     * @param userid  用户的ID
     * @param groupid 群组的ID
     * @return object[0] 结果码 object[1] 结果信息
     * @throws Exception
     */

    public static Object[] quitGroup(String userid, String groupid)
            throws Exception {
        String result = http.httpGet(HOST + QUITGROUP, "?userid=" + userid
                + "&groupid=" + groupid);
        JSONObject jsonObject = new JSONObject(result);
        return new Object[]{jsonObject.getString("code"),
                jsonObject.getString("msg")};
    }

    /**
     * 删除群
     *
     * @param userid  用户的ID
     * @param groupid 群组的ID
     * @return object[0] 结果码 object[1] 结果信息
     * @throws Exception
     */
    public static Object[] delGroup(String userid, String groupid)
            throws Exception {
        String result = http.httpGet(HOST + DELGROUP, "?userid=" + userid
                + "&groupid=" + groupid);
        JSONObject jsonObject = new JSONObject(result);
        return new Object[]{jsonObject.getString("code"),
                jsonObject.getString("msg")};
    }

	/*
	 * public static List<CollectBean> getUserCollect() throws Exception{ String
	 * result = http.httpGet(HOST + getUserCollect, "?gid=" + groupid);
	 * System.out.println("sssss=== " + HOST + GETGROUPINFO + "====" + result);
	 * JSONObject jsonObject = new JSONObject(result); }
	 */

    /**
     * 上传频道排序
     *
     * @param @param  userid
     * @param @param  channeljson
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: uploadChannellist
     * @Description: TODO
     */
    public static Object[] uploadChannellist(String userid, String channeljson)
            throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("uid", userid));
        paramPairs.add(new Parameter("listdata", channeljson));
        Object[] obs = myGetNetPost(UPLOAD_CHANNELLIST, paramPairs, null);
        // Object[] obs = getNetStr(UPLOAD_CHANNELLIST, "uid=" + userid+
        // "&listdata=" + channeljson,
        // null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 获取群组详情
     *
     * @param @param  groupid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getVeryGroupInfo
     * @Description: TODO
     */
    public static Object[] getVeryGroupInfo(String groupid, String userid)
            throws Exception {
        Object[] obs = getNetStr(GET_GROUPINFO, "gid=" + groupid + "&userid="
                + userid, GroupInfomation.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    public static List<RecommenBean> getFriendRecommend(String userId,
                                                        String phone) throws Exception {
        RecommenBean bean = null;
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("userid", userId));
        paramPairs.add(new Parameter("phone", phone));
        @SuppressWarnings("static-access")
        String result = http.httpPost(HOST + FRIENDRECOMMEND, paramPairs);

        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        List<RecommenBean> list = null;
        if (null != msg && !msg.equals("") && !msg.equals("[]")) {
            JSONArray jsonArray = new JSONArray(msg);
            list = new ArrayList<RecommenBean>();
            for (int i = 0; i < jsonArray.length(); i++) {
                bean = new RecommenBean();
                JSONObject temp = jsonArray.getJSONObject(i);
                bean.userid = temp.getString("userid");
                bean.name = temp.getString("name");
                bean.bookname = temp.getString("bookname");
                bean.img = temp.getString("img");
                bean.otherinfo = temp.getString("otherinfo");
                bean.location = temp.getString("location");
                bean.date = temp.getString("date");
                list.add(bean);
            }
        }
        return list;
    }

    /**
     * 大搜索用户
     *
     * @param @param  keyword
     * @param @param  num
     * @param @param  page
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: searchAllUser
     * @Description: TODO
     */
    public static Object[] searchAllUser(String keyword, String uid, int num,
                                         int page) throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("search", keyword));
        paramPairs.add(new Parameter("num", String.valueOf(num)));
        paramPairs.add(new Parameter("page", String.valueOf(page)));
        paramPairs.add(new Parameter("uid", uid));
        Object[] obs = myGetNetPost(ALL_SEARCH_USER, paramPairs, UserBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 大搜索群组
     *
     * @param @param  keyword
     * @param @param  uid
     * @param @param  num
     * @param @param  page
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: searchAllGroup
     * @Description: TODO
     */
    public static Object[] searchAllGroup(String keyword, String uid, int num,
                                          int page) throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("search", keyword));
        paramPairs.add(new Parameter("num", String.valueOf(num)));
        paramPairs.add(new Parameter("page", String.valueOf(page)));
        paramPairs.add(new Parameter("uid", uid));
        Object[] obs = myGetNetPost(ALL_SEARCH_GROUP, paramPairs,
                CateChannel.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 大搜索资讯
     *
     * @param @param  keyword
     * @param @param  uid
     * @param @param  num
     * @param @param  page
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: searchAllNews
     * @Description: TODO
     */
    public static Object[] searchAllNews(String keyword, String uid, int num,
                                         int page) throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("search", keyword));
        paramPairs.add(new Parameter("num", String.valueOf(num)));
        paramPairs.add(new Parameter("page", String.valueOf(page)));
        paramPairs.add(new Parameter("uid", uid));
        Object[] obs = myGetNetPost(ALL_SEARCH_NEWS, paramPairs,
                ChannelList.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 设置群组聊天模式
     *
     * @param @param  groupid
     * @param @param  type
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: setGroupChatType
     * @Description: TODO
     */
    public static Object[] setGroupChatType(String groupid, String type)
            throws Exception {
        Object[] obs = getNetStr(SET_GROUPCHAT_TYPE, "groupid=" + groupid
                + "&type=" + type, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 获取群组聊天模式
     *
     * @param @param  groupid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getGroupChatType
     * @Description: TODO
     */
    public static Object[] getGroupChatType(String groupid) throws Exception {
        Object[] obs = getNetStr(GET_GROUPCHAT_TYPE, "groupid=" + groupid,
                GroupChatTypeBean.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /*
     * 反馈接口
     */
    public static Object[] feedBack(String userid, String content)
            throws Exception {
        List<Parameter> paramPairs = new ArrayList<Parameter>();
        paramPairs.add(new Parameter("userid", userid));
        paramPairs.add(new Parameter("content", content));
        Object[] obs = myGetNetPost(FEEDBACK, paramPairs, null);
        return new Object[]{obs[0], obs[1]};
    }

    public static List<FansBean> getFansData(String groupid, String userid)
            throws Exception {
        FansBean bean = new FansBean();
        String result = http
                .httpGet(HOST + GETGROUPFANS, "?groupid=" + groupid); // +"&userid"+userid
        System.out.println("sssss=== " + HOST + GETGROUPFANS + "?groupid="
                + groupid);
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        List<FansBean> list = null;
        if (null != msg && !msg.equals("") && !msg.equals("[]")) {
            JSONArray jsonArray = new JSONArray(msg);
            list = new ArrayList<FansBean>();
            for (int i = 0; i < jsonArray.length(); i++) {
                bean = new FansBean();
                JSONObject temp = jsonArray.getJSONObject(i);
                bean.id = temp.getString("id");
                bean.name = temp.getString("name");
                bean.img = temp.getString("img");
                bean.des = temp.getString("des");
                bean.location = temp.getString("location");
                bean.lastTime = temp.getString("lastTime");
                bean.isAttention = temp.getString("isAttention");
                list.add(bean);
            }
        }
        return list;
    }

    public static List<InviteJoinBean> getInviteJoinData(String userid,
                                                         String groupId) throws Exception {
        InviteJoinBean bean = new InviteJoinBean();
        String result = http.httpGet(HOST + INVITEJOIN, "?userid=" + userid
                + "&groupid=" + groupId);
        System.out
                .println("sssss=== "
                        + (HOST + INVITEJOIN + "?userid=" + userid
                        + "&groupid=" + groupId+result));
        JSONObject jsonObject = new JSONObject(result);
        int resultcode = jsonObject.getInt("code");
        if (resultcode == 4001) {
            Configs.getApp().loginout();
            return null;
        }
        String msg = jsonObject.getString("msg");
        List<InviteJoinBean> list = null;
        if (null != msg && !msg.equals("") && !msg.equals("[]")) {
            JSONArray jsonArray = new JSONArray(msg);
            list = new ArrayList<InviteJoinBean>();
            for (int i = 0; i < jsonArray.length(); i++) {
                bean = new InviteJoinBean();
                JSONObject temp = jsonArray.getJSONObject(i);
                bean.frienduserid = temp.getString("frienduserid");
                bean.name = temp.getString("name");
                bean.img = temp.getString("img");
                bean.location = temp.getString("location");
                bean.otherinfo = temp.getString("otherinfo");
                list.add(bean);
            }
        }
        return list;
    }

    /**
     * 我的收藏
     *
     * @param @param  uid
     * @param @param  num
     * @param @param  page
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: getMyCollect
     * @Description: TODO
     */
    public static Object[] getMyCollect(String uid, int num, int page)
            throws Exception {
        Object[] obs = getNetStr(GETUSERCOLLECT, "uid=" + uid + "&num=" + num
                + "&page=" + page, ChannelList.class);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

    /**
     * 退出登录
     *
     * @param @param  uid
     * @param @return
     * @param @throws Exception
     * @return Object[]
     * @Title: loginout
     * @Description: TODO
     */
    public static Object[] loginout(String uid) throws Exception {
        Object[] obs = getNetStr(LOGINOUT, "userid=" + uid, null);
        return new Object[]{obs[0], obs[1], obs[2]};
    }

}
