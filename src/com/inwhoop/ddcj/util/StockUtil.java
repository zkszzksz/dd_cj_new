package com.inwhoop.ddcj.util;

import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.widget.ImageView;
import android.widget.TextView;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.Fouces;
import com.inwhoop.ddcj.bean.LoadStockData;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.db.HandleStocklistDB;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

/**
 * 自选股设置
 * 
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: StockUtil.java
 * @Package com.inwhoop.ddcj.util
 * @Description: TODO
 * @date 2014-10-27 下午7:01:53
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class StockUtil {
	
	public static void isStockFouces(final String stockCode, final Handler handler, final Context mContext){
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				Message msg = new Message();
				try {
					Fouces fouces = new Fouces();
                    String uid = UserInfoUtil.getUserInfo(mContext).userid;
					if ("".equals(uid)) {
						HandleStocklistDB db = new HandleStocklistDB(mContext);
						
						if (db.isExist(stockCode)) {
							fouces.foucs = 1;
						} else {
							fouces.foucs = 0;
						}
						msg.what = 20;
						msg.obj = fouces;
					} else {
						Object[] data = JsonUtils.getIfFoucs(uid,
								stockCode);
						if ((Boolean) data[0]) {
							fouces = (Fouces) data[2];
							msg.what = 20;
							msg.obj = fouces;
						} else {
							msg.what = 21;
						}
					}
//					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = 21;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	/**
	 * 添加自选股
	 * 
	 * @param @param info
	 * @param @param handler
	 * @param @param mContext
	 * @return void
	 * @Title: addStockInfoToDB
	 * @Description: TODO
	 */
	public static void addStockInfoToDB(final StockInfoBean info,
			final Handler handler, final Context mContext,
			final TextView textView, final ImageView imageView,
			final String type, final int position) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					if ("".equals(UserInfoUtil.getUserInfo(mContext).userid)) {
						HandleStocklistDB db = new HandleStocklistDB(mContext);
						if (db.isExist(info.stockcode)) {
							msg.obj = new Object[] { false, "该股票已经在您的股票列表中了",
									textView, imageView, type, position };
						} else {
							db.saveStock(info);
							SysPrintUtil.pt("添加的选股的Code", info.stockcode);
							msg.obj = new Object[] { true, null, textView,
									imageView, type, position };
						}
					} else {
						Object[] obj = JsonUtils.addStock(
								UserInfoUtil.getUserInfo(mContext).userid,
								info.stockcode, info.stockname, "zhds",
								info.position, textView, imageView, type,
								position);
						msg.obj = obj;
					}
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.obj = new Object[] { false, null, textView, imageView,
							type, position };
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	/**
	 * 取消自选股
	 * 
	 * @param @param info
	 * @param @param handler
	 * @param @param mContext
	 * @return void
	 * @Title: deleteStockInfoToDB
	 * @Description: TODO
	 */
	public static void deleteStockInfoToDB(final StockInfoBean info,
			final Handler handler, final Context mContext,
			final TextView textView, final ImageView imageView,
			final String type, final int position) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					if ("".equals(UserInfoUtil.getUserInfo(mContext).userid)) {
						HandleStocklistDB db = new HandleStocklistDB(mContext);
						db.deleteStockinfolistByCode(info.stockcode);
						msg.obj = new Object[] { true, null, textView,
								imageView, type, position };
					} else {
						Object[] obj = JsonUtils.cancelStock(
								UserInfoUtil.getUserInfo(mContext).userid,
								info.stockcode, textView, imageView, type,
								position);
						msg.obj = obj;
					}
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					SysPrintUtil.pt("取消自选股异常", e.toString());
					msg.obj = new Object[] { false, null, textView, imageView,
							type, position };
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	/**
	 * 排序上传
	 * 
	 * @param @param list
	 * @param @param handler
	 * @param @param mContext
	 * @return void
	 * @Title: saveList
	 * @Description: TODO
	 */
	public static void saveList(final List<StockInfoBean> list,
			final Handler handler, final Context mContext) {
		final List<LoadStockData> loadStockDatas = new ArrayList<LoadStockData>();
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					if ("".equals(UserInfoUtil.getUserInfo(mContext).userid)) {
						HandleStocklistDB db = new HandleStocklistDB(mContext);
						for (int i = 0; i < list.size(); i++) {
							// db.saveStock(list.get(i));
							db.updateStock(list.get(i), i + 1);
						}
						msg.obj = new Object[] { true, 0 };
					} else {// 上传网络
						for (int i = 0; i < list.size(); i++) {
							LoadStockData loadStockData = new LoadStockData();
							loadStockData.position = i;
							loadStockData.stockcode = list.get(i).stockcode;
							loadStockDatas.add(loadStockData);
							// System.out.println("position== "
							// + loadStockData.position + "  --- "
							// + loadStockData.stockcode);
						}
						String listdata = JsonUtils.listToJson(loadStockDatas);
						boolean isLoad = JsonUtils.loadOrderList(
								UserInfoUtil.getUserInfo(mContext).userid,
								URLEncoder.encode(listdata));
						msg.obj = new Object[] { isLoad, 0 };
					}
				} catch (Exception e) {
					msg.obj = new Object[] { false, 0 };
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	/**
	 * 批量删除
	 * 
	 * @param @param list
	 * @param @param handler
	 * @param @param mContext
	 * @return void
	 * @Title: deleteStocklist
	 * @Description: TODO
	 */
	public static void deleteStocklist(final List<StockInfoBean> list,
			final Handler handler, final Context mContext) {
		final List<LoadStockData> loadStockDatas = new ArrayList<LoadStockData>();
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					if ("".equals(UserInfoUtil.getUserInfo(mContext).userid)) {
						HandleStocklistDB db = new HandleStocklistDB(mContext);
						for (int i = 0; i < list.size(); i++) {
							db.deleteStockinfolistByCode(list.get(i).stockcode);
						}
						msg.obj = new Object[] { true, Configs.DELETE_SUCCESS };
					} else {
						for (int i = 0; i < list.size(); i++) {
							LoadStockData loadStockData = new LoadStockData();
							loadStockData.position = i;
							loadStockData.stockcode = list.get(i).stockcode;
							System.out.println("loadStockData== "
									+ list.get(i).stockname);
							loadStockDatas.add(loadStockData);
						}
						String listdata = JsonUtils.listToJson(loadStockDatas);
						boolean isDelete = JsonUtils.deleteList(
								UserInfoUtil.getUserInfo(mContext).userid,
								URLEncoder.encode(listdata));
						if (isDelete) {
							msg.obj = new Object[] { isDelete,
									Configs.DELETE_SUCCESS };
						} else {
							msg.obj = new Object[] { isDelete,
									Configs.DELETE_FAIL };
						}
					}
				} catch (Exception e) {
					msg.obj = new Object[] { false, Configs.DELETE_FAIL };
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	/**
	 * 获取自选股列表
	 * 
	 * @param @param handler
	 * @param @param mContext
	 * @return void
	 * @Title: getStockInfoList
	 * @Description: TODO
	 */
	public static void getStockInfoList(final Handler handler,
			final Context mContext) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					String uid = UserInfoUtil.getUserInfo(mContext).userid;
					if ("".equals(uid)) {
						HandleStocklistDB db = new HandleStocklistDB(mContext);
						List<StockInfoBean> list = db.getFriendlist();
						msg.obj = new Object[] { true, "", list };
					} else {
						msg.obj = JsonUtils.getNetMyStock(uid,"");
					}
					msg.what = 1;
				} catch (Exception e) {
					msg.obj = e.toString();
					msg.what = 0;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

}
