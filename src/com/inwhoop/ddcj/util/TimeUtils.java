package com.inwhoop.ddcj.util;

import android.annotation.SuppressLint;
import android.text.TextUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * 时间工具类
 * 
 * @Project: DDCJ
 * @Title: TimeUtils.java
 * @Package com.inwhoop.ddcj.util
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-7 上午11:32:57
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
@SuppressLint("SimpleDateFormat")
public class TimeUtils {

	/**
	 * 计算两个日期型的时间相差多少时间
	 * 
	 * @param startTime
	 *            开始日期
	 * @param endTime
	 *            结束日期
	 * @return
	 */
	public static String twoDateDistance(String startTime, String endTime) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			Date startDate = df.parse(startTime);
			Date endDate = df.parse(endTime);
			if (startDate == null || endDate == null) {
				return null;
			}
			long timeLong = endDate.getTime() - startDate.getTime();
			if (timeLong / 1000 == 0) {
				return "刚刚";
			} else if (timeLong < 60 * 1000)
				return timeLong / 1000 + "秒前";
			else if (timeLong < 60 * 60 * 1000) {
				timeLong = timeLong / 1000 / 60;
				return timeLong + "分钟前";
			} else if (timeLong < 60 * 60 * 24 * 1000) {
				timeLong = timeLong / 60 / 60 / 1000;
				return timeLong + "小时前";
			} else if (timeLong < 60 * 60 * 24 * 1000 * 7) {
				timeLong = timeLong / 1000 / 60 / 60 / 24;
				return timeLong + "天前";
			} else if (timeLong < 60 * 60 * 24 * 1000 * 7 * 4) {
				timeLong = timeLong / 1000 / 60 / 60 / 24 / 7;
				return timeLong + "周前";
			} else {
				// SimpleDateFormat sdf = new SimpleDateFormat(
				// "yyyy-MM-dd HH:mm:ss");
				// sdf.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
				// return sdf.format(startDate);
				return startTime;
			}
		} catch (Exception e) {
			return startTime;
		}
	}

	/**
	 * 计算两个日期型的时间相差多少时间
	 * 
	 * @param startTime
	 *            开始日期
	 * @param endTime
	 *            结束日期
	 * @return
	 */
	public static String twoDateDistanceToday(String startTime, String endTime) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			Date startDate = df.parse(startTime);
			Date endDate = df.parse(endTime);
			if (startDate == null || endDate == null) {
				return null;
			}
			// twoDateDistanceToday===start==1418982840000
			// twoDateDistanceToday===end== 1419062040000
			// twoDateDistanceToday===timeLong==79320000
			long mon = 60L * 60 * 24 * 1000 * 30;
			long timeLong = endDate.getTime() - startDate.getTime();
			if (endDate.getYear() == startDate.getYear()
					&& endDate.getMonth() == startDate.getMonth()
					&& endDate.getDay() == startDate.getDay()) {
				return "今天 " + startDate.getHours() + ":"
						+ startDate.getMinutes();
			} else {
				if (timeLong < 60 * 60 * 24 * 1000 * 7) {
					timeLong = timeLong / 1000 / 60 / 60 / 24;
					if (timeLong == 0) {
						return "昨天 " + startDate.getHours() + ":"
								+ startDate.getMinutes();
					} else {
						return timeLong + "天前";
					}
				} else if (timeLong < mon) {
					timeLong = timeLong / 1000 / 60 / 60 / 24 / 7;
					return timeLong + "周前";
				} else {
					// SimpleDateFormat sdf = new SimpleDateFormat(
					// "yyyy-MM-dd HH:mm:ss");
					// sdf.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
					// return sdf.format(startDate);
					return startTime;
				}

			}
		} catch (Exception e) {
			System.out.println("ParseException= " + e.toString());
			return startTime;
		}
	}

	/**
	 * 计算两个日期型的时间相差多少时间
	 * 
	 * @param startTime
	 *            开始日期
	 * @param endTime
	 *            结束日期
	 * @return
	 */
	public static String yestodayDistanceToday(String startTime, String endTime) {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		try {
			Date startDate = df.parse(startTime);
			Date endDate = df.parse(endTime);
			if (startDate == null || endDate == null) {
				return null;
			}
			long timeLong = endDate.getTime() - startDate.getTime();
			if (endDate.getYear() == startDate.getYear()
					&& endDate.getMonth() == startDate.getMonth()
					&& endDate.getDay() == startDate.getDay()) {
				return "今天 " + startDate.getHours() + ":"
						+ startDate.getMinutes();
			} else {
				timeLong = timeLong / 1000 / 60 / 60 / 24;
				if (timeLong == 0) {
					return "昨天 " + startDate.getHours() + ":"
							+ startDate.getMinutes();
				} else {
					SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm");
					sdf.setTimeZone(TimeZone.getTimeZone("GMT+08:00"));
					return sdf.format(startDate);
				}

			}
		} catch (Exception e) {
			System.out.println("ParseException= " + e.toString());
			return startTime;
		}
	}

	/**
	 * 将时间戳转为字符串
	 * 
	 * @Title: getStrTime
	 * @Description: TODO
	 * @param @param cc_time
	 * @param @return
	 * @return String
	 */
	public static String getStrTime(long cc_time) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");
		Date curDate = new Date(cc_time);
		return formatter.format(curDate);

	}
}
