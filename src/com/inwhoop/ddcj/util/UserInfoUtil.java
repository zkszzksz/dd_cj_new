package com.inwhoop.ddcj.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.MsgNoticeBean;
import com.inwhoop.ddcj.bean.UserBean;

/**
 * @author dingwenlong
 * @version V1.0
 * @Project: zhixin_2.0
 * @Title:
 * @Package com.inwhoop.zhixin.util
 * @Description: TODO
 * @date 2014/4/11 17:50
 * @Copyright: 2014 成都呐喊信息技术 All rights reserved.
 */
public class UserInfoUtil {

	public static void rememberUserInfo(final Context context,
			final UserBean userInfo) {
		new Thread(new Runnable() {
			@Override
			public void run() {
				rememberUser(context, userInfo);
			}
		}).start();
	}

	private static void rememberUser(Context context, UserBean userInfo) {
		// System.out.println("用户密码+密码111" + userInfo.email + "++" +
		// userInfo.password + "+++" + userInfo.nick);
		SharedPreferences setting = context.getSharedPreferences(
				Configs.SETTING_INFO, 0);
		setting.edit().putString(Configs.USER_ID, userInfo.userid)
				.putString(Configs.NAME, userInfo.name)
				.putString(Configs.PASSWORD, userInfo.pwd)
				.putString(Configs.USER_IMG, userInfo.img)
				.putString(Configs.USER_SINA_ID, userInfo.sinaid)
				.putInt(Configs.USER_SEX, userInfo.sex)
				.putString(Configs.USER_DDID, userInfo.ddid)
				.putString(Configs.USER_QQID, userInfo.qqid)
				.putString(Configs.LOCATION, userInfo.location)
				.putString(Configs.MOBILE, userInfo.tel)
				.putString(Configs.OTHER_INFO, userInfo.otherinfo)
				.putString(Configs.OLD_NORMAL_PWD, userInfo.oldNormalPwd)
				.putString(Configs.USER_WEIXIN_ID, userInfo.wxid)
				// .putString(Configs.BACKGROUND, userInfo.background)
				.commit();
		// System.out.println("保存的用户的身日为" + userInfo.birthday);
	}

	public static UserBean getUserInfo(Context context) {
		UserBean userInfo = new UserBean();
		SharedPreferences setting = context.getSharedPreferences(
				Configs.SETTING_INFO, 0);
		userInfo.userid = setting.getString(Configs.USER_ID, "");
		userInfo.name = setting.getString(Configs.NAME, "");
		userInfo.pwd = setting.getString(Configs.PASSWORD, "");
		userInfo.img = setting.getString(Configs.USER_IMG, "");
		userInfo.sinaid = setting.getString(Configs.USER_SINA_ID, "");
		userInfo.sex = setting.getInt(Configs.USER_SEX, 0);
		userInfo.ddid = setting.getString(Configs.USER_DDID, "");
		userInfo.qqid = setting.getString(Configs.USER_QQID, "");
		userInfo.location = setting.getString(Configs.LOCATION, "");
		userInfo.tel = setting.getString(Configs.MOBILE, "");
		userInfo.otherinfo = setting.getString(Configs.OTHER_INFO, "");
		userInfo.oldNormalPwd=setting.getString(Configs.OLD_NORMAL_PWD,"");
		userInfo.wxid = setting.getString(Configs.USER_WEIXIN_ID, "");
		// userInfo.background = setting.getString(Configs.BACKGROUND, "");
		// userInfo.membername=setting.getString(Configs.MEMBERNAME,"");
		return userInfo;
	}

	public static void clear(Context context) {
		SharedPreferences sp = context.getSharedPreferences(
				Configs.SETTING_INFO, Context.MODE_PRIVATE);
		sp.edit().clear().commit();
	}

	public static void setStealthModeInfo(Context context, int mode) {
		SharedPreferences sp = context.getSharedPreferences(
				Configs.SETTING_INFO, Context.MODE_PRIVATE);
		sp.edit().putInt("SETTING_STEALTHMODE", mode).commit();
	}

	public static int getStealthModeInfo(Context context) {
		SharedPreferences sp = context.getSharedPreferences(
				Configs.SETTING_INFO, Context.MODE_PRIVATE);
		return sp.getInt("SETTING_STEALTHMODE", 0);
	}

	public static void setInfoNotifyMode(Context context, MsgNoticeBean bean) {
		SharedPreferences sp = context.getSharedPreferences(
				Configs.SETTING_INFO, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putInt("CHECK1",bean.checkTag1);
		editor.putInt("CHECK2",bean.checkTag2);
		editor.putInt("CHECK3",bean.checkTag3);
		editor.putInt("CHECK4",bean.checkTag4);
		editor.putInt("CHECK5",bean.checkTag5);
		editor.putInt("CHECK6",bean.checkTag6);
		editor.putInt("CHECK7",bean.checkTag7);
		editor.commit();
	}

	public static MsgNoticeBean  getInfoNotifyMode(Context context) {
		SharedPreferences sp = context.getSharedPreferences(
				Configs.SETTING_INFO, Context.MODE_PRIVATE);
		MsgNoticeBean bean=new MsgNoticeBean();
		bean.checkTag1=sp.getInt("CHECK1",1);
		bean.checkTag2=sp.getInt("CHECK2",1);
		bean.checkTag3=sp.getInt("CHECK3",0);
		bean.checkTag4=sp.getInt("CHECK4",1);
		bean.checkTag5=sp.getInt("CHECK5",1);
		bean.checkTag6=sp.getInt("CHECK6",0);
		bean.checkTag7=sp.getInt("CHECK7",1);
		return bean;

	}
}
