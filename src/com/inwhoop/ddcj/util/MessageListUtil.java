package com.inwhoop.ddcj.util;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.text.SpannableString;
import com.inwhoop.ddcj.bean.*;
import com.inwhoop.ddcj.db.*;
import com.inwhoop.ddcj.xmpp.FaceConversionUtil;


/**
 * 获取消息列表
 *
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: MainActivity
 * @Title: MessageListUtil.java
 * @Package com.inwhoop.gameproduct.utils
 * @Description: TODO
 * @date 2014-6-18 上午9:13:01
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class MessageListUtil {

    private static MessageListUtil messageListUtil = null;

    private HandleFriendchatlistRecordDB userdb = null;

    private HandleFriendchatRecordDB fdb = null;

    private HandleGroupchatRecordDB gdb = null;

    private HandleFriendchatNoReadRecordDB fnodb = null;

    private HandleGroupchatNoReadRecordDB gnodb = null;
    
    private HandleSetGrouptopDB topdb = null;

    private Context mcontext = null;

    public static MessageListUtil getInstance(Context context) {
        if (null == messageListUtil) {
            messageListUtil = new MessageListUtil(context);
        }
        return messageListUtil;
    }

    private MessageListUtil(Context context) {
        mcontext = context;
        userdb = new HandleFriendchatlistRecordDB(context);
        fdb = new HandleFriendchatRecordDB(context);
        gdb = new HandleGroupchatRecordDB(context);
        fnodb = new HandleFriendchatNoReadRecordDB(context);
        gnodb = new HandleGroupchatNoReadRecordDB(context);
        topdb = new HandleSetGrouptopDB(context);
    }

    /**
     * 获取消息列表
     *
     * @param @return
     * @return List<UserMessage>
     * @Title: getUserMsglist
     * @Description: TODO
     */
    public List<UserMessage> getUserMsglist() {
        List<UserMessage> list = new ArrayList<UserMessage>();
        try{
        	 UserBean userinfo = UserInfoUtil.getUserInfo(mcontext);
             List<ChatTagetList> tagetlist = userdb.getAlltagetlist(userinfo.tel);
             tagetlist = getRealList(tagetlist);
             for (int i = 0; i < tagetlist.size(); i++) {
                 UserMessage message = new UserMessage();
                 if (tagetlist.get(i).msgtype == 1) {
                     FriendChatinfo chatinfo = new FriendChatinfo();
                     List<FriendChatinfo> flist = fnodb.getUserBygroupid(tagetlist.get(i).tagetid, mcontext);
                     if (flist.size() > 0) {
                         chatinfo = flist.get(flist.size() - 1);
                         message.number = flist.size();
                     } else {
                         chatinfo = fdb.getfristBygroupid(tagetlist.get(i).tagetid, userinfo.tel);
                         message.number = 0;
                     }
                     if (chatinfo.msgtype == 1) {
                         SpannableString spannableString = FaceConversionUtil.getInstace()
                                 .getExpressionString(mcontext, chatinfo.content);
                         message.info = spannableString.toString();
                     } else if (chatinfo.msgtype == 2) {
                         message.info = "[图片]";
                     } else {
                         message.info = "[音频]";
                     }
                     message.title = tagetlist.get(i).tagetnick;
                     message.logo_url = tagetlist.get(i).tagethead;
                     message.type = "" + 1;
                     message.time = Utils.compareNowdate(chatinfo.time);
                     message.chatid = tagetlist.get(i).tagetid;
                 } else {
                     GroupChatinfo chatinfo = new GroupChatinfo();
                     List<GroupChatinfo> glist = gnodb.getUserBygroupid(tagetlist.get(i).tagetid, mcontext);
                     if (glist.size() > 0) {
                         chatinfo = glist.get(glist.size() - 1);
                         message.number = glist.size();
                     } else {
                         chatinfo = gdb.getfristBygroupid(tagetlist.get(i).tagetid, userinfo.tel);
                         message.number = 0;
                     }
                     if (chatinfo.msgtype == 1) {
                         SpannableString spannableString = FaceConversionUtil.getInstace()
                                 .getExpressionString(mcontext, chatinfo.usernick + ":" + chatinfo.content);
                         message.info = spannableString.toString();
                     } else if (chatinfo.msgtype == 2) {
                         message.info = chatinfo.usernick + ":" + "[图片]";
                     } else {
                         message.info = chatinfo.usernick + ":" + "[音频]";
                     }
                     message.title = chatinfo.groupname;
                     message.logo_url = chatinfo.groupphoto;
                     message.type = "" + 2;

                     message.time = Utils.compareNowdate(chatinfo.time);
                     message.chatid = tagetlist.get(i).tagetid;
                 }
                 list.add(message);
             }
        }catch(Exception e){
        }
        return list;
    }
    
    private  List<ChatTagetList> getRealList(List<ChatTagetList> list){
    	List<String> strl = topdb.getAlltagetlist(UserInfoUtil.getUserInfo(mcontext).tel);
    	for (int i = 0; i < strl.size(); i++) {
			for (int j = 0; j < list.size(); j++) {
				if(strl.get(i).equals(list.get(j).tagetid)){
					ChatTagetList info = list.get(j);
					list.remove(j);
					list.add(0, info);
					break;
				}
			}
		}
    	return list;
    }

}
