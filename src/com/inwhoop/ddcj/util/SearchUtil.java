package com.inwhoop.ddcj.util;

import java.util.ArrayList;
import java.util.List;

import android.os.Handler;

import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.ChannelList;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.search.SearchTask;
import com.inwhoop.ddcj.search.SearchTaskManagerThread;
import com.inwhoop.ddcj.search.SearchTaskMannger;

/**
 * 搜索工具类
 * 
 * @Project: DDCJ
 * @Title: SearchUtil.java
 * @Package com.inwhoop.ddcj.util
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2015-1-23 上午11:13:08
 * @Copyright: 2015 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class SearchUtil {
	private SearchTaskManagerThread searchTaskManagerThread;
	private String[] taskName = { "用户", "群组", "资讯" };
	public static List<UserBean> userList = new ArrayList<UserBean>();
	public static List<CateChannel> groupList = new ArrayList<CateChannel>();
	public static List<ChannelList> newsList = new ArrayList<ChannelList>();

	public SearchUtil() {
		searchTaskManagerThread = new SearchTaskManagerThread();
		new Thread(searchTaskManagerThread).start();
	}

	public void startSearch(String keyword, String uid, Handler handler) {
		// new一个线程管理队列
		SearchTaskMannger searchTaskMannger = SearchTaskMannger.getInstance();
		searchTaskMannger.clear();
		for (int i = 0; i < taskName.length; i++) {
			searchTaskMannger.addSearchTask(new SearchTask(taskName[i],
					keyword, uid, handler));
		}

	}

	public void stop(boolean isbool) {
		searchTaskManagerThread.setStop(isbool);
	}
}
