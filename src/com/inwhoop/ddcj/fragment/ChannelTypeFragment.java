package com.inwhoop.ddcj.fragment;

import java.util.ArrayList;
import java.util.List;

import android.widget.LinearLayout;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.NewsDetailActivity;
import com.inwhoop.ddcj.adapter.ChannelAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ChannelList;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

/**
 * 频道分类界面（沸点财经等）
 *
 * @Project: DDCJ
 * @Title: ChannelTypeFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 *
 * @author ligang@inwhoop.com
 * @date 2014-11-6 下午7:54:51
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class ChannelTypeFragment extends Fragment implements
		IXListViewListener, OnItemClickListener {

	private static final String ARG_POSITION = "position";

	private static final String CHANNEL_ID = "channel_id";

	private Context mContext;

	private int position;

	private int channelId;

	private String channelName;

	private XListView mListView;

	private List<ChannelList> dataList = new ArrayList<ChannelList>();
	private List<ChannelList> tempDataList = new ArrayList<ChannelList>();
	private ChannelAdapter adapter;

	private boolean isInit; // 是否可以开始加载数据

	private boolean isLoadMore = false;

	private int index = 0;
	private LinearLayout progressLinear;

	public static ChannelTypeFragment newInstance(int position, int channelId) {
		ChannelTypeFragment cFragment = new ChannelTypeFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		b.putInt(CHANNEL_ID, channelId);
		cFragment.setArguments(b);
		return cFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		isInit = true;
		mContext = getActivity();
		position = getArguments().getInt(ARG_POSITION);
		channelId = getArguments().getInt(CHANNEL_ID);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.channel_list, null);
		init(view);
		return view;
	}

	private void init(View view) {
		view.findViewById(R.id.title_layout).setVisibility(View.GONE);
		mListView = (XListView) view.findViewById(R.id.xListView);
		progressLinear=(LinearLayout)view.findViewById(R.id.progress_linear);
		progressLinear.setVisibility(View.VISIBLE);
		mListView.setPullLoadEnable(false);
		mListView.setPullRefreshEnable(true);
		mListView.setXListViewListener(this);
		mListView.setOnItemClickListener(this);
		adapter = new ChannelAdapter(getActivity());
		mListView.setAdapter(adapter);
	}

	private void getData() {
		new Thread(new Runnable() {

			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				String uid = UserInfoUtil.getUserInfo(mContext).userid;
				try {
					Object[] data = JsonUtils.getNewsList(position, uid,
							channelId + "", Configs.COUNT, index);
					if ((Boolean) data[0]) {
						dataList = (List<ChannelList>) data[2];
						tempDataList.addAll(dataList);
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}

			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			progressLinear.setVisibility(View.GONE);
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				if (dataList.size() >= Configs.COUNT) {
					mListView.setPullLoadEnable(true);
				} else {
					mListView.setPullLoadEnable(false);
				}
				adapter.setPosition(position);
				adapter.addChannelList(dataList, isLoadMore);
				adapter.notifyDataSetChanged();
				break;

			case Configs.READ_FAIL:
				break;
			}
		}
	};

	private void onLoad() {
		mListView.stopRefresh();
		mListView.stopLoadMore();
		mListView.setRefreshTime("刚刚");
	}

	private void showData() {
		if (isInit) {
			isInit = false;// 加载数据完成
			// 加载各种数据
			getData();
		}
	}

	@Override
	public void onRefresh() {
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				isLoadMore = false;
				index = 0;
				tempDataList.clear();
				getData();
				onLoad();
			}
		}, 2000);

	}

	@Override
	public void onLoadMore() {
		handler.postDelayed(new Runnable() {

			@Override
			public void run() {
				isLoadMore = true;
				try {
					index = Integer.valueOf(dataList.get(dataList.size() - 1).id);
				} catch (Exception e) {
				}
				getData();
				onLoad();
			}
		}, 2000);

	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(getActivity(), NewsDetailActivity.class);
		position -= 1;
		if (position >= 0 && position < tempDataList.size()) {
			intent.putExtra("news_id", tempDataList.get(position).id);
			intent.putExtra("channel_id", tempDataList.get(position).channelid);
			intent.putExtra("channel_name", tempDataList.get(position).channelname);
			intent.putExtra("news", tempDataList.get(position));
			getActivity().startActivity(intent);
		}

	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			showData();
		}
	}
}
