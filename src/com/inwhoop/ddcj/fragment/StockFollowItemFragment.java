package com.inwhoop.ddcj.fragment;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;


import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inwhoop.R;
import com.inwhoop.ddcj.activity.PersonInfoActivity;
import com.inwhoop.ddcj.activity.StockInfoActivity;
import com.inwhoop.ddcj.activity.StockInfoActivity.OnLoadMoreListener;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.StockUserInfo;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.TimeUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.CircleImageview;

/**
 * 同时他们也在关注
 * 
 * @Project: DDCJ
 * @Title: StockFollowItemFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-12-18 上午10:49:28
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class StockFollowItemFragment extends BaseFragment implements
		OnLoadMoreListener {
	private View mainView;
	private LinearLayout contentlayout;
	private List<StockUserInfo> list = new ArrayList<StockUserInfo>();

	private LayoutInflater inflater = null;

	private boolean isOver = false;

//	private KJBitmap kjBitmap = null;
	
	private FinalBitmap fb;

	private int position = 0;

	public StockFollowItemFragment(int position) {
		this.position = position;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		mainView = inflater.inflate(R.layout.stock_follow_item_layout, null);
		contentlayout = (LinearLayout) mainView.findViewById(R.id.listlayout);
		inflater = LayoutInflater.from(getActivity());
//		kjBitmap = KJBitmap.create();
		fb=FinalBitmap.create(mContext);
		fb.configLoadingImage(R.drawable.qingbao_loading_img);
		fb.configLoadfailImage(R.drawable.qingbao_loading_img);
		setOnloadMorelistener();
		read(-1);
		return mainView;
	}

	public void setOnloadMorelistener() {
		((StockInfoActivity) getActivity()).setLoadMoreListener(this);
		if (isOver) {
			((StockInfoActivity) getActivity()).kjListView
					.setPullLoadEnable(false);
		} else {
			((StockInfoActivity) getActivity()).kjListView
					.setPullLoadEnable(true);
		}
	}

	private void read(final int id) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = JsonUtils.getFollowlist(
							UserInfoUtil.getUserInfo(getActivity()).userid,
							((StockInfoActivity) getActivity()).stockBean.stockcode,
							id, 10, position + 1);
					msg.obj = obj;
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			((StockInfoActivity) getActivity()).kjListView.stopLoadMore();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					@SuppressWarnings("unchecked")
					List<StockUserInfo> newlist = (List<StockUserInfo>) obj[2];
					if (null != newlist) {
						for (int i = 0; i < newlist.size(); i++) {
							list.add(newlist.get(i));
						}
						if (newlist.size() < 10) {
							isOver = true;
							((StockInfoActivity) getActivity()).kjListView
									.setPullLoadEnable(false);
						}
						setContentlayout(newlist);
					}
				} else {
					isOver = true;
					((StockInfoActivity) getActivity()).kjListView
							.setPullLoadEnable(false);
					showToast("" + obj[1].toString());
				}
				break;

			case Configs.READ_FAIL:
				isOver = true;
				((StockInfoActivity) getActivity()).kjListView
						.setPullLoadEnable(false);
				break;

			default:
				break;
			}
		};
	};

	private void setContentlayout(final List<StockUserInfo> list) {
		for (int i = 0; i < list.size(); i++) {
			StockUserInfo info = list.get(i);
			if (null != info) {
				View view = LayoutInflater.from(getActivity()).inflate(
						R.layout.gf_list__item_layout, null);
				view.setTag(i);
				view.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						int pos = (Integer) v.getTag();
						Intent intent = new Intent(getActivity(), PersonInfoActivity.class);
						intent.putExtra("userid", list.get(pos).userid);
						startActivity(intent);
					}
				});
				CircleImageview img = (CircleImageview) view
						.findViewById(R.id.headimg);
				if(list.get(i).img.startsWith("http://")){
					fb.display(img, list.get(i).img);
				}else{
					fb.display(img, Configs.HOST +list.get(i).img);
				}
				
				TextView name = (TextView) view.findViewById(R.id.nametv);
				TextView time = (TextView) view.findViewById(R.id.timetv);
				TextView content = (TextView) view.findViewById(R.id.content);
				name.setText(list.get(i).name);
				time.setText(""
						+ TimeUtils.twoDateDistance(
								list.get(i).createtime,
								TimeUtils.getStrTime(System.currentTimeMillis())));
				content.setTag(list.get(i).othreinfo);
				contentlayout.addView(view);
			}
		}
	}

	@Override
	public void onLoadMore() {
		read(list.get(list.size() - 1).id);
	}

}
