package com.inwhoop.ddcj.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.inwhoop.R;

/**
 * @Describe: TODO * * * ****** Created by ZK ********
 * @Date: 2014/10/20 17:59
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public abstract class BaseFragment extends Fragment {

	protected ProgressDialog progressDialog = null;
	protected Context mContext;
	protected View title_left_layout, title_second_right_layout,
			title_right_layout;
	protected TextView title_left_tv, title_center_tv, title_second_right_tv,
			title_right_tv;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = getActivity();
		// CrashHandler.create(getActivity());
	}

	public void initData(View view) {
		title_left_layout = view.findViewById(R.id.title_left_layout);
		title_right_layout = view.findViewById(R.id.title_right_layout);
		title_second_right_layout = view
				.findViewById(R.id.title_right_second_layout);

		title_left_tv = (TextView) view.findViewById(R.id.title_left_tv);
		title_center_tv = (TextView) view.findViewById(R.id.title_center_tv);
		title_right_tv = (TextView) view.findViewById(R.id.title_right_tv);
		title_second_right_tv = (TextView) view
				.findViewById(R.id.title_right_second_tv);
	}

	/**
	 * TODO 设置左按钮,目前设定了固定正方形大小，放文字会有问题，若有文字到时候再说
	 * 
	 * @param resId
	 *            资源id。文字的或者图片的
	 * @param isText
	 *            判断是文字还是图片
	 */
	protected void setLeftLayout(int resId, boolean isText) {
		title_left_layout.setVisibility(View.VISIBLE);
		if (isText)
			title_left_tv.setText(resId);
		else
			title_left_tv.setBackgroundResource(resId);
	}

	/**
	 * TODO 设置倒数第二个右按钮
	 * 
	 * @param resId
	 *            资源id。文字的或者图片的
	 * @param isText
	 *            判断是文字还是图片
	 */
	protected void setSecondRightLayout(int resId, boolean isText) {
		title_second_right_layout.setVisibility(View.VISIBLE);
		if (isText)
			title_second_right_tv.setText(resId);
		else
			title_second_right_tv.setBackgroundResource(resId);
	}

	/**
	 * TODO 设置右按钮
	 * 
	 * @param resId
	 *            资源id。文字的或者图片的
	 * @param isText
	 *            判断是文字还是图片
	 */
	protected void setRightLayout(int resId, boolean isText) {
		title_right_layout.setVisibility(View.VISIBLE);
		if (isText)
			title_right_tv.setText(resId);
		else
			title_right_tv.setBackgroundResource(resId);
	}

	/**
	 * TODO 设置中间
	 * 
	 * @param resId
	 *            资源id。文字
	 */
	protected void setTitleResText(int resId) {
		title_center_tv.setVisibility(View.VISIBLE);
		title_center_tv.setText(resId);
	}

	/**
	 * TODO 设置中间
	 * 
	 * @param resId
	 *            资源id。文字
	 */
	protected void setTitleTextStr(String resId) {
		title_center_tv.setVisibility(View.VISIBLE);
		title_center_tv.setText(resId);
	}

	/**
	 * 显示toast
	 * 
	 * @Title: showToast
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showToast(String msg) {
		Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
	}

	/**
	 * 显示进度框
	 * 
	 * @Title: showProgressDialog
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showProgressDialog(String msg) {
		if (null == progressDialog) {
			progressDialog = new ProgressDialog(mContext);
		}
		progressDialog.setMessage(msg);
		progressDialog.setCancelable(true);
		progressDialog.show();
	}

	/**
	 * 显示进度框
	 * 
	 * @Title: showProgressDialog
	 * @Description: TODO
	 * @param @param msg
	 * @return void
	 */
	protected void showProgressDialogFalse(String msg) {
		if (null == progressDialog) {
			progressDialog = new ProgressDialog(mContext);
		}
		progressDialog.setMessage(msg);
		progressDialog.setCancelable(false);
		progressDialog.show();
	}

	/**
	 * 关闭进度框
	 * 
	 * @Title: dismissProgressDialog
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	protected void dismissProgressDialog() {
		if (null != progressDialog && progressDialog.isShowing()) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}

}
