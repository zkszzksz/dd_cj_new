package com.inwhoop.ddcj.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.LoginActivity;
import com.inwhoop.ddcj.activity.PersonInfoActivity;
import com.inwhoop.ddcj.adapter.ContactsAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ContactsInfo;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.List;

/**
 * 关注或者粉丝
 * 
 * @author ZOUXU
 */
public class ContactMiserWhatchAndFansFragment extends Fragment implements
		IXListViewListener {

	/**
	 * 财迷列表
	 */
	private XListView lv_misers = null;
	private ViewGroup contents = null;
	private TextView title;
	private TextView title2;
	int num = 0;
	private int flag = 0;
	private Context context;
	private UserBean userBean;
	private List<ContactsInfo> list = new ArrayList<ContactsInfo>();
	private List<ContactsInfo> list1 = new ArrayList<ContactsInfo>();
	private ContactsAdapter friends_adapter;
	private int position = 0;
	private int page = 1;
	private int page1 = 1;
	private boolean load = true;
	private boolean load1 = true;
	private LinearLayout progressLinear;
	private LinearLayout alertLogin;
	private String ddid;
    private boolean isInit_1=true;

	public ContactMiserWhatchAndFansFragment(int postion) {
		this.position = postion;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		context = getActivity();
		contents = (ViewGroup) inflater.inflate(
				R.layout.frag_my_contacts_son_groups, null);
		initComponet();
		return contents;

	}

	private void initComponet() {
		lv_misers = (XListView) contents.findViewById(R.id.lv_group);
		lv_misers.setPullLoadEnable(true);
		lv_misers.setXListViewListener(this);
		alertLogin = (LinearLayout) contents.findViewById(R.id.alert_login);
		progressLinear = (LinearLayout) contents
				.findViewById(R.id.progress_linear);
		progressLinear.setVisibility(View.VISIBLE);
		friends_adapter = new ContactsAdapter(context);
		lv_misers.setAdapter(friends_adapter);
		// if (position == 0) {
		// // 关注
		// getMiserData();
		// } else if (position == 1) {
		// // 粉丝
		// getMyFanceData();
		// }

		lv_misers.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int i, long l) {
				ContactsInfo info = friends_adapter.getList().get(i - 1);
				Intent intent = new Intent(context, PersonInfoActivity.class);
				intent.putExtra("userid", info.userid);
				startActivity(intent);
			}
		});
	}

	/**
	 * 传入窗体标题栏的引用
	 * 
	 * @param title
	 */
	public void setTitleAndFlag(TextView title, TextView title2, int flag) {
		this.title = title;
		this.title2 = title2;
		this.flag = flag;
	}

	@Override
	public void onRefresh() {
		if (flag == 0) {
			// 关注
			page = 1;
			load = false;
			getMiserData();
		} else if (flag == 1) {
			// 粉丝
			page1 = 1;
			load1 = false;
			getMyFanceData();
		}
	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub
		if (flag == 0) {
            // 关注
            page++;
            load = true;
            getMiserData();
        } else if (flag == 1) {
            // 粉丝
            page1++;
            load1 = true;
            getMyFanceData();
        }
    }

	private void setActivityTitle() {
        num = friends_adapter.getAllSize();
        if (flag == 0) {
            // 关注
            if (!isHidden())
                title.setText("关注(" + num + ")");
            title2.setText("关注(" + num + ")");
        } else if (flag == 1) {
            // 粉丝
            if (!isHidden())
                title.setText("粉丝(" + num + ")");
            title2.setText("粉丝(" + num + ")");
        }
    }

    /*
    * 我的关注接口
    */
	public void getMiserData() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					userBean = UserInfoUtil.getUserInfo(context);
//                    if (!load)
//					    list.clear();
					list = JsonUtils.myConcern(userBean.userid, Configs.COUNT,
							page);
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
					e.printStackTrace();
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			lv_misers.stopRefresh();
			lv_misers.stopLoadMore();
			progressLinear.setVisibility(View.GONE);
			switch (msg.what) {
			case Configs.READ_SUCCESS:
                if (list.size() < Configs.COUNT) {
                    lv_misers.setPullLoadEnable(false);
                } else
                    lv_misers.setPullLoadEnable(true);
                friends_adapter.addList(list, load);
				friends_adapter.notifyDataSetChanged();
				setActivityTitle();
				break;
			case Configs.READ_FAIL:
                setActivityTitle();
                lv_misers.setPullLoadEnable(false);
                break;
			}
		}
	};

	/*
	 * 关注我的接口
	 */
	public void getMyFanceData() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					userBean = UserInfoUtil.getUserInfo(context);
					list1 = JsonUtils.MyFance(userBean.userid, Configs.COUNT,
							page1);
                    msg.what = Configs.READ_SUCCESS;
                } catch (Exception e) {
					e.printStackTrace();
                    msg.what = Configs.READ_FAIL;
				}
				mhandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler mhandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			lv_misers.stopRefresh();
			lv_misers.stopLoadMore();
			progressLinear.setVisibility(View.GONE);
			switch (msg.what) {
			case Configs.READ_SUCCESS:
                if (list1.size() < Configs.COUNT) {
                    lv_misers.setPullLoadEnable(false);
                } else
                    lv_misers.setPullLoadEnable(true);
                friends_adapter.addList(list1, load1);
				friends_adapter.notifyDataSetChanged();
				setActivityTitle();
				break;
			case Configs.READ_FAIL:
                setActivityTitle();
                lv_misers.setPullLoadEnable(false);
                break;
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		ddid = UserInfoUtil.getUserInfo(context).ddid;
		if (TextUtils.isEmpty(ddid)) {
			alertLogin.setVisibility(View.VISIBLE);
			lv_misers.setVisibility(View.GONE);
			progressLinear.setVisibility(View.GONE);
			alertLogin.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Act.toAct(context, LoginActivity.class);
				}
			});
			return;
		} else {
			alertLogin.setVisibility(View.GONE);
			lv_misers.setVisibility(View.VISIBLE);
            if (Configs.isCareChage&&flag==0) {
                progressLinear.setVisibility(View.VISIBLE);
                page = 1;
                load = false;
                getMiserData();
                Configs.isCareChage = false;
            }
            if (isInit_1&&flag==1) {
                progressLinear.setVisibility(View.VISIBLE);
                page1 = 1;
                load1 = false;
                getMyFanceData();
                isInit_1 = false;
            }
        }

	}
}
