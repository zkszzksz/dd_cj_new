package com.inwhoop.ddcj.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.LoginActivity;
import com.inwhoop.ddcj.activity.MyStockManagerActivity;
import com.inwhoop.ddcj.activity.MyStockSearchActivity;
import com.inwhoop.ddcj.activity.SettingActivity;
import com.inwhoop.ddcj.activity.StartActivity;
import com.inwhoop.ddcj.adapter.MyStockListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.HVListView;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO 自选股 * * * ****** Created by ZK ********
 * @Date: 2014/10/20 17:59
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class Tab0MyStockCertificateFragment extends BaseFragment implements
        View.OnClickListener {
    private View mainView;
    private HVListView listview;
    private MyStockListAdapter adapter;
    private UserBean user;
    private String searchCodeStrs = "";
    private List<StockInfoBean> dbOrNetList = new ArrayList<StockInfoBean>();// 读取的本地或服务器下来的自选股，只有code和name
    private boolean isRunning = true; // 是否在resume里跑线程重新查询
    private boolean isUpdate = false;// 是否在重新拉取股票
    private boolean isSinaUpdate = false;// 是否在访问sina查询
    private LinearLayout progressLinear;

    @Override
    public void onResume() {
        super.onResume();
        readData();
    }

    private Handler handler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Configs.READ_FAIL:
                    isSinaUpdate = false;
                    break;

                case Configs.READ_SUCCESS:
                    if ("".equals(searchCodeStrs)) {
                        isSinaUpdate = false;
                        return;
                    }
                    List<StockInfoBean> result = (List<StockInfoBean>) msg.obj;
                    if (result.size() != 0) {
                        for (int i = 0; i < result.size()
                                && i < adapter.getList().size(); i++) {
                            // [{"userid":"6","id":"281","stockcode":"sh601007","orderid":"0","notice":1,"location":"成都市","aliasname":"jz"},{"userid":"6","id":"262","stockcode":"sh601006","orderid":"1","notice":1,"location":"成都市","aliasname":"dqtl"}]
                            adapter.getList().get(i).stockname = result.get(i).stockname;
                            adapter.getList().get(i).nowDayOP = result.get(i).nowDayOP;
                            adapter.getList().get(i).yestprice = result.get(i).yestprice;
                            adapter.getList().get(i).nowPrice = result.get(i).nowPrice;
                            adapter.getList().get(i).highPrice = result.get(i).highPrice;
                            adapter.getList().get(i).lowPrice = result.get(i).lowPrice;
                            adapter.getList().get(i).addPrice = result.get(i).addPrice;
                            adapter.getList().get(i).swing = result.get(i).swing;
                            adapter.getList().get(i).addRate = result.get(i).addRate;
                            adapter.getList().get(i).volume = result.get(i).volume;
                            adapter.getList().get(i).total = result.get(i).total;

                            adapter.getList().get(i).focus = 1;// 标识是自选股
                        }
                        // adapter.getList().clear();
                        adapter.notifyDataSetChanged();
                        isSinaUpdate = false;
                    }
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mContext = getActivity();
        mainView = inflater.inflate(R.layout.frag_tab0_my_shares, null);
        user = UserInfoUtil.getUserInfo(mContext);
        initData();
        return mainView;
    }

    private void readData() {
        StockUtil.getStockInfoList(nethandler, mContext);
    }

    private void readDataFromSina() {
        isRunning = true;
        new Thread(new Runnable() {

            @Override
            public void run() {
                while (isRunning) {
                    Message msg = new Message();
                    try {
                        // 先获取最新自选股后，再去xinna查询具体股票
                        msg.obj = JsonUtils.getStockInfoList(searchCodeStrs);
                        msg.what = Configs.READ_SUCCESS;
                        handler.sendMessage(msg);
                    } catch (Exception e) {
                        // LogUtil.e("exception,tab0:" + e.toString()); //经常异常
                        // java.lang.StringIndexOutOfBoundsException: length=0;
                        // regionStart=0; regionLength=1
                        msg.what = Configs.READ_FAIL;
                        handler.sendMessage(msg);
                    }
                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    private Handler nethandler = new Handler() {
        @SuppressWarnings("unchecked")
        public void handleMessage(android.os.Message msg) {
            // searchCodeStrs = "sh601006,sh601001,sh600601,sh601106";
            progressLinear.setVisibility(View.GONE);
            switch (msg.what) {
                case 1: // success
                    Object[] obj = (Object[]) msg.obj;
                    searchCodeStrs = "";
                    if ((Boolean) obj[0]) {
                        dbOrNetList.clear();
                        dbOrNetList = (List<StockInfoBean>) obj[2];
                        for (int i = 0; i < dbOrNetList.size(); i++) {
                            dbOrNetList.get(i).stockname = "未知股票";
                            if (i == (dbOrNetList.size() - 1)) {
                                searchCodeStrs += dbOrNetList.get(i).stockcode;
                            } else {
                                searchCodeStrs = searchCodeStrs
                                        + dbOrNetList.get(i).stockcode + ",";
                            }
                        }
                        adapter.getList().clear();
                        adapter.add(dbOrNetList);
                        adapter.notifyDataSetChanged();
                        isSinaUpdate = true;
                        isUpdate = true;
                        readDataFromSina();
                    } else {
                        if (obj[1].toString().contains("未添加")) {
                            adapter.getList().clear();
                            adapter.notifyDataSetChanged();
                        }
                        if (!"nostockcode".equals(Utils.getpreference(getActivity(), "nodata"))) {
                            Utils.savePreference(getActivity(), "nodata", "nostockcode");
                            ToastUtils.showShort(mContext, obj[1] + "");
                        }
                        isUpdate = false;
                    }
                    break;
                case 0:// wrong
                    LogUtil.i("myStock,wrong:" + msg.obj);
                    isUpdate = false;
                    // ToastUtils.showShort(mContext, msg.obj + "");
                    break;
            }
        }
    };

    private void initData() {
        super.initData(mainView);
        setLeftLayout(R.drawable.more_btn_selector, false);
        setTitleResText(R.string.zixuangu);
        setRightLayout(R.drawable.search_btn_selector, false);
        listview = (HVListView) mainView.findViewById(R.id.listview);
        // 设置列头
        listview.mListHead = (LinearLayout) mainView.findViewById(R.id.head);
        adapter = new MyStockListAdapter(mContext, listview);
        listview.setAdapter(adapter);

        mainView.findViewById(R.id.title_left_layout).setOnClickListener(this);
        mainView.findViewById(R.id.title_right_layout).setOnClickListener(this);
        mainView.findViewById(R.id.title_right_layout22).setOnClickListener(
                this);

        progressLinear = (LinearLayout) mainView.findViewById(R.id.progress_linear);
        progressLinear.setVisibility(View.VISIBLE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            // Act.toAct(mContext, IntellDetailsActivity.class);
            case R.id.title_left_layout: // 设置
//            	String ddid = UserInfoUtil.getUserInfo(mContext).ddid;
//    			if (TextUtils.isEmpty(ddid)) {
//    				Act.toAct(mContext, StartActivity.class);
//    			} else {
    				Act.toAct(mContext, SettingActivity.class);
//    			}
                break;
            case R.id.title_right_layout: // 搜索
                Act.toAct(mContext, MyStockSearchActivity.class);
                break;
            case R.id.title_right_layout22: // 管理
                if (adapter.getList().size() == 0) {
                    showToast("您未添加任何自选股，无法管理");
                    return;
                }
                if (isUpdate && isSinaUpdate) {
                    showToast("数据更新中，请稍后");
                    return;
                }
                Intent intent = new Intent(mContext, MyStockManagerActivity.class);
                intent.putExtra("list", adapter.getList());
                startActivity(intent);
                break;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        isRunning = false;
    }

    @Override
    public void onPause() {
        super.onPause();
        isRunning = false;
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);
        if (hidden) {
            isRunning = false;
        }
    }

}
