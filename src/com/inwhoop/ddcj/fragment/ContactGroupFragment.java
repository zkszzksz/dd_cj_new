package com.inwhoop.ddcj.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.GroupChatActivity;
import com.inwhoop.ddcj.activity.LoginActivity;
import com.inwhoop.ddcj.adapter.ContactGroupsAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.ContactGroupsInfo;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import java.io.Serializable;
import java.util.List;

/**
 * 联系人里的的 群组
 */
public class ContactGroupFragment extends Fragment implements
		IXListViewListener {

	private XListView lv_groups = null;
	private ViewGroup contents = null;
	private List<ContactGroupsInfo> mData = null;
	private TextView title;
	private Context context;
	public int num;
	private UserBean userBean;
	private List<CateChannel> list;
	private ContactGroupsAdapter groups_adapter;
	private LinearLayout progressLinear;
	private LinearLayout alertLogin;
	private String ddid;
	private boolean isInit = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		context = getActivity();
		title = ((Tab4ContactsFragment) getParentFragment()).title;
		this.contents = (ViewGroup) inflater.inflate(
				R.layout.frag_my_contacts_son_groups, null);

		initComponet(inflater);
		return this.contents;

	}

	private void initComponet(LayoutInflater inflater) {
		userBean = UserInfoUtil.getUserInfo(context);
		groups_adapter = new ContactGroupsAdapter(context);
		alertLogin = (LinearLayout) contents.findViewById(R.id.alert_login);
		progressLinear = (LinearLayout) contents
				.findViewById(R.id.progress_linear);
		progressLinear.setVisibility(View.VISIBLE);
		lv_groups = (XListView) contents.findViewById(R.id.lv_group);
		lv_groups.setPullLoadEnable(false);
		lv_groups.setXListViewListener(this);
		lv_groups.setAdapter(groups_adapter);
		// getGroupData();

		lv_groups.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> adapterView, View view,
					int i, long l) {
				List<CateChannel> lists = groups_adapter.getAllList();
				Intent intent = new Intent(context, GroupChatActivity.class);
				intent.putExtra("groupid", lists.get(i - 1).id);
				intent.putExtra("groupname", lists.get(i - 1).name);
				intent.putExtra("groupphoto", lists.get(i - 1).img);
				intent.putExtra("listobj", (Serializable) lists);
				// intent.putExtra("isCreate",1);
				startActivity(intent);
			}
		});
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		// new hideHeadViewAsyncTask().execute();
		getGroupData();
	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub

	}

	private void getGroupData() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getUserGroupList(userBean.userid);
					if (null != list && list.size() > 0) {
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
				} catch (Exception e) {
					e.printStackTrace();
					msg.what = Configs.READ_ERROR;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			lv_groups.stopRefresh();
			progressLinear.setVisibility(View.GONE);
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				num = list.size();
				title.setText("群组(" + num + ")");
				groups_adapter.addList(list);
				groups_adapter.notifyDataSetChanged();
				break;
			case Configs.READ_FAIL:
				Toast.makeText(context, "你暂未加入任何群", Toast.LENGTH_SHORT).show();
				break;
			case Configs.READ_ERROR:
				Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		ddid = UserInfoUtil.getUserInfo(context).ddid;
		if (TextUtils.isEmpty(ddid)) {
			alertLogin.setVisibility(View.VISIBLE);
			lv_groups.setVisibility(View.GONE);
			progressLinear.setVisibility(View.GONE);
			alertLogin.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Act.toAct(context, LoginActivity.class);
				}
			});
			return;
		} else {
			alertLogin.setVisibility(View.GONE);
			lv_groups.setVisibility(View.VISIBLE);
			if (isInit) {
				progressLinear.setVisibility(View.VISIBLE);
				getGroupData();
				isInit = false;
			}
		}

	}

}
