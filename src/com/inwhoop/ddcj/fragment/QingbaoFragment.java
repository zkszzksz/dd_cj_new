package com.inwhoop.ddcj.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.AdapterView.OnItemClickListener;

import com.inwhoop.R;
import com.inwhoop.ddcj.activity.IntellDetailsActivity;
import com.inwhoop.ddcj.activity.IntelligenceListActivity;
import com.inwhoop.ddcj.activity.LoginActivity;
import com.inwhoop.ddcj.adapter.QingbaoListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.NewsBean;
import com.inwhoop.ddcj.bean.QingbaoInfo;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

/**
 * 资讯频道
 * 
 * @Project: DDCJ
 * @Title: ChannelFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-11-6 下午3:27:56
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class QingbaoFragment extends BaseFragment implements
		IXListViewListener, MLocationLinsener {

	private XListView listView = null;
	private List<NewsBean> list = new ArrayList<NewsBean>();
	private QingbaoListAdapter adapter = null;

	private int position = 0;

	private String address = "";

	private LinearLayout alertLogin;

	private String ddid;

	private boolean isInit = true;
	private boolean isInit_2 = true;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.same_city_qb_info_layout, null);
		mContext = getActivity();
		init(view);
		return view;
	}

	public QingbaoFragment(int position) {
		this.position = position;
	}

	public void init(View view) {
		ddid = UserInfoUtil.getUserInfo(mContext).ddid;
		listView = (XListView) view.findViewById(R.id.qblist);
		listView.setXListViewListener(this);
		listView.setPullLoadEnable(false);
		alertLogin = (LinearLayout) view.findViewById(R.id.alert_login);
		adapter = new QingbaoListAdapter(mContext, list);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				if (position == 0) {
					return;
				}
				Intent intent = new Intent(mContext,
						IntellDetailsActivity.class);
				// Bundle bundle = new Bundle();
				// bundle.putSerializable("info",
				// adapter.getAll().get(position-1));
				// intent.putExtras(bundle);
				intent.putExtra("newsid", adapter.getAll().get(position - 1).id);
				startActivity(intent);
			}
		});
		alertLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				 Act.toAct(mContext, LoginActivity.class);
//				Intent intent = new Intent(getActivity(), LoginActivity.class);
//				getActivity();
//				startActivityForResult(intent, Activity.RESULT_FIRST_USER);
			}
		});
		showProgressDialog("正在加载数据...");
		// swithread();
	}

	public void swithread() {
		if (position == 0) {
			((IntelligenceListActivity) getActivity()).startLocation(this);
		} else {
			read(-1);
		}
	}

	private void read(final int id) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = null;
					switch (position) {
					case 0:
						obj = JsonUtils.searchNewsForLocation(
								UserInfoUtil.getUserInfo(mContext).userid,
								address, id, 10, 1);
						msg.what = Configs.READ_SUCCESS;
						break;
					case 1:
						obj = JsonUtils.searchNews(
								UserInfoUtil.getUserInfo(mContext).userid, id,
								10, 2);
						msg.what = Configs.READ_SUCCESS;
						break;
					case 2:
						obj = JsonUtils.searchNews(
								UserInfoUtil.getUserInfo(mContext).userid, id,
								10, 3);
						msg.what = Configs.READ_SUCCESS;
						break;

					default:
						break;
					}
					msg.obj = obj;
					msg.arg1 = id;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			listView.stopLoadMore();
			listView.stopRefresh();
			switch (msg.what) {
			case Configs.READ_FAIL:

				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					List<NewsBean> nblist = (List<NewsBean>) obj[2];
					if (nblist.size() < 10) {
						listView.setPullLoadEnable(false);
					} else {
						listView.setPullLoadEnable(true);
					}
					if (msg.arg1 == -1) {
						adapter.getAll().clear();
					}
					adapter.addList((List<NewsBean>) obj[2]);
					adapter.notifyDataSetChanged();
				} else {
					showToast(obj[1].toString());
				}
				break;

			default:
				break;
			}

		};
	};

	@Override
	public void onRefresh() {
		read(-1);
	}

	@Override
	public void onLoadMore() {
		read(adapter.getAll().get(adapter.getAll().size() - 1).id);
	}

	public List<QingbaoInfo> getQingbaoInfos() {
		List<QingbaoInfo> list = new ArrayList<QingbaoInfo>();
		QingbaoInfo info = null;
		for (int i = 0; i < 10; i++) {
			info = new QingbaoInfo();
			if (i % 4 == 0) {
				info.imglist.add("");
				info.imglist.add("");
				info.imglist.add("");
			} else if (i % 4 == 2) {
				info.imglist.add("");
				info.imglist.add("");
			} else {
				info.imglist.add("");
			}
			list.add(info);
		}
		return list;
	}

	@Override
	public void onResume() {
		super.onResume();
		ddid = UserInfoUtil.getUserInfo(mContext).ddid;
		if (TextUtils.isEmpty(ddid)) {
			if (position > 0) {
				alertLogin.setVisibility(View.VISIBLE);
				listView.setVisibility(View.GONE);
				dismissProgressDialog();
				return;
			}else{
				if (isInit) {
					swithread();
					isInit = false;
				}
			}
		} else {
			alertLogin.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			if (isInit_2) {
				swithread();
				isInit_2 = false;
			}
		}
	}

	@Override
	public void mLocationSucess(String address) {
		this.address = address;
		read(-1);
	}

	@Override
	public void mLocationFaile(String errorInfo) {

	}

}
