package com.inwhoop.ddcj.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.inwhoop.R;
import com.inwhoop.ddcj.activity.SearchAllActivity;
import com.inwhoop.ddcj.activity.SettingActivity;
import com.inwhoop.ddcj.util.Act;

/**
 * @Describe: TODO 联系人 * * * ****** Created by ZOUXU ********
 * @Date: 2014/11/7 15:00
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class Tab4ContactsFragment extends Fragment implements OnClickListener {

	private FragmentManager fm = null;
	private FragmentTransaction ft = null;
	private Fragment[] contacts = { null, null, null, null };

	private RadioGroup rg_tab;
	private View view = null;

	// 标题栏
	TextView title;
	private TextView title_right_tv;
	private TextView title_second_right_tv;
	private TextView haoyou;
	private TextView qunzu;
	private TextView caimi;
	private TextView tuijian;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		fm = getChildFragmentManager();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.frag_tab4_my_contacts, null);
		initComponet(inflater);
		loadFragment(0);
		return view;

	}

	private void initComponet(LayoutInflater inflater) {
		title = (TextView) view.findViewById(R.id.title_center_tv);

		title_right_tv = (TextView) view.findViewById(R.id.title_right_tv);
		title_right_tv.setClickable(true);
		title_right_tv.setOnClickListener(this);

		title_second_right_tv = (TextView) view
				.findViewById(R.id.title_second_right_tv);
		title_second_right_tv.setClickable(true);
		title_second_right_tv.setOnClickListener(this);

		haoyou = (TextView) view.findViewById(R.id.contacts_haoyou);
		qunzu = (TextView) view.findViewById(R.id.contacts_qunzu);
		caimi = (TextView) view.findViewById(R.id.contacts_caimi);
		tuijian = (TextView) view.findViewById(R.id.contacts_tuijian);

		haoyou.setOnClickListener(this);
		qunzu.setOnClickListener(this);
		caimi.setOnClickListener(this);
		tuijian.setOnClickListener(this);
		loadFragment(0);

		view.findViewById(R.id.title_left_layout).setOnClickListener(
				new OnClickListener() {
					@Override
					public void onClick(View view) {

						Act.toAct(getActivity(), SettingActivity.class);

					}
				});
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.title_left_layout: // 设置
			Act.toAct(getActivity(), SettingActivity.class);
			break;
		case R.id.title_right_tv:
			startActivity(new Intent(getActivity(),
					Tab4ContactsFragmentAddContact.class));
			break;
		case R.id.title_second_right_tv:
			// 搜索
			Act.toAct(getActivity(), SearchAllActivity.class);
			break;
		case R.id.contacts_haoyou:
			setColor(haoyou, R.drawable.shape_left_file);
			loadFragment(0);
			break;
		case R.id.contacts_qunzu:
			setColor(qunzu, R.drawable.shape_file);
			loadFragment(1);
			break;
		case R.id.contacts_caimi:
			setColor(caimi, R.drawable.shape_file);
			loadFragment(2);
			break;
		case R.id.contacts_tuijian:
			setColor(tuijian, R.drawable.shape_right_file);
			loadFragment(3);
			break;
		}
	}

	private void setColor(TextView view, int color) {
		View[] views = { haoyou, qunzu, caimi, tuijian };
		for (int i = 0; i < views.length; i++) {
			views[i].setBackgroundResource(android.R.color.transparent);
		}
		view.setBackgroundResource(color);
	}

	private void loadFragment(int position) {

		ft = fm.beginTransaction();
		int length = contacts.length;
		for (int i = 0; i < length; i++) {
			if (contacts[i] != null) {
				ft.hide(contacts[i]);
			}
		}
		int num;
		switch (position) {
		case 0:
			if (null == contacts[0]) {
				contacts[0] = new ContactFriendFragment();
				ft.add(R.id.frag_my_contacts_container, contacts[0],
						ContactFriendFragment.class.getSimpleName());
			} else {
				ft.show(contacts[0]);
			}
			num = ((ContactFriendFragment) contacts[0]).num;
			title.setText("好友(" + num + ")");
			break;
		case 1:
			if (null == contacts[1]) {
				contacts[1] = new ContactGroupFragment();
				ft.add(R.id.frag_my_contacts_container, contacts[1],
						ContactGroupFragment.class.getSimpleName());
			} else {
				ft.show(contacts[1]);
			}
			num = ((ContactGroupFragment) contacts[1]).num;
			title.setText("群组(" + num + ")");
			break;
		case 2:
			if (null == contacts[2]) {
				contacts[2] = new ContactMiserFragment();
				ft.add(R.id.frag_my_contacts_container, contacts[2],
						ContactMiserFragment.class.getSimpleName());
			} else {
				ft.show(contacts[2]);
				ContactMiserFragment cmf = (ContactMiserFragment) contacts[2];
				cmf.setActivityTitle(cmf.flag == 0 ? true : false);
			}
			break;
		case 3:
			if (null == contacts[3]) {
				contacts[3] = new ContactRecomendFragment();
				ft.add(R.id.frag_my_contacts_container, contacts[3],
						ContactRecomendFragment.class.getSimpleName());
			} else {
				ft.show(contacts[3]);
			}
			// 进入推荐页
			title.setText("推荐");
			break;
		}
		ft.commit();
	}

}
