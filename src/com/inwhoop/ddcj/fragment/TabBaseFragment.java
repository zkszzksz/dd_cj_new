package com.inwhoop.ddcj.fragment;

import com.inwhoop.ddcj.impl.TabBaseFramentListener;

import android.support.v4.app.Fragment;

/**
 * 详情界面股票的fragment基类
 * 
 * @Project: DDCJ
 * @Title: TabBaseFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2014-12-2 上午11:42:16
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public abstract class TabBaseFragment extends Fragment {
	public TabBaseFramentListener baseListener = null;

	/**
	 * 设置监听器
	 * 
	 * @param listener
	 */
	public void setTabBaseFramentListener(TabBaseFramentListener listener) {
		this.baseListener = listener;
	}

	/**
	 * 设置ListView Top
	 * 
	 * @param scrollHeight
	 */
	public abstract void resetListViewState(int scrollHeight);

	/**
	 * 设置listview headView top
	 * 
	 * @param height
	 */
	public abstract void resetHeaderView(int height);
}
