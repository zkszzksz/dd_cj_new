package com.inwhoop.ddcj.fragment;

import java.util.ArrayList;
import java.util.List;

import net.tsz.afinal.FinalBitmap;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.inwhoop.R;
import com.inwhoop.ddcj.activity.IntellDetailsActivity;
import com.inwhoop.ddcj.activity.StockInfoActivity;
import com.inwhoop.ddcj.activity.StockInfoActivity.OnLoadMoreListener;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.NewInfoBean;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.TimeUtils;
import com.inwhoop.ddcj.view.CircleImageview;
import com.inwhoop.ddcj.xmpp.FaceConversionUtil;

/**
 * 
 * @Project: DDCJ
 * @Title: StockNewInfoFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-22 下午7:51:34
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 * 
 *          个股关联最新情报
 * 
 */
public class StockNewInfoFragment extends BaseFragment implements
		OnLoadMoreListener {
	private View mainView;
	private LinearLayout contentlayout;
	private List<NewInfoBean> list = new ArrayList<NewInfoBean>();

	private LayoutInflater inflater = null;

	private boolean isOver = false;

	private FinalBitmap fb = null;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		mainView = inflater.inflate(R.layout.new_info_layout, null);
		contentlayout = (LinearLayout) mainView
				.findViewById(R.id.infolistlayout);
		inflater = LayoutInflater.from(getActivity());
		fb = FinalBitmap.create(mContext);
		fb.configLoadingImage(R.drawable.qingbao_loading_img);
		fb.configLoadfailImage(R.drawable.qingbao_loading_img);
		setOnloadMorelistener();
		read(-1);
		return mainView;
	}

	public void setOnloadMorelistener() {
		((StockInfoActivity) getActivity())
				.setLoadMoreListener(StockNewInfoFragment.this);
		if (isOver) {
			((StockInfoActivity) getActivity()).kjListView
					.setPullLoadEnable(false);
		} else {
			((StockInfoActivity) getActivity()).kjListView
					.setPullLoadEnable(true);
		}
	}

	private void read(final int id) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = JsonUtils
							.getGeguguanlianqingbao(
									((StockInfoActivity) getActivity()).stockBean.stockcode,
									id, 10);
					msg.obj = obj;
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			if (null != ((StockInfoActivity) getActivity())
					&& null != ((StockInfoActivity) getActivity()).kjListView){
				((StockInfoActivity) getActivity()).kjListView.stopLoadMore();
			}
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					@SuppressWarnings("unchecked")
					List<NewInfoBean> newlist = (List<NewInfoBean>) obj[2];
					if (null != newlist) {
						for (int i = 0; i < newlist.size(); i++) {
							list.add(newlist.get(i));
						}
						if (newlist.size() < 10) {
							isOver = true;
							if (null != ((StockInfoActivity) getActivity())
									&& null != ((StockInfoActivity) getActivity()).kjListView){
								((StockInfoActivity) getActivity()).kjListView
									.setPullLoadEnable(false);
							}
						}
						setContentlayout(newlist);
					}
				} else {
					isOver = true;
					if (null != ((StockInfoActivity) getActivity())
							&& null != ((StockInfoActivity) getActivity()).kjListView){
						((StockInfoActivity) getActivity()).kjListView
						.setPullLoadEnable(false);
					}
					showToast("" + obj[1].toString());
				}
				break;

			case Configs.READ_FAIL:
				isOver = true;
				((StockInfoActivity) getActivity()).kjListView
						.setPullLoadEnable(false);
				break;

			default:
				break;
			}
		};
	};

	private void setContentlayout(final List<NewInfoBean> list) {
		for (int i = 0; i < list.size(); i++) {
			NewInfoBean info = list.get(i);
			if (null != info) {
				View view = LayoutInflater.from(getActivity()).inflate(
						R.layout.infolistview_item_layout, null);
				view.setTag(i);
				view.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						int pos = (Integer) v.getTag();
						Intent intent = new Intent(mContext,
								IntellDetailsActivity.class);
						intent.putExtra("newsid", list.get(pos).id);
						startActivity(intent);
					}
				});
				CircleImageview img = (CircleImageview) view
						.findViewById(R.id.headimg);
				if(list.get(i).img.startsWith("http://")){
					fb.display(img, list.get(i).img);
				}else{
					fb.display(img, Configs.HOST+list.get(i).img);
				}
				
				TextView name = (TextView) view.findViewById(R.id.nametv);
				TextView count = (TextView) view
						.findViewById(R.id.commentcount);
				TextView time = (TextView) view.findViewById(R.id.timetv);
				TextView content = (TextView) view
						.findViewById(R.id.commentcontent);
				name.setText(list.get(i).name);
				count.setText("" + list.get(i).commnetnum);
				time.setText(""
						+ TimeUtils.twoDateDistance(
								TimeRender
										.getStandardDate(list.get(i).createtime * 1000),
								TimeUtils.getStrTime(System.currentTimeMillis())));
				SpannableString spannableString = FaceConversionUtil
						.getInstace().getExpressionString(getActivity(),
								list.get(i).content);
				content.setText(spannableString);
				contentlayout.addView(view);
			}
		}
	}

	@Override
	public void onLoadMore() {
		read(list.get(list.size() - 1).id);
	}

}
