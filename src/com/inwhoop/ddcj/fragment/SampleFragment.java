package com.inwhoop.ddcj.fragment;

import java.util.ArrayList;

import com.inwhoop.R;
import com.inwhoop.ddcj.view.HVListView;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

@SuppressLint("ValidFragment")
public class SampleFragment extends TabBaseFragment implements
		OnScrollListener, OnItemClickListener {
	private boolean isNoRestListView = false;
	private int mPosition = 0;
	private int headHeight = 0;
	private int scrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
	private HVListView mListView;
	private ArrayList<String> mListItems;

	public SampleFragment(int mPosition, int headH) {
		this.mPosition = mPosition;
		this.headHeight = headH;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mListItems = new ArrayList<String>();
		int len = 100;

		// if (mPosition == 2) {
		// len = 1;
		// }
		for (int i = 1; i <= len; i++) {
			mListItems.add(i + ". item - currnet page: " + (mPosition + 1));
		}
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.detail_stock_list_item, null);

		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		mListView = (HVListView) getActivity().findViewById(R.id.listview);

		mListView.setOnScrollListener(this);
		mListView.setAdapter(new ArrayAdapter<String>(getActivity(),
				R.layout.news_item_my_stock_head, R.id.item1, mListItems));
		mListView.setOnItemClickListener(this);

		baseListener.restScrollState();
	}

	public void resetListViewState(int scrollHeight) {
		if (isNoRestListView) {
			return;
		}

		if (mListView == null) {
			return;
		}

		if (scrollHeight == 0 && mListView.getFirstVisiblePosition() >= 1) {
			return;
		}

		mListView.setSelectionFromTop(1, scrollHeight);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {
		if (visibleItemCount == totalItemCount && totalItemCount > 1) {
			baseListener.onScroll(view, firstVisibleItem, visibleItemCount,
					totalItemCount);
			isNoRestListView = true;
		} else {
			isNoRestListView = false;
		}

		if (baseListener != null
				&& scrollState != AbsListView.OnScrollListener.SCROLL_STATE_IDLE) {
			baseListener.onScroll(view, firstVisibleItem, visibleItemCount,
					totalItemCount);
		}
	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {
		// nothing
		this.scrollState = scrollState;
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		Toast.makeText(getActivity(), "toast lixiaodaoaaaa 010",
				Toast.LENGTH_SHORT).show();
	}

	@Override
	public void resetHeaderView(int height) {
		// TODO Auto-generated method stub

	}
}