package com.inwhoop.ddcj.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import cn.sharesdk.framework.Platform;
import cn.sharesdk.framework.PlatformActionListener;
import cn.sharesdk.framework.ShareSDK;
import cn.sharesdk.sina.weibo.SinaWeibo;
import cn.sharesdk.tencent.qq.QQ;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.*;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.FriendMsgBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.LogUtil;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.HashMap;

/**
 * 添加联系人
 * 
 * @author ZOUXU
 */
public class Tab4ContactsFragmentAddContact extends BaseFragmentActivity implements
		OnClickListener {

	// 返回按钮
	private RelativeLayout title_left_layout;
	private EditText searchEdit;
	private TextView tv_ID;
	private UserBean userBean;
    private FriendMsgBean bean;

    @Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ShareSDK.initSDK(this);
		setContentView(R.layout.frag_tab4_add_contacts);
		initView();
	}

	private void initView() {
		userBean = UserInfoUtil.getUserInfo(mContext);
		title_left_layout = (RelativeLayout) findViewById(R.id.title_left_layout);
		tv_ID = (TextView) findViewById(R.id.tv_ID);
		tv_ID.setText(userBean.ddid);
		title_left_layout.setOnClickListener(this);
		findViewById(R.id.ll_addWeixin).setOnClickListener(this);
		findViewById(R.id.ll_addQQ).setOnClickListener(this);
		findViewById(R.id.ll_addWeibo).setOnClickListener(this);
		findViewById(R.id.add_city_friend_layout).setOnClickListener(this);
		findViewById(R.id.add_phone).setOnClickListener(this);

		searchEdit = (EditText) findViewById(R.id.et_2);
		searchEdit.setOnKeyListener(new View.OnKeyListener() {
			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (keyCode == KeyEvent.KEYCODE_ENTER
						&& event.getAction() == KeyEvent.ACTION_DOWN) {// 修改回车键功能
					((InputMethodManager) getSystemService(INPUT_METHOD_SERVICE))
							.hideSoftInputFromWindow(
									Tab4ContactsFragmentAddContact.this
											.getCurrentFocus().getWindowToken(),
									InputMethodManager.HIDE_NOT_ALWAYS);
					if (searchEdit.getText().toString().trim().equals("")) {
						Toast.makeText(mContext, "请输入用户或群组的ID",
								Toast.LENGTH_SHORT).show();
					} else {
						getSearchData();
					}
				}
				return false;
			}
		});
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
            case R.id.title_left_layout:
                finish();
                break;
		case R.id.add_phone:
			Act.toAct(mContext, LocalPhoneActivity.class);
			break;
		case R.id.add_city_friend_layout:// 添加同城好友
			Act.toAct(mContext, AddSameCityFriendActivity.class);
			break;
		case R.id.ll_addWeixin:
			Platform.ShareParams sp = new Platform.ShareParams();
			sp.setTitle("大大财经");
			sp.setText("大大财经");
			// sp1.setTitleUrl("http://sharesdk.cn"); // 标题的超链接
			sp.setShareType(Platform.SHARE_TEXT);

			Platform plat = null;
			plat = ShareSDK.getPlatform("Wechat");
			plat.setPlatformActionListener(paListener);
			plat.share(sp);
			break;
		case R.id.ll_addQQ:
			// Toast.makeText(context, "QQ", 0).show();
			QQ.ShareParams sp2 = new QQ.ShareParams();
			sp2.setTitle("大大财经");
			sp2.setTitleUrl("http://sharesdk.cn"); // 标题的超链接
			sp2.setText("大大财经");
			sp2.setImageUrl("http://www.someserver.com/测试图片网络地址.jpg");
			sp2.setSite("发布分享的网站名称");
			sp2.setSiteUrl("发布分享网站的地址");

			Platform qzone2 = ShareSDK.getPlatform(QQ.NAME);
			qzone2.setPlatformActionListener(paListener); // 设置分享事件回调
			// 执行图文分享
			qzone2.share(sp2);
			break;
		case R.id.ll_addWeibo:
			// Toast.makeText(context, "微博", 0).show();

			SinaWeibo.ShareParams sp3 = new SinaWeibo.ShareParams();
			sp3.setTitle("大大财经");
			sp3.setTitleUrl("http://sharesdk.cn"); // 标题的超链接
			sp3.setText("大大财经");
			sp3.setImageUrl("http://www.someserver.com/测试图片网络地址.jpg");
			sp3.setSite("发布分享的网站名称");
			sp3.setSiteUrl("发布分享网站的地址");

			Platform qzone3 = ShareSDK.getPlatform(SinaWeibo.NAME);
			qzone3.setPlatformActionListener(paListener); // 设置分享事件回调
			// 执行图文分享
			qzone3.share(sp3);
			break;

		}
	}

	PlatformActionListener paListener = new PlatformActionListener() {
		@Override
		public void onComplete(Platform platform, int i,
				HashMap<String, Object> hashMap) {
               LogUtil.i("分享成功");
		}

		@Override
		public void onError(Platform platform, int i, Throwable throwable) {
            LogUtil.d("分享失败");
		}

		@Override
		public void onCancel(Platform platform, int i) {
            LogUtil.i("取消分享");
        }
	};

	private void getSearchData() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					bean = JsonUtils.searchFriend(searchEdit.getText()
							.toString().trim());
                    if (null != bean && null != bean.list && bean.list.size() > 0) {
                        msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
				} catch (Exception e) {
					e.printStackTrace();
					System.out.println("异常信息为" + e.toString());
					msg.what = Configs.READ_ERROR;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Intent intent = new Intent(mContext, PersonInfoActivity.class);
				intent.putExtra("userid", bean.list.get(0).userid);
				startActivity(intent);
				break;
			case Configs.READ_FAIL:
                if (bean.groupList.size()>0){  //群组
                    Bundle b = new Bundle();
                    b.putInt("flag", 2);
                    b.putString("groupid", bean.groupList.get(0).id);
                    b.putSerializable("CateChannel", bean.groupList.get(0));
                    Act.toAct(mContext, GroupInfoActivity.class, b);
                    return;
                }
				Toast.makeText(mContext, bean.msg, Toast.LENGTH_SHORT)
						.show();
				break;
			case Configs.READ_ERROR:
				Toast.makeText(mContext, bean.msg, Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};
}
