package com.inwhoop.ddcj.fragment;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout.LayoutParams;

import com.inwhoop.R;
import com.inwhoop.ddcj.activity.QingbaoCommentActivity;
import com.inwhoop.ddcj.activity.SettingOtherActivity;
import com.inwhoop.ddcj.activity.WriteCommentActivity;
import com.inwhoop.ddcj.adapter.CommentMeListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CommentMeList;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.ExitAppUtils;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

/**
 * 评论我的
 * 
 * @Project: DDCJ
 * @Title: CommentMeFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-12-16 上午9:57:40
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class CommentMeFragment extends BaseFragment implements
		IXListViewListener, MLocationLinsener {

	private XListView listView = null;

	private String address = "";

	private List<CommentMeList> list = new ArrayList<CommentMeList>();

	private CommentMeListAdapter adapter = null;

	private PopupWindow popupWindow = null;

	private int nowposition = 0;

	private LinearLayout alertLogin;

	private String ddid;

	private boolean isInit = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.comment_me_layout, null);
		mContext = getActivity();
		init(view);
		initPopupWindow();
		return view;
	}

	public void init(View view) {
		ddid = UserInfoUtil.getUserInfo(mContext).ddid;
		alertLogin = (LinearLayout) view.findViewById(R.id.alert_login);
		listView = (XListView) view.findViewById(R.id.commentmelist);
		listView.setXListViewListener(this);
		listView.setPullLoadEnable(false);
		listView.setPullRefreshEnable(true);
		adapter = new CommentMeListAdapter(mContext);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long arg3) {
				nowposition = position - 1;
				showPopupWindow(v);
			}
		});
		showProgressDialog("正在加载数据...");
		// read(-1);
	}

	public void read(final int id) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = JsonUtils.getCommentMelist(
							UserInfoUtil.getUserInfo(mContext).userid, 10, id);
					msg.obj = obj;
					msg.arg1 = id;
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			dismissProgressDialog();
			listView.stopLoadMore();
			listView.stopRefresh();
			switch (msg.what) {
			case Configs.READ_FAIL:
				showToast("获取数据失败，请稍后再试");
				break;

			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					@SuppressWarnings("unchecked")
					List<CommentMeList> nblist = (List<CommentMeList>) obj[2];
					if (nblist.size() < 10) {
						listView.setPullLoadEnable(false);
					} else {
						listView.setPullLoadEnable(true);
					}
					if (msg.arg1 == -1) {
						adapter.getAll().clear();
					}
					adapter.addList(nblist);
					adapter.notifyDataSetChanged();
				} else {
					showToast(obj[1].toString());
				}
				break;
			default:
				break;
			}

		};
	};

	@Override
	public void onRefresh() {
		read(-1);
	}

	@Override
	public void onLoadMore() {
		read(adapter.getAll().get(adapter.getAll().size() - 1).id);
	}

	public List<CommentMeList> getQingbaoInfos() {
		List<CommentMeList> list = new ArrayList<CommentMeList>();
		CommentMeList info = null;
		for (int i = 0; i < 9; i++) {
			info = new CommentMeList();
			list.add(info);
		}
		return list;
	}

	@Override
	public void mLocationSucess(String address) {
		this.address = address;
		read(-1);
	}

	@Override
	public void mLocationFaile(String errorInfo) {

	}

	@SuppressWarnings("deprecation")
	private void initPopupWindow() {
		View view = LayoutInflater.from(getActivity()).inflate(
				R.layout.comment_hint_layout, null);
		popupWindow = new PopupWindow(view, LayoutParams.MATCH_PARENT,
				LayoutParams.MATCH_PARENT, true);
		popupWindow.setContentView(view);
		popupWindow.setFocusable(true);
		popupWindow.setOutsideTouchable(false);
		popupWindow.setBackgroundDrawable(new BitmapDrawable());
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		TextView huifu = (TextView) view.findViewById(R.id.huifu);
		huifu.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (null != popupWindow && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
				if (UserInfoUtil.getUserInfo(getActivity()).userid
						.equals(adapter.getAll().get(nowposition).userid)) {
					showToast("亲，不能对自己进行回复~~");
					return;
				}
				Intent intent = new Intent(getActivity(),
						QingbaoCommentActivity.class);
				intent.putExtra("id", adapter.getAll().get(nowposition).newsid);
				intent.putExtra("commentid",
						adapter.getAll().get(nowposition).id);
				intent.putExtra("touserid",
						adapter.getAll().get(nowposition).userid);
				getActivity().startActivityForResult(intent, 1005);
			}
		});
		TextView delete = (TextView) view.findViewById(R.id.delete);
		delete.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				if (null != popupWindow && popupWindow.isShowing()) {
					popupWindow.dismiss();
				}
				new AlertDialog.Builder(getActivity())
						.setTitle("删除这条评论")
						.setMessage("是否删除？")
						.setPositiveButton("是",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface arg0,
											int arg1) {
										deleteComment();
									}
								})
						.setNegativeButton("否",
								new DialogInterface.OnClickListener() {

									@Override
									public void onClick(DialogInterface dialog,
											int which) {
									}
								}).show();
			}
		});
	}

	private void showPopupWindow(View v) {
		popupWindow.showAtLocation(v, Gravity.CENTER, 0, 0);
	}

	private void deleteComment() {
		showProgressDialogFalse("正在删除，请稍后~~");
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					Object[] obj = JsonUtils.deleteComment(adapter.getAll()
							.get(nowposition).userid,
							adapter.getAll().get(nowposition).id);
					msg.obj = obj;
					msg.what = Configs.READ_SUCCESS;
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
				deleteHandler.sendMessage(msg);
			}
		}).start();
	}

	private Handler deleteHandler = new Handler() {
		public void handleMessage(Message msg) {
			dismissProgressDialog();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					if (null != popupWindow && popupWindow.isShowing()) {
						popupWindow.dismiss();
					}
					adapter.getAll().remove(nowposition);
					adapter.notifyDataSetChanged();
				} else {
					showToast("" + obj[1]);
				}
				break;

			case Configs.READ_FAIL:
				showToast("删除失败请稍后再试~~");
				break;

			default:
				break;
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		ddid = UserInfoUtil.getUserInfo(mContext).ddid;
		if (TextUtils.isEmpty(ddid)) {
			alertLogin.setVisibility(View.VISIBLE);
			listView.setVisibility(View.GONE);
			dismissProgressDialog();
			return;
		} else {
			alertLogin.setVisibility(View.GONE);
			listView.setVisibility(View.VISIBLE);
			if (isInit) {
				read(-1);
				isInit = false;
			}
		}
	}
}
