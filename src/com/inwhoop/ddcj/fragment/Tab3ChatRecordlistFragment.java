package com.inwhoop.ddcj.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.*;
import com.inwhoop.ddcj.adapter.ChatRecordlistAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.bean.JpushBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.bean.UserMessage;
import com.inwhoop.ddcj.db.PushMsgDb;
import com.inwhoop.ddcj.util.*;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO 小纸条
 * @Date: 2014/10/20 17:59
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class Tab3ChatRecordlistFragment extends BaseFragment implements
		IXListViewListener, OnClickListener {
	private View mainView;

	private XListView kjListView = null;

	private ChatRecordlistAdapter adapter = null;

	private ReceiveBroadCast broadCast;
	private MessageListUtil messageListUtil;
	private LayoutInflater mInflater;
	private UserBean userBean;
	private TextView content;
	private TextView time;
	private TextView num;
	private PushMsgDb db;
	private LinearLayout progressLinear;
    private LinearLayout alertLogin;

    private View sureMsgLayout;//验证消息layout

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		mainView = inflater.inflate(R.layout.tab_chatrecord_list, null);
		initData();
		return mainView;
	}

	private void initData() {
		super.initData(mainView);
		userBean = UserInfoUtil.getUserInfo(mContext);
		messageListUtil = MessageListUtil.getInstance(mContext);
		broadCast = new ReceiveBroadCast();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Configs.COME_MESSAGE1);
		mContext.registerReceiver(broadCast, filter);

		setLeftLayout(R.drawable.more_btn_selector, false);
		title_left_tv.setTextSize(0);
		setRightLayout(R.drawable.search_btn_selector, false);
		title_right_layout.setOnClickListener(this);
		title_left_layout.setOnClickListener(this);
		kjListView = (XListView) mainView.findViewById(R.id.crlistview);
		kjListView.setPullLoadEnable(false);
		kjListView.setXListViewListener(this);

		progressLinear = (LinearLayout) mainView
				.findViewById(R.id.progress_linear);
		progressLinear.setVisibility(View.VISIBLE);
		
		db = new PushMsgDb(mContext);

		mInflater = LayoutInflater.from(mContext);
		sureMsgLayout = mInflater.inflate(R.layout.sys_msg_item, null);
		content = (TextView) sureMsgLayout.findViewById(R.id.content);
		time = (TextView) sureMsgLayout.findViewById(R.id.time);
		num = (TextView) sureMsgLayout.findViewById(R.id.count);
		sureMsgLayout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                db.updateReadStatData("1", userBean.userid);
                startActivity(new Intent(mContext, ValidateMsgActivity.class));
            }
        });

		kjListView.addHeaderView(sureMsgLayout);
		
		adapter = new ChatRecordlistAdapter(getActivity());
		kjListView.setAdapter(adapter);

		final List<UserMessage> lists = messageListUtil.getUserMsglist();
		if (null != lists && lists.size() > 0) {
			setTitleTextStr("小纸条(" + lists.size() + ")");
			getMsgNum(lists);
			adapter.addList(lists);
			adapter.notifyDataSetChanged();
		} else {
			setTitleTextStr("小纸条");
		}
		kjListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				UserMessage msg = adapter.getAll().get(position - 2);
				if (msg.type.equals("1")) {
					Bundle bundle = new Bundle();
					bundle.putString("usernick", msg.title);
					bundle.putString("username", msg.chatid);
					Intent intent = new Intent(mContext,
							FriendChatActivity.class);
					intent.putExtras(bundle);
					startActivity(intent);
				} else if (msg.type.equals("2")) {
					List<CateChannel> list = Configs.groupList;
					// Intent intent = new Intent(mContext,
					// GroupChatActivity.class);
					Intent intent = new Intent(mContext,
							GroupChatActivity.class);
					intent.putExtra("groupid", msg.chatid);
					intent.putExtra("groupname", msg.title);
					intent.putExtra("groupphoto", msg.logo_url);
					intent.putExtra("listobj", (Serializable) list);
					startActivity(intent);
				}
			}
		});

        alertLogin = (LinearLayout) mainView.findViewById(R.id.alert_login);

    }

	@Override
	public void onResume() {
		super.onResume();
        myResume();

        String ddid = UserInfoUtil.getUserInfo(mContext).ddid;
        if (TextUtils.isEmpty(ddid)) {
            alertLogin.setVisibility(View.VISIBLE);
            progressLinear.setVisibility(View.GONE);
            alertLogin.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Act.toAct(mContext, LoginActivity.class);
                }
            });
            sureMsgLayout.setVisibility(View.GONE);
            return;
        } else {
            alertLogin.setVisibility(View.GONE);
            sureMsgLayout.setVisibility(View.VISIBLE);
        }
	}

    private void myResume() {
        List<UserMessage> lists = messageListUtil.getUserMsglist();
        if (null != lists && lists.size() > 0) {
            getMsgNum(lists);
            adapter.addList(lists);
            adapter.notifyDataSetChanged();
            setTitleTextStr("小纸条(" + lists.size() + ")");
        } else {
            setTitleTextStr("小纸条");
        }

        List<JpushBean> beans = db.getQbData(userBean.userid);
        if (null != beans && beans.size() > 0) {
            content.setText(beans.get(0).name + " " + beans.get(0).content);
            time.setText(TimeUtils.twoDateDistance(TimeRender
                            .getStandardDate(Long.valueOf(beans.get(0).time) * 1000),
                    TimeRender.getStandardDate()));
            List<JpushBean> bean1 = new ArrayList<JpushBean>();
            for (int i = 0; i < beans.size(); i++) {
                System.out.println("未读的消息为" + beans.get(i).isread);
                if (beans.get(i).isread.equals("0")) {
                    bean1.add(beans.get(i));
                }
            }
            if (null != bean1 && bean1.size() > 0) {
                num.setText(bean1.size() + "+");
                num.setVisibility(View.VISIBLE);
            } else {
                num.setVisibility(View.INVISIBLE);
                num.setText("0");
            }
            bean1 = null;
        }
        progressLinear.setVisibility(View.GONE);
    }

    @Override
	public void onHiddenChanged(boolean hidden) {
		// System.out.println("消息页面是否显示"+hidden);
		if (!hidden) {
			List<UserMessage> lists = messageListUtil.getUserMsglist();
			if (null != lists && lists.size() > 0) {
				getMsgNum(lists);
				adapter.addList(lists);
				adapter.notifyDataSetChanged();
			}

			List<JpushBean> beans = db.getQbData(userBean.userid);
			if (null != beans && beans.size() > 0) {
				content.setText(beans.get(beans.size() - 1).name + " "
						+ beans.get(beans.size() - 1).content);
				time.setText(TimeUtils.twoDateDistance(
						TimeRender.getStandardDate(Long.valueOf(beans.get(beans
								.size() - 1).time) * 1000), TimeRender
								.getStandardDate()));
				List<JpushBean> bean1 = new ArrayList<JpushBean>();
				for (int i = 0; i < beans.size(); i++) {
					System.out.println("未读的消息为" + beans.get(i).isread);
					if (beans.get(i).isread.equals("0")) {
						bean1.add(beans.get(i));
					}
				}
                if (null != bean1 && bean1.size() > 0) {
                    num.setText(bean1.size() + "+");
                    num.setVisibility(View.VISIBLE);
                } else {
                    num.setVisibility(View.INVISIBLE);
                    num.setText("0");
                }
				bean1 = null;
			}
		}
	}

	@Override
	public void onRefresh() {
		kjListView.stopRefresh();
		List<UserMessage> lists = messageListUtil.getUserMsglist();
		if (null != lists && lists.size() > 0) {
			getMsgNum(lists);
			adapter.addList(lists);
			adapter.notifyDataSetChanged();
		}
	}

	@Override
	public void onLoadMore() {
		kjListView.stopLoadMore();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_right_layout: // 搜索
			Act.toAct(mContext, SearchAllActivity.class);
			break;

		case R.id.title_left_layout: // 设置
			// String ddid = UserInfoUtil.getUserInfo(mContext).ddid;
			// if (TextUtils.isEmpty(ddid)) {
			// Act.toAct(mContext, LoginActivity.class);
			// } else {
			Act.toAct(mContext, SettingActivity.class);
			// }
			break;

		default:
			break;
		}
	}

	private void getMsgNum(List<UserMessage> list) {
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).number > 0) {
				Intent intent = new Intent();
				intent.setAction(Configs.RED_SHOW);
				mContext.sendBroadcast(intent);
				break;
			}
		}
		int count = 0;
		for (int i = 0; i < list.size(); i++) {
			count += list.get(i).number;
		}
		if (count == 0) {
			Intent intent = new Intent();
			intent.setAction(Configs.RED_SHOW);
			mContext.sendBroadcast(intent);
		}
	}

	private class ReceiveBroadCast extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			// 得到广播中得到的数据，并显示出来
			if (null != intent) {
				List<UserMessage> lists = messageListUtil.getUserMsglist();
				if (null != lists && lists.size() > 0) {
					getMsgNum(lists);
					adapter.addList(lists);
					adapter.notifyDataSetChanged();
				}
			}
		}

	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mContext.unregisterReceiver(broadCast);
	}
}
