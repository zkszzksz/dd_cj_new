package com.inwhoop.ddcj.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.FriendChatActivity;
import com.inwhoop.ddcj.activity.LoginActivity;
import com.inwhoop.ddcj.adapter.ContactsAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ContactsInfo;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.DialogShowStyle;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.NetWorkUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import java.util.ArrayList;
import java.util.List;

public class ContactFriendFragment extends Fragment implements
		IXListViewListener {

	private XListView lv_friends = null;
	private ViewGroup contents = null;
	private List<ContactsInfo> mData = null;
	private TextView title;
	private Context context;
	int num;
	private UserBean userBean;
	private DialogShowStyle dialogShowStyle;
	private ContactsAdapter friends_adapter;
	private LinearLayout progressLinear;
	private LinearLayout alertLogin;
	private String ddid;
	private boolean isInit = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		context = getActivity();
		title = ((Tab4ContactsFragment) getParentFragment()).title;
		this.contents = (ViewGroup) inflater.inflate(
				R.layout.frag_my_contacts_son_friends, null);

		initComponet(inflater);
		return this.contents;

	}

	private void initComponet(LayoutInflater inflater) {
		mData = new ArrayList<ContactsInfo>();
		lv_friends = (XListView) contents.findViewById(R.id.lv_myFriends);
		lv_friends.setPullLoadEnable(false);
		lv_friends.setXListViewListener(this);
		alertLogin = (LinearLayout) contents.findViewById(R.id.alert_login);
		progressLinear = (LinearLayout) contents
				.findViewById(R.id.progress_linear);
		progressLinear.setVisibility(View.VISIBLE);
		friends_adapter = new ContactsAdapter(context);
		lv_friends.setAdapter(friends_adapter);
		// new GetNetDataTask().execute();

	}

	private void getData() {
		Object[] objs;
		try {
			objs = JsonUtils.getUserFriendsList(Configs.USER_FRIENDS_LIST,
					userBean.userid);
			mData.clear();
			List<ContactsInfo> data_temp = (List<ContactsInfo>) objs[2];
			for (ContactsInfo info : data_temp) {
				mData.add(info);
			}
			num = mData.size();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onRefresh() {
		new hideHeadViewAsyncTask().execute();
	}

	@Override
	public void onLoadMore() {
		lv_friends.stopLoadMore();
	}

	private class hideHeadViewAsyncTask extends
			AsyncTask<String, Integer, Long> {

		@Override
		protected Long doInBackground(String... params) {

			getData();
			return 0L;
		}

		@Override
		protected void onPostExecute(Long result) {
			title.setText("好友(" + num + ")");
			lv_friends.stopRefresh();
		}
	}

	class GetNetDataTask extends AsyncTask<Void, Integer, Integer> {
		private static final int NONET = 0;
		private static final int SLOWNET = 2;

		@Override
		protected void onPreExecute() {

			int netType = new NetWorkUtils(context).getNetType();
			switch (netType) {
			case NONET:
				Toast.makeText(context, "请检查网络连接", Toast.LENGTH_LONG).show();
				this.cancel(true);
				break;
			case SLOWNET:
				dialogShowStyle = new DialogShowStyle(context,
						"当前为非WIFI网络，正在获取频道信息");
				dialogShowStyle.dialogShow();
				break;
			}

			userBean = UserInfoUtil.getUserInfo(context);
		}

		@Override
		protected Integer doInBackground(Void... params) {
			try {
				getData();
			} catch (Exception e) {
				e.printStackTrace();
			}
			return 0;
		}

		@Override
		protected void onPostExecute(Integer result) {
			if (dialogShowStyle != null) {
				dialogShowStyle.dialogDismiss();
				dialogShowStyle = null;
			}
			progressLinear.setVisibility(View.GONE);
			title.setText("好友(" + num + ")");

			friends_adapter.addList(mData, false);
			friends_adapter.notifyDataSetChanged();
			lv_friends
					.setOnItemClickListener(new AdapterView.OnItemClickListener() {
						@Override
						public void onItemClick(AdapterView<?> adapterView,
								View view, int i, long l) {
							if (null != mData) {
								Bundle bundle = new Bundle();
								bundle.putString("usernick",
										mData.get(i - 1).name);
								bundle.putString("username",
										mData.get(i - 1).tel);
								Intent intent = new Intent(context,
										FriendChatActivity.class);
								intent.putExtras(bundle);
								startActivity(intent);
							}
						}
					});
		}
	}

	@Override
	public void onResume() {
		super.onResume();
		ddid = UserInfoUtil.getUserInfo(context).ddid;
		if (TextUtils.isEmpty(ddid)) {
			alertLogin.setVisibility(View.VISIBLE);
			lv_friends.setVisibility(View.GONE);
			progressLinear.setVisibility(View.GONE);
			alertLogin.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Act.toAct(context, LoginActivity.class);
				}
			});
			return;
		} else {
			alertLogin.setVisibility(View.GONE);
			lv_friends.setVisibility(View.VISIBLE);
			if (isInit) {
				progressLinear.setVisibility(View.VISIBLE);
				new GetNetDataTask().execute();
				isInit = false;
			}
		}

	}
}
