package com.inwhoop.ddcj.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.ChannelManageActivity;
import com.inwhoop.ddcj.activity.SearchAllActivity;
import com.inwhoop.ddcj.activity.SettingActivity;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.ChannelInfo;
import com.inwhoop.ddcj.bean.ChannelItem;
import com.inwhoop.ddcj.bean.ChannelManage;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.PagerSlidingTabStrip;

import java.util.ArrayList;
import java.util.List;

/**
 * 资讯/频道
 * 
 * @author ligang@inwhoop.com
 * @version V1.0
 * @Project: DDCJ
 * @Title: ChannelFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 * @date 2014-11-6 下午3:27:56
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 */
public class ChannelFragment extends BaseFragment implements OnClickListener {

	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private MyPagerAdapter adapter;

	private List<ChannelInfo> titleList = new ArrayList<ChannelInfo>();
	private List<ChannelInfo> allTitleList = new ArrayList<ChannelInfo>();
	private UserBean userBean;

    @Override
    public void onResume() {
        super.onResume();
        setChannelType();
    }

    @Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.channel_index, null);
		initData(view);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setLeftLayout(R.drawable.more_btn_selector, false);
		setTitleResText(R.string.zixun);
		title_left_tv.setTextSize(0);
		setRightLayout(R.drawable.rss_bg_selector, false);
		setSecondRightLayout(R.drawable.search_btn_selector, false);
		title_left_layout.setOnClickListener(this);
		title_right_layout.setOnClickListener(this);
		title_second_right_layout.setOnClickListener(this);
	}

	public void initData(View view) {
		super.initData(view);
		userBean = UserInfoUtil.getUserInfo(getActivity());
		tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
		pager = (ViewPager) view.findViewById(R.id.pager);
		final int pageMargin = (int) TypedValue.applyDimension(
				TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
						.getDisplayMetrics());
		pager.setPageMargin(pageMargin);
		pager.setCurrentItem(0);

	}

	/**
	 * 设置频道栏目
	 * 
	 * @Title: setChannelType
	 * @Description: TODO
	 * @param
	 * @return void
	 */
	private void setChannelType() {
		if (userBean.ddid.equals("")) {
			titleList.clear();
			ArrayList<ChannelItem> list = ((ArrayList<ChannelItem>) ChannelManage
					.getManage(Configs.getApp().getSQLHelper())
					.getUserChannel());
			ChannelInfo info = null;
			if (null != list) {
				if (list.size() > 1) {
					for (int i = 0; i < list.size(); i++) {
						if (i > 0) {
							info = new ChannelInfo();
							info.id = list.get(i).id;
							info.name = list.get(i).name;
							info.foucs = list.get(i).selected;
							info.foucslist.add(info);
							titleList.add(info);
						}
					}
					setMyChannel();
				} else {
					loadData();
				}
			}

		} else {
			loadData();
		}
	}

	private void setDefault() {
		ChannelInfo info = new ChannelInfo();
		info.name = "推荐";
		info.id = -1;
		allTitleList.add(info);
	}

	private void loadData() {
		new Thread(new Runnable() {

			@SuppressWarnings("unchecked")
			@Override
			public void run() {
				Message msg = handler.obtainMessage();
				try {
					String uid = UserInfoUtil.getUserInfo(mContext).userid;
					Object[] data = JsonUtils.getUsersChannelList(uid);
					if ((Boolean) data[0]) {
						titleList = (List<ChannelInfo>) data[2];
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
					handler.sendMessage(msg);
				} catch (Exception e) {
					msg.what = Configs.READ_FAIL;
				}
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				setMyChannel();
				break;

			case Configs.READ_FAIL:

				break;
			}

		}
	};

	private void setMyChannel() {
		allTitleList.clear();
		setDefault();
		// if (userBean.ddid.equals("")) {// 匿名用户
		// for (int i = 0; i < titleList.size(); i++) {
		// if (titleList.get(i).foucs == 1) {
		// allTitleList.add(titleList.get(i));
		// }
		// }
		// } else {
		for (int i = 0; i < titleList.size(); i++) {
			if (null != titleList.get(i)) {
				for (int j = 0; j < titleList.get(i).foucslist.size(); j++) {
					if (null != titleList.get(i).foucslist.get(j)) {
						allTitleList.add(titleList.get(i).foucslist.get(j));
					}
				}

			}
		}
		// }
		adapter = new MyPagerAdapter(getActivity().getSupportFragmentManager());
		pager.setAdapter(adapter);
		pager.setOffscreenPageLimit(allTitleList.size());
		tabs.setViewPager(pager);
	}

	public class MyPagerAdapter extends FragmentStatePagerAdapter {

		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return allTitleList.get(position).name;
		}

		@Override
		public int getCount() {
			return allTitleList.size();
		}

		@Override
		public Fragment getItem(int position) {
			return ChannelTypeFragment.newInstance(position,
					allTitleList.get(position).id);
		}

		@Override
		public Object instantiateItem(ViewGroup container, int position) {
			ChannelTypeFragment f = (ChannelTypeFragment) super
					.instantiateItem(container, position);
			return f;
		}

		@Override
		public int getItemPosition(Object object) {
			// TODO Auto-generated method stub
			return PagerAdapter.POSITION_NONE;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == 100) {
			setChannelType();
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.title_left_layout: // 设置
//			String ddid = UserInfoUtil.getUserInfo(mContext).ddid;
//			if (TextUtils.isEmpty(ddid)) {
//				Act.toAct(mContext, StartActivity.class);
//			} else {
				Act.toAct(mContext, SettingActivity.class);
//			}
			break;
		case R.id.title_right_layout: // 频道管理
			Intent intent = new Intent(getActivity(),
					ChannelManageActivity.class);
			startActivityForResult(intent, getActivity().RESULT_FIRST_USER);
			break;
		case R.id.title_right_second_layout:
			Act.toAct(mContext, SearchAllActivity.class);
			break;
		}

	}

}
