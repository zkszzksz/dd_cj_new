package com.inwhoop.ddcj.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.SpannableString;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.*;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.AllCateBean;
import com.inwhoop.ddcj.bean.GroupList;
import com.inwhoop.ddcj.bean.JpushBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.db.PushMsgDb;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.CircleImageview;
import com.inwhoop.ddcj.xmpp.FaceConversionUtil;

import net.tsz.afinal.FinalBitmap;

import java.util.ArrayList;
import java.util.List;

/**
 * @Describe: TODO 发现 * * * ****** Created by ZK ********
 * @Date: 2014/10/20 18:00
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class Tab1FindFragment extends BaseFragment implements OnClickListener {

	private View mainView;

	private LinearLayout groupLayout = null, tolistLayout = null;

	private LayoutInflater inflater = null;

	private List<GroupList> groupLists = new ArrayList<GroupList>();

	private CircleImageview qbCircleImageview = null;
	private AllCateBean allCateBean;
	private FinalBitmap fb;
	private JpushBean jpushBean;
	private TextView time;
	private TextView contentText;
	private ReceiveBroadCast receiveBroadCast;
	private TextView findText;
	private LinearLayout findLinrar;
	private boolean isGet = true;
	private String num = "";
	private TextView info_count;
	private PushMsgDb db = null;
	private UserBean userBean;
	private List<JpushBean> list;
	private LinearLayout progressLinear;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		this.inflater = inflater;
		mainView = inflater.inflate(R.layout.find_home_layout, null);
		initData();
		return mainView;
	}

	private void initData() {
		super.initData(mainView);
		db = new PushMsgDb(mContext);
		userBean = UserInfoUtil.getUserInfo(mContext);
		receiveBroadCast = new ReceiveBroadCast();
		IntentFilter filter = new IntentFilter();
		filter.addAction(Configs.PUSH_ACTION1);
		mContext.registerReceiver(receiveBroadCast, filter);
		fb = FinalBitmap.create(mContext);
		fb.configLoadingImage(R.drawable.qingbao_loading_img);
		fb.configLoadfailImage(R.drawable.qingbao_loading_img);
		setLeftLayout(R.drawable.more_btn_selector, false);
		title_left_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// String ddid = UserInfoUtil.getUserInfo(mContext).ddid;
				// if (TextUtils.isEmpty(ddid)) {
				// Act.toAct(mContext, LoginActivity.class);
				// } else {
				Act.toAct(mContext, SettingActivity.class);
				// }
			}
		});
		title_left_tv.setTextSize(0);
		setTitleTextStr("发现");
		setRightLayout(R.drawable.search_btn_selector, false);
		title_right_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent intent = new Intent(mContext, SearchAllActivity.class);
				startActivity(intent);
			}
		});
		title_second_right_tv.setTextSize(0);
		setSecondRightLayout(R.drawable.emailforinvitation_btn_selector, false);
		// setSecondRightLayout(R.drawable.email_blue_white_btn_selector,
		// false);
		title_second_right_layout.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Intent intent = new Intent(mContext, InvitationActivity.class);
				startActivity(intent);
			}
		});

		progressLinear = (LinearLayout) mainView
				.findViewById(R.id.progress_linear);
		progressLinear.setVisibility(View.VISIBLE);
		info_count = (TextView) mainView.findViewById(R.id.info_count);
		findText = (TextView) mainView.findViewById(R.id.find_text);
		findLinrar = (LinearLayout) mainView.findViewById(R.id.find_linear);
		qbCircleImageview = (CircleImageview) mainView
				.findViewById(R.id.headimg);
		time = (TextView) mainView.findViewById(R.id.qb_time);
		contentText = (TextView) mainView.findViewById(R.id.qb_title);
		qbCircleImageview.setBackground(getResources().getColor(R.color.black));
		groupLayout = (LinearLayout) mainView
				.findViewById(R.id.grouplistlayout);
		tolistLayout = (LinearLayout) mainView.findViewById(R.id.tolistlayout);
		tolistLayout.setOnClickListener(this);
		readGrouplist();
	}

	private void setQbShow() {
		list = db.getQbData("1", userBean.userid);
		if (null != list && list.size() > 0) {
			findText.setVisibility(View.GONE);
			findLinrar.setVisibility(View.VISIBLE);
			if (list.get(list.size() - 1).headImg.startsWith("http://")) {
				fb.display(qbCircleImageview, list.get(list.size() - 1).headImg);

			} else {
				fb.display(qbCircleImageview,
						Configs.HOST + list.get(list.size() - 1).headImg);

			}
			try {
				time.setText(TimeRender.getStandardH(Long.valueOf(list.get(list
						.size() - 1).time) * 1000)
						+ " "
						+ list.get(list.size() - 1).name + " 发表了新情报：");
			} catch (Exception e) {
				e.printStackTrace();
			}
			SpannableString spannableString = FaceConversionUtil.getInstace()
					.getExpressionString(mContext,
							list.get(list.size() - 1).content);
			contentText.setText(spannableString);
			findLinrar.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View view) {
					try {
						Intent intent = new Intent(mContext,
								IntellDetailsActivity.class);
						intent.putExtra("newsid",
								Integer.valueOf(list.get(list.size() - 1).id));
						startActivity(intent);
					} catch (Exception e) {
						// TODO: handle exception
					}
				}
			});

		} else {
			findText.setVisibility(View.VISIBLE);
			findLinrar.setVisibility(View.GONE);
		}
	}

	private void readGrouplist() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					allCateBean = JsonUtils.getAllCateData();
					if (null != allCateBean) {
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
				} catch (Exception e) {
					System.out.println("频道发现数据异常" + e.toString());
					msg.what = Configs.READ_ERROR;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			progressLinear.setVisibility(View.GONE);
			switch (msg.what) {
			case Configs.READ_FAIL:
				break;
			case Configs.READ_SUCCESS:
				setGrouplayout();
				break;
			default:
				break;
			}

		}

		;
	};

	private void setGrouplayout() {
		View view = null;
		int groupsSize = 0;
		int msgSize = 0;
		if (null != allCateBean.groupsList && allCateBean.groupsList.size() > 0) {
			groupsSize = allCateBean.groupsList.size();
		}
		if (null != allCateBean.msgList && allCateBean.msgList.size() > 0) {
			msgSize = allCateBean.msgList.size();
		}
		for (int i = 0; i < groupsSize + msgSize; i++) {
			view = inflater.inflate(R.layout.find_group_list_item, null);
			ImageView head = (ImageView) view.findViewById(R.id.group_headimg);
            LinearLayout linear = (LinearLayout) view.findViewById(R.id.find_group_list_item_view);
			TextView name = (TextView) view.findViewById(R.id.group_name);
			TextView title = (TextView) view
					.findViewById(R.id.find_group_list_item_title);
			title.setVisibility(View.GONE);
			if (i == 0) {
				title.setVisibility(View.VISIBLE);
				title.setText("群组排行榜");
			}
			if (i >= 0 && i < groupsSize) {
				if (allCateBean.groupsList.get(i).cateimg.startsWith("http://")) {
					fb.display(head, allCateBean.groupsList.get(i).cateimg);
				} else {
					fb.display(head,
							Configs.HOST
									+ allCateBean.groupsList.get(i).cateimg);
				}
				name.setText(allCateBean.groupsList.get(i).catename);
				final int finalI = i;
                linear.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						Bundle bundle = new Bundle();
						bundle.putString(
								"id",
								allCateBean.groupsList.get(finalI).cateforgroupid);
						bundle.putString("name",
								allCateBean.groupsList.get(finalI).catename);
						bundle.putString("type", "0");
						Intent intent = new Intent(mContext,
								RecommendGroupListActivity.class);
						intent.putExtras(bundle);
						startActivity(intent);
					}
				});
			}
			if (i == groupsSize) {
				title.setVisibility(View.VISIBLE);
				title.setText("消息源类型");
			}
			if (i >= groupsSize) {
				int position = i - groupsSize;
				if (allCateBean.msgList.get(position).cateimg
						.startsWith("http://")) {
					fb.display(head, allCateBean.msgList.get(position).cateimg);
				} else {
					fb.display(head,
							Configs.HOST
									+ allCateBean.msgList.get(position).cateimg);
				}
				name.setText(allCateBean.msgList.get(position).catename);
				final int finalI = position;
                linear.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						Bundle bundle = new Bundle();
						bundle.putString("id",
								allCateBean.msgList.get(finalI).cateformsgid);
						bundle.putString("name",
								allCateBean.msgList.get(finalI).catename);
						bundle.putString("type", "1");
						Intent intent = new Intent(mContext,
								RecommendGroupListActivity.class);
						intent.putExtras(bundle);
						startActivity(intent);
					}
				});
			}

			groupLayout.addView(view);
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tolistlayout:
			Act.toAct(mContext, IntelligenceListActivity.class);
			break;
		default:
			break;
		}
	}

	public class ReceiveBroadCast extends BroadcastReceiver {

		@Override
		public void onReceive(Context context, Intent intent) {
			jpushBean = (JpushBean) intent.getSerializableExtra("pushBean");
			if (null != jpushBean) {
				findText.setVisibility(View.GONE);
				findLinrar.setVisibility(View.VISIBLE);
				fb.display(qbCircleImageview, Configs.HOST + jpushBean.headImg);
				time.setText(TimeRender.getStandardH(Long
						.valueOf(jpushBean.time) * 1000)
						+ " "
						+ jpushBean.name
						+ " 发表了新情报：");
				SpannableString spannableString = FaceConversionUtil
						.getInstace().getExpressionString(context,
								jpushBean.content);
				contentText.setText(spannableString);
			} else {
				findText.setVisibility(View.VISIBLE);
				findLinrar.setVisibility(View.GONE);
			}
		}

	}

	public void getCountnewsNum() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = null;
				while (isGet) {
					try {
						msg = new Message();
						Thread.sleep(2000);
						num = JsonUtils.getCountnews();
						if (null != num && !TextUtils.isEmpty(num)) {
							msg.what = Configs.READ_SUCCESS;
						} else {
							msg.what = Configs.READ_FAIL;
						}
					} catch (Exception e) {
						e.printStackTrace();
						msg.what = Configs.READ_ERROR;
					}
					mHandler.sendMessage(msg);
					msg = null;
				}
			}
		}).start();
	}

	private Handler mHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				// System.out.println("当前已累积发布" + num + "条情报");
				info_count.setText("当前已累积发布" + num + "条情报");
				break;
			case Configs.READ_FAIL:
				info_count.setText("当前已累积发布0条情报");
				break;
			case Configs.READ_ERROR:
				info_count.setText("当前已累积发布0条情报");
				break;
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		isGet = true;
		getCountnewsNum();
		setQbShow();
	}

	@Override
	public void onStop() {
		super.onStop();
		isGet = false;
	}

	@Override
	public void onHiddenChanged(boolean hidden) {
		if (hidden) {
			isGet = false;
		} else {
			isGet = true;
			getCountnewsNum();
			setQbShow();
		}
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		mContext.unregisterReceiver(receiveBroadCast);
	}
}
