package com.inwhoop.ddcj.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.inwhoop.R;

public class ContactMiserFragment extends Fragment implements OnClickListener {

	/**
	 * 财迷列表
	 */
	private ViewGroup contents = null;
	private RelativeLayout rl_whatch, rl_fans;
	private ImageView iv_whatch, iv_fans;
	private TextView tv_whatch, tv_fans;

	private FragmentManager fm = null;
	private FragmentTransaction ft = null;
	private Fragment[] contacts = { null, null };
	private TextView title;
	/**
	 * 标识当前显示项frag
	 */
	int flag = 0;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		title = ((Tab4ContactsFragment) getParentFragment()).title;
		fm = getChildFragmentManager();
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		contents = (ViewGroup) inflater.inflate(
				R.layout.frag_my_contacts_son_miser, null);
		initComponet(inflater);

		setActivityTitle(true);
		return contents;
	}

	private void initComponet(LayoutInflater inflater) {
        loadFragment(1);
        loadFragment(0);

		rl_whatch = (RelativeLayout) contents.findViewById(R.id.rl_whatch);
		rl_fans = (RelativeLayout) contents.findViewById(R.id.rl_fans);
		rl_whatch.setOnClickListener(this);
		rl_fans.setOnClickListener(this);
		iv_whatch = (ImageView) rl_whatch.findViewById(R.id.iv_whatch);
		iv_fans = (ImageView) rl_fans.findViewById(R.id.iv_fans);
		tv_whatch = (TextView) contents.findViewById(R.id.tv_whatch);
		tv_fans = (TextView) contents.findViewById(R.id.tv_fans);

		int num = ((ContactMiserWhatchAndFansFragment) contacts[0]).num;
		((ContactMiserWhatchAndFansFragment) contacts[0]).setTitleAndFlag(
				title, tv_whatch, 0);
		int num2 = ((ContactMiserWhatchAndFansFragment) contacts[1]).num;
		((ContactMiserWhatchAndFansFragment) contacts[1]).setTitleAndFlag(
				title, tv_fans, 1);
		tv_whatch.setText("关注(" + num + ")");
		tv_fans.setText("粉丝(" + num2 + ")");
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.rl_whatch:
			iv_fans.setVisibility(View.INVISIBLE);
			iv_whatch.setVisibility(View.VISIBLE);
			loadFragment(0);
			break;
		case R.id.rl_fans:
			iv_whatch.setVisibility(View.INVISIBLE);
			iv_fans.setVisibility(View.VISIBLE);
			loadFragment(1);
			break;

		}
	}

	/**
	 * 加载Frag，标识当前frag
	 * 
	 * @param position
	 */
	private void loadFragment(int position) {

		ft = fm.beginTransaction();
		int length = contacts.length;
		for (int i = 0; i < length; i++) {
			if (contacts[i] != null) {
				ft.hide(contacts[i]);
			}
		}
		switch (position) {
		case 0:
			if (null == contacts[0]) {
				contacts[0] = new ContactMiserWhatchAndFansFragment(0);
				ft.add(R.id.ll_misers, contacts[0]);
			} else {
				ft.show(contacts[0]);
			}
			setActivityTitle(true);
			break;
		case 1:
			if (null == contacts[1]) {
				contacts[1] = new ContactMiserWhatchAndFansFragment(1);
				ft.add(R.id.ll_misers, contacts[1]);

			} else {
				ft.show(contacts[1]);
			}
			setActivityTitle(false);
			break;
		}
		ft.commit();
		flag = position;
	}

	/**
	 * 设置子Frag对应的标题
	 * 
	 * @param who
	 */
	public void setActivityTitle(boolean who) {
		if (who) {
			// 关注
			int num = ((ContactMiserWhatchAndFansFragment) contacts[0]).num;
			title.setText("关注(" + num + ")");
		} else {
			// 粉丝
			int num = ((ContactMiserWhatchAndFansFragment) contacts[1]).num;
			title.setText("粉丝(" + num + ")");
		}
	}
}
