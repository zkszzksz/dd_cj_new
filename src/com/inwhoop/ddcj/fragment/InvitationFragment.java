package com.inwhoop.ddcj.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.inwhoop.R;
import com.inwhoop.ddcj.activity.IntelligenceListActivity;
import com.inwhoop.ddcj.activity.InvitationActivity;
import com.inwhoop.ddcj.activity.InvitationContentMeetingActivity;
import com.inwhoop.ddcj.activity.LoginActivity;
import com.inwhoop.ddcj.activity.StartActivity;
import com.inwhoop.ddcj.adapter.InvitationListAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.InvitationListInfo;
import com.inwhoop.ddcj.impl.AlertLoginLisenner;
import com.inwhoop.ddcj.impl.MLocationLinsener;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * 频道分类界面（沸点财经等）
 * 
 * @author ligang@inwhoop.com
 * @version V1.0
 * @Project: DDCJ
 * @Title: ChannelTypeFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 * @date 2014-11-6 下午7:54:51
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 */
public class InvitationFragment extends Fragment implements IXListViewListener,
		OnItemClickListener, MLocationLinsener {

	private static final String ARG_POSITION = "position";

	private Context mContext;

	private int position; // 标识的第几个：String[] titles =
							// {"全部","同城","群组","好友","关注","我的"};

	private XListView mListView;

	private List<InvitationListInfo> dataList = new ArrayList<InvitationListInfo>();

	private InvitationListAdapter adapter;

	private boolean isInit = true; // 是否可以开始加载数据

	private String city = "";

	private LinearLayout alertLogin;

	private String ddid;

	public static InvitationFragment newInstance(int position) {
		InvitationFragment cFragment = new InvitationFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		cFragment.setArguments(b);
		return cFragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		isInit = true;
		mContext = getActivity();
		position = getArguments().getInt(ARG_POSITION);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.invitation_item_fragment_layout,
				null);
		init(view);
		// showData();
		return view;
	}

	private void init(View view) {
		ddid = UserInfoUtil.getUserInfo(mContext).ddid;
		mListView = (XListView) view.findViewById(R.id.invitation_xListView);
		alertLogin = (LinearLayout) view.findViewById(R.id.alert_login);
		// if (TextUtils.isEmpty(ddid)) {
		// if (position > 0) {
		// alertLogin.setVisibility(View.VISIBLE);
		// mListView.setVisibility(View.GONE);
		alertLogin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// Act.toAct(mContext, LoginActivity.class);
				Intent intent = new Intent(getActivity(), LoginActivity.class);
				getActivity();
				startActivityForResult(intent, Activity.RESULT_FIRST_USER);
			}
		});
		//
		// }
		// }
		mListView.setXListViewListener(this);
		mListView.setOnItemClickListener(this);
		adapter = new InvitationListAdapter(getActivity(),
				new ArrayList<InvitationListInfo>());
		mListView.setAdapter(adapter);
		mListView.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				Bundle bundle = new Bundle();
				bundle.putSerializable("info",
						adapter.getAll().get(position - 1));
				Act.toAct(mContext, InvitationContentMeetingActivity.class,
						bundle);
			}
		});

	}

	private void getData(final boolean flag) {
		ddid = UserInfoUtil.getUserInfo(mContext).ddid;
		if (TextUtils.isEmpty(ddid)) {
			if (position > 0) {
				return;
			}
		}

		new Thread(new Runnable() {

			@Override
			public void run() {
				Message msg = new Message();
				try {
					String uid = UserInfoUtil.getUserInfo(mContext).userid;
					Object[] obj = null;
					if (null == adapter)
						adapter = new InvitationListAdapter(getActivity(),
								new ArrayList<InvitationListInfo>());
					if (adapter.getAll().size() > 0) {
						obj = JsonUtils.getInvitationlist(
								uid,
								position + 1,
								10,
								adapter.getAll().get(
										adapter.getAll().size() - 1).id, flag,
								city);
					} else {
						obj = JsonUtils.getInvitationlist(uid, position + 1,
								10, 0, flag, city);
					}
					msg.what = Configs.READ_SUCCESS;
					if (flag) {
						msg.arg1 = 1;
					} else {
						msg.arg1 = 2;
					}
					msg.obj = obj;
				} catch (Exception e) {
					System.out.printf("exception:" + e.toString());
					msg.what = Configs.READ_FAIL;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		public void handleMessage(android.os.Message msg) {
			onLoad();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Object[] obj = (Object[]) msg.obj;
				if ((Boolean) obj[0]) {
					if (msg.arg1 == 1) {
						adapter.getAll().clear();
					}
					dataList = (List<InvitationListInfo>) obj[2];
					try {
						dataList = getHandleList(dataList);
					} catch (ParseException e) {
						e.printStackTrace();
					}
					if (dataList.size() >= 10) {
						mListView.setPullLoadEnable(true);
					} else {
						mListView.setPullLoadEnable(false);
					}
					adapter.addChannelList(dataList);
					adapter.notifyDataSetChanged();
				} else {
					Toast.makeText(getActivity(), obj[1].toString(), 0).show();
				}
				break;

			case Configs.READ_FAIL:
				Toast.makeText(getActivity(), "数据加载失败", 0).show();
				break;
			}
		}
	};

	private void onLoad() {
		mListView.stopRefresh();
		mListView.stopLoadMore();
	}

	private void showData() {
		if (isInit) {
			isInit = false;// 加载数据完成
			// 加载各种数据
			if (position == 1) {
				((InvitationActivity) getActivity()).startLocation(this);
			} else {
				getData(true);
			}
		}
	}

	@Override
	public void onRefresh() {
		isInit = true;
		showData();
	}

	@Override
	public void onLoadMore() {
		getData(false);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {

	}

	@Override
	public void setUserVisibleHint(boolean isVisibleToUser) {
		super.setUserVisibleHint(isVisibleToUser);
		if (isVisibleToUser) {
			showData();
		}
	}

	public List<InvitationListInfo> getHandleList(List<InvitationListInfo> list)
			throws ParseException {
		for (int i = 0; i < list.size(); i++) {
			String[] str = TimeRender.getTimeStrs(list.get(i).begintime);
			list.get(i).week = str[0];
			list.get(i).hourmin = str[1];
			list.get(i).time = str[2];
		}
		return list;
	}

	@Override
	public void onResume() {
		super.onResume();
		ddid = UserInfoUtil.getUserInfo(mContext).ddid;
		if (TextUtils.isEmpty(ddid)) {
			if (position > 0) {
				alertLogin.setVisibility(View.VISIBLE);
				mListView.setVisibility(View.GONE);
				return;
			}
		} else {
			alertLogin.setVisibility(View.GONE);
			mListView.setVisibility(View.VISIBLE);
		}

		if (Configs.isRe) {
			isInit = true;
			showData();
			Configs.isRe = false;
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if (resultCode == Configs.RESULT_CODE) {
			alertLogin.setVisibility(View.GONE);
			mListView.setVisibility(View.VISIBLE);
			isInit = true;
			showData();
		}
	}

	@Override
	public void mLocationSucess(String address) {
		city = address;
		getData(true);
	}

	@Override
	public void mLocationFaile(String errorInfo) {
		// TODO Auto-generated method stub

	}

}
