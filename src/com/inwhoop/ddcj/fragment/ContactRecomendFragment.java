package com.inwhoop.ddcj.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

import android.widget.AdapterView;
import android.widget.LinearLayout;

import com.google.gson.Gson;
import com.inwhoop.R;
import com.inwhoop.ddcj.activity.LoginActivity;
import com.inwhoop.ddcj.activity.PersonInfoActivity;
import com.inwhoop.ddcj.adapter.RecomendsAdapter;
import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.bean.PhoneInfo;
import com.inwhoop.ddcj.bean.RecomendsInfo;
import com.inwhoop.ddcj.bean.RecommenBean;
import com.inwhoop.ddcj.bean.SendPhone;
import com.inwhoop.ddcj.bean.SendPhoneData;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.Act;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.view.XListView;
import com.inwhoop.ddcj.view.XListView.IXListViewListener;

/**
 * 联系人推荐
 * 
 * @Project: DDCJ
 * @Title: ContactRecomendFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 * 
 * @author ligang@inwhoop.com
 * @date 2015-2-6 下午4:45:27
 * @Copyright: 2015 成都呐喊信息 All rights reserved.
 * @version V1.0
 */
public class ContactRecomendFragment extends Fragment implements
		IXListViewListener {

	private XListView lv_recomends = null;
	private ViewGroup contents = null;
	private List<RecomendsInfo> mData = null;
	private Context context;
	private UserBean userBean;
	private int page = 1;
	private List<RecommenBean> list;
	private List<RecommenBean> orderList = new ArrayList<RecommenBean>();// 排序后的数据
	private RecomendsAdapter recomends_adapter;
	private boolean isLoad = false;
	private LinearLayout progressLinear;
	private LinearLayout alertLogin;
	private String ddid;
	private SendPhoneData sPhoneData;
	private boolean isInit = true;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.context = getActivity();
		this.contents = (ViewGroup) inflater.inflate(
				R.layout.frag_my_contacts_son_groups, null);
		initComponet(inflater);
		sPhoneData = new SendPhoneData();
		getPhoneContacts();
		return this.contents;

	}

	private void initComponet(LayoutInflater inflater) {
		recomends_adapter = new RecomendsAdapter(context);
		alertLogin = (LinearLayout) contents.findViewById(R.id.alert_login);
		progressLinear = (LinearLayout) contents
				.findViewById(R.id.progress_linear);
		progressLinear.setVisibility(View.VISIBLE);
		lv_recomends = (XListView) contents.findViewById(R.id.lv_group);
		lv_recomends.setXListViewListener(this);
		lv_recomends.setPullLoadEnable(false);
		lv_recomends.setAdapter(recomends_adapter);
		// getData();

		lv_recomends
				.setOnItemClickListener(new AdapterView.OnItemClickListener() {
					@Override
					public void onItemClick(AdapterView<?> adapterView,
							View view, int i, long l) {
						Intent intent = new Intent(context,
								PersonInfoActivity.class);
						String userid = recomends_adapter.getList().get(i - 1).userid;
						intent.putExtra("userid", userid);
						startActivity(intent);
					}
				});
	}

	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		page = 1;
		isLoad = false;
		getData();
	}

	@Override
	public void onLoadMore() {
		// TODO Auto-generated method stub
		page++;
		isLoad = true;
		getData();
	}

	private void getData() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					userBean = UserInfoUtil.getUserInfo(context);
					list = JsonUtils.getFriendRecommend(userBean.userid,
							getPhoneJson(sPhoneData));
					orderData();
					if (null != orderList && orderList.size() > 0) {
						// if (list.size() < Configs.COUNT) {
						// lv_recomends.setPullLoadEnable(false);
						// }
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
				} catch (Exception e) {
					e.printStackTrace();
					msg.what = Configs.READ_ERROR;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private void orderData() {
		List<RecommenBean> bookList = new ArrayList<RecommenBean>();
		List<RecommenBean> strangeList = new ArrayList<RecommenBean>();
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).otherinfo.contains("通讯录好友")) {
				bookList.add(list.get(i));
			} else {
				strangeList.add(list.get(i));
			}
		}
		orderList.addAll(bookList);
		orderList.addAll(strangeList);
	}

	/**
	 * 获取本地通讯录
	 * 
	 * @Title: getPhoneContacts
	 * @Description: TODO
	 * @param @return
	 */
	private void getPhoneContacts() {
		ContentResolver resolver = context.getContentResolver();
		ArrayList<SendPhone> sendPhones = new ArrayList<SendPhone>();
		String[] columns = new String[] { Phone.DISPLAY_NAME, Phone.NUMBER,
				Phone.PHOTO_ID, Phone.CONTACT_ID };
		Cursor cursor = resolver.query(Phone.CONTENT_URI, columns, null, null,
				null);
		while (cursor.moveToNext()) {
			SendPhone sPhone = new SendPhone();
			String name = cursor.getString(cursor
					.getColumnIndex(Phone.DISPLAY_NAME));
			String number = cursor
					.getString(cursor.getColumnIndex(Phone.NUMBER))
					.replaceAll("\\s*", "").replace("+86", "");
			if (number.startsWith("17951")) {
				number = number.replace("17951", "");
			}
			if (TextUtils.isEmpty(number)) { // 判断号码是否为空
				continue;
			}
			sPhone.number = number;
			sPhone.name = name;
			sendPhones.add(sPhone);
		}
		sPhoneData.phone = sendPhones;
		cursor.close();
	}

	/**
	 * 生成上传通讯录的json
	 * 
	 * @param @param phone
	 * @param @return
	 * @return String
	 * @Title: getPhoneJson
	 * @Description: TODO
	 */
	private String getPhoneJson(SendPhoneData phone) {
		String phoneJson = "";
		Gson gson = new Gson();
		phoneJson = gson.toJson(phone);
		return phoneJson;
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			progressLinear.setVisibility(View.GONE);
			lv_recomends.stopRefresh();
			lv_recomends.stopLoadMore();
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				recomends_adapter.addList(orderList, isLoad);
				recomends_adapter.notifyDataSetChanged();
				break;
			case Configs.READ_FAIL:
				break;
			case Configs.READ_ERROR:
				break;
			}
		}
	};

	@Override
	public void onResume() {
		super.onResume();
		ddid = UserInfoUtil.getUserInfo(context).ddid;
		if (TextUtils.isEmpty(ddid)) {
			alertLogin.setVisibility(View.VISIBLE);
			lv_recomends.setVisibility(View.GONE);
			progressLinear.setVisibility(View.GONE);
			alertLogin.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					Act.toAct(context, LoginActivity.class);
				}
			});
			return;
		} else {
			alertLogin.setVisibility(View.GONE);
			lv_recomends.setVisibility(View.VISIBLE);
			if (isInit) {
				progressLinear.setVisibility(View.VISIBLE);
				getData();
				isInit = false;
			}
		}

	}

}
