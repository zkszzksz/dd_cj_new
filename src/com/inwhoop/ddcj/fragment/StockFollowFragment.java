package com.inwhoop.ddcj.fragment;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.TextView;

import com.inwhoop.R;

/**
 * 
 * @Project: DDCJ
 * @Title: StockNewInfoFragment.java
 * @Package com.inwhoop.ddcj.fragment
 * @Description: TODO
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-10-22 下午7:51:34
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 * 
 *          个股关联最新情报
 * 
 */
public class StockFollowFragment extends BaseFragment {
	private View mainView;

	private TextView gfTextView, stTextView, msTextView;

	private List<TextView> textlist = new ArrayList<TextView>();

	private StockFollowItemFragment gfFollowItemFragment,
			cityFollowItemFragment, otherFollowItemFragment;

	private FragmentTransaction mFt;

	public int nowposition = 0;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mContext = getActivity();
		mainView = inflater.inflate(R.layout.follow_item_layout, null);
		gfTextView = (TextView) mainView.findViewById(R.id.gf_tv);
		stTextView = (TextView) mainView.findViewById(R.id.sc_tv);
		msTextView = (TextView) mainView.findViewById(R.id.ms_tv);
		textlist.add(gfTextView);
		textlist.add(stTextView);
		textlist.add(msTextView);
		for (int i = 0; i < textlist.size(); i++) {
			textlist.get(i).setTag(i);
			textlist.get(i).setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v) {
					int pos = (Integer) v.getTag();
					nowposition = pos;
					setNowPostionKLine(pos);
				}
			});
		}
		setNowPostionKLine(nowposition);
		return mainView;
	}

	public void setNowPostionKLine(int position) {
		for (int i = 0; i < textlist.size(); i++) {
			textlist.get(i).setTextColor(
					getResources().getColor(R.color.stock_follow_title));
			textlist.get(i).setBackgroundDrawable(null);
		}
		textlist.get(position).setTextColor(
				getResources().getColor(R.color.white));
		textlist.get(position).setBackgroundResource(
				R.drawable.stock_follow_shape);
		mFt = getActivity().getSupportFragmentManager().beginTransaction();
		if (null != gfFollowItemFragment) {
			mFt.hide(gfFollowItemFragment);
		}
		if (null != cityFollowItemFragment) {
			mFt.hide(cityFollowItemFragment);
		}
		if (null != otherFollowItemFragment) {
			mFt.hide(otherFollowItemFragment);
		}
		if (position == 0) {
			if (null == gfFollowItemFragment) {
				gfFollowItemFragment = new StockFollowItemFragment(0);
				mFt.add(R.id.followframelayout, gfFollowItemFragment);
			} else {
				mFt.show(gfFollowItemFragment);
				gfFollowItemFragment.setOnloadMorelistener();
			}
		} else if (position == 1) {
			if (null == cityFollowItemFragment) {
				cityFollowItemFragment = new StockFollowItemFragment(1);
				mFt.add(R.id.followframelayout, cityFollowItemFragment);
			} else {
				mFt.show(cityFollowItemFragment);
				cityFollowItemFragment.setOnloadMorelistener();
			}
		} else {
			if (null == otherFollowItemFragment) {
				otherFollowItemFragment = new StockFollowItemFragment(2);
				mFt.add(R.id.followframelayout, otherFollowItemFragment);
			} else {
				mFt.show(otherFollowItemFragment);
				otherFollowItemFragment.setOnloadMorelistener();
			}
		}
		mFt.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
		mFt.commit();
	}

}
