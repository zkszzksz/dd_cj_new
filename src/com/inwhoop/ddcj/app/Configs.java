package com.inwhoop.ddcj.app;

import android.app.Application;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import cn.jpush.android.api.JPushInterface;
import com.inwhoop.ddcj.activity.LoginActivity;
import com.inwhoop.ddcj.bean.CateChannel;
import com.inwhoop.ddcj.db.SQLHelper;
import com.inwhoop.ddcj.util.ExitAppUtils;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.List;

/**
 * @Describe: TODO * * * ****** Created by ZK ********
 * @Date: 2014/10/20 17:11
 * @Copyright: 2014 成都呐喊信息 All rights reserved.
 * @version: 1.0
 */
public class Configs extends Application {

	public static int widthPixels = 0;
	public static int heightPixels = 0;

	public static String groupid = "-1";

	public static String friendid = "-1";

	public static final int QINGBAO_COMMENT_TAG = 1;// 情报跳转到评论界面的标示

	public static final int NEWS_COMMENT_TAG = 2;// 资讯跳转到评论界面的标示

	public static final int COUNT = 5;// 每次获取网络数据的数量
	
	public static final int RESULT_CODE = 100;// 请求结果码

	public static boolean isRe = false;

	// 推送相关的变量
	public static final String MESSAGE_RECEIVED_ACTION = "com.example.jpushdemo.MESSAGE_RECEIVED_ACTION";
	public static final String PUSH_ACTION1 = "com.inwhoop.ddcj.PUSH_ACTION1";
	public static final String PUSH_ACTION6 = "com.inwhoop.ddcj.PUSH_ACTION6";
	public static final String PUSH_ACTION7 = "com.inwhoop.ddcj.PUSH_ACTION7";
	public static final String KEY_TITLE = "title";
	public static final String KEY_MESSAGE = "message";
	public static final String KEY_EXTRAS = "extras";

	public static final String COME_MESSAGE = "com.inwhoop.ddcj.comemessage";
	public static final String COME_MESSAGE1 = "com.inwhoop.ddcj.comemessage1";
	public static final String COME_MESSAGE2 = "com.inwhoop.ddcj.comemessage2";

	public static final String DB_NAME = "DDCJ.db";
	public static final String HOST = "http://120.24.52.36:8090";
	public static final String LOCATION_HOST = "http://192.168.0.245/Ddcj/index.php";

	public static final String HTTP_UPDATA = "http://yxg.mxcod.com/huaxi/";
	public static final String FAG = "\r\n";
	public static String UPDATA_SERVER;
	public static final String UPDATA_VERJSON = "version.json";
	public static final String UPDATA_SAVEAPK = "xiwangcheng.apk";

	public final static int READ_SUCCESS = 200; // 读取网络数据成功
	public final static int READ_FAIL = 500; // 读取网络为空
	public final static int READ_ERROR = 600; // 读取网络错误
	public final static int DELETE_FAIL = 1; // 删除失败
	public final static int DELETE_SUCCESS = 2; // 删除成功

	public final static int SEARCH_USER_SUCESS = 21; // 用户搜索成功
	public final static int SEARCH_USER_FAIL = 22; //
	public final static int SEARCH_GROUP_SUCESS = 23; // 群组搜索成功
	public final static int SEARCH_GROUP_FAIL = 24; //
	public final static int SEARCH_NEWS_SUCESS = 25; // 资讯搜索成功
	public final static int SEARCH_NEWS_FAIL = 26; //

	public final static int MSG_BODY = 88; // 消息
	public static boolean messagelistishiden = false;
	public final static String ACTION_MESSAGE = "com.inwhoop.gameproduct.addmessage";
	public final static String CHAT_AGAIN_INIT = "com.inwhoop.gameproduct.chatinit";
	public final static String RED_SHOW = "com.inwhoop.gameproduct.redshow";
	public final static String RED_HIDE = "com.inwhoop.gameproduct.redhide";

	public static final String SETTING_INFO = "SETTING_INFO";
	public static final String USER_ID = "USER_ID";
	public static final String PASSWORD = "PASSWORD";
	public static final String NAME = "USER_NAME";
	public static final String USER_IMG = "USER_IMG";
	public static final String USER_SINA_ID = "USER_SINA_ID";
	public static final String USER_SEX = "USER_SEX";
	public static final String USER_DDID = "USER_DDID";
	public static final String USER_QQID = "USER_QQID";
	public static final String LOCATION = "LOCATION";
	public static final String MOBILE = "USER_MOBILE";
	public static final String OTHER_INFO = "OTHER_INFO";
	public static final String OLD_NORMAL_PWD = "OLD_NORMAL_PWD";
	public static final String USER_WEIXIN_ID = "USER_WEIXIN_ID";

	public static final String GET_STOCKINFO = "http://hq.sinajs.cn/list="; // 新浪api获取自选股信息列表

	public static final String USER_REGISTER = "/userapi/RegisterUser"; // 用户注册
	public static final String USER_GETCODE = "/userapi/Validatetel"; // 注册时第一步，验证并发送短信
	public static final String USER_CHECK_YZM = "/userapi/checkyzm"; // 注册时第2步，请求验证码是否正确
	public static final String USER_FORGET_GETCODE = "/userapi/forgetpwdTel"; // 找回密码，验证码
	public static final String USER_GETPWD = "/userapi/forgetpwd";
	public static final String UPDATE_USER = "/userapi/updatemsg"; // 修改个人信息
	public static final String USER_UPDATE_PWD = "/userapi/updatePwd"; // 修改密码

	public static final String USER_LOGIN = "/userapi/login";// 登录
	public static final String GET_MY_STOCK_LIST = "/mystock/Getmystocklist"; // 获取我的自选股
	public static final String ADD_STOCK = "/mystock/addstock"; // 添加自选股
	public static final String CANCEL_STOCK = "/mystock/delstock"; // 取消自选股
	public static final String ORDER_LIST = "/mystock/mystockinfoorder";// 上传排序list
	public static final String DELETE_LIST = "/mystock/delmystocklist";// 批量删除接口

	public static final String CHANNEL_INFO_ID = "/mystock/Getuserchannel"; // 获取当前用户关注的所有频道下的资讯Id
	public static final String GET_CHANNEL_LIST = "/anonymous/subscribe"; // 获取用户频道列表
	public static final String ADD_USER_CHANNEL = "/mystock/Addsubscribe"; // 添加用户频道列表
	public static final String CANCLE_USER_CHANNEL = "/mystock/delsubscribe"; // 删除用户频道列表
	public static final String BIND_USER = "/userapi/bind"; // 已有账户绑定 第三方
															// （在登录界面）
	public static final String LOGIN_BIND_USER = "/userapi/loginBind"; // 已有账户绑定
																		// 第三方（已经登录之后）
	public static final String SEARCH_STOCK = "/mystock/serach"; // 自选股搜索接口

	// public static final String SEARCH_CHANNEL = "/mystock/subscribe/";//
	// 查询用户频道接口 //错误
	public static final String INSERT_PERSON_NEWS = "/mystock/Insertpersonnews/"; // 往personnews
																					// 插入新闻数据
	public static final String SUBCATEG_INFO = "/anonymous/Subcateginfo/";// 优选列表,点单独一项进去后的列表的查询
	public static final String SEARCH_MY_COMMENT = "/mystock/Comment/"; // 根据uid查询对我的评论
	public static final String NEWS_INFO = "/anonymous/Newsinfo/"; // 根据stockcode查询新闻
	public static final String SEARCH_NEWS_FOR_LOCATION = "/mystock/Getpersonnews";// 根据地理位置查询新闻
	public static final String NEWS_COUNT = "/anonymous/newscount"; // 新闻总条数
	public static final String WHAT_RANKING_LIST = "/anonymous/cate"; // 排行榜分类查询
	public static final String DEL_FOUCS_STOCK = "/mystock/Delfoucsstock"; // 删除用户追踪股票
	public static final String ADD_FOCUS_STOCK = "/mystock/Addfocusstock";
	public static final String MY_FOCUS_STOCK = "/mystock/Myfocusstock"; // 查看用户追踪股票
	public static final String GET_USER_CHANNEL = "/anonymous/Getuserchannel"; // 获取当前用户关注的所有频道下的资讯Id

	public static final String DIAN_ZAN = "/news/dianzan";// 点赞
	public static final String SEND_MSG = "/news/Postmsg";// 发帖
	public static final String THREAD_MSG = "/news/threadmsg"; // 用户跟帖接口
	public static final String COLLECT_NEWS = "/news/collection"; // 收藏资讯接口
	public static final String FORWARD_MSG = "/news/forwardmsg"; // 信息转发接口
	public static final String COMMENT_LIST = "/news/GetComment";// 获取评论
	public static final String NEWS_LIST = "/Anonymous/Getchannellistbyid/";// 获取频道资讯列表

	public static final String NEWS_COMMENT = "/mystock/Newscomment"; // 获得频道资讯的评论列表

	public static final String NEWS_CONTENT = "/mystock/readnews/"; // 获取具体的新闻/资讯详情
	public static final String USER_FRIENDS_LIST = "/UserFriend/Friendslist/"; // 好友列表接口
	public static final String ATTENTION_USER = "/UserFriend/AttentionUser/"; // 关注好友接口
	public static final String CANCEL_ATTENTION = "/UserFriend/CancelAttentio"; // 取消关注好友接口
																				// 1
	public static final String MY_CONCERN = "/UserFriend/MyConcern"; // 我的关注接口
																		// ?????
	public static final String MY_FANCE = "/UserFriend/MyFance"; // 关注我的接口
	public static final String EDIT_QIBAO = "/index.php/mystock/Insertpersonnews"; // 关注我的接口
	public static final String SET_SUGGEST = "/mystock/Advise/"; // 上传意见反馈
	public static final String CHANNELTONEWS = "/mystock/Channeltonews/"; // 资讯转载到情报的接口
	public static final String INVITATION = "/mystock/Invitation/"; // 发布邀请函接口
	public static final String GETINVITATION = "/mystock/Getinvitation/"; // 邀请函列表获取接口
	public static final String JOININVITATION = "/mystock/Joininvitation/"; // 用户加入邀请函接口
	public static final String INVITATIONUSERLIST = "/mystock/Invitationuserlist/"; // 邀请人列表接口
	public static final String INVITATIONCONTENT = "/anonymous/Invitationcontent/"; // 邀请函详细信息
	public static final String COUNTINVITATION = "/anonymous/Countinvitation/"; // 邀请函参与人数接口
	public static final String DELINVITATIONUSER = "/mystock/Delinvitationuser/"; // 删除邀请函
	public static final String COMMENTINVITATION = "/mystock/Commentinvitation/"; // 邀请函评论接口
	public static final String TEL = "/mystock/tel/"; // 上传本地通讯录接口
	public static final String SETTING_UPDATE_STEALTH_MODE = "/index.php/UserFriend/SetUserState";
	public static final String INVATATION_LIST = "/mystock/Getinvitation"; // 邀请函列表
	public static final String USER_ATTENTION_LIST = "/UserFriend/MyConcern";
	public static final String USER_FANS_LIST = "/UserFriend/toMyFance";
	public static final String BAOMING_INVITATION = "/mystock/Joininvitation"; // 邀请函报名
	public static final String ORDER_CHANNEL = "/mystock/Channelscribte"; // 用户订阅频道
	public static final String CANCLE_ORDER_CHANNEL = "/mystock/Cancelchannel"; // 用户取消订阅频道
	public static final String SEARCH_FRIEND = "/index.php/Userapi/Search"; // 通过用户、群组ID搜索
	public static final String INVITATIONINFO = "/mystock/Invitationcontent"; // 邀请函详情
	public static final String INVITATION_COMMENTLIST = "/anonymous/Commentinvitationlist"; // 邀请函评论列表
	public static final String INVITATION_SENDCOMMENT = "/mystock/Commentinvitation"; // 邀请函评论
	public static final String NEWS_SENDCOMMENT = "/mystock/Createcomment"; // 发表资讯评论
	public static final String QINGBAO_INFO = "/anonymous/Readpersonnews"; // 情报详情
	public static final String FINDUSER = "/index.php/UserFriend/Finduser"; // 发布邀请函
	public static final String FINDUSERS = "/index.php/UserFriend/Findusers"; // 发布邀请函
	public static final String STOCKCODECONNECT_QINGBAO = "/anonymous/Stockcodepersonnews"; // 个股关联情报
	public static final String STOCK_FOLLOW = "/mystock/theynotice/"; // 他们也关注

	public static final String PUSH_INVITATION = "/index.php/mystock/Invitation"; // 发布邀请函
	public static final String SHARE = "/anonymous/Shareplusone"; // 分享接口
	public static final String TOP_NEWS = "/anonymous/topnews"; // 推荐置顶的资讯
	public static final String IF_FOUCE = "/mystock/Iffoucsstock"; // 用户是否关注了某只股票
	public static final String CITY_FRIEND = "/UserFriend/Cityfriend";// 查询同城的用户
	public static final String GET_QINGBAOCOMMENTLIST = "/anonymous/Commentpersonnews"; // 获取情报评论列表
	public static final String SEND_QINGBAOCOMMENT = "/mystock/Personnewscomment"; // 发表评论
	public static final String SEND_COMMENTTOQINGBAO = "/mystock/topersonnewscomment"; // 对情报发表评论
	public static final String GETALLCATE = "/index.php/Group/GetAllCate"; // 发表评论
	public static final String GETCATECHANNEL = "/index.php/Group/GetCateChannel"; // 发表评论
	public static final String DELETE_INVITATION = "/mystock/Delinvitationuser/";// 删除邀请函

	public static final String GET_CHANNELLIST = "/mystock/Getuserchannellist";// 获取频道专栏
	public static final String NEWS_COLLECT = "/mystock/Newscollectadd";// 收藏资讯接口
	public static final String COUNTNEWS = "/Anonymous/Countnews";// 情报数
	public static final String PERSONNEWSTONEWSINFO = "/index.php/mystock/personnewstonewsinfo/";// 情报转载到频道
	public static final String CANCEL_COLLECT = "/mystock/Newscollectdel";// 删除用户收藏
	public static final String GET_COMMENTLIST = "/mystock/Getcommentbyid";// 评论我的列表
	public static final String DELETE_COMMENT = "/mystock/Deletepersonnewscomment";// 删除评论
	public static final String USER_GROUP_LIST = "/Group/GetAGroupList"; // 得到用户的群组
	public static final String CREATECHANNEL = "/index.php/News/CreateChannel"; // 创建频道
	public static final String APPLAYJOINGROUP = "/index.php/News/Applayjoingroup"; // 申请加入群
	public static final String HANDLEJOINGROUP = "/index.php/News/Handlejoingroup"; // 处理加群申请接口
	public static final String GETGROUPINFO = "/index.php/Group/GetInfoApp"; // 获取群信息接口
	public static final String INVITEJOINGROUP = "/index.php/Group/HandleJoinGroup"; // 邀请加群接口
	public static final String QUITGROUP = "/index.php/Group/Quitgroup"; // 退群接口
	public static final String DELGROUP = "/index.php/Group/DelGroup"; // 删群接口
	public static final String GETUSERCOLLECT = "/index.php/mystock/Newscollectlist/"; // 收藏接口

	public static final String UPLOAD_CHANNELLIST = "/mystock/Recordchannel"; // 用户自定义频道排序
	public static final String GET_GROUPINFO = "/Group/GetInfoApp"; // 获取群组信息
	public static final String FRIENDRECOMMEND = "/UserFriend/Friendrecommend"; // 获取群组信息
	public static final String GETGROUPFANS = "/Group/GetGroupFans"; // 获取群粉丝列表
	public static final String GETGROUPITEM = "/index.php/Group/GetGroupUser"; // 获取群成员列表
	public static final String INVITEJOIN = "/Group/InviteJoin"; // 获取群成员列表
	public static final String FEEDBACK = "/News/Feedback"; // 反馈

	public static final String ALL_SEARCH_USER = "/Search/Searchuser"; // 大搜索好友/
	public static final String GET_GROUPCHAT_TYPE = "/Group/GetGroupType"; // 获取群组模式
	public static final String SET_GROUPCHAT_TYPE = "/Group/SetGroupType"; // 设置群组模式
	public static final String ALL_SEARCH_GROUP = "/Search/Searchgroup"; // 大搜索群组
	public static final String ALL_SEARCH_NEWS = "/Search/Searchnews"; // 大搜索资讯
	
	public static final String LOGINOUT = "/UserFriend/Loginout"; // 退出登录

	private static Configs mAppApplication;
	public static List<CateChannel> groupList;
    public static boolean isCareChage =true; //用于关注界面的列表是否刷新
    private SQLHelper sqlHelper;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		mAppApplication = this;
		JPushInterface.setDebugMode(true);
		JPushInterface.init(this);
	}

	/** 获取Application */
	public static Configs getApp() {
		return mAppApplication;
	}

	/** 获取数据库Helper */
	public SQLHelper getSQLHelper() {
		if (sqlHelper == null)
			sqlHelper = new SQLHelper(mAppApplication);
		return sqlHelper;
	}

	/** 摧毁应用进程时候调用 */
	public void onTerminate() {
		if (sqlHelper != null)
			sqlHelper.close();
		super.onTerminate();
	}

	public void clearAppCache() {
	}

	public void loginout() {
		UserInfoUtil.clear(getApplicationContext());
		ExitAppUtils.getInstance().exit();
		handler.sendEmptyMessageDelayed(0, 100);
	}

	private Handler handler = new Handler() {
		public void handleMessage(Message msg) {
			Intent intent = new Intent(getApplicationContext(),
					LoginActivity.class);
			intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);
		};
	};

}
