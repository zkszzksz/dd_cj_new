package com.inwhoop.ddcj.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.inwhoop.ddcj.bean.JpushBean;
import com.inwhoop.ddcj.bean.UserBean;
import com.inwhoop.ddcj.util.UserInfoUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Administrator on 2015/1/6.
 */
public class PushMsgDb {
	private DatabaseDBhelper dbhelper = null;
	private UserBean userBean;
	private SQLiteDatabase db;

	public PushMsgDb(Context context) {
		dbhelper = new DatabaseDBhelper(context);
		userBean = UserInfoUtil.getUserInfo(context);
	}

	/**
	 * 保存推送的消息
	 * 
	 * @param bean
	 *            推送的消息Bean
	 */
	public void saveMsg(JpushBean bean) {
		try {
			db = dbhelper.getWritableDatabase();
			if (null != db && db.isOpen()) {
				db.execSQL(
						"INSERT INTO pushdb (pushid,content,sender,time,headImg,type,name,userid,isagree,isread) VALUES (?,?,?,?,?,?,?,?,?,?)",
						new Object[] { bean.id, bean.content, bean.sender,
								bean.time, bean.headImg, bean.type, bean.name,
								userBean.userid, bean.isAgree, bean.isread });
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != db && db.isOpen()) {
				db.close();
			}
		}

	}

	/**
	 * 查询保存的推送信息
	 * 
	 * @param userId
	 *            用户的ID
	 * @return
	 */
	public List<JpushBean> getQbData(String userId) {
		List<JpushBean> list = new ArrayList<JpushBean>();
		try{
			db = dbhelper.getReadableDatabase();
			JpushBean bean;
			Cursor cursor = db
					.rawQuery(
							"select *  from  pushdb where userid=? and type>1 ORDER by id desc", // type=?
																									// and
							new String[] { userId });
			if (cursor.moveToFirst()) {
				for (int i = 0; i < cursor.getCount(); i++) {
					bean = new JpushBean();
					bean.id = cursor.getString(cursor.getColumnIndex("pushid"));
					bean.content = cursor.getString(cursor
							.getColumnIndex("content"));
					bean.sender = cursor.getString(cursor.getColumnIndex("sender"));
					bean.time = cursor.getString(cursor.getColumnIndex("time"));
					bean.headImg = cursor.getString(cursor
							.getColumnIndex("headImg"));
					bean.type = cursor.getInt(cursor.getColumnIndex("type"));
					bean.name = cursor.getString(cursor.getColumnIndex("name"));
					bean.isAgree = cursor.getString(cursor
							.getColumnIndex("isagree"));
					bean.isread = cursor.getString(cursor.getColumnIndex("isread"));
					list.add(bean);
					cursor.moveToNext();
				}
			}
			if(null!=cursor)
				cursor.close();
		}catch(Exception e){
			e.printStackTrace();
		}finally{
			if(null!=db){
				db.close();
			}
		}
		return list;
	}

	public List<JpushBean> getQbData(String type, String userId) {
		List<JpushBean> list = new ArrayList<JpushBean>();
		try{
			db = dbhelper.getReadableDatabase();
			JpushBean bean;
			Cursor cursor = db.rawQuery(
					"select *  from  pushdb where type=? and userid=?",
					new String[] { type, userId });
			if (cursor.moveToFirst()) {
				for (int i = 0; i < cursor.getCount(); i++) {
					bean = new JpushBean();
					bean.id = cursor.getString(cursor.getColumnIndex("pushid"));
					bean.content = cursor.getString(cursor
							.getColumnIndex("content"));
					bean.sender = cursor.getString(cursor.getColumnIndex("sender"));
					bean.time = cursor.getString(cursor.getColumnIndex("time"));
					bean.headImg = cursor.getString(cursor
							.getColumnIndex("headImg"));
					bean.type = cursor.getInt(cursor.getColumnIndex("type"));
					bean.name = cursor.getString(cursor.getColumnIndex("name"));
					bean.isAgree = cursor.getString(cursor
							.getColumnIndex("isagree"));
					bean.isread = cursor.getString(cursor.getColumnIndex("isread"));
					list.add(bean);
					cursor.moveToNext();
				}
			}
			if(null!=cursor)
			cursor.close();
		}catch(Exception e){
			
		}finally{
			if(null!=db){
				db.close();
			}
		}
		return list;
	}

	/**
	 * 更改消息是否被同意的状态
	 * 
	 * @param isAgree
	 *            同意状态
	 * @param msgType
	 *            消息类型
	 * @param userId
	 *            用户的ID
	 * @param pushId
	 *            群组ID
	 */
	public void updatePushData(String isAgree, String msgType, String userId,
			String pushId) {
		try{
			db = dbhelper.getWritableDatabase();

			if (null != db && db.isOpen()) {
				db.execSQL(
						"UPDATE pushdb SET isagree=? WHERE type=? and userid=? and pushid=?",
						new Object[] { isAgree, msgType, userId, pushId });
			}
		}catch(Exception e){
			
		}finally{
			if (null != db && db.isOpen()) {
				db.close();
			}
		}
	}

	/**
	 * 更新情报读取状态
	 * 
	 * @param isRead
	 *            读取状态
	 * @param msgType
	 *            消息类型
	 * @param userId
	 *            用户ID
	 */
	public void updateQbiaoData(String isRead, String msgType, String userId) {
		try{
			db = dbhelper.getWritableDatabase();

			if (null != db && db.isOpen()) {
				db.execSQL("UPDATE pushdb SET isread=? WHERE type=? and userid=?",
						new Object[] { isRead, msgType, userId });
			}
		}catch(Exception e){
		}finally{
			if (null != db && db.isOpen()) {
				db.close();
			}
		}
	}

	/**
	 * 更新小纸条系统消息读取状态
	 * 
	 * @param isRead
	 *            是否读取
	 * @param userId
	 *            用户的ID
	 */
	public void updateReadStatData(String isRead, String userId) {
		try{
			db = dbhelper.getWritableDatabase();
			if (null != db && db.isOpen()) {
				db.execSQL("UPDATE pushdb SET isread=? WHERE userid=?",
						new Object[] { isRead, userId });
			}
		}catch(Exception e){
		}finally{
			if (null != db && db.isOpen()) {
				db.close();
			}
		}
	}
}
