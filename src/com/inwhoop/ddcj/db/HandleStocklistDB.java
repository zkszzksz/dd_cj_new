package com.inwhoop.ddcj.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.inwhoop.ddcj.bean.StockInfoBean;
import com.inwhoop.ddcj.util.SysPrintUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * @Project: WZSchool
 * @Title: HandleGroupmemberDB.java
 * @Package com.wz.db
 * @Description: TODO 操作群成员表
 * 
 * @author dyong199046@163.com 代勇
 * @date 2014-2-12 下午3:00:13
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 * @version V1.0
 */
public class HandleStocklistDB {

	private StocklistDatabaseDBhelper dbhelper = null;

	private SQLiteDatabase readDB, writeDB;

	private String saveSql = "insert into stocklist(stockcode,stockname) values(?,?)";

	public HandleStocklistDB(Context context) {
		dbhelper = new StocklistDatabaseDBhelper(context);
	}

	/**
	 * 
	 * @Title: saveGmember
	 * @Description: TODO 存储数据
	 * @param @param
	 * @param @param user
	 * @return void
	 */
	public void saveStock(StockInfoBean info) {
		try {
			writeDB = dbhelper.getWritableDatabase();
			writeDB.beginTransaction();
			writeDB.execSQL(saveSql, new Object[] { info.stockcode,
					info.stockname });
			writeDB.setTransactionSuccessful();
		} catch (Exception e) {
			SysPrintUtil.pt("添加自选股异常111", e.toString());
			e.printStackTrace();
		} finally {
			// 结束事务
			writeDB.endTransaction();
			writeDB.close();
		}
	}

	/**
	 * 更新数据
	 * 
	 * @Title: updateStock
	 * @Description: TODO
	 * @param @param info
	 * @param @param id
	 * @return void
	 */
	public void updateStock(StockInfoBean info, int id) {
		writeDB = dbhelper.getWritableDatabase();
		String sql = "update stocklist set stockcode ='" + info.stockcode + "'"
				+ ",stockname = '" + info.stockname + "' where id =" + id;
		writeDB.execSQL(sql);

	}

	/**
	 * 
	 * @Title: deleteGmember
	 * @Description: TODO
	 * @param @param groupid
	 * @return void
	 */
	public void deleteStockinfolist() {
		writeDB = dbhelper.getWritableDatabase();
		writeDB.execSQL("delete from stocklist");
	}

	/**
	 * 
	 * @Title: deleteGmember
	 * @Description: TODO
	 * @param @param groupid
	 * @return void
	 */
	public void deleteStockinfolistByCode(String code) {
		writeDB = dbhelper.getWritableDatabase();
		writeDB.execSQL("delete from stocklist where stockcode=" + "'" + code
				+ "'");
	}

	/**
	 * 
	 * @Title: getUserBygroupid
	 * @Description: TODO 查询
	 * @param @param groupid
	 * @param @return
	 * @return List<User>
	 */
	public List<StockInfoBean> getFriendlist() {
		List<StockInfoBean> list = new ArrayList<StockInfoBean>();
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			Cursor cursor = readDB.query("stocklist", null, null, null, null,
					null, null);
			if (cursor != null && cursor.getCount() > 0) {
				StockInfoBean user = null;
				while (cursor.moveToNext()) {
					user = new StockInfoBean();
					user.stockcode = cursor.getString(cursor
							.getColumnIndex("stockcode"));
					user.stockname = cursor.getString(cursor
							.getColumnIndex("stockname"));
					list.add(user);
				}
				cursor.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// 结束事务
			readDB.endTransaction();
			readDB.close();
		}
		return list;
	}

	/**
	 * 
	 * @Title: isExist
	 * @Description: TODO 判断用户是否存在
	 * @param @param userid
	 * @param @return
	 * @return boolean
	 */
	public boolean isExist(String stockcode) {
		Cursor cursor = null;
		try {
			readDB = dbhelper.getReadableDatabase();
			readDB.beginTransaction();
			cursor = readDB.rawQuery(
					"select * from stocklist where stockcode=?",
					new String[] { stockcode });
			String isuserid = "";
			if (cursor != null && cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					isuserid = cursor.getString(cursor
							.getColumnIndex("stockcode"));
				}
			}
			if (!"".equals(isuserid)) {
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (null != cursor) {
				cursor.close();
			}
			if (null != readDB) {
				readDB.endTransaction();
				readDB.close();
			}
		}
		return false;
	}

}
