package com.inwhoop.ddcj.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.content.res.AssetFileDescriptor;
import android.content.res.AssetManager;
import android.media.MediaPlayer;
import android.os.*;
import com.inwhoop.ddcj.bean.*;
import com.inwhoop.ddcj.util.JsonUtils;
import com.inwhoop.ddcj.xmpp.*;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.filter.PacketTypeFilter;
import org.jivesoftware.smack.packet.Packet;

import android.app.Service;
import android.content.Context;
import android.content.Intent;

import com.inwhoop.ddcj.app.Configs;
import com.inwhoop.ddcj.db.HandleFriendchatNoReadRecordDB;
import com.inwhoop.ddcj.db.HandleFriendchatRecordDB;
import com.inwhoop.ddcj.db.HandleFriendchatlistRecordDB;
import com.inwhoop.ddcj.db.HandleGroupchatNoReadRecordDB;
import com.inwhoop.ddcj.db.HandleGroupchatRecordDB;
import com.inwhoop.ddcj.util.TimeRender;
import com.inwhoop.ddcj.util.UserInfoUtil;
import com.inwhoop.ddcj.util.Utils;

/**
 * @author dyong199046@163.com 代勇
 * @version V1.0
 * @Project: DDCJ
 * @Title: XmppService.java
 * @Package com.inwhoop.ddcj.service
 * @Description: TODO
 * @date 2014-11-18 下午2:53:23
 * @Copyright: 2014 呐喊信息技术 All rights reserved.
 */
public class XmppService extends Service {

	private PacketListener mPacketListener;

	private HandleFriendchatlistRecordDB fldb = null;

	private HandleFriendchatRecordDB fcdb = null;

	private HandleFriendchatNoReadRecordDB fndb = null;

	private HandleGroupchatRecordDB gcdb = null;

	private HandleGroupchatNoReadRecordDB gndb = null;
	private TaxiConnectionListener connectionListener;
	private Context context;
	private UserBean userBean;
	private List<CateChannel> list;
	private AssetManager assetManager;
	private AssetFileDescriptor fileDescriptor;
	private MsgNoticeBean settings;
	private Vibrator vibrator;

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		initData();
		initXmpp();
		return START_STICKY;
	}

	private void initData() {
		context = this;

		vibrator = (Vibrator) context
				.getSystemService(Context.VIBRATOR_SERVICE);
		userBean = UserInfoUtil.getUserInfo(context);
		fldb = new HandleFriendchatlistRecordDB(this);
		fcdb = new HandleFriendchatRecordDB(this);
		fndb = new HandleFriendchatNoReadRecordDB(this);
		gcdb = new HandleGroupchatRecordDB(this);
		gndb = new HandleGroupchatNoReadRecordDB(this);
	}

	private void initXmpp() {
		new Thread(new Runnable() {

			@Override
			public void run() {
				try {
					connectOpenfire();
				} catch (Exception e) {
					System.out.println("====");
				}
			}
		}).start();
	}

	/*
	 * 链接登录openfire
	 */
	private void connectOpenfire() throws Exception {
		if (!XmppConnectionUtil.getInstance().getConnection().isAuthenticated()) {
			// String username = UserInfoUtil.getUserInfo(XmppService.this).tel;
			// String password = UserInfoUtil.getUserInfo(XmppService.this).pwd;
			// XmppConnectionUtil.getInstance().login(username, password);
			System.out.println("登录openfire的账号和密码为" + userBean.tel + "==="
					+ userBean.pwd);
			XmppConnectionUtil.getInstance().login(userBean.tel, userBean.pwd);
		}
		if (XmppConnectionUtil.getInstance().getConnection().isAuthenticated()) {
			connectionListener = new TaxiConnectionListener();
			XmppConnectionUtil.getInstance().getConnection()
					.addConnectionListener(connectionListener);
			getGroupData();
			System.out.println("XMPPService重连服务器成功");
			if (null != mPacketListener) {
				XmppConnectionUtil.getInstance().getConnection()
						.removePacketListener(mPacketListener);
			}
			PacketTypeFilter filter = new PacketTypeFilter(
					org.jivesoftware.smack.packet.Message.class);
			mPacketListener = new PacketListener() {
				public void processPacket(Packet packet) {
					try {
						if (packet instanceof org.jivesoftware.smack.packet.Message) {// 如果是消息类型
							org.jivesoftware.smack.packet.Message message = (org.jivesoftware.smack.packet.Message) packet;
							if (message.getFrom().contains(
									XmppConnectionUtil.getInstance()
											.getConnection().getUser())) {
							} else if (message
									.getFrom()
									.toString()
									.substring(
											message.getFrom().indexOf("@") + 1,
											message.getFrom().lastIndexOf("/"))
									.equals(XmppConnectionUtil.getInstance()
											.getConnection().getServiceName())) {
								// 获取单聊信息
								System.out
										.println("========messagegetBody======"
												+ message.getBody());
								System.out
										.println("========message=getFrom======"
												+ message.getFrom());
								// message.getFrom().cantatins(获取列表上的用户，组，管理消息);
								// 获取用户、消息、时间、IN
								String[] args = new String[] {
										message.getFrom(), message.getBody(),
										TimeRender.getStandardDate(), "IN" };
								handleFriendChat(args);
							}
							if (!message
									.getFrom()
									.toString()
									.substring(
											message.getFrom().indexOf("@") + 1,
											message.getFrom().lastIndexOf("/"))
									.equals(XmppConnectionUtil.getInstance()
											.getConnection().getServiceName())) {
								// 获取群聊信息
								System.out
										.println("========message.getBody()=====group=="
												+ message.getBody());
								String body = message.getBody();
								String from = message.getFrom();
								handleGroupchat(from, body);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			};
			XmppConnectionUtil.getInstance().getConnection()
					.addPacketListener(mPacketListener, filter);// 这是最关健的了，少了这句，前面的都是白费功夫
			// 如果已经登录，则重新维护长连接
			JLPingManager.getInstanceFor(XmppConnectionUtil.getInstance()
					.getConnection(), 10, new XmppConnectionCallback() {
				@Override
				public void XmppReconnectingIn(int seconds) {
					try {
						Thread.sleep(seconds * 1000);
						initXmpp();
					} catch (Exception e) {
					}
				}
			});
		}
	}


	@Override
	public void onDestroy() {
		super.onDestroy();
		System.out.println("===========================XMPPService服务被关闭");
		try {
			if (null != mPacketListener) {
				XmppConnectionUtil.getInstance().getConnection()
						.removePacketListener(mPacketListener);
			}
			XmppConnectionUtil.getInstance().getConnection()
					.removeConnectionListener(connectionListener);
			XmppConnectionUtil.getInstance().closeConnection();
		} catch (Exception e) {
			System.out
					.println("===========================XMPPService服务被关闭===="
							+ e.toString());
			e.printStackTrace();
		}

	}

	/**
	 * @param @param from
	 * @param @param body
	 * @return void
	 * @Title: handleGroupchat
	 * @Description: TODO
	 */
	private void handleGroupchat(String from, String body) {
		GroupChatinfo info;
		try {
			System.out.println("群聊的BODY===" + body);
			info = GroupChatPacketExtension.getBosy(body);
			if (!info.chatid.toLowerCase().equals(
					UserInfoUtil.getUserInfo(this).tel.toLowerCase())) {
				info.isMymsg = false;
				info.userid = UserInfoUtil.getUserInfo(this).tel;
				if (info.msgtype == 3) {
					Utils.decoderBase64File(info.content,
							Environment.getExternalStorageDirectory()
									+ info.audiopath);
				}
				if (fldb.isExist(info.groupid, 2, info.time,
						"" + UserInfoUtil.getUserInfo(this).tel)) {
					fldb.deleteGmember(info.groupid,"" + UserInfoUtil.getUserInfo(this).tel);
				}
				fldb.saveGmember(info.groupid, 2, info.time,
						"" + UserInfoUtil.getUserInfo(this).tel,
						info.userheadpath, info.groupname);
				if (info.groupid.equals(Configs.groupid)) {
					gcdb.saveGmember(info);
					Intent intent = new Intent();
					Bundle bundle = new Bundle();
					bundle.putSerializable("info", info);
					intent.putExtras(bundle);
					intent.setAction(Configs.COME_MESSAGE);
					sendBroadcast(intent);
				} else {
					settings = UserInfoUtil.getInfoNotifyMode(context);
					if (settings.checkTag1 == 0) {

					} else if (settings.checkTag2 == 1
							&& settings.checkTag3 == 0) {
						playPrompt();
					} else if (settings.checkTag2 == 0
							&& settings.checkTag3 == 1) {
						vibrator.vibrate(200);
					} else if (settings.checkTag2 == 1
							&& settings.checkTag3 == 1) {
						playPrompt();
						vibrator.vibrate(200);
					}
					gndb.saveGmember(info);
					Intent intent = new Intent();
					intent.setAction(Configs.COME_MESSAGE1);
					sendBroadcast(intent);
				}
				Intent intent = new Intent();
				intent.setAction(Configs.COME_MESSAGE2);
				sendBroadcast(intent);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @param @param args
	 * @return void
	 * @Title: handleFriendChat
	 * @Description: TODO
	 */
	private void handleFriendChat(String[] args) {
		FriendChatinfo info = null;
		String from = args[0];
		from = from.substring(0, from.indexOf("@"));
		try {
			info = SingleChatPacketExtension.getBosy(args[1]);
			info.userid = UserInfoUtil.getUserInfo(this).tel;
			info.username = from;
			info.isMymsg = false;
			if (info.msgtype == 3) {
				Utils.decoderBase64File(info.content,
						"" + Environment.getExternalStorageDirectory()
								+ info.audiopath);
				info.content = "";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
//		if (!fldb.isExist(from, 1, info.time,
//				"" + UserInfoUtil.getUserInfo(this).tel)) {
//			fldb.saveGmember(from, 1, info.time,
//					"" + UserInfoUtil.getUserInfo(this).tel, info.userheadpath,
//					info.usernick);
//		} else {
//			fldb.UpdateChatagetTime(from, 1,
//					"" + UserInfoUtil.getUserInfo(this).tel, info.time,
//					info.userheadpath, info.usernick);
//		}
		if (fldb.isExist(from, 1, info.time,
				"" + UserInfoUtil.getUserInfo(this).tel)) {
			fldb.deleteGmember(from,"" + UserInfoUtil.getUserInfo(this).tel);
		} 
		fldb.saveGmember(from, 1, info.time,
				"" + UserInfoUtil.getUserInfo(this).tel, info.userheadpath,
				info.usernick);
		System.out.println("接受到的头像地址为" + info.userheadpath + "===" + from
				+ "==" + Configs.friendid);

		if (from.equals(Configs.friendid)) {
			fcdb.saveGmember(info);
			Intent intent = new Intent();
			Bundle bundle = new Bundle();
			bundle.putSerializable("info", info);
			intent.putExtras(bundle);
			intent.setAction(Configs.COME_MESSAGE);
			sendBroadcast(intent);
		} else {
			settings = UserInfoUtil.getInfoNotifyMode(context);
			if (settings.checkTag1 == 0) {

			} else if (settings.checkTag2 == 1 && settings.checkTag3 == 0) {
				playPrompt();
			} else if (settings.checkTag2 == 0 && settings.checkTag3 == 1) {
				vibrator.vibrate(200);
			} else if (settings.checkTag2 == 1 && settings.checkTag3 == 1) {
				playPrompt();
				vibrator.vibrate(200);
			}
			fndb.saveGmember(info);
			Intent intent = new Intent();
			intent.setAction(Configs.COME_MESSAGE1);
			sendBroadcast(intent);
		}
		Intent intent = new Intent();
		intent.setAction(Configs.COME_MESSAGE2);
		sendBroadcast(intent);
	}

	class TaxiConnectionListener implements ConnectionListener {
		private Timer tExit;
		private int logintime = 2000;

		@Override
		public void connectionClosed() {
			XmppConnectionUtil.getInstance().getConnection()
					.removePacketListener(mPacketListener);
			XmppConnectionUtil.getInstance().closeConnection();
			initXmpp();
//			tExit = new Timer();
//			tExit.schedule(new timetask(), logintime);
			System.out.println("===========wuwuduankaile============");
		}

		@Override
		public void connectionClosedOnError(Exception e) {
			boolean error = e.getMessage().equals("stream:error (conflict)");
			if (!error) {
				System.out.println("===========wuwuduankaile===error=========");
				// ClienConServer.client = null;
				// ClientGroupServer.client = null;
				// MypacketListener.getInstance().loginout();
				// // 重连服务器
				System.out.println("XMPPService重连服务器11111");
				XmppConnectionUtil.getInstance().getConnection()
						.removePacketListener(mPacketListener);
				XmppConnectionUtil.getInstance().closeConnection();
//				tExit = new Timer();
//				tExit.schedule(new timetask(), logintime);
			}
		}

//		class timetask extends TimerTask {
//			@Override
//			public void run() {
//				if (null != UserInfoUtil.getUserInfo(context)) {
//					System.out.println("XMPPService重连服务器111112222222");
//					try {
//						connectOpenfire();
//					} catch (Exception e) {
//						e.printStackTrace();
//					}
//				}
//			}
//		}

		@Override
		public void reconnectingIn(int arg0) {
		}

		@Override
		public void reconnectionFailed(Exception arg0) {
		}

		@Override
		public void reconnectionSuccessful() {
		}

	}

	private void getGroupData() {
		new Thread(new Runnable() {
			@Override
			public void run() {
				Message msg = new Message();
				try {
					list = JsonUtils.getUserGroupList(userBean.userid);
					if (null != list && list.size() > 0) {
						msg.what = Configs.READ_SUCCESS;
					} else {
						msg.what = Configs.READ_FAIL;
					}
				} catch (Exception e) {
					e.printStackTrace();
					msg.what = Configs.READ_ERROR;
				}
				handler.sendMessage(msg);
			}
		}).start();
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case Configs.READ_SUCCESS:
				Groups groups = null;
				List<Groups> list1 = new ArrayList<Groups>();
				Configs.groupList = list;
				for (int i = 0; i < list.size(); i++) {
					groups = new Groups();
					groups.groupid = list.get(i).id;
					groups.groupname = list.get(i).name;
					list1.add(groups);
				}
				ClientGroupServer.getInstance(context).initMultiUserChat(list1);
				break;
			case Configs.READ_FAIL:
				// Toast.makeText(context, "你暂未加入任何群",
				// Toast.LENGTH_SHORT).show();
				break;
			case Configs.READ_ERROR:
				// Toast.makeText(context, "数据异常", Toast.LENGTH_SHORT).show();
				break;
			}
		}
	};

	private void playPrompt() {
		try {
			assetManager = context.getAssets();
			fileDescriptor = assetManager.openFd("tishi.mp3");
			MediaPlayer mediaPlayer = new MediaPlayer();
			mediaPlayer
					.setDataSource(fileDescriptor.getFileDescriptor(),
							fileDescriptor.getStartOffset(),
							fileDescriptor.getLength());
			mediaPlayer.prepare();
			mediaPlayer.start();
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			mediaPlayer.stop();
			mediaPlayer.release();
			mediaPlayer = null;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("音频播放异常" + e.toString());
		}
	}
}
